var HakAkses = {
	module: function () {
		return 'hak_akses';
	},

	add: function () {
		window.location.href = url.base_url(HakAkses.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(HakAkses.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(HakAkses.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(HakAkses.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'hak_akses': $('#hak_akses').val(),
		};

		return data;
	},

	getPostListAkses:()=>{
		let tableData = $('table#table-hak-akses').find('tbody').find('tr');
		let data = [];
		$.each(tableData, function(){
			let params = {};
			params.phm_id = $(this).attr('phm_id');
			params.module = $(this).attr('data_id');
			params.read = $(this).find('input#read').is(':checked') ? 1 : 0;
			params.create = $(this).find('input#create').is(':checked') ? 1 : 0;
			params.update = $(this).find('input#update').is(':checked') ? 1 : 0;
			params.delete = $(this).find('input#delete').is(':checked') ? 1 : 0;
			params.status = $(this).find('input#status').is(':checked') ? 1 : 0;
			data.push(params);
		});

		return data;
	},

	simpan: function (id) {
		var data = HakAkses.getPostData();
		data.akses_list = HakAkses.getPostListAkses();

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: data,
				dataType: 'json',
				url: url.base_url(HakAkses.module()) + "simpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					message.closeLoading();
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						// console.log(url.base_url(HakAkses.module()) + "detail" + '/' + resp.id);
						var reload = function () {
							window.location.href = url.base_url(HakAkses.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(HakAkses.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(HakAkses.module()) + "detail/" + id;
	},



	delete: function (id) {
  let html = `<div class="row">
  <div class="col-md-12 text-center">
   <p>Apakah anda yakin menghapus data ?</p>
  </div>
  <div class="col-md-12 text-center">
   <br/>
   <button class="btn btn-success" onclick="HakAkses.execDelete(${id})">Ya</button>
   <button class="btn btn-warning" onclick="message.closeDialog()">Tidak</button>
  </div>
  </div>`;

  bootbox.dialog({
   message: html
  });
	},

	execDelete: (id) => {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url.base_url(HakAkses.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(HakAkses.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	}
};
