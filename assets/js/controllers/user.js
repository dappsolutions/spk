var User = {
	module: function () {
		return 'user';
	},

	add: function () {
		window.location.href = url.base_url(User.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(User.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(User.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(User.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'username': $('#username').val(),
			'password': $('#password').val(),
			'priveledge': $('#akses').val(),
			'pegawai': $('#pegawai').val()
		};

		return data;
	},

	simpan: function (id) {
		var data = User.getPostData();
		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append("id", id);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				// async: false,
				url: url.base_url(User.module()) + "simpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan "+resp);
						var reload = function () {
							window.location.href = url.base_url(User.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(User.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(User.module()) + "detail/" + id;
	},

	setUserAktifNonAktif: function (id, is_active) {
		if (is_active == 1) {
			is_active = 0;
		} else {
			is_active = 1;
		}
		$.ajax({
			type: 'POST',
			dataType: 'json',
			async: false,
			url: url.base_url(User.module()) + "setUserAktifNonAktif" + '/' + id + '/' + is_active,
			error: function () {
				toastr.error("Program Error");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses('Proses...');
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Diproses");
					var reload = function () {
						window.location.reload();
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Diproses");
				}

				message.closeLoading();
			}
		});
	},

	delete: function (id) {
		let html = `<div class="row">
  <div class="col-md-12 text-center">
   <p>Apakah anda yakin menghapus data ?</p>
  </div>
  <div class="col-md-12 text-center">
   <br/>
   <button class="btn btn-success" onclick="Wilayah.execDelete(${id})">Ya</button>
   <button class="btn btn-warning" onclick="message.closeDialog()">Tidak</button>
  </div>
  </div>`;

		bootbox.dialog({
			message: html
		});
	},

	execDelete: (id) => {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url.base_url(Wilayah.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(Wilayah.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

 setSelect2: function () {
		$('select#pegawai').select2();
	},
};

$(function(){
 User.setSelect2();
});
