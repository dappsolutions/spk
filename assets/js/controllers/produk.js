var Produk = {
	module: function () {
		return 'produk';
	},

	add: function () {
		window.location.href = url.base_url(Produk.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(Produk.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(Produk.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(Produk.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'kode_produk': $('#kode_produk').val(),
			'nama_produk': $('#nama_produk').val(),
		};

		return data;
	},

	getPostSatuan: () => {
		let tableSatuan = $('table#table-satuan').find('tbody').find('tr');
		let data = [];
		$.each(tableSatuan, function () {
			let params = {};
			let no = $(this).attr('no');
			params.id = $(this).attr('data_id');
			params.harga = $(this).find('input#harga_produk').val();
			params.satuan = $(this).find(`#satuan-${no}`).val();
			params.deleted = $(this).hasClass('remove') ? 1 : 0;
			if(params.harga != '' && params.satuan != ''){
				data.push(params);
			}
		});

		return data;
	},

	simpan: function (id) {
		var data = Produk.getPostData();
		data.list_satuan = Produk.getPostSatuan();

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: data,
				dataType: 'json',
				url: url.base_url(Produk.module()) + "simpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					message.closeLoading();
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						// console.log(url.base_url(Produk.module()) + "detail" + '/' + resp.id);
						var reload = function () {
							window.location.href = url.base_url(Produk.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(Produk.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(Produk.module()) + "detail/" + id;
	},



	delete: function (id) {
		let html = `<div class="row">
  <div class="col-md-12 text-center">
   <p>Apakah anda yakin menghapus data ?</p>
  </div>
  <div class="col-md-12 text-center">
   <br/>
   <button class="btn btn-success" onclick="Produk.execDelete(${id})">Ya</button>
   <button class="btn btn-warning" onclick="message.closeDialog()">Tidak</button>
  </div>
  </div>`;

		bootbox.dialog({
			message: html
		});
	},

	execDelete: (id) => {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url.base_url(Produk.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(Produk.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	setSelect2: function () {
		let tableSatuan = $('table#table-satuan').find('tbody').find('tr');
		$.each(tableSatuan, function () {
			let no = $(this).attr('no');
			$(this).find(`#satuan-${no}`).select2();
		});
	},

	addSatuan: (elm) => {
		$('.satuan').select2('destroy');
		// console.log($select);


		let tr = $(elm).closest('tr');
		let newNo = parseInt(tr.attr('no')) + 1;
		let params = {};
		params.no = newNo;

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Produk.module()) + "addSatuan",

			error: function () {
				toastr.error("Gagal");
			},

			success: function (resp) {
				let newTr = tr.clone();
				newTr.attr('no', newNo);
				newTr.find('td:eq(1)').html(resp);
				newTr.find('td:eq(2)').html(`<i class="fa fa-trash" onclick="Produk.removeSatuan(this)"></i>`);
				newTr.find('input').val('');
				// newTr.find(`satuan-${newNo}`).select2();
				tr.after(newTr);

				Produk.setSelect2();
			}
		});
	},

	removeSatuan:(elm)=>{
		let dataId= $(elm).closest('tr').attr('data_id');
		console.log('dataId', dataId);
		if(dataId == ''){
			$(elm).closest('tr').remove();
		}else{
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	}
};

$(function () {
	Produk.setSelect2();
});
