var Agen = {
	module: function () {
		return 'agen';
	},

	add: function () {
		window.location.href = url.base_url(Agen.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(Agen.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(Agen.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(Agen.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'nama_agen': $('#nama_agen').val(),
		};

		return data;
	},

	simpan: function (id) {
		var data = Agen.getPostData();

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: data,
				dataType: 'json',
				url: url.base_url(Agen.module()) + "simpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					message.closeLoading();
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						// console.log(url.base_url(Agen.module()) + "detail" + '/' + resp.id);
						var reload = function () {
							window.location.href = url.base_url(Agen.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(Agen.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(Agen.module()) + "detail/" + id;
	},



	delete: function (id) {
  let html = `<div class="row">
  <div class="col-md-12 text-center">
   <p>Apakah anda yakin menghapus data ?</p>
  </div>
  <div class="col-md-12 text-center">
   <br/>
   <button class="btn btn-success" onclick="Agen.execDelete(${id})">Ya</button>
   <button class="btn btn-warning" onclick="message.closeDialog()">Tidak</button>
  </div>
  </div>`;

  bootbox.dialog({
   message: html
  });
	},

	execDelete: (id) => {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url.base_url(Agen.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(Agen.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},
	setDate: function () {
		$('input#tanggal').datepicker({
			dateFormat: 'yy-mm-dd',
			todayHighlight: true,
			autoclose: true
		});
	},
};

$(function(){
	Agen.setDate();
});
