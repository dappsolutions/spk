var uri = 'data:application/vnd.ms-excel;base64,',
	template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
	base64 = function (s) {
		return window.btoa(unescape(encodeURIComponent(s)))
	},
	format = function (s, c) {
		return s.replace(/{(\w+)}/g, function (m, p) {
			return c[p];
		})
	}

var Document = {
	module: function () {
		return 'document';
	},

	add: function (elm, e) {
		e.preventDefault();
		let urlBase = url.base_url(Document.module());
		let html = `<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
			<div class="panel-heading">
			<h5>Jenis Pembuatan Dokumen</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-aqua">
							<div class="inner">
								<h3>-</h3>

								<p>SPBJL</p>
							</div>
							<div class="icon">
								<i class="ion ion-android-menu"></i>
							</div>
							<a href="${urlBase+"add"}" class="small-box-footer"> Create <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					</div>
					
					<div class="col-lg-6 col-xs-6">
						<!-- small box -->
						<div class="small-box bg-yellow">
							<div class="inner">
								<h3>-</h3>

								<p>SPK</p>
							</div>
							<div class="icon">
								<i class="ion ion-android-menu"></i>
							</div>
							<a href="${urlBase+"addSpk"}" class="small-box-footer"> Create <i class="fa fa-arrow-circle-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
		</div>`;

		bootbox.dialog({
			message: html
		});
		// window.location.href = url.base_url(Document.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(Document.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(Document.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(Document.module()) + "index";
			}
		}
	},

	getPostItemDireksiLapangan: () => {
		let tableData = $('table#table-direksi-lapangan').find('tbody').find('tr.input');
		let data = [];
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.pegawai_id = $(this).find('label#pegawai').attr('data_id');
			params.pegawai_name = $.trim($(this).find('label#pegawai').text());
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			data.push(params);
		});

		return data;
	},

	getPostItemPengawasLapangan: () => {
		let tableData = $('table#table-pengawas-lapangan').find('tbody').find('tr.input');
		let data = [];
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.pegawai_id = $(this).find('label#pegawai').attr('data_id');
			params.pegawai_name = $.trim($(this).find('label#pegawai').text());
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			data.push(params);
		});

		return data;
	},

	getPostItemDataJadwal: () => {
		let tableData = $('table#table-jadwal').find('tbody').find('tr');
		let data = [];
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.data_transaksi_id = $(this).attr('data_transaksi_id');
			params.no = $.trim($(this).find('td:eq(0)').text());
			params.uraian_kegiatan = $.trim($(this).find('td#uraian_pekerjaan').text());
			params.no_rab = $.trim($(this).find('td#no-rab').text());
			params.no_dokumen = $.trim($(this).find('td#no_dokumen').text());
			params.tanggal = $.trim($(this).find('td#tanggal').text());
			params.standard = $.trim($(this).find('td#standard').text());
			params.cepat = $.trim($(this).find('td#cepat').text());
			params.manual = $(this).find('input#manual').val();
			data.push(params);
		});

		return data;
	},

	getPostItemDataRab: () => {
		let tableData = $('table#table-rab').find('tbody').find('tr.input');
		let data = [];
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.material_id = $(this).hasClass('auto') ? $.trim($(this).find('td:eq(0)').find('select').val()) : '';
			params.material_name = $(this).hasClass('auto') ? $.trim($(this).find('td:eq(0)').find('select').find(`option[value="${params.material_id}"]`).text()) : $(this).find('input#uraian').val();
			params.jml_vol = $(this).find('input#jml_vol').val();
			params.satuan_id = $(this).hasClass('auto') ? $.trim($(this).find('td:eq(2)').find('select').val()) : '';
			params.satuan_name = $(this).hasClass('auto') ? $.trim($(this).find('td:eq(2)').find('select').find(`option[value="${params.satuan_id}"]`).text()) : $(this).find('input#satuan_vol').val();
			params.satuan_anggaran = $(this).find('input#satuan_anggaran').val();
			params.jumlah_anggaran = $(this).find('input#jumlah_anggaran').val();
			params.satuan_hps = $(this).find('input#satuan_hps').val();
			params.jumlah_hps = $(this).find('input#jumlah_hps').val();
			params.status = $(this).hasClass('auto') ? 'auto' : 'manual';
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			data.push(params);
		});

		return data;
	},

	getPostItemDataPenawaran: () => {
		let tableData = $('table#table-penawaran-harga').find('tbody').find('tr.input');
		let data = [];
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.satuan_penawaran = $(this).find('input#satuan-rp-penawaran').val();
			params.jumlah_penawaran = $(this).find('input#jumlah-rp-penawaran').val();
			params.satuan_sepakat = $(this).find('input#harga-sepakat').length == 0 ? $.trim($(this).find('td#satuan-sepakat').text()) : $(this).find('input#harga-sepakat').val();
			params.jumlah_sepakat = $.trim($(this).find('td#jumlah-sepakat').text());
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			data.push(params);
		});

		return data;
	},

	getPostItemDataRekanan: () => {
		let tableData = $('table#table-data-rekanan').find('tbody').find('tr.input');
		let data = [];
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.vendor = $(this).find('select').val();
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			if (params.vendor != '') {
				data.push(params);
			}
		});

		return data;
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'jenis_transaksi': $('#jenis_transaksi').val(),
			'form_dokumen': {
				'submission_form_id': $('#submission-form-id').val(),
				'format_pengadaan': $('#format-pengadaan').val(),
				'sifat_pekerjaan': $('#sifat-pekerjaan').val(),
				'judul_pekerjaan': $('#judul-pekerjaan').val(),
				'lama_pekerjaan': $('#lama-pekerjaan').val(),
				'vendor': $('#vendor').val(),
				'no_rab': $('#no-rab').val(),
				'tgl_rab': $('#tgl-rab').val(),
				'jenis_pengadaan': $('#jenis-pengadaan').val(),
				'jenis_pengadaan_id': $('#jenis-pengadaan').find(`option[value="${$('#jenis-pengadaan').val()}"]`).attr('data_id'),
				'tanggal_awal': $('#tgl-awal').val(),
				'tanggal_selesai': $('#tgl-selesai').val(),
				'lama_pekerjaan': $('#lama-pekerjaan').val(),
				'jenis_anggaran': $('#jenis-anggaran').val(),
				'no_skk': $('#no_skk').val(),
				'sub_bidang_usaha': $('#sub-badan-usaha').val(),
				'data_direksi_lap': Document.getPostItemDireksiLapangan(),
				'data_pengawas_lap': Document.getPostItemPengawasLapangan(),
			},
			'form_jadwal': {
				'data': Document.getPostItemDataJadwal()
			},
			'form_rab': {
				'data': Document.getPostItemDataRab()
			},
			'form_penawaran': {
				'data': Document.getPostItemDataPenawaran()
			},
			'form_rekanan': {
				'data': Document.getPostItemDataRekanan()
			},
			'form_dpl': {
				'approval_pengesahan': $('#approval-pengesahan').val(),
			},
		};

		return data;
	},

	saveAll: function (id) {
		let params = Document.getPostData();
		let formData = new FormData();
		formData.append('data', JSON.stringify(params));
		formData.append('file_rab', $('input#file_rab').prop('files')[0]);
		formData.append('file_kak', $('input#file_kak').prop('files')[0]);
		formData.append('file_rpb', $('input#file_rpb').prop('files')[0]);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				// data: params,
				data: formData,
				contentType: false,
				processData: false,
				dataType: 'json',
				url: url.base_url(Document.module()) + "saveAll",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						var reload = function () {
							window.location.href = url.base_url(Document.module()) + "ubah" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
					message.closeLoading();
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(Document.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(Document.module()) + "detail/" + id;
	},

	cetak: function (elm) {
		let dataId = $(elm).closest('tr').attr('data_id');
		window.location.href = url.base_url(Document.module()) + "cetak/" + dataId;
	},

	cetakSpk: function (elm) {
		let dataId = $(elm).closest('tr').attr('data_id');
		window.location.href = url.base_url(Document.module()) + "cetakSpk/" + dataId;
	},

	delete: function (id) {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			async: false,
			url: url.base_url(Document.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(Document.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	showListPegawai: function (elm, e) {
		e.preventDefault();
		let params = {};
		params.state = $(elm).attr('state');
		params.index = $(elm).closest('tr').index();
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Document.module()) + "showListPegawai",

			error: function () {
				toastr.error("Gagal");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	pilihPegawai: function (elm, e) {
		e.preventDefault();
		let tableContent = $(elm).attr('state') == 'direksi' ? 'table-direksi-lapangan' : 'table-pengawas-lapangan';
		let indexTr = $(elm).attr('index');
		tableContent = $(`table#${tableContent}`).find('tbody').find(`tr:eq(${indexTr})`);
		let newTrContent = tableContent.clone();
		tableContent.after(newTrContent);

		let pegawai = $(elm).closest('tr').attr('data_id');
		let nip = $.trim($(elm).closest('tr').find('td#nip').text());
		let nama_pegawai = $.trim($(elm).closest('tr').find('td#nama_pegawai').text());
		tableContent.addClass('input');
		tableContent.find('td:eq(0)').html(`<label id="pegawai" data_id="${pegawai}">${nip} - ${nama_pegawai}</label>`);
		tableContent.find('td:eq(1)').html(`<i class="fa fa-trash" onclick="Document.removeItemPegawai(this)"></i>`);
		message.closeDialog();
	},

	removeItemPegawai: (elm) => {
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id == '') {
			$(elm).closest('tr').remove();
		} else {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	},

	setSelect2: function () {
		$('select#vendor').select2();
	},

	setDate: function () {
		$('input#tgl-rab').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#tgl-awal').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			startDate: new Date(),
		});

		$('input#tgl-selesai').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			startDate: new Date(),
		});
	},


	generateNoInJadwal: (elm, e) => {
		let tableJadwal = $('table#table-jadwal').find('tbody').find('tr');
		let noRabInput = $(elm).val();
		let dataIdDokumen = $('input#id').val();
		if (dataIdDokumen != '') {
			let noRabHide = $('#no-rab-hide').val();
			$.each(tableJadwal, function () {
				let labelNoRab = $(this).find('label#nomor-pekerjaan');
				console.log(labelNoRab.text());
				noRabInputBaru = labelNoRab.text().replace(noRabHide, '');
				labelNoRab.html(noRabInputBaru);
			});

			console.log('noRabInput', noRabInput);
			$.each(tableJadwal, function () {
				let labelNoRab = $(this).find('label#no-rab-label');
				let noRab = $(this).find('td#no-rab');
				noRab.html(`<label>${noRabInput}</label>`);
				labelNoRab.html(noRabInput);
			});

		} else {
			$.each(tableJadwal, function () {
				let noRab = $(this).find('td#no-rab');
				let labelNoRab = $(this).find('label#no-rab-label');
				noRab.html(`<label>${noRabInput}</label>`);
				labelNoRab.html(noRabInput);
			});
		}
	},

	setTanggalPenjadwalan: (elm, e) => {
		let tglAwalRab = $('#tgl-rab').val();
		let tableJadwal = $('table#table-jadwal').find('tbody').find('tr');
		let jenisPengadaan = $('#jenis-pengadaan').val();
		$('td.td-nilai').removeClass('bg-warning');
		$('td.td-nilai').removeClass('hide');
		$('th.th-nilai').removeClass('hide');

		$.each(tableJadwal, function () {
			if (jenisPengadaan != '') {
				if (jenisPengadaan != 'manual') {
					let addDay = $.trim($(this).find(`td#${jenisPengadaan}`).text());
					if (addDay == '') {
						addDay = 0;
					} else {
						addDay = parseInt(addDay);
					}

					if (tglAwalRab != '') {
						tglAwalRab = Document.addDays(tglAwalRab, addDay, $.trim($(this).find('td:eq(1)').text()));
						let tglUraianPekerjan = Document.formatDate(tglAwalRab);
						$(this).find(`td#${jenisPengadaan}`).addClass('bg-warning');
						$(this).find('td#tanggal').html(`${tglUraianPekerjan}`);
						$(this).find('td#manual').html(``);
					}

					//hide 
					$(this).find('td#manual').addClass('hide');
					$('th#manual').addClass('hide');
					if (jenisPengadaan == 'standard') {
						$(this).find('td#cepat').addClass('hide');
						$('th#cepat').addClass('hide');
					}
					if (jenisPengadaan == 'cepat') {
						$(this).find('td#standard').addClass('hide');
						$('th#standard').addClass('hide');
					}
				} else {
					//nilai standardsebagai acuan pengisian manual
					let nilai = $.trim($(this).find('td#standard').text());
					$(this).find(`td#${jenisPengadaan}`).addClass('bg-warning');
					$(this).find('td#tanggal').html(`${tglAwalRab}`);
					$(this).find('td#manual').html(`<input id="manual" value="${nilai}" onkeyup="Document.calculateDate(this, event)"/>`);
					$(this).find('td#standard').addClass('hide');
					$(this).find('td#cepat').addClass('hide');
					$('th#standard').addClass('hide');
					$('th#cepat').addClass('hide');
				}

				//hide kolom

			} else {
				$(this).find(`td.td-nilai`).removeClass('bg-warning');
				$(this).find('td#tanggal').html(``);
				$(this).find('td#manual').html(``);
			}
		});

		let as_tanggal_pekerjaan = $('table#table-jadwal').find('tbody').find('tr.as_tanggal_pekerjaan');
		if (as_tanggal_pekerjaan.length == 0) {
			toastr.warning('Tanggal Pekerjaan Belum di Atur pada Master Jadwal');
			return;
		} else {
			let dateTanggalPekerjaan = $.trim(as_tanggal_pekerjaan.find('td#tanggal').text());
			$('input#tgl-awal').val(dateTanggalPekerjaan);
		}

		Document.validasiTanggalSelesai(elm, e);
	},

	calculateDate: (elm, e) => {
		let addDays = isNaN(parseInt($(elm).val())) ? 0 : parseInt($(elm).val());
		let tglAwalRab = $('#tgl-rab').val();
		let tableJadwal = $('table#table-jadwal').find('tbody').find('tr');

		$(elm).val(addDays);

		$.each(tableJadwal, function () {
			let addDay = $.trim($(this).find(`input#manual`).val());
			if (isNaN(addDay)) {
				addDay = 0;
			} else {
				addDay = parseInt(addDay);
			}
			tglAwalRab = Document.addDays(tglAwalRab, addDay, $.trim($(this).find('td:eq(1)').text()));
			let tglUraianPekerjan = Document.formatDate(tglAwalRab);
			$(this).find(`td#manual`).addClass('bg-warning');
			$(this).find('td#tanggal').html(`${tglUraianPekerjan}`);
		});

		let as_tanggal_pekerjaan = $('table#table-jadwal').find('tbody').find('tr.as_tanggal_pekerjaan');
		if (as_tanggal_pekerjaan.length == 0) {
			toastr.warning('Tanggal Pekerjaan Belum di Atur pada Master Jadwal');
			return;
		} else {
			let dateTanggalPekerjaan = $.trim(as_tanggal_pekerjaan.find('td#tanggal').text());
			$('input#tgl-awal').val(dateTanggalPekerjaan);
		}

		Document.validasiTanggalSelesai(elm, e);
	},

	addDays: (date, days, uraian = '') => {
		// var result = new Date(date);		
		var dateProses = new Date(date);
		// console.log('date', date);
		//dayoff 
		let dataDayOff = JSON.parse($('input#day-off').val());

		for (let i = 1; i <= days; i++) {
			dateProses.setDate(dateProses.getDate() + 1);
			let dateGet = Document.formatDate(dateProses);
			for (let x = 0; x < dataDayOff.length; x++) {
				let dateOff = dataDayOff[x].tanggal;
				if (dateGet == dateOff) {
					dateProses.setDate(dateProses.getDate() + 1);
				}
			}

			//hari minggu
			let dayIsSunday = dateProses.getDay();
			if (!dayIsSunday) {
				dateProses.setDate(dateProses.getDate() + 1);
			}
		}
		// result.setDate(result.getDate() + days);
		return dateProses;
	},

	formatDate: (date) => {
		let day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
		return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + day;
	},

	validasiTanggalSelesai: (elm, e) => {
		$('#lama-pekerjaan').val('');
		if ($('#tgl-awal').val() == '') {
			toastr.error('Tanggal Awal Pekerjaan Belum Diisi');
		} else {
			let tglAwal = $('#tgl-awal').val();
			let tglSelesai = $('#tgl-selesai').val();

			if (tglSelesai != '') {
				const date1 = new Date(`${tglAwal}`);
				const date2 = new Date(`${tglSelesai}`);
				const diffTime = Math.abs(date2 - date1);
				const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

				if (diffDays >= 0) {
					$('#lama-pekerjaan').val(diffDays);
				} else {
					toastr.error('Pekerjaan Tidak Boleh Kurang Dari Tanggal Awal Pekerjaan');
				}
			}
		}
	},

	addRab: (elm, e) => {
		e.preventDefault();
		let state = $(elm).attr('state');
		if (state == "manual") {
			let tableLast = $(elm).closest('tr');
			let newTr = tableLast.clone();
			newTr.attr('data_id', '');
			newTr.addClass('input');
			newTr.addClass('manual');
			newTr.html(`
				<td class="td_jsa">
					<input type="text" id="uraian" class="form-control required" error="Uraian Material"/>
				</td>
				<td class="td_jsa">
					<input type="text" id="jml_vol" onkeyup="Document.setCalculateHargaManual(this)" class="form-control required" error="Jml Vol"/>
				</td>
				<td class="td_jsa">
					<input type="text" id="satuan_vol" class="form-control required" error="Satuan Vol"/>
				</td>
				<td class="td_jsa">
					<input type="text" id="satuan_anggaran" class="form-control required" error="Satuan Anggaran" onkeyup="Document.hitungJumlahAnggaran(this, event)"/>
				</td>
				<td class="td_jsa">
					<input type="text" id="jumlah_anggaran" disabled class="form-control required" error="Jumlah Anggaran"/>
				</td>
				<td class="td_jsa">
					<input type="text" id="satuan_hps" onkeyup="Document.setCalculateHargaManual(this)" class="form-control required" error="Satuan HPS"/>
				</td>
				<td class="td_jsa">
					<input type="text" id="jumlah_hps" class="form-control required" readonly error="Jumlah HPS"/>
				</td>
				<td class="td_jsa text-center">
					<i class="fa fa-trash" onclick="Document.removeRab(this)"></i>
				</td>
			`);
			tableLast.before(newTr);
		} else {
			let params = {};
			$.ajax({
				type: 'POST',
				dataType: 'html',
				data: params,
				url: url.base_url(Document.module()) + "addRab",

				error: function () {
					toastr.error("Gagal");
				},

				success: function (resp) {
					let tableLast = $(elm).closest('tr');
					let newTr = tableLast.clone();
					newTr.attr('data_id', '');
					newTr.addClass('input');
					newTr.addClass('auto');
					newTr.html(`${resp}`);
					tableLast.before(newTr);

					Document.setIndexingRab();
				}
			});
		}
	},

	removeRab: (elm) => {
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id == '') {
			$(elm).closest('tr').remove();
		} else {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	},

	setIndexingRab: () => {
		let tableLast = $('table#table-rab').find('tbody').find('tr.input');
		$.each(tableLast, function () {
			let material = $(this).find('select#uraian');
			let satuan = $(this).find('select#satuan_vol');
			let indexTr = $(this).index();
			if (material.length > 0) {
				material.attr('id', `uraian-${indexTr}`);
				satuan.attr('id', `satuan_vol-${indexTr}`);
			}
		});

		Document.setSelect2Rab();
	},

	setSelect2Rab: () => {
		let tableLast = $('table#table-rab').find('tbody').find('tr.input');
		$.each(tableLast, function () {
			let indexTr = $(this).index();
			let material = $(this).find(`select#uraian-${indexTr}`);
			let satuan = $(this).find(`select#satuan_vol-${indexTr}`);
			if (material.length > 0) {
				material.select2();
				satuan.select2();
			}
		});
	},

	getSatuanProduk: (elm) => {
		let produk = $(elm).val();
		let indexTr = $(elm).closest('tr').index();
		let params = {};
		params.produk = produk;
		params.indexTr = indexTr;
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Document.module()) + "getSatuanProduk",

			error: function () {
				toastr.error("Gagal");
			},

			success: function (resp) {
				$(elm).closest('tr').find('td:eq(2)').html(resp);
				Document.setSelect2Rab();
			}
		});
	},

	setHpsHarga: (elm) => {
		let indexTr = $(elm).closest('tr').index();
		let satuan = $(elm).closest('tr').find(`select#satuan_vol-${indexTr}`).val();
		let harga = $(elm).closest('tr').find(`select#satuan_vol-${indexTr}`).find(`option[value="${satuan}"]`).attr('harga');
		harga = isNaN(parseInt(harga)) ? 0 : parseInt(harga);
		let jumlahVol = $(elm).closest('tr').find('#jml_vol').val();
		jumlahVol = isNaN(parseInt(jumlahVol)) ? 0 : parseInt(jumlahVol);

		let jumlahTotalRp = jumlahVol * harga;
		$(elm).closest('tr').find('#satuan_hps').val(harga);
		$(elm).closest('tr').find('#jumlah_hps').val(jumlahTotalRp);

		Document.hitungJumlahAnggaran(elm);
	},

	setCalculateHargaManual: (elm) => {
		let jmlVol = $(elm).closest('tr').find('#jml_vol').val();
		jmlVol = isNaN(parseInt(jmlVol)) ? 0 : parseInt(jmlVol);
		let satuanRp = $(elm).closest('tr').find('#satuan_hps').val();
		satuanRp = isNaN(parseInt(satuanRp)) ? 0 : parseInt(satuanRp);
		let jumlahTotalRp = jmlVol * satuanRp;
		$(elm).closest('tr').find('#jumlah_hps').val(jumlahTotalRp);

		Document.hitungJumlahAnggaran(elm);
	},

	setHargaSepakat: (elm, e) => {
		let tr = $(elm).closest('tr');
		let jmlVol = $.trim(tr.find('td#jumlah-vol').text());
		jmlVol = isNaN(parseInt(jmlVol)) ? 0 : parseInt(jmlVol);
		let satuanInput = tr.find('input#satuan-rp-penawaran').val();
		satuanInput = isNaN(parseInt(satuanInput)) ? 0 : parseInt(satuanInput);
		let jumlahInput = tr.find('input#jumlah-rp-penawaran');

		let totalJumlah = jmlVol * satuanInput;
		jumlahInput.val(totalJumlah);

		//cek harga disepakati
		let hpsHarga = $.trim(tr.find('td#satuan-rp-hps').text());
		hpsHarga = isNaN(parseInt(hpsHarga)) ? 0 : parseInt(hpsHarga);
		let satuanSepakat = tr.find('td#satuan-sepakat');
		let inputManual = false;
		if (hpsHarga > satuanInput) {
			satuanSepakat.html(satuanInput);
		} else {
			let inputHargaSepakat = `<input onkeyup="Document.hitungHargaKesepakatan(this, event)" type="text" id="harga-sepakat" value="${hpsHarga}"/>`;
			satuanSepakat.html(inputHargaSepakat);
			inputManual = true;
			// satuanSepakat.html(hpsHarga);
		}

		if (inputManual) {
			Document.hitungHargaKesepakatan(elm, e);
		} else {
			let satuanSepakatVal = $.trim(satuanSepakat.text());
			satuanSepakatVal = isNaN(parseInt(satuanSepakatVal)) ? 0 : parseInt(satuanSepakatVal);
			let jumlanSepakat = tr.find('td#jumlah-sepakat');
			let totalJumlahSepakat = jmlVol * satuanSepakatVal;
			jumlanSepakat.html(totalJumlahSepakat);
		}

		Document.setTotalHargaDisepakati();
	},

	hitungJumlahAnggaran: (elm, e) => {
		let jml = $(elm).closest('tr').find('input#jml_vol').val();
		jml = isNaN(parseInt(jml)) ? 0 : parseInt(jml);

		let satuan_anggaran = $(elm).closest('tr').find('input#satuan_anggaran').val();
		satuan_anggaran = isNaN(parseInt(satuan_anggaran)) ? 0 : parseInt(satuan_anggaran);

		let jmlAnggaran = jml * satuan_anggaran;
		$(elm).closest('tr').find('input#jumlah_anggaran').val(jmlAnggaran);
	},

	hitungHargaKesepakatan: (elm, e) => {
		let hargaSepakat = $(elm).closest('tr').find('input#harga-sepakat').val();
		hargaSepakat = isNaN(parseInt(hargaSepakat)) ? 0 : parseInt(hargaSepakat);

		let jmlSatuan = $.trim($(elm).closest('tr').find('td#jumlah-vol').text());
		jmlSatuan = isNaN(parseInt(jmlSatuan)) ? 0 : parseInt(jmlSatuan);

		let total = jmlSatuan * hargaSepakat;
		$(elm).closest('tr').find('td#jumlah-sepakat').html(total);
	},

	addRekanan: (elm) => {
		let tr = $(elm).closest('tr');
		let newTr = tr.clone();
		newTr.find('select').val('');
		newTr.find('td#action').html(`<i class="fa fa-trash" onclick="Document.removeRekanan(this)"></i>`);
		tr.after(newTr);
	},

	removeRekanan: (elm) => {
		let id = $(elm).closest('tr').attr('data_id');
		if (id == '') {
			$(elm).closest('tr').remove();
		} else {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	},

	checkFile: function (elm) {
		if (window.FileReader) {
			var data_file = $(elm).get(0).files[0];
			var file_name = data_file.name;
			var data_from_file = data_file.name.split('.');

			var type_file = $.trim(data_from_file[data_from_file.length - 1]);
			type_file = type_file.toLowerCase();
			if (type_file == 'pdf' || type_file == 'png' || type_file == 'jpeg' || type_file == 'jpg') {
				if (data_file.size <= 1324000) {
					$(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
				} else {
					toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
					message.closeLoading();
				}
			} else {
				toastr.error('File Harus Berformat Png, Jpg, Jpeg');
				$(elm).val('');
				message.closeLoading();
			}
		} else {
			toastr.error('FileReader is Not Supported');
			message.closeLoading();
		}
	},

	gantiFileRab: function (elm, e) {
		e.preventDefault();
		var file_input = $('div.content-input-rab-file').find('div#file_input_rab').closest('div.col-md-6');
		// console.log('file_input', $('div.content-input-rab-file'));
		file_input.removeClass('hide');
		$('div.content-input-rab-file').find('div#detail_file_rab').closest('div').addClass('hide');
	},

	gantiFileKak: function (elm, e) {
		e.preventDefault();
		var file_input = $('div.content-input-kak-file').find('div#file_input_kak').closest('div.col-md-6');
		// console.log('file_input', $('div.content-input-kak-file'));
		file_input.removeClass('hide');
		$('div.content-input-kak-file').find('div#detail_file_kak').closest('div').addClass('hide');
	},

	gantiFileRpb: function (elm, e) {
		e.preventDefault();
		var file_input = $('div.content-input-rpb-file').find('div#file_input_rpb').closest('div.col-md-6');
		// console.log('file_input', $('div.content-input-rpb-file'));
		file_input.removeClass('hide');
		$('div.content-input-rpb-file').find('div#detail_file_rpb').closest('div').addClass('hide');
	},

	showFileRab: (elm, e) => {
		let params = {};
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Document.module()) + "showFile",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	showFileKak: (elm, e) => {
		let params = {};
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Document.module()) + "showFileKak",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	showFileRpb: (elm, e) => {
		let params = {};
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Document.module()) + "showFileRpb",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	exportExcel: (elm, e) => {
		e.preventDefault();
		let idExportContent = $(elm).attr('idexport');
		window.open('data:application/vnd.ms-excel,' + encodeURIComponent($(`div#${idExportContent}`).html()));
	},

	tableToExcel: (elm, table, name) => {
		if (!table.nodeType) table = document.getElementById(table);
		let idExportContent = $(elm).attr('idexport');
		var ctx = {
			worksheet: name || 'Worksheet',
			// table: table.innerHTML
			table: $(`div#${idExportContent}`).html()
		};

		$(elm).attr('href', uri + base64(format(template, ctx)));
		$(elm).attr('download', 'test.xls');
	},

	setVendorEvalusai: (elm) => {
		let vendorId = $('select#vendor').val();
		if (vendorId != '') {
			let namaVendor = $('select#vendor').find(`option[value=${vendorId}]`).text();
			$('td#supplier-evaluasi').html(namaVendor);			
		} else {
			$('td#supplier-evaluasi').html('');
		}

		// let params = {};
		// params.vendorId = vendorId;
		// $.ajax({
		// 	type: 'POST',
		// 	dataType: 'html',
		// 	data: params,
		// 	url: url.base_url(Document.module()) + "getVendorEvaluasi",

		// 	error: function () {
		// 		toastr.error("Gagal Diproses");
		// 	},

		// 	success: function (resp) {
		// 		bootbox.dialog({
		// 			message: resp,
		// 			size: 'large'
		// 		});
		// 	}
		// });
	},

	setBarangdanJasaPekerjaan: (elm, e) => {
		let namaPekerjaan = $('#judul-pekerjaan').val();
		$('td#barang_pekerjaan_jasa').html(namaPekerjaan);
	},

	setTotalHargaDisepakati: () => {
		let tablePh = $('#table-penawaran-harga').find('tbody').find('tr');
		let totalSepakat = 0;
		let totalPh = 0;
		
		$.each(tablePh, function() {
			let tr = $(this);
			let totalHargaSepakat = $.trim(tr.find('td#jumlah-sepakat').text());
			totalHargaSepakat = isNaN(parseFloat(totalHargaSepakat)) ? 0 : parseInt(totalHargaSepakat);
			totalSepakat += totalHargaSepakat;
			
			let totalHargaPh = $.trim(tr.find('input#jumlah-rp-penawaran').val());
			totalHargaPh = isNaN(parseFloat(totalHargaPh)) ? 0 : parseInt(totalHargaPh);
			totalPh += totalHargaPh;
		});

		$('td#harga_ph').html(totalPh);
		$('td#harga_sepakat_nego').html(totalSepakat);
	},

	setDataLampiranDataNegosiasi:()=>{
		let table_lampiran = $('table#table-lampiran-negosiasi').find('tbody');
		let table_ph = $('table#table-penawaran-harga').find('tbody').find('tr');
		let html = '';
		let no =1;
		$.each(table_ph, function(){
			let tr = $(this);
			let barang = $.trim(tr.find('td#uraian').text());
			let jumlah = $.trim(tr.find('td#jumlah-vol').text());
			let satuan = $.trim(tr.find('td#satuan').text());
			let satuan_rp = $.trim(tr.find('input#satuan-rp-penawaran').val());
			let jumlah_rp = $.trim(tr.find('input#jumlah-rp-penawaran').val());
			let satuan_rp_hps = $.trim(tr.find('td#satuan-rp-hps').text());
			let jumlah_rp_hps = $.trim(tr.find('td#jumlah-rp-hps').text());
			let satuan_sepakat = $.trim(tr.find('td#satuan-sepakat').text());
			let jumlah_sepakat = $.trim(tr.find('td#jumlah-sepakat').text());
			html +=`<tr>
				<td class="td_jsa">${no++}</td>
				<td class="td_jsa">${barang}</td>
				<td class="td_jsa">${jumlah}</td>
				<td class="td_jsa">${satuan}</td>
				<td class="td_jsa">${satuan_rp}</td>
				<td class="td_jsa">${jumlah_rp}</td>
				<td class="td_jsa">${satuan_rp_hps}</td>
				<td class="td_jsa">${jumlah_rp_hps}</td>
				<td class="td_jsa">${satuan_sepakat}</td>
				<td class="td_jsa">${jumlah_sepakat}</td>
			</tr>`;
		});

		table_lampiran.html(html);
	}
};

$(function () {

	// Document.validasiTanggalSelesai();
	Document.setDate();
	Document.setSelect2();
	Document.setIndexingRab();
	Document.setVendorEvalusai();
	Document.setBarangdanJasaPekerjaan();
	Document.setTotalHargaDisepakati();
	Document.setDataLampiranDataNegosiasi();
});
