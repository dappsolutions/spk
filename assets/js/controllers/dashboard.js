var Dashboard = {

 setGrafikSpk: function () {
  var data_pj = $('input#data-spk').val();
  var data_result = data_pj.toString().split(',');

  var max_y = $('input#total-data-spk').val();

  console.log('data_result', data_result);
  var bar = new Morris.Bar({
   element: 'line-chart',
   resize: true,
   data: [
    {y: `${$('#year').val()} Jan`, a: data_result[0]},
    {y: `${$('#year').val()} Feb`, a: data_result[1]},
    {y: `${$('#year').val()} Mar`, a: data_result[2]},
    {y: `${$('#year').val()} Apr`, a: data_result[3]},
    {y: `${$('#year').val()} Mei`, a: data_result[4]},
    {y: `${$('#year').val()} Jun`, a: data_result[5]},
    {y: `${$('#year').val()} Jul`, a: data_result[6]},
    {y: `${$('#year').val()} Ags`, a: data_result[7]},
    {y: `${$('#year').val()} Sep`, a: data_result[8]},
    {y: `${$('#year').val()} Okt`, a: data_result[9]},
    {y: `${$('#year').val()} Nov`, a: data_result[10]},
    {y: `${$('#year').val()} Des`, a: data_result[11]}
   ],
   barColors: ['#49a65b'],
   xkey: 'y',
   ymax: max_y,
   ymin: 0,
   ykeys: ['a'],
   labels: ['SPK'],
   hideHover: 'auto'
  });
 },

 setDataTable:()=>{
  let table = $('table#tb_data_supplier');
  table.DataTable();
 }
};


$(function () {
 Dashboard.setGrafikSpk();
 Dashboard.setDataTable();
//  Dashboard.setGrafikKas();
//  Dashboard.setGrafikPembelian();
//  Dashboard.setGrafikPembelianKredit();
});
