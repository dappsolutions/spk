var Supplier = {
	module: function () {
		return 'supplier';
	},

	add: function () {
		window.location.href = url.base_url(Supplier.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(Supplier.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(Supplier.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(Supplier.module()) + "index";
			}
		}
	},

	getPostInputAgen: () => {
		let tableData = $('table#table-data-agen').find('tbody').find('tr.input');
		let data = [];

		let counterAgen = 0;
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.agen_resmi_merk = $(this).find('input#agen_resmi_merk').val();
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			// $('#videoUploadFile').get(0).files.length
			params.has_file = 0;
			if ($(this).find('input#file_agen').length > 0) {
				if ($(this).find('input#file_agen').get(0).files.length > 0) {
					params.has_file = 1;
				}
			}
			params.counter_agen = counterAgen;
			if (params.agen_resmi_merk != '') {
				data.push(params);
			}
			counterAgen += 1;
		});

		return data;
	},

	getPostInputAkta: () => {
		let tableData = $('table#table-data-akta').find('tbody').find('tr.input');
		let data = [];

		let counterAkta = 0;
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.akta = $(this).find('input#akta').val();
			params.expired_date_akta = $(this).find('td#expired_akta').find('input').val();
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			// $('#videoUploadFile').get(0).files.length
			params.has_file = 0;
			if ($(this).find('input#file_akta').length > 0) {
				if ($(this).find('input#file_akta').get(0).files.length > 0) {
					params.has_file = 1;
				}
			}
			params.counter_akta = counterAkta;
			if (params.akta != '') {
				data.push(params);
			}
			counterAkta += 1;
		});

		return data;
	},

	getPostInputSpk: () => {
		let tableData = $('table#table-data-spk-pengalaman').find('tbody').find('tr.input');
		let data = [];

		let counterSpk = 0;
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.spk_pengalaman = $(this).find('input#spk_pengalaman').val();
			params.expired_date_spk = $(this).find('td#expired_spk').find('input').val();
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			// $('#videoUploadFile').get(0).files.length
			params.has_file = 0;
			if ($(this).find('input#file_spk_pengalaman').length > 0) {
				if ($(this).find('input#file_spk_pengalaman').get(0).files.length > 0) {
					params.has_file = 1;
				}
			}
			params.counter_spk = counterSpk;
			if (params.spk_pengalaman != '') {
				data.push(params);
			}
			counterSpk += 1;
		});

		return data;
	},

	getPostInputSertifkat: () => {
		let tableData = $('div.content-input-sertifikat-badan-usaha');
		let data = [];

		let counterSertifikat = 0;
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.sertifikat_id = $(this).find('input#sertifikat_id').val();
			params.expired_sertifikat = $(this).find('div#content_expired_sertifikat').find('input').val();
			params.kriteria = $(this).find('#kriteria').val();
			params.bidang_usaha = $(this).find('#bidang_usaha').val();
			params.sub_bidang_usaha = $(this).find('#sub_bidang_usaha').val();
			params.sub_kualifikasi = $(this).find('#sub_kualifikasi').val();
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			// $('#videoUploadFile').get(0).files.length
			params.has_file = 0;
			if ($(this).find('input#file_sertifikat').length > 0) {
				if ($(this).find('input#file_sertifikat').get(0).files.length > 0) {
					params.has_file = 1;
				}
			}
			params.counter_serifikat = counterSertifikat;
			if (params.expired_sertifikat != '') {
				data.push(params);
			}
			counterSertifikat += 1;
		});

		return data;
	},

	getPostInputSertifkatLain: () => {
		let tableData = $('div.content-input-sertifikat-lain');
		let data = [];

		let counterSertifikat = 0;
		$.each(tableData, function () {
			let params = {};
			params.id = $(this).attr('data_id');
			params.sertifikat_id = $(this).find('input#sertifikat_lai_id').val();
			params.expired_sertifikat = $(this).find('div#content_expired_sertifikat_lain').find('input').val();
			params.nama_sertifikat = $(this).find('#nama_sertifikat_lain').val();
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			// $('#videoUploadFile').get(0).files.length
			params.has_file = 0;
			if ($(this).find('input#file_sertifikat_lain').length > 0) {
				if ($(this).find('input#file_sertifikat_lain').get(0).files.length > 0) {
					params.has_file = 1;
				}
			}
			params.counter_serifikat = counterSertifikat;
			if (params.expired_sertifikat != '') {
				data.push(params);
			}
			counterSertifikat += 1;
		});

		return data;
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'nama_vendor': $('#nama_vendor').val(),
			'nama_pimpinan': $('#nama_pimpinan').val(),
			'jabatan_pimpinan': $('#jabatan_pimpinan').val(),
			'email': $('#email').val(),
			'nama_bank': $('#nama_bank').val(),
			'no_rekening': $('#no_rekening').val(),
			'alamat': $('#alamat').val(),
			// 'sertifikat_id': $('#sertifikat_id').val(),
			// 'kriteria': $('#kriteria').val(),
			// 'bidang_usaha': $('#bidang_usaha').val(),
			// 'sub_bidang_usaha': $('#sub_bidang_usaha').val(),
			// 'sub_kualifikasi': $('#sub_kualifikasi').val(),
			// 'expired_sertifikat': $('#expired_sertifikat').val(),
			// 'sertifikat_lain_id': $('#sertifikat_lain_id').val(),
			// 'nib': $('#nib').val(),
			// 'expired_sertifikat_nib': $('#expired_sertifikat_nib').val(),
			// 'npwp': $('#npwp').val(),
			// 'expired_sertifikat_npwp': $('#expired_sertifikat_npwp').val(),
			// 'pkp': $('#pkp').val(),
			// 'expired_sertifikat_pkp': $('#expired_sertifikat_pkp').val(),
			// 'susanan_pengurus': $('#susanan_pengurus').val(),
			// 'expired_sertifikat_susunan': $('#expired_sertifikat_susunan').val(),
			// 'surat_keterangan_domisili': $('#surat_keterangan_domisili').val(),
			// 'expired_sertifikat_domisili': $('#expired_sertifikat_domisili').val(),
			// 'surat_keterangan_rekening': $('#surat_keterangan_rekening').val(),
			// 'expired_sertifikat_rekening': $('#expired_sertifikat_rekening').val(),
			// 'pakta_integritas': $('#pakta_integritas').val(),
			// 'expired_sertifikat_pakta': $('#expired_sertifikat_pakta').val(),
			// 'ak3': $('#ak3').val(),
			// 'expired_sertifikat_ak3': $('#expired_sertifikat_ak3').val(),
			'data_agen': Supplier.getPostInputAgen(),
			'data_akta': Supplier.getPostInputAkta(),
			'data_spk': Supplier.getPostInputSpk(),
			'data_sertifikat': Supplier.getPostInputSertifkat(),
			'data_sertifikat_lain': Supplier.getPostInputSertifkatLain(),
		};

		return data;
	},

	simpan: function (id) {
		var data = Supplier.getPostData();

		let formData = new FormData();
		formData.append('data', JSON.stringify(data));
		// formData.append(`file_sertifikat`, $('input#file_sertifikat').prop('files')[0]);
		// formData.append(`file_nib`, $('input#file_nib').prop('files')[0]);
		// formData.append(`file_npwp`, $('input#file_npwp').prop('files')[0]);
		// formData.append(`file_pkp`, $('input#file_pkp').prop('files')[0]);
		// formData.append(`file_susunan`, $('input#file_susunan').prop('files')[0]);
		// formData.append(`file_domisili`, $('input#file_domisili').prop('files')[0]);
		// formData.append(`file_rekening`, $('input#file_rekening').prop('files')[0]);
		// formData.append(`file_integritas`, $('input#file_integritas').prop('files')[0]);
		// formData.append(`file_ak3`, $('input#file_ak3').prop('files')[0]);

		//get all file sertifikat badan usaha
		let tableDataSertifikat = $('div.content-input-sertifikat-badan-usaha');
		let counterFileSertifikat = 0;
		$.each(tableDataSertifikat, function () {
			if ($(this).find('input#file_sertifikat').length > 0) {
				if ($(this).find('input#file_sertifikat').get(0).files.length > 0) {
					formData.append(`file_sertifikat${counterFileSertifikat}`, $(this).find('input#file_sertifikat').prop('files')[0]);
				}
			}
			counterFileSertifikat += 1;
		});

		//get all file sertifikat lain
		let tableDataSertifikatLain = $('div.content-input-sertifikat-lain');
		let counterFileSertifikatLain = 0;
		$.each(tableDataSertifikatLain, function () {
			if ($(this).find('input#file_sertifikat_lain').length > 0) {
				if ($(this).find('input#file_sertifikat_lain').get(0).files.length > 0) {
					formData.append(`file_sertifikat_lain${counterFileSertifikatLain}`, $(this).find('input#file_sertifikat_lain').prop('files')[0]);
				}
			}
			counterFileSertifikatLain += 1;
		});

		//get all file agen
		let tableDataAgen = $('table#table-data-agen').find('tbody').find('tr.input');
		let counterFileAgen = 0;
		$.each(tableDataAgen, function () {
			if ($(this).find('input#file_agen').length > 0) {
				if ($(this).find('input#file_agen').get(0).files.length > 0) {
					formData.append(`file_agen${counterFileAgen}`, $(this).find('input#file_agen').prop('files')[0]);
				}
			}
			counterFileAgen += 1;
		});

		//get all file akata
		let tableDataAkta = $('table#table-data-akta').find('tbody').find('tr.input');
		let counterFileAkta = 0;
		$.each(tableDataAkta, function () {
			if ($(this).find('input#file_akta').length > 0) {
				if ($(this).find('input#file_akta').get(0).files.length > 0) {
					formData.append(`file_akta${counterFileAkta}`, $(this).find('input#file_akta').prop('files')[0]);
				}
			}
			counterFileAkta += 1;
		});

		//get all file spk pengalaman
		let tableDataSpk = $('table#table-data-spk-pengalaman').find('tbody').find('tr.input');
		let counterFileSpk = 0;
		$.each(tableDataSpk, function () {
			if ($(this).find('input#file_spk_pengalaman').length > 0) {
				if ($(this).find('input#file_spk_pengalaman').get(0).files.length > 0) {
					formData.append(`file_spk_pengalaman${counterFileSpk}`, $(this).find('input#file_spk_pengalaman').prop('files')[0]);
				}
			}
			counterFileSpk += 1;
		});

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				contentType: false,
				processData: false,
				dataType: 'json',
				url: url.base_url(Supplier.module()) + "simpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					message.closeLoading();
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						// console.log(url.base_url(Supplier.module()) + "detail" + '/' + resp.id);
						var reload = function () {
							window.location.href = url.base_url(Supplier.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(Supplier.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(Supplier.module()) + "detail/" + id;
	},



	delete: function (id) {
		let html = `<div class="row">
  <div class="col-md-12 text-center">
   <p>Apakah anda yakin menghapus data ?</p>
  </div>
  <div class="col-md-12 text-center">
   <br/>
   <button class="btn btn-success" onclick="Supplier.execDelete(${id})">Ya</button>
   <button class="btn btn-warning" onclick="message.closeDialog()">Tidak</button>
  </div>
  </div>`;

		bootbox.dialog({
			message: html
		});
	},

	execDelete: (id) => {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url.base_url(Supplier.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(Supplier.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	checkFile: function (elm) {
		if (window.FileReader) {
			var data_file = $(elm).get(0).files[0];
			var file_name = data_file.name;
			var data_from_file = data_file.name.split('.');

			var type_file = $.trim(data_from_file[data_from_file.length - 1]);
			type_file = type_file.toLowerCase();
			if (type_file == 'pdf' || type_file == 'png' || type_file == 'jpeg' || type_file == 'jpg') {
				if (data_file.size <= 1324000) {
					$(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
				} else {
					toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
					message.closeLoading();
				}
			} else {
				toastr.error('File Harus Berformat Png, Jpg, Jpeg');
				$(elm).val('');
				message.closeLoading();
			}
		} else {
			toastr.error('FileReader is Not Supported');
			message.closeLoading();
		}
	},

	setDate: function () {
		// $('input#expired_sertifikat').datepicker({
		// 	format: 'yyyy-mm-dd',
		// 	todayHighlight: true,
		// 	autoclose: true,
		// 	startDate: new Date(),
		// });

		$('input#expired_sertifikat_nib').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_akte').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_npwp').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_pkp').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_susunan').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_domisili').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_rekening').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_pakta').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_ak3').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});

		$('input#expired_sertifikat_spk').datepicker({
			format: 'yyyy-mm-dd',
			todayHighlight: true,
			autoclose: true,
			// startDate: new Date(),
		});
	},

	addAgen: (elm, e) => {
		e.preventDefault();
		let tr = $('table#table-data-agen').find('tbody').find('tr.input:last');
		let newTr = tr.clone();
		newTr.find('input#agen_resmi_merk').val('');
		newTr.find('td#file-agen').html(`<input type="file" id="file_agen" class="form-control" onchange="Supplier.checkFile(this)">`);
		newTr.find('td#action').html(`<a href="" onclick="Supplier.removeAgen(this, event)"><i class="mdi mdi-minus mdi-18px"></i></a>`);
		tr.after(newTr);
	},

	addAkta: (elm, e) => {
		e.preventDefault();
		let tr = $('table#table-data-akta').find('tbody').find('tr.input:last');
		let indexTr = tr.index();
		indexTr += 1;
		let newTr = tr.clone();
		newTr.find('input#akta').val('');
		newTr.find('td#file-akta').html(`<input type="file" id="file_akta" class="form-control" onchange="Supplier.checkFile(this)">`);
		newTr.find('td#expired_akta').html(`<input type="text" readonly index="${indexTr}" id="expired_date_akta${indexTr}" class="form-control">`);
		newTr.find('td#action').html(`<a href="" onclick="Supplier.removeAkta(this, event)"><i class="mdi mdi-minus mdi-18px"></i></a>`);
		tr.after(newTr);
		Supplier.setDataDateAkta();
	},

	addSpkPengalaman: (elm, e) => {
		e.preventDefault();
		let tr = $('table#table-data-spk-pengalaman').find('tbody').find('tr.input:last');
		let indexTr = tr.index();
		indexTr += 1;
		let newTr = tr.clone();
		newTr.find('input#spk_pengalaman').val('');
		newTr.find('td#file-spk-pengalaman').html(`<input type="file" id="file_spk_pengalaman" class="form-control" onchange="Supplier.checkFile(this)">`);
		newTr.find('td#expired_spk').html(`<input type="text" readonly index="${indexTr}" id="expired_date_spk${indexTr}" class="form-control">`);
		newTr.find('td#action').html(`<a href="" onclick="Supplier.removeSpkPengalaman(this, event)"><i class="mdi mdi-minus mdi-18px"></i></a>`);
		tr.after(newTr);
		Supplier.setDataDateSpk();
	},

	removeAgen: (elm, e) => {
		e.preventDefault();
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id == '') {
			$(elm).closest('tr').remove();
		} else {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	},

	removeAkta: (elm, e) => {
		e.preventDefault();
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id == '') {
			$(elm).closest('tr').remove();
		} else {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	},

	removeSpkPengalaman: (elm, e) => {
		e.preventDefault();
		let data_id = $(elm).closest('tr').attr('data_id');
		if (data_id == '') {
			$(elm).closest('tr').remove();
		} else {
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	},

	showFile: (elm, e) => {
		let params = {};
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Supplier.module()) + "showFile",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	showFileAgen: (elm, e) => {
		let params = {};
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Supplier.module()) + "showFileAgen",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	showFileAkta: (elm, e) => {
		let params = {};
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Supplier.module()) + "showFileAkta",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	showFileSpk: (elm, e) => {
		let params = {};
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Supplier.module()) + "showFileSpk",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	gantiFileSertifikat: function (elm, e) {
		e.preventDefault();
		var file_input = $(elm).closest('div.content-input-sertifikat-badan-usaha').find('div#file_input_sertifikat').closest('div.col-md-6');
		// console.log('file_input', file_input);
		file_input.removeClass('hide');

		$(elm).closest('div.content-input-sertifikat-badan-usaha').find('div#detail_file_sertifikat').closest('div').addClass('hide');
	},

	addSertifikatBadanUsaha: (elm) => {
		let divContent = $(elm).closest('div.content-input-sertifikat-badan-usaha');
		let indexTr = divContent.index() - 1;
		indexTr += 1;
		let newDivContent = divContent.clone();
		newDivContent.attr('data_id', '');
		newDivContent.find('input').val('');
		newDivContent.find('#action-badan-usaha').html(`<button class="btn btn-danger" onclick="Supplier.removeSertifikatBadanUsaha(this)"><i class="fa fa-minus"></i></button>`);
		newDivContent.find('div#file_input_sertifikat').html(`<label for="">Upload Sertifikat</label><input type="file" id="file_sertifikat" class="form-control" onchange="Supplier.checkFile(this)">`);
		newDivContent.find('div#content_expired_sertifikat').html(`<label for="">Expired Sertifikat</label><input type="text" index="${indexTr}" id="expired_sertifikat${indexTr}" readonly class="form-control" error="" value="">`);
		divContent.after(newDivContent);
		Supplier.setDataDateSertifikat();
	},

	addSertifikatBadanUsahaLain: (elm) => {
		let divContent = $(elm).closest('div.content-input-sertifikat-lain');
		let indexTr = divContent.index() - 1;
		indexTr += 1;
		let newDivContent = divContent.clone();
		newDivContent.attr('data_id', '');
		newDivContent.find('input').val('');
		newDivContent.find('#action-lain').html(`<button class="btn btn-danger" onclick="Supplier.removeSertifikatBadanUsahaLain(this)"><i class="fa fa-minus"></i></button>`);
		newDivContent.find('div#file_input_sertifikat_lain').html(`<label for="">Upload Sertifikat</label><input type="file" id="file_sertifikat_lain" class="form-control" onchange="Supplier.checkFile(this)">`);
		newDivContent.find('div#content_expired_sertifikat_lain').html(`<label for="">Expired Sertifikat</label><input type="text" index="${indexTr}" id="expired_sertifikat_lain${indexTr}" readonly class="form-control" error="" value="">`);
		divContent.after(newDivContent);
		Supplier.setDataDateSertifikatLain();
	},

	removeSertifikatBadanUsaha: (elm) => {
		let data_id = $(elm).closest('div.content-input-sertifikat-badan-usaha').attr('data_id');
		if (data_id != '') {
			$(elm).closest('div.content-input-sertifikat-badan-usaha').addClass('hide');
			$(elm).closest('div.content-input-sertifikat-badan-usaha').addClass('remove');
		} else {
			$(elm).closest('div.content-input-sertifikat-badan-usaha').remove();
		}
	},

	removeSertifikatBadanUsahaLain: (elm) => {
		let data_id = $(elm).closest('div.content-input-sertifikat-lain').attr('data_id');
		if (data_id != '') {
			$(elm).closest('div.content-input-sertifikat-lain').addClass('hide');
			$(elm).closest('div.content-input-sertifikat-lain').addClass('remove');
		} else {
			$(elm).closest('div.content-input-sertifikat-lain').remove();
		}
	},

	gantiFileSertifikatLain: function (elm, e) {
		e.preventDefault();
		let purpose_id = $(elm).attr('purpose_id');
		let current_id = $(elm).attr('current_id');
		var file_input = $(`div#${purpose_id}`).closest('div.col-md-4');
		file_input.removeClass('hide');

		$(`div#${current_id}`).closest('div').addClass('hide');
	},

	gantiFileAgen: function (elm, e) {
		e.preventDefault();
		var file_input = $(elm).closest('td');
		file_input.html(`<input type="file" id="file_agen" class="form-control" onchange="Supplier.checkFile(this)">`);
	},

	gantiFileAkta: function (elm, e) {
		e.preventDefault();
		var file_input = $(elm).closest('td');
		file_input.html(`<input type="file" id="file_akta" class="form-control" onchange="Supplier.checkFile(this)">`);
	},

	gantiFileSpk: function (elm, e) {
		e.preventDefault();
		var file_input = $(elm).closest('td');
		file_input.html(`<input type="file" id="file_spk_pengalaman" class="form-control" onchange="Supplier.checkFile(this)">`);
	},

	setDataDateAkta: () => {
		let tableData = $('table#table-data-akta').find('tbody').find('tr.input');
		$.each(tableData, function () {
			let index = $(this).index();
			$(this).find(`input#expired_date_akta${index}`).datepicker({
				format: 'yyyy-mm-dd',
				todayHighlight: true,
				autoclose: true,
				// startDate: new Date(),
			});
		});
	},

	setDataDateSpk: () => {
		let tableData = $('table#table-data-spk-pengalaman').find('tbody').find('tr.input');
		$.each(tableData, function () {
			let index = $(this).index();
			$(this).find(`input#expired_date_spk${index}`).datepicker({
				format: 'yyyy-mm-dd',
				todayHighlight: true,
				autoclose: true,
				// startDate: new Date(),
			});
		});
	},

	setDataDateSertifikat: () => {
		let tableData = $('div.content-input-sertifikat-badan-usaha');
		// console.log('tableData', tableData);
		$.each(tableData, function () {
			let index = $(this).index();
			console.log('index', index);
			$(`input#expired_sertifikat${index}`).datepicker({
				format: 'yyyy-mm-dd',
				todayHighlight: true,
				autoclose: true,
				// startDate: new Date(),
			});
		});
	},

	setDataDateSertifikatLain: () => {
		let tableData = $('div.content-input-sertifikat-lain');
		// console.log('tableData', tableData);
		$.each(tableData, function () {
			let index = $(this).index();
			console.log('index', index);
			$(`input#expired_sertifikat_lain${index}`).datepicker({
				format: 'yyyy-mm-dd',
				todayHighlight: true,
				autoclose: true,
				// startDate: new Date(),
			});
		});
	},

	exportView: (elm) => {
		let params = {};
		params.id = $('input#id').val();

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(Supplier.module()) + "exportView",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	exportExcel: (elm, e) => {
		e.preventDefault();
		let idExportContent = $(elm).attr('idexport');
		window.open('data:application/vnd.ms-excel,' + encodeURIComponent($(`div#${idExportContent}`).html()));
	},
};

$(function () {
	Supplier.setDate();
	Supplier.setDataDateAkta();
	Supplier.setDataDateSpk();
	Supplier.setDataDateSertifikat();
	Supplier.setDataDateSertifikatLain();
});
