var SkkNo = {
	module: function () {
		return 'skk_no';
	},

	add: function () {
		window.location.href = url.base_url(SkkNo.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(SkkNo.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(SkkNo.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(SkkNo.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'skk_no': $('#skk_no').val(),
		};

		return data;
	},

	simpan: function (id) {
		var data = SkkNo.getPostData();

		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append('file_skk', $('input#file_skk').prop('files')[0]);


		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				url: url.base_url(SkkNo.module()) + "simpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					message.closeLoading();
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						// console.log(url.base_url(SkkNo.module()) + "detail" + '/' + resp.id);
						var reload = function () {
							window.location.href = url.base_url(SkkNo.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(SkkNo.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(SkkNo.module()) + "detail/" + id;
	},



	delete: function (id) {
		let html = `<div class="row">
  <div class="col-md-12 text-center">
   <p>Apakah anda yakin menghapus data ?</p>
  </div>
  <div class="col-md-12 text-center">
   <br/>
   <button class="btn btn-success" onclick="SkkNo.execDelete(${id})">Ya</button>
   <button class="btn btn-warning" onclick="message.closeDialog()">Tidak</button>
  </div>
  </div>`;

		bootbox.dialog({
			message: html
		});
	},

	execDelete: (id) => {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url.base_url(SkkNo.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(SkkNo.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	showFile: (elm, e) => {
		let params = {};
		// params.file = $.trim($(elm).text());
		params.file = $.trim($(elm).attr('file'));

		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: params,
			url: url.base_url(SkkNo.module()) + "showFile",

			error: function () {
				toastr.error("Gagal Diproses");
			},

			success: function (resp) {
				bootbox.dialog({
					message: resp,
					size: 'large'
				});
			}
		});
	},

	gantiFile: function (elm, e) {
		e.preventDefault();
		var file_input = $('div#file_input_skk');
		file_input.removeClass('hide');

		$('div#detail_file_skk').addClass('hide');
	},

	setDate: function () {
		$('input#tanggal').datepicker({
			dateFormat: 'yy-mm-dd',
			todayHighlight: true,
			autoclose: true
		});
	},

	checkFile: function (elm) {
		if (window.FileReader) {
			var data_file = $(elm).get(0).files[0];
			var file_name = data_file.name;
			var data_from_file = data_file.name.split('.');

			var type_file = $.trim(data_from_file[data_from_file.length - 1]);
			type_file = type_file.toLowerCase();
			if (type_file == 'pdf' || type_file == 'png' || type_file == 'jpeg' || type_file == 'jpg') {
				if (data_file.size <= 1324000) {
					$(elm).closest('div').find('span.fileinput-filename').text($(elm).val());
				} else {
					toastr.error('Gagal Upload, Ukuran File Maximal 1 MB');
					message.closeLoading();
				}
			} else {
				toastr.error('File Harus Berformat Png, Jpg, Jpeg');
				$(elm).val('');
				message.closeLoading();
			}
		} else {
			toastr.error('FileReader is Not Supported');
			message.closeLoading();
		}
	},
};

$(function () {
	SkkNo.setDate();
});
