var Jadwal = {
	module: function () {
		return 'jadwal';
	},

	add: function () {
		window.location.href = url.base_url(Jadwal.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(Jadwal.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(Jadwal.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(Jadwal.module()) + "index";
			}
		}
	},


	addItemSpbjl:(elm, e)=>{
		e.preventDefault();
		let tr = $(elm).closest('tr');
		let newTr = tr.clone();		
		newTr.addClass('input');
		newTr.attr('data_id', '');
		newTr.html(`
		<td>-</td>
		<td>
			<input type="text" class="form-control required" id="uraian-pekerjaan" error="Uraian Pekerjaan"/>
		</td>
		<td>
			<input type="text" class="form-control required" id="no-dokumen" error="No Dokumen"/>
		</td>
		<td>
			<input type="text" class="form-control" id="standard"/>
		</td>
		<td>
			<input type="text" class="form-control" id="cepat"/>
		</td>
		<td class="hide">
			<input type="text" class="form-control" id="manual"/>
		</td>
		<td>-</td>
		<td class="text-center hide">
				<input type="radio" name="as_tanggal_pekerjaan" id="as_tanggal_pekerjaan"/>
		</td>
		<td class="text-center">
			<i class="fa fa-trash" onclick="Jadwal.removeItem(this)"></i>
		</td>
		`);				
		tr.before(newTr);

		let classInput = $('table#table-spbjl').find('tbody').find('tr.input');
		let no = 1;
		$.each(classInput, function(){
				let tdNo = $(this).find('td:eq(0)');
				tdNo.html(no++);
		});
	},

	getPostItemSpbjl:(elm)=>{
		let data = [];
		let tableData = $('table#table-spbjl').find('tbody').find('tr.input');
		$.each(tableData, function(){
			let params = {};
			params.id = $(this).attr('data_id');
			params.uraian_pekerjaan = $(this).find('input#uraian-pekerjaan').val();
			params.no_dokumen = $(this).find('input#no-dokumen').val();
			params.standard = $(this).find('input#standard').val();
			params.cepat = $(this).find('input#cepat').val();
			params.manual = $(this).find('input#manual').val();
			params.as_tanggal_pekerjaan = $(this).find('input#as_tanggal_pekerjaan').is(':checked') ? 1 : 0;
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			data.push(params);
		});

		return data;
	},
	
	getPostItemSpk:(elm)=>{
		let data = [];
		let tableData = $('table#table-spk').find('tbody').find('tr.input');
		$.each(tableData, function(){
			let params = {};
			params.id = $(this).attr('data_id');
			params.uraian_pekerjaan = $(this).find('input#uraian-pekerjaan').val();
			params.no_dokumen = $(this).find('input#no-dokumen').val();
			params.standard = $(this).find('input#standard').val();
			params.cepat = $(this).find('input#cepat').val();
			params.manual = $(this).find('input#manual').val();
			params.as_tanggal_pekerjaan = $(this).find('input#as_tanggal_pekerjaan').is(':checked') ? 1 : 0;
			params.remove = $(this).hasClass('remove') ? 1 : 0;
			data.push(params);
		});

		return data;
	},

	simpanSpbjl: function (elm) {
		let params = {};
		params.dataInput = Jadwal.getPostItemSpbjl(elm);
		

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: params,
				dataType: 'json',
				url: url.base_url(Jadwal.module()) + "simpanSpbjl",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						
						setTimeout(function(){
							window.location.reload();
						}, 1000);
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				}
			});
		}
	},
	
	simpanSpk: function (elm) {
		let params = {};
		params.dataInput = Jadwal.getPostItemSpk(elm);
		

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: params,
				dataType: 'json',
				url: url.base_url(Jadwal.module()) + "simpanSpk",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						
						setTimeout(function(){
							window.location.reload();
						}, 1000);
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				}
			});
		}
	},

	removeItem:(elm)=>{
		let dataId = $(elm).closest('tr').attr('data_id');
		if(dataId == ''){
			$(elm).closest('tr').remove();
		}else{
			$(elm).closest('tr').addClass('hide');
			$(elm).closest('tr').addClass('remove');
		}
	}
};

$(function () {
 
});
