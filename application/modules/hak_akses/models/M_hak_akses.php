
<?php

class M_hak_akses extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}


	public function getListMenuModule($params = array())
	{
		$hak_akses = $this->session->userdata('hak_akses');
		$upt = $this->session->userdata('upt');

		$sql = "select * from module where parent is not null ";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$value['read'] = 0;
				$value['create'] = 0;
				$value['update'] = 0;
				$value['delete'] = 0;
				$value['status'] = 0;
				$value['phm_id'] = '';
				$foreign = trim($value['id']);
				foreach ($params['dataListMenu'] as $v_menu) {
					if (trim($v_menu['module']) == $foreign) {
						$value['phm_id'] = $v_menu['id'];
						$value['read'] = $v_menu['read'] == '' ? 0 : $v_menu['read'];
						$value['create'] = $v_menu['create'] == '' ? 0 : $v_menu['create'];
						$value['update'] = $v_menu['update'] == '' ? 0 : $v_menu['update'];
						$value['delete'] = $v_menu['delete'] == '' ? 0 : $v_menu['delete'];
						$value['status'] = $v_menu['status'] == '' ? 0 : $v_menu['status'];
						// break;
					}
				}
				$result[] = $value;
			}
		}

		return $result;
	}

	public function getListAktifMenuModule($id = "")
	{
		$hak_akses = $this->session->userdata('hak_akses');
		$upt = $this->session->userdata('upt');

		$sql = "
		select phm.* 
		, m.nama_module
		from priveledge p
		left join priveledge_has_module phm 
			on phm.priveledge = p.id 
		left join module m 
			on m.id = phm.module 
		where p.id = " . $id." and phm.deleted = 0";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$result[] = $value;
			}
		}

		return $result;
	}
}
