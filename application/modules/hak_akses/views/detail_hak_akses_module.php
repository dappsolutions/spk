<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-hak-akses">
    <thead>
     <tr class="bg-info">
      <th>No</th>
      <th>Nama Menu</th>
      <th class="text-center">Lihat</th>
      <th class="text-center">Tambah</th>
      <th class="text-center">Edit</th>
      <th class="text-center">Hapus</th>
      <th class="text-center">Status</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data)) { ?>
      <?php $no = 1 ?>
      <?php foreach ($data as $key => $value) { ?>
       <tr data_id="<?php echo $value['id'] ?>" phm_id="<?php echo $value['phm_id'] ?>">
        <td><?php echo $no++ ?></td>
        <td><?php echo $value['alias_name'] ?></td>
        <td class="text-center">
         <?php if ($value['read'] == '' || $value['read'] == '0') { ?>
          <input type="checkbox" id="read">
         <?php }else{ ?>
          <input type="checkbox" id="read" checked disabled>
         <?php } ?>
        </td>
        <td class="text-center">
         <?php if ($value['create'] == '' || $value['create'] == '0') { ?>
          <input type="checkbox" id="create">
         <?php }else{ ?>
          <input type="checkbox" id="create" checked disabled>
         <?php } ?>
        </td>
        <td class="text-center">
         <?php if ($value['update'] == '' || $value['update'] == '0') { ?>
          <input type="checkbox" id="update">
         <?php }else{ ?>
          <input type="checkbox" id="update" checked disabled>
         <?php } ?>
        </td>
        <td class="text-center">
         <?php if ($value['delete'] == '' || $value['delete'] == '0') { ?>
          <input type="checkbox" id="delete">
         <?php }else{ ?>
          <input type="checkbox" id="delete" checked disabled>
         <?php } ?>
        </td>
        <td class="text-center">
         <?php if ($value['status'] == '' || $value['status'] == '0') { ?>
          <input type="checkbox" id="status">
         <?php }else{ ?>
          <input type="checkbox" id="status" checked disabled>
         <?php } ?>
        </td>
       </tr>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>