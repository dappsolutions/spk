<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="row">
  <div class="col-md-12">

    <ul class="timeline">

      <!-- timeline time label -->
      <li class="time-label">
        <span class="bg-yellow">
          Form Submission
        </span>
      </li>
      <!-- /.timeline-label -->

      <!-- timeline item -->
      <li>
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">Form Hak Akses</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_input'); ?>
              </div>
            </div>
          </div>

          <div class="timeline-footer">
            <hr>
            <div class="row">
              <div class="col-md-12 text-right">
                <button class="btn btn-warning" onclick="HakAkses.back(this)">Kembali</button>
                <button class="btn btn-default" onclick="HakAkses.simpan(this)">Submit</button>
              </div>
            </div>
          </div>
        </div>
      </li>


      <li>
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">Form Menu Aktif</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_edit_hak_akses_module'); ?>
              </div>
            </div>
          </div>

          <div class="timeline-footer">
            <hr>
            <div class="row">
              <div class="col-md-12 text-right">
                <button class="btn btn-warning" onclick="HakAkses.back(this)">Kembali</button>
                <button class="btn btn-default" onclick="HakAkses.simpan(this)">Submit</button>
              </div>
            </div>
          </div>
        </div>
      </li>
      <!-- END timeline item -->
    </ul>
  </div>
</div>