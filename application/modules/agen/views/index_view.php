<?php if (isset($menu_akses->agen)) { ?>
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-default">
    <div class="panel-heading">
     <h5>Daftar Agen</h5>
    </div>
    <div class="panel-body">
     <div class="row">
      <div class="col-md-12">
       <div class="input-group">
        <input type="text" class="form-control" onkeyup="Agen.search(this, event)" id="keyword" placeholder="Pencarian">
        <span class="input-group-addon"><i class="fa fa-search"></i></span>
       </div>
      </div>
     </div>
     <br />
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>

     <br />
     <div class="row">
      <div class="col-md-12">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-list-draft">
         <thead>
          <tr class="bg-info">
           <th>No</th>
           <th>Nama Agen</th>
           <th>Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = $pagination['last_no'] + 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td><?php echo $no++ ?></td>
             <td><?php echo $value['nama_agen'] ?></td>
             <td class="text-center">
              <?php if ($menu_akses->agen->delete) { ?>
               <i class="fa fa-trash grey-text hover" onclick="Agen.delete('<?php echo $value['id'] ?>')"></i>
              <?php } ?>
              <?php if ($menu_akses->agen->update) { ?>
               &nbsp;
               <i class="fa fa-pencil grey-text  hover" onclick="Agen.ubah('<?php echo $value['id'] ?>')"></i>
              <?php } ?>
              &nbsp;
              <i class="fa fa-file-text grey-text  hover" onclick="Agen.detail('<?php echo $value['id'] ?>')"></i>
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
           </tr>
          <?php } ?>

         </tbody>
        </table>
       </div>
      </div>
     </div>
    </div>

    <div class="panel-footer">
     <ul class="pagination pagination-sm no-margin pull-right">
      <?php echo $pagination['links'] ?>
     </ul>
    </div>
   </div>
  </div>
 </div>

 <?php if ($menu_akses->agen->create) { ?>
  <?php //if (count($content) == 0) { ?>
   <div class="row">
    <div class="col-md-12">
     <a href="#" class="float" onclick="Agen.add()">
      <i class="fa fa-plus my-float fa-lg"></i>
     </a>
    </div>
   </div>
  <?php //} ?>
 <?php } ?>
<?php } else { ?>
 <div class="row">
  <div class="col-md-12">
   <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
    Menu Tidak Tersedia
   </div>
  </div>
 </div>
<?php } ?>