<?php

class Login extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('hak_akses/m_hak_akses', 'akses');
	}

	public function index()
	{
		$result = array();

		echo $this->load->view('index', $result, true);
	}

	public function getDataAdmin($username, $password)
	{
		// $password = md5($password);
		$password = $password;
		//  echo $password;die;
		$data = Modules::run('database/get', array(
			'table' => 'user u',
			'field' => array('u.*', 'p.hak_akses', 'pg.upt'),
			'join' => array(
				array('priveledge p', 'u.priveledge = p.id'),
				array('pegawai pg', 'u.pegawai = pg.id', 'left'),
			),
			'where' => "u.username = '" . $username . "' and u.password = '" . $password . "' and (u.deleted = 0 or u.deleted is null)"
		));


		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
			$menuAkses = $this->akses->getListAktifMenuModule($result['priveledge']);
			$resultMenu = [];
			foreach ($menuAkses as $key => $value) {
				$resultMenu[$value['nama_module']] = $value;
			}
			$result['list_akses'] = json_encode($resultMenu);
		}

		return $result;
	}

	public function getDataPegawai($username, $password)
	{
		$data = Modules::run('database/get', array(
			'table' => 'user u',
			'field' => array('u.*', 'pv.priveledge as hak_akses'),
			'join' => array(
				array('priveledge pv', 'u.priveledge = pv.id')
			),
			'where' => array(
				'u.username' => $username,
				'u.password' => $password,
				'u.is_active' => true
			)
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDataUser($username, $password)
	{
		$data = $this->getDataAdmin($username, $password);
		$result = array();
		if (!empty($data)) {
			$result = $data;
		}
		//  else{
		//   $data = $this->getDataPegawai($username, $password);
		//   if(!empty($data)){
		//    $result = $data;
		//   }   
		//  }

		return $result;
	}

	public function sign_in()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$is_valid = false;
		try {
			$data = $this->getDataUser($username, $password);
			if (!empty($data)) {
				$is_valid = true;
			}
		} catch (Exception $exc) {
			$is_valid = false;
		}

		$hak_akses = '';
		if ($is_valid) {
			$this->setSessionData($data);
			$hak_akses = $data['hak_akses'];
		}
		echo json_encode(array('is_valid' => $is_valid, 'hak_akses' => $hak_akses));
	}

	public function setSessionData($data)
	{
		$session['user_id'] = $data['id'];
		$session['username'] = $data['username'];
		$session['hak_akses'] = $data['hak_akses'];
		$session['upt'] = $data['upt'];
		$session['list_akses'] = $data['list_akses'];
		$this->session->set_userdata($session);
	}

	public function sign_out()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}
}
