<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>Login - iMuamalah</title>
 <meta name="description" content="Sufee Admin - HTML5 Admin Template">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <link rel="icon" type="image/png" href="<?php echo base_url().'assets/images/architecture.png' ?>" />

 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/normalize.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/themify-icons.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/flag-icon.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/lib/mdi/css/materialdesignicons.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/cs-skin-elastic.css">
 <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/scss/style.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/wjc.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/css-loader.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">

 <!--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>-->

    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body class="bg-content">


 <div class="sufee-login d-flex align-content-center flex-wrap">
  <div class="loader">

  </div>
  <div class="container">
   <form>
    <div class="login-content">
     <div class="login-box">
      <div class="login-head text-center">
       <h3 class="login-title">
								<img src="<?php echo base_url().'assets/images/logo/I-Muamalah-logo.png' ?>"/>
							</h3>
      </div>
      <div class="login-body">
       <div class="text-center margin-bottom-1 margin-top-1">
        <img width="100" height="100" class="align-content" src="<?php echo base_url() ?>assets/images/pharmacist.png" alt="Logo Login">
       </div>
       <div class="form-group">
        <label>Username</label>
        <div class="form-inside-icon icon-pos-left">
         <input type="text" id="username" error="Username" class="form-control required" placeholder="Username">
         <div class="form-icon">
          <i class="mdi mdi-account mdi-dark mdi-inactive"></i>
         </div>
        </div>
       </div>
       <div class="form-group">
        <label>Password</label>
        <div class="form-inside-icon icon-pos-left">
         <input id="password" type="password" error="Password" class="form-control required" placeholder="Password">
         <div class="form-icon">
          <i class="mdi mdi-key mdi-dark mdi-inactive"></i>
         </div>
        </div>
       </div>
      </div>
      <div class="login-foot text-right">
       <button type="submit" class="btn btn-proses-baru" onclick="Login.sign_in(this, event)">
        Masuk <i class="mdi mdi-login-variant fa-lg"></i>
       </button>
      </div>
     </div>
    </div>
   </form>
  </div>
 </div>


 <script src="<?php echo base_url() ?>assets/js/jquery.js"></script>
 <script src="<?php echo base_url() ?>assets/js/vendor/jquery-2.1.4.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/plugins.js"></script>
 <script src="<?php echo base_url() ?>assets/js/main.js"></script>
 <script src="<?php echo base_url() ?>assets/js/message.js"></script>
 <script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
 <script src="<?php echo base_url() ?>assets/js/url.js"></script>
 <script src="<?php echo base_url() ?>assets/js/controllers/login.js"></script>


</body>
</html>
