<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Pegawai</label>
   <select name="" id="pegawai" class="required form-control" error="Pegawai">
    <option value="">-- PILIH PEGAWAI -- </option>
    <?php foreach ($list_pegawai as $key => $value) {?>
     <option value="<?php echo $value['id'] ?>" <?php echo isset($pegawai) ? $pegawai == $value['id'] ? 'selected' : '' : '' ?>><?php echo $value['nama_pegawai'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Hak Akses</label>
   <select name="" id="akses" class="required form-control" error="Hak Akses">
    <option value="">-- PILIH AKSES -- </option>
    <?php foreach ($list_hak_akses as $key => $value) {?>
     <option value="<?php echo $value['id'] ?>" <?php echo isset($priveledge) ? $priveledge == $value['id'] ? 'selected' : '' : '' ?>><?php echo $value['hak_akses'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Username</label>
   <input type="text" id="username" class="form-control required" error="Username" value="<?php echo isset($username) ? $username : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Password</label>
   <input type="text" id="password" class="form-control required" error="Password" value="<?php echo isset($password) ? $password : '' ?>">
  </div>
 </div>
</div>
