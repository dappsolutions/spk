<?php

class User extends MX_Controller
{

    public $segment;
    public $limit;
    public $page;
    public $last_no;

    public function __construct()
    {
        parent::__construct();
        $this->limit = 10;
    }

    public function getModuleName()
    {
        return 'user';
    }

    public function getHeaderJSandCSS()
    {
        //versioning
        $version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
        $version = substr($version, 0, 11);
        //versioning

        $data = array(
            '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
            '<script src="' . base_url() . 'assets/js/controllers/user.js?v=' . $version . '"></script>'
        );

        return $data;
    }

    public function getTableName()
    {
        return 'user';
    }

    public function index()
    {
        $this->segment = 3;
        $this->page = $this->uri->segment($this->segment) ?
            $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;

        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data User";
        $data['title_content'] = 'Data User';
        $content = $this->getDataUser();
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);


        echo Modules::run('template', $data);
    }

    public function getTotalDataUser($keyword = '')
    {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('u.username', $keyword),
                array('u.password', $keyword),
                array('p.hak_akses', $keyword),
                array('pg.nama_pegawai', $keyword)
            );
        }
        $total = Modules::run('database/count_all', array(
            'table' => $this->getTableName() . ' u',
            'field' => array('u.*', 'pg.nama_pegawai', 'p.hak_akses'),
            'join' => array(
                array('priveledge p', 'u.priveledge = p.id'),
                array('pegawai pg', 'u.pegawai = pg.id')
            ),
            'like' => $like,
            'is_or_like' => true,
            'where' => 'u.deleted= 0'
        ));

        return $total;
    }

    public function getDataUser($keyword = '')
    {
        $like = array();
        if ($keyword != '') {
            $like = array(
                array('u.username', $keyword),
                array('u.password', $keyword),
                array('p.hak_akses', $keyword),
                array('pg.nama_pegawai', $keyword)
            );
        }
        $data = Modules::run('database/get', array(
            'table' => $this->getTableName() . ' u',
            'field' => array('u.*', 'pg.nama_pegawai', 'p.hak_akses'),
            'join' => array(
                array('priveledge p', 'u.priveledge = p.id'),
                array('pegawai pg', 'u.pegawai = pg.id')
            ),
            'like' => $like,
            'is_or_like' => true,
            'limit' => $this->limit,
            'offset' => $this->last_no,
            'where' => 'u.deleted= 0'
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }

        return array(
            'data' => $result,
            'total_rows' => $this->getTotalDataUser($keyword)
        );
    }

    public function getDetailDataUser($id)
    {
        $data = Modules::run('database/get', array(
            'table' => $this->getTableName() . ' u',
            'field' => array(
                'u.*', 'p.hak_akses',
                'pg.nama_pegawai'
            ),
            'join' => array(
                array('priveledge p', 'u.priveledge = p.id'),
                array('pegawai pg', 'u.pegawai = pg.id')
            ),
            'where' => "u.id = '" . $id . "'"
        ));

        return $data->row_array();
    }

    public function getListPegawai($withFilter = true)
    {
        $data = Modules::run('database/get', array(
            'table' => 'pegawai p',
            'field' => array('p.*', 'u.priveledge as hak_akses'),
            'join' => array(
                array('user u', 'p.id = u.pegawai', 'left')
            ),
            'where' => 'p.deleted = 0'
        ));

        // echo '<pre>';
        // print_r($data->result_array());
        // die;
        $result = array();
        if (!empty($data)) {
            if ($withFilter) {
                foreach ($data->result_array() as $value) {
                    if ($value['hak_akses'] == '') {
                        array_push($result, $value);
                    }
                }
            } else {
                foreach ($data->result_array() as $value) {
                    array_push($result, $value);
                }
            }
        }


        return $result;
    }

    public function getListHakAkses()
    {
        $data = Modules::run('database/get', array(
            'table' => 'priveledge'
        ));

        $result = array();
        if (!empty($data)) {
            foreach ($data->result_array() as $value) {
                array_push($result, $value);
            }
        }


        return $result;
    }

    public function add()
    {
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Tambah User";
        $data['title_content'] = 'Tambah User';
        $data['list_pegawai'] = $this->getListPegawai();
        $data['list_hak_akses'] = $this->getListHakAkses();
        echo Modules::run('template', $data);
    }

    public function ubah($id)
    {
        $data = $this->getDetailDataUser($id);
        $data['view_file'] = 'form_add_edit_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Ubah User";
        $data['title_content'] = 'Ubah User';
        $data['list_pegawai'] = $this->getListPegawai(false);
        // echo '<pre>';
        // print_r($data);
        // die;
        $data['list_hak_akses'] = $this->getListHakAkses();
        echo Modules::run('template', $data);
    }

    public function detail($id)
    {
        $data = $this->getDetailDataUser($id);
        $data['view_file'] = 'detail_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Detail User";
        $data['title_content'] = 'Detail User';
        echo Modules::run('template', $data);
    }

    public function getPostDataHeader($value)
    {
        $data['username'] = $value->username;
        $data['password'] = $value->password;
        $data['priveledge'] = $value->priveledge;
        $data['pegawai'] = $value->pegawai;
        return $data;
    }

    public function simpan()
    {
        $data = json_decode($this->input->post('data'));
        $id = $data->id;
        // echo '<pre>';
        // print_r($id);die;
        $is_valid = false;
        $this->db->trans_begin();
        try {
            $post_user = $this->getPostDataHeader($data);
            if ($id == '') {
                $id = Modules::run('database/_insert', $this->getTableName(), $post_user);
            } else {
                //update
                Modules::run('database/_update', $this->getTableName(), $post_user, array('id' => $id));
            }
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
    }

    public function search($keyword)
    {
        $this->segment = 4;
        $this->page = $this->uri->segment($this->segment) ?
            $this->uri->segment($this->segment) - 1 : 0;
        $this->last_no = $this->page * $this->limit;
        $keyword = urldecode($keyword);

        $data['keyword'] = $keyword;
        $data['view_file'] = 'index_view';
        $data['header_data'] = $this->getHeaderJSandCSS();
        $data['module'] = $this->getModuleName();
        $data['title'] = "Data User";
        $data['title_content'] = 'Data User';
        $content = $this->getDataUser($keyword);
        $data['content'] = $content['data'];
        $total_rows = $content['total_rows'];
        $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
        echo Modules::run('template', $data);
    }

    public function setUserAktifNonAktif($id, $is_active)
    {
        $is_valid = false;
        $this->db->trans_begin();
        try {
            Modules::run('database/_update', 'user', array('is_active' => $is_active), array('id' => $id));
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid));
    }

    public function delete($id)
    {
        $is_valid = false;
        $this->db->trans_begin();
        try {
            Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
            $this->db->trans_commit();
            $is_valid = true;
        } catch (Exception $ex) {
            $this->db->trans_rollback();
        }

        echo json_encode(array('is_valid' => $is_valid));
    }
}
