<?php

class Jadwal extends MX_Controller
{

  public $segment;
  public $limit;
  public $page;
  public $last_no;
  public $menu_akses;

  public function __construct()
  {
    parent::__construct();
    $this->limit = 10;
    $this->load->model('m_jadwal', 'jadwal');
    $this->menu_akses = json_decode($this->session->userdata('list_akses'));
  }

  public function getModuleName()
  {
    return 'jadwal';
  }

  public function getHeaderJSandCSS()
  {
    //versioning
    $version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
    $version = substr($version, 0, 11);
    //versioning

    $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/jadwal.js?v=' . $version . '"></script>'
    );

    return $data;
  }

  public function getTableName()
  {
    return 'schedule';
  }

  public function index()
  {
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Tambah Jadwal";
    $data['title_content'] = 'Tambah Jadwal';
    $data['menu_akses'] = $this->menu_akses;
    $data['data_spbjl'] = $this->jadwal->getListDataJadwal('SUB_SPBJL');
    $data['data_spk'] = $this->jadwal->getListDataJadwal('SUB_SPK');
    echo Modules::run('template', $data);
  }

  public function simpanSpbjl()
  {
    $data = $_POST;
    $result['is_valid'] = false;
    $dataTypeTrans = $this->jadwal->getDetailTypeTrans('SUB_SPBJL');

    $upt_id = $this->session->userdata('upt');
    $user_id = $this->session->userdata('user_id');

    $this->db->trans_begin();
    try {

      $date = date('Y-m-d H:i:s');

      $push = array();
      $push['user'] = $user_id;
      $push['createddate'] = $date;
      $this->db->insert('actor', $push);
      $actor_id = $this->db->insert_id();

      $idLastUraian = "";
      foreach ($data['dataInput'] as $key => $value) {
        $push = array();
        $push['type_transaction '] = $dataTypeTrans['id'];
        $push['uraian_pekerjaan '] = $value['uraian_pekerjaan'];
        $push['no_dokumen'] = $value['no_dokumen'];
        $push['standard'] = $value['standard'];
        $push['cepat'] = $value['cepat'];
        $push['manual'] = $value['manual'];
        $push['upt'] = $upt_id;
        $push['as_tanggal_pekerjaan'] = 0;
        if ($value['id'] == '') {
          $push['createddate'] = $date;
          $push['createdby'] = $actor_id;
          $this->db->insert($this->getTableName(), $push);
          $idLastUraian = $this->db->insert_id();
        } else {
          if($value['remove'] == '1'){
            $push['deleted'] = 1;
          }
          $push['updateddate'] = $date;
          $push['updatedby'] = $actor_id;
          $this->db->update($this->getTableName(), $push, array('id' => $value['id']));
        }

        if($value['remove'] != '1'){
          if($value['id'] != ''){
            $idLastUraian = $value['id'];
          }
        }
        // $push['createdby'] = $date;  
      }

      if($idLastUraian != ''){
        $push = array();
        $push['as_tanggal_pekerjaan'] = 1;
        $this->db->update($this->getTableName(), $push, array('id' => $idLastUraian));
      }
      

      $this->db->trans_commit();

      $result['is_valid'] = true;
    } catch (Exception $ex) {
      $result['message'] = $ex->getMessage();
      $this->db->trans_rollback();
    }

    echo json_encode($result);
  }
  
  public function simpanSpk()
  {
    $data = $_POST;
    $result['is_valid'] = false;
    $dataTypeTrans = $this->jadwal->getDetailTypeTrans('SUB_SPK');

    // echo '<pre>';
    // print_r($data);die;

    $upt_id = $this->session->userdata('upt');
    $user_id = $this->session->userdata('user_id');

    $this->db->trans_begin();
    try {

      $date = date('Y-m-d H:i:s');

      $push = array();
      $push['user'] = $user_id;
      $push['createddate'] = $date;
      $this->db->insert('actor', $push);
      $actor_id = $this->db->insert_id();

      $idLastUraian = "";
      foreach ($data['dataInput'] as $key => $value) {
        $push = array();
        $push['type_transaction '] = $dataTypeTrans['id'];
        $push['uraian_pekerjaan '] = $value['uraian_pekerjaan'];
        $push['no_dokumen'] = $value['no_dokumen'];
        $push['standard'] = $value['standard'];
        $push['cepat'] = $value['cepat'];
        $push['manual'] = $value['manual'];
        $push['upt'] = $upt_id;
        $push['as_tanggal_pekerjaan'] = 0;
        if ($value['id'] == '') {
          $push['createddate'] = $date;
          $push['createdby'] = $actor_id;
          $this->db->insert($this->getTableName(), $push);
          $idLastUraian = $this->db->insert_id();
        } else {
          if($value['remove'] == '1'){
            $push['deleted'] = 1;
          }
          $push['updateddate'] = $date;
          $push['updatedby'] = $actor_id;
          $this->db->update($this->getTableName(), $push, array('id' => $value['id']));
        }

        if($value['remove'] != '1'){
          if($value['id'] != ''){
            $idLastUraian = $value['id'];
          }
        }
        // $push['createdby'] = $date;  
      }

      if($idLastUraian != ''){
        $push = array();
        $push['as_tanggal_pekerjaan'] = 1;
        $this->db->update($this->getTableName(), $push, array('id' => $idLastUraian));
      }
      

      $this->db->trans_commit();

      $result['is_valid'] = true;
    } catch (Exception $ex) {
      $result['message'] = $ex->getMessage();
      $this->db->trans_rollback();
    }

    echo json_encode($result);
  }


  public function delete($id)
  {
    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
      $this->db->trans_commit();
      $is_valid = true;
    } catch (Exception $ex) {
      $this->db->trans_rollback();
    }

    echo json_encode(array('is_valid' => $is_valid));
  }
}
