<?php

class M_jadwal extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}


	public function getDetailTypeTrans($id= "")
	{
		$sql = "select * from type_transaction where term_id = '".$id."'";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$data = $data->row_array();
			$result = $data;
		}

		return $result;
	}
	
	public function getListDataJadwal($id= "")
	{
		$hak_akses = $this->session->userdata('hak_akses');
		$upt = $this->session->userdata('upt');
		$filterUpt = "";
		if($hak_akses == 'superadmin'){
			$filterUpt = " and ut.id = '".$upt."'";
		}

		$sql = "
		SELECT 
		s.*
		, ut.nama_upt
		FROM schedule s
		join type_transaction tt 
			on tt.id = s.type_transaction 
		join upt ut
			on ut.id = s.upt
		where tt.term_id = '".$id."'
		and s.deleted = 0
		".$filterUpt."
		order by s.id asc";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$result[] = $value;
			}
		}

		return $result;
	}
}
