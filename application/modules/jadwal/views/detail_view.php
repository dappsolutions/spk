<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama
     </div>
     <div class='col-md-3'>
      <?php echo $nama_vendor ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      No HP
     </div>
     <div class='col-md-3'>
      <?php echo $no_hp ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Email
     </div>
     <div class='col-md-3'>
      <?php echo $email ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Status
     </div>
     <div class='col-md-3'>
      <?php
      if ($status) {
       echo 'Aktif';
      } else {
       echo 'Tidak Aktif';
      }
      ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Alamat
     </div>
     <div class='col-md-3'>
      <?php echo $alamat ?>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Vendor.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
