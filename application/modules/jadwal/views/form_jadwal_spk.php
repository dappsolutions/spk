<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-spk">
    <thead>
     <tr class="bg-info">
      <th>No</th>
      <th>Uraian Kegiatan</th>
      <th>No Dokumen</th>
      <th>Standard</th>
      <th>Cepat</th>
      <th class="hide">Manual</th>
      <th>Unit</th>
      <th class="text-center hide">Atur Sbg. Tgl. Pekerjaan</th>
      <th></th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data_spk)) { ?>
      <?php $no = 1 ?>
      <?php foreach ($data_spk as $key => $value) { ?>
       <tr class="input" data_id="<?php echo $value['id'] ?>">
        <td><?php echo $no++ ?></td>
        <td>
         <input type="text" <?php echo $menu_akses->jadwal->update ? '' : 'disabled' ?> class="form-control required" id="uraian-pekerjaan" error="Uraian Pekerjaan" value="<?php echo $value['uraian_pekerjaan'] ?>" />
        </td>
        <td>
         <input type="text" <?php echo $menu_akses->jadwal->update ? '' : 'disabled' ?> class="form-control required" id="no-dokumen" error="No Dokumen" value="<?php echo $value['no_dokumen'] ?>" />
        </td>
        <td>
         <input type="text" <?php echo $menu_akses->jadwal->update ? '' : 'disabled' ?> class="form-control" id="standard" value="<?php echo $value['standard'] == '0' ? '' : $value['standard'] ?>" />
        </td>
        <td>
         <input type="text" <?php echo $menu_akses->jadwal->update ? '' : 'disabled' ?> class="form-control" id="cepat" value="<?php echo $value['cepat'] == '0' ? '' : $value['cepat'] ?>" />
        </td>
        <td class="hide">
         <input type="text" <?php echo $menu_akses->jadwal->update ? '' : 'disabled' ?> class="form-control" id="manual" value="<?php echo $value['manual'] == '0' ? '' : $value['manual'] ?>" />
        </td>
        <td>
         <?php echo $value['nama_upt'] ?>
        </td>
        <td class="text-center hide">
         <input type="radio" name="as_tanggal_pekerjaan" id="as_tanggal_pekerjaan" <?php echo $value['as_tanggal_pekerjaan'] == '0' ? '' : 'checked' ?> />
        </td>
        <td class="text-center">
         <?php if ($menu_akses->jadwal->delete) { ?>
          <i class="fa fa-trash" onclick="Jadwal.removeItem(this)"></i>
         <?php } ?>
        </td>
       </tr>
      <?php } ?>
     <?php } ?>
     <tr>
      <td colspan="6">
       <?php if ($menu_akses->jadwal->update) { ?>
        <a href="" onclick="Jadwal.addItemSpbjl(this, event)" jenis="spbjl">Add Item</a>
       <?php } ?>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>