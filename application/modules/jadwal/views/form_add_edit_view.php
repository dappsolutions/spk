<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<?php if (isset($menu_akses->jadwal)) { ?>
  <div class="row">
    <div class="col-md-12">

      <ul class="timeline">

        <!-- timeline time label -->
        <li class="time-label">
          <span class="bg-yellow">
            Master Jadwal SPBJL
          </span>
        </li>
        <!-- /.timeline-label -->

        <!-- timeline item -->
        <li>
          <!-- timeline icon -->
          <i class="fa fa-sticky-note bg-blue"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
            <h3 class="timeline-header"><a href="#">Form Jadwal SPBJL</a> </h3>
            <div class="timeline-body">
              <div class="row">
                <div class="col-md-12">
                  <?php echo $this->load->view('form_jadwal_spbjl'); ?>
                </div>
              </div>
            </div>

            <?php if ($menu_akses->jadwal->create) { ?>
              <div class="timeline-footer">
                <hr>
                <div class="row">
                  <div class="col-md-12 text-right">
                    <button class="btn btn-default" onclick="Jadwal.simpanSpbjl(this)">Submit</button>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </li>

        <!-- END timeline item -->
        
        <!-- timeline time label -->
        <li class="time-label">
          <span class="bg-yellow">
            Master Jadwal SPK
          </span>
        </li>
        <!-- /.timeline-label -->

        <!-- timeline item -->
        <li>
          <!-- timeline icon -->
          <i class="fa fa-sticky-note bg-blue"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
            <h3 class="timeline-header"><a href="#">Form Jadwal SPK</a> </h3>
            <div class="timeline-body">
              <div class="row">
                <div class="col-md-12">
                  <?php echo $this->load->view('form_jadwal_spk'); ?>
                </div>
              </div>
            </div>

            <?php if ($menu_akses->jadwal->create) { ?>
              <div class="timeline-footer">
                <hr>
                <div class="row">
                  <div class="col-md-12 text-right">
                    <button class="btn btn-default" onclick="Jadwal.simpanSpk(this)">Submit</button>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>
        </li>

        <!-- END timeline item -->
      </ul>
    </div>
  </div>
<?php } else { ?>
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
        Menu Tidak Tersedia
      </div>
    </div>
  </div>
<?php } ?>