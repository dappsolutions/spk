<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Kode Produk</label>
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <?php echo $kode_produk ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Produk</label>
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nama_produk ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-12">
  <div class="panel panel-default">
   <div class="panel-heading">
    <h5>Satuan Produk</h5>
   </div>
   <div class="panel-body">
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table table-bordered" id="table-satuan">
        <thead>
         <tr class="bg-info">
          <th>Harga Rp</th>
          <th>Satuan</th>
         </tr>
        </thead>
        <tbody>
         <?php foreach ($list_satuan as $key => $value) { ?>
          <tr data_id="<?php echo $value['id'] ?>">
           <td>
            <?php echo number_format($value['harga_jual']) ?>
           </td>
           <td>
            <?php echo $value['nama_satuan'] ?>
           </td>
          </tr>
         <?php } ?>
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>