<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Kode Produk</label>
   <input type="text" id="kode_produk" class="form-control required" error="Kode Produk" value="<?php echo isset($kode_produk) ? $kode_produk : '' ?>">
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Produk</label>
   <input type="text" id="nama_produk" class="form-control required" error="Nama Produk" value="<?php echo isset($nama_produk) ? $nama_produk : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-12">
  <div class="panel panel-default">
   <div class="panel-heading">
    <h5>Satuan Produk</h5>
   </div>
   <div class="panel-body">
    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table table-bordered" id="table-satuan">
        <thead>
         <tr class="bg-info">
          <th>Harga</th>
          <th>Satuan</th>
          <th></th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($data_list_satuan)) { ?>
          <?php $no = 1 ?>
          <?php foreach ($data_list_satuan as $key => $value) { ?>
           <tr no="<?php echo $no ?>" data_id="<?php echo $value['id'] ?>">
            <td>
             <input type="text" id="harga_produk" class="form-control required" error="Harga Produk" value="<?php echo $value['harga_jual'] ?>">
            </td>
            <td>
             <select name="" id="satuan-<?php echo $no ?>" class="form-control satuan required" error="Satuan">
              <option value="">--PILIH--</option>
              <?php foreach ($list_satuan as $key => $v_sat) { ?>
               <option <?php echo $v_sat['id'] == $value['satuan'] ? 'selected' : '' ?> value="<?php echo $v_sat['id'] ?>"><?php echo $v_sat['nama_satuan'] ?></option>
              <?php } ?>
             </select>
            </td>
            <td class="text-center">
             <i class="fa fa-trash" onclick="Produk.removeSatuan(this)"></i>
            </td>
           </tr>
           <?php $no += 1 ?>
          <?php } ?>
         <?php } ?>

         <tr no="<?php echo count($data_list_satuan) + 1 ?>" data_id="">
          <td>
           <input type="text" id="harga_produk" class="form-control <?php echo count($data_list_satuan) > 0 ? '' : 'required' ?>" error="Harga Produk" value="<?php echo isset($harga_produk) ? $harga_produk : '' ?>">
          </td>
          <td>
           <select name="" id="satuan-<?php echo count($data_list_satuan) + 1 ?>" class="form-control satuan <?php echo count($data_list_satuan) > 0 ? '' : 'required' ?>" error="Satuan">
            <option value="">--PILIH--</option>
            <?php foreach ($list_satuan as $key => $value) { ?>
             <option value="<?php echo $value['id'] ?>"><?php echo $value['nama_satuan'] ?></option>
            <?php } ?>
           </select>
          </td>
          <td class="text-center">
           <i class="fa fa-plus" onclick="Produk.addSatuan(this)"></i>
          </td>
         </tr>
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>