<input type="hidden" id="sertifikat_lain_id" value="<?php echo isset($sertifikat_lain_id) ? $sertifikat_lain_id : '' ?>">

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">NIB/TDP</label>
   <br>
   <?php echo isset($nib) ? $nib : '' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_nib) ? $file_nib != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_nib">
   <label for="">Upload File NIB/TDP</label>
   <br>
   <label for="">-</label>
  </div>
 </div>


 <div class="col-md-4 <?php echo isset($file_nib) ? $file_nib != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_nib">
   <label for="">File NIB/TDP</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_nib" class="form-control" value="<?php echo isset($file_nib) ? $file_nib : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_nib) ? $file_nib : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>


 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired Sertifikat NIB</label>
   <br>
   <?php echo isset($expired_sertifikat_nib) ? $expired_sertifikat_nib == '0000-00-00' ? '' : $expired_sertifikat_nib : '' ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">NPWP</label>
   <br>
   <?php echo isset($npwp) ? $npwp : '-' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_npwp) ? $file_npwp != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_npwp">
   <label for="">Upload File NPWP</label>
   <br>
   -
  </div>
 </div>


 <div class="col-md-4 <?php echo isset($file_npwp) ? $file_npwp != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_npwp">
   <label for="">File NPWP</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_npwp" class="form-control" value="<?php echo isset($file_npwp) ? $file_npwp : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_npwp) ? $file_npwp : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>


 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired Sertifikat NPWP</label>
   <br>
   <?php echo isset($expired_sertifikat_npwp) ? $expired_sertifikat_npwp == '0000-00-00' ? '-' : $expired_sertifikat_npwp : '-' ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">PKP</label>
   <br>
   <?php echo isset($pkp) ? $pkp : '-' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_pkp) ? $file_pkp != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_pkp">
   <label for="">Upload File PKP</label>
   <br>
   -
  </div>
 </div>


 <div class="col-md-4 <?php echo isset($file_pkp) ? $file_pkp != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_pkp">
   <label for="">File PKP</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_pkp" class="form-control" value="<?php echo isset($file_pkp) ? $file_pkp : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_pkp) ? $file_pkp : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>


 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired Sertifikat PKP</label>
   <br>
   <?php echo isset($expired_sertifikat_pkp) ? $expired_sertifikat_pkp == '0000-00-00' ? '-' : $expired_sertifikat_pkp : '-' ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">Susunan Pengurus</label>
   <br>
   <?php echo isset($susanan_pengurus) ? $susanan_pengurus : '' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_susunan) ? $file_susunan != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_susunan">
   <label for="">Upload File Susunan</label>
   <br>
   -
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_susunan) ? $file_susunan != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_susunan">
   <label for="">File Susunan</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_susunan" class="form-control" value="<?php echo isset($file_susunan) ? $file_susunan : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_susunan) ? $file_susunan : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>

 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired Susunan Pengurus</label>
   <br>
   <?php echo isset($expired_sertifikat_susunan) ? $expired_sertifikat_susunan == '0000-00-00' ? '-' : $expired_sertifikat_susunan : '-' ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">Surat Keterangan Domisili</label>
   <br>
   <?php echo isset($surat_keterangan_domisili) ? $surat_keterangan_domisili : '-' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_domisili) ? $file_domisili != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_domisili">
   <label for="">Upload File Domisili</label>
   <br>
   -
  </div>
 </div>


 <div class="col-md-4 <?php echo isset($file_domisili) ? $file_domisili != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_domisili">
   <label for="">File Domisili</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_domisili" class="form-control" value="<?php echo isset($file_domisili) ? $file_domisili : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_domisili) ? $file_domisili : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>


 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired Domisili</label>
   <br>
   <?php echo isset($expired_sertifikat_domisili) ? $expired_sertifikat_domisili == '0000-00-00' ? '-' : $expired_sertifikat_domisili : '-' ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">Surat Keterangan Rekening</label>
   <br>
   <?php echo isset($surat_keterangan_rekening) ? $surat_keterangan_rekening : '-' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_rekening) ? $file_rekening != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_rekening">
   <label for="">Upload File Rekening</label>
   <br>
   -
  </div>
 </div>


 <div class="col-md-4 <?php echo isset($file_rekening) ? $file_rekening != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_rekening">
   <label for="">File Rekening</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_rekening" class="form-control" value="<?php echo isset($file_rekening) ? $file_rekening : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_rekening) ? $file_rekening : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>

 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired Rekening</label>
   <br>
   <?php echo isset($expired_sertifikat_rekening) ? $expired_sertifikat_rekening == '0000-00-00' ? '-' : $expired_sertifikat_rekening : '-' ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">Pakta Integritas</label>
   <br>
   <?php echo isset($pakta_integritas) ? $pakta_integritas : '-' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_integritas) ? $file_integritas != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_integritas">
   <label for="">Upload File Integritas</label>
   <br>
   -
  </div>
 </div>


 <div class="col-md-4 <?php echo isset($file_integritas) ? $file_integritas != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_integritas">
   <label for="">File Integritas</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_integritas" class="form-control" value="<?php echo isset($file_integritas) ? $file_integritas : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_integritas) ? $file_integritas : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>


 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired Pakta Integritas</label>
   <br>
   <?php echo isset($expired_sertifikat_pakta) ? $expired_sertifikat_pakta == '0000-00-00' ? '-' : $expired_sertifikat_pakta : '-' ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-4">
  <div class="form-group">
   <label for="">AK3</label>
   <br>
   <?php echo isset($ak3) ? $ak3 : '-' ?>
  </div>
 </div>

 <div class="col-md-4 <?php echo isset($file_ak3) ? $file_ak3 != '' ? 'hide' : '' : ''  ?>">
  <div class="form-group" id="file_input_ak3">
   <label for="">Upload File AK3</label>
   <br>
   -
  </div>
 </div>


 <div class="col-md-4 <?php echo isset($file_ak3) ? $file_ak3 != '' ? '' : 'hide' : 'hide'  ?>">
  <div class="form-group " id="detail_file_ak3">
   <label for="">File AK3</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str_ak3" class="form-control" value="<?php echo isset($file_ak3) ? $file_ak3 : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_ak3) ? $file_ak3 : '' ?>" onclick="Supplier.showFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>

 <div class="col-md-4">
  <div class="form-group">
   <label for="">Expired AK3</label>
   <br>
   <?php echo isset($expired_sertifikat_ak3) ? $expired_sertifikat_ak3 == '0000-00-00' ? '-' : $expired_sertifikat_ak3 : '-' ?>
  </div>
 </div>
</div>