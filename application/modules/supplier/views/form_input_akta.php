<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-data-akta">
    <thead>
     <tr>
      <th>Akta</th>
      <th>Upload File</th>
      <th>Tanggal Terbit</th>
      <th></th>
     </tr>
    </thead>
    <tbody>
     <?php $counter_index = 0 ?>
     <?php if (isset($list_akta)) { ?>
      <?php if (!empty($list_akta)) { ?>
       <?php foreach ($list_akta as $key => $value) { ?>
        <tr class="input" data_id="<?php echo $value['id'] ?>">
         <td>
          <input type="text" id="akta" class="form-control" error="" value="<?php echo $value['akta'] ?>">
         </td>
         <td id="file-akta">
          <?php if ($value['file_akta'] != '') { ?>
           <div class="form-group " id="detail_file_akta">
            <div class="input-group">
             <input disabled="" type="text" id="file_str_akta" class="form-control" value="<?php echo isset($value['file_akta']) ? $value['file_akta'] : '' ?>">
             <span class="input-group-addon">
              <i class="fa fa-image hover-content" file="<?php echo isset($value['file_akta']) ? $value['file_akta'] : '' ?>" onclick="Supplier.showFileAkta(this, event)"></i>
             </span>
             <span class="input-group-addon">
              <i class="fa fa-close hover-content" onclick="Supplier.gantiFileAkta(this, event)"></i>
             </span>
            </div>
           </div>
          <?php } else { ?>
           <input type="file" id="file_akta" class="form-control" onchange="Supplier.checkFile(this)">
          <?php } ?>
         </td>
         <td id="expired_akta">
          <input type="text" readonly index="<?php echo $counter_index ?>" id="expired_date_akta<?php echo $counter_index ?>" class="form-control" value="<?php echo $value['expired_date_akta'] ?>">
         </td>
         <td class="text-center" id="action">
          <a href="" onclick="Supplier.removeAkta(this, event)"><i class="mdi mdi-plus mdi-18px"></i></a>
         </td>
        </tr>
        <?php $counter_index += 1 ?>
       <?php } ?>
      <?php } ?>
     <?php } ?>

     <tr class="input" data_id="">
      <td>
       <input type="text" id="akta" class="form-control" error="" value="">
      </td>
      <td id="file-akta">
       <input type="file" id="file_akta" class="form-control" onchange="Supplier.checkFile(this)">
      </td>
      <td id="expired_akta">
       <input type="text" readonly index="<?php echo $counter_index ?>" id="expired_date_akta<?php echo $counter_index  ?>" class="form-control">
      </td>
      <td class="text-center" id="action">
       <a href="" onclick="Supplier.addAkta(this, event)"><i class="mdi mdi-plus mdi-18px"></i></a>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>