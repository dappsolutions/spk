<?php if (isset($list_sertifikat)) { ?>
 <?php if (!empty($list_sertifikat)) { ?>
  <?php foreach ($list_sertifikat as $key => $value) { ?>
   <div data_id="<?php echo $value['id'] ?>" class="content-input-sertifikat-badan-usaha">
    <input type="hidden" id="sertifikat_id" value="<?php echo $value['id'] ?>">
    <div class="row">
     <div class="col-md-12">
      <div class="row">
       <div class="col-md-3 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? 'hide' : '' : ''  ?>">
        <div class="form-group" id="file_input_sertifikat">
         <label for="">Upload Sertifikat</label>
         <input type="file" id="file_sertifikat" class="form-control" onchange="Supplier.checkFile(this)">
        </div>
       </div>
       <div class="col-md-3 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? 'hide' : '' : ''  ?>">
        <div class="form-group" id="file_input_sertifikat">
         
        </div>
       </div>


       <div class="col-md-3 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? '' : 'hide' : 'hide'  ?>">
        <div class="form-group " id="detail_file_sertifikat">
         <label for="">File Sertifikat</label>
        </div>
       </div>

       <div class="col-md-3 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? '' : 'hide' : 'hide'  ?>">
        <div class="form-group " id="detail_file_sertifikat">
         <div class="input-group">
          <input disabled="" type="text" id="file_str_sertifikat" class="form-control" value="<?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] : '' ?>">
          <span class="input-group-addon">
           <i class="fa fa-image hover-content" file="<?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] : '' ?>" onclick="Supplier.showFile(this, event)"></i>
          </span>
         </div>
        </div>
       </div>


       <div class="col-md-3">
        <div class="form-group" id="content_expired_sertifikat">
         <label for="">Expired Sertifikat</label>
        </div>
       </div>
       <div class="col-md-3">
        <?php echo $value['expired_sertifikat'] ?>
       </div>
      </div>

      <div class="row">
       <div class="col-md-3">
        <div class="form-group">
         <label for="">Kriteria</label>
        </div>
       </div>

       <div class="col-md-3">
        <div class="form-group">
         <?php echo $value['nama_agen'] ?>
        </div>
       </div>

       <div class="col-md-3">
        <div class="form-group">
         <label for="">Bidang Usaha</label>
        </div>
       </div>
       <div class="col-md-3">
        <div class="form-group">
         <?php echo $value['bidang_usaha'] ?>
        </div>
       </div>
      </div>



      <div class="row">
       <div class="col-md-3">
        <div class="form-group">
         <label for="">Sub Bidang Usaha</label>
        </div>
       </div>
       <div class="col-md-3">
        <div class="form-group">
         <?php echo $value['sub_bidang_usaha'] ?>
        </div>
       </div>

       <div class="col-md-3">
        <div class="form-group">
         <label for="">Sub Kualifikasi</label>
        </div>
       </div>
       <div class="col-md-3">
        <div class="form-group">
         <?php echo $value['sub_kualifikasi'] ?>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
   <hr>

  <?php } ?>
 <?php } else { ?>
  <div data_id="" class="content-input-sertifikat-badan-usaha">
   <div class="row">
    <div class="col-md-12">
     <div class="row">
      <div class="col-md-6">
       <div class="form-group" id="file_input_sertifikat">
        <label for="">File Sertifikat</label>

       </div>
      </div>

      <div class="col-md-6">
       <div class="form-group" id="content_expired_sertifikat">
        <label for="">Expired Sertifikat</label>
       </div>
      </div>
     </div>

     <div class="row">
      <div class="col-md-6">
       <div class="form-group">
        <label for="">Kriteria</label>        
       </div>
      </div>

      <div class="col-md-6">
       <div class="form-group">
        <label for="">Bidang Usaha</label>

       </div>
      </div>
     </div>

     <div class="row">
      <div class="col-md-6">
       <div class="form-group">
        <label for="">Sub Bidang Usaha</label>

       </div>
      </div>

      <div class="col-md-6">
       <div class="form-group">
        <label for="">Sub Kualifikasi</label>

       </div>
      </div>
     </div>
    </div>
   </div>
  </div>
 <?php } ?>
<?php }
