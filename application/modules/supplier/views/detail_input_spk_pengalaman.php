<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-data-spk-pengalaman">
    <thead>
     <tr>
      <th>SPK Pengalaman Kerja</th>
      <th>Upload File</th>
      <th>Expired Date</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_spk)) { ?>
      <?php if (!empty($list_spk)) { ?>
       <?php foreach ($list_spk as $key => $value) { ?>
        <tr class="input" data_id="<?php echo $value['id'] ?>">
         <td>
          <?php echo $value['spk_pengalaman'] ?>
         </td>
         <td id="file-spk-pengalaman">
          <?php if ($value['file_spk_pengalaman'] != '') { ?>
           <div class="form-group " id="detail_file_spk">
            <div class="input-group">
             <input disabled="" type="text" id="file_str_spk" class="form-control" value="<?php echo isset($value['file_spk_pengalaman']) ? $value['file_spk_pengalaman'] : '' ?>">
             <span class="input-group-addon">
              <i class="fa fa-image hover-content" file="<?php echo isset($value['file_spk_pengalaman']) ? $value['file_spk_pengalaman'] : '' ?>" onclick="Supplier.showFileSpk(this, event)"></i>
             </span>
            </div>
           </div>
          <?php } else { ?>
           -
          <?php } ?>
         </td>
         <td>
          <?php echo $value['expired_date_spk'] ?>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>