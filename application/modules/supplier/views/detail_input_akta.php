<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-data-akta">
    <thead>
     <tr>
      <th>SPK Akta</th>
      <th>Upload File</th>
      <th>Tanggal Terbit</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_akta)) { ?>
      <?php if (!empty($list_akta)) { ?>
       <?php foreach ($list_akta as $key => $value) { ?>
        <tr class="input" data_id="<?php echo $value['id'] ?>">
         <td>
          <?php echo $value['akta'] ?>
         </td>
         <td id="file-akta">
          <?php if ($value['file_akta'] != '') { ?>
           <div class="form-group " id="detail_file_akta">
            <div class="input-group">
             <input disabled="" type="text" id="file_str_akta" class="form-control" value="<?php echo isset($value['file_akta']) ? $value['file_akta'] : '' ?>">
             <span class="input-group-addon">
              <i class="fa fa-image hover-content" file="<?php echo isset($value['file_akta']) ? $value['file_akta'] : '' ?>" onclick="Supplier.showFileAkta(this, event)"></i>
             </span>
            </div>
           </div>
          <?php } else { ?>
           -
          <?php } ?>
         </td>
         <td>
          <?php echo $value['expired_date_akta'] ?>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>