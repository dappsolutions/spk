<!--FORM EXPORT EXCEL-->
<table style="width: 100%;" id="table-excel-form-akta">
 <tr>
  <td colspan="3" class="td_jsa">Detail Akta</td>
 </tr>
 <tr>
  <td class="td_jsa">Spk Akta</td>
  <td class="td_jsa">File</td>
  <td class="td_jsa">Tanggal Terbit</td>
 </tr>
 <?php if (isset($list_akta)) { ?>
  <?php if (!empty($list_akta)) { ?>
   <?php foreach ($list_akta as $key => $value) { ?>
    <tr>
     <td class="td_jsa"><?php echo $value['akta'] ?></td>
     <td class="td_jsa"><?php echo $value['file_akta'] ?></td>
     <td class="td_jsa"><?php echo $value['expired_date_akta'] ?></td>
    </tr>
   <?php } ?>
  <?php } ?>
 <?php } ?>
</table>