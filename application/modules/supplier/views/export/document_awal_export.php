<!--FORM EXPORT EXCEL-->
<table style="width: 100%;" id="table-excel-form-awal">
<tr>
 <td colspan="3" class="td_jsa">Detail Supplier</td>
</tr>
 <tr>
  <td class="td_jsa">Nama Vendor</td>
  <td class="td_jsa">:</td>
  <td class="td_jsa"><?php echo $nama_vendor ?></td>
 </tr>
 <tr>
  <td class="td_jsa">Nama Pimpinan</td>
  <td class="td_jsa">:</td>
  <td class="td_jsa"><?php echo $nama_pimpinan ?></td>
 </tr>
 <tr>
  <td class="td_jsa">Jabatan Pimpinan</td>
  <td class="td_jsa">:</td>
  <td class="td_jsa"><?php echo $jabatan_pimpinan ?></td>
 </tr>
 <tr>
  <td class="td_jsa">Email</td>
  <td class="td_jsa">:</td>
  <td class="td_jsa"><?php echo $email ?></td>
 </tr>
 <tr>
  <td class="td_jsa">Nama Bank</td>
  <td class="td_jsa">:</td>
  <td class="td_jsa"><?php echo $nama_bank ?></td>
 </tr>
 <tr>
  <td class="td_jsa">No. Rekening</td>
  <td class="td_jsa">:</td>
  <td class="td_jsa"><?php echo $no_rekening ?></td>
 </tr>
 <tr>
  <td class="td_jsa">Alamat</td>
  <td class="td_jsa">:</td>
  <td class="td_jsa"><?php echo $alamat ?></td>
 </tr>
</table>