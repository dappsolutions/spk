<!--FORM EXPORT EXCEL-->
<table style="width: 100%;" id="table-excel-form-sertifikat-badan-usaha">
 <tr>
  <td colspan="6" class="td_jsa">Detail Sertifikat Badan Usaha</td>
 </tr>
 <tr>
  <td class="td_jsa">Nama Sertifikat</td>
  <td class="td_jsa">Expired Sertifikat</td>
  <td class="td_jsa">Kriteria</td>
  <td class="td_jsa">Bidang Usaha</td>
  <td class="td_jsa">Sub Bidang Usaha</td>
  <td class="td_jsa">Sub Kualifikasi</td>
 </tr>
 <?php if (isset($list_sertifikat)) { ?>
  <?php if (!empty($list_sertifikat)) { ?>
   <?php foreach ($list_sertifikat as $key => $value) { ?>
    <tr>
     <td class="td_jsa"><?php echo $value['file_sertifikat'] ?></td>
     <td class="td_jsa"><?php echo $value['expired_sertifikat'] ?></td>
     <td class="td_jsa"><?php echo $value['nama_agen'] ?></td>
     <td class="td_jsa"><?php echo $value['bidang_usaha'] ?></td>
     <td class="td_jsa"><?php echo $value['sub_bidang_usaha'] ?></td>
     <td class="td_jsa"><?php echo $value['sub_kualifikasi'] ?></td>
    </tr>
   <?php } ?>
  <?php } ?>
 <?php } ?>
</table>