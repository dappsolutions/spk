<div class="row">
 <div class="col-md-12">
  <div class="panel panel-default">
   <div class="panel-heading">
    <h5>Detail Export Excel Supplier</h5>
   </div>
   <div class="panel-body">
    <div class="row">
     <div class="col-md-12 text-right">
      <a idexport="content-export" onclick="Supplier.exportExcel(this, event)" class="btn btn-success">Download Excel</a>
     </div>
    </div>
    <hr>

    <div id="content-export">
     <table>
      <tr>
       <?php echo $this->load->view('document_awal_export') ?>
      </tr>
      <tr>
       <?php echo $this->load->view('document_sertifikat_badan_usaha') ?>
      </tr>
      <tr>
       <?php echo $this->load->view('document_detail_akta') ?>
      </tr>
      <tr>
       <?php echo $this->load->view('document_detail_spk_pengalaman') ?>
      </tr>
      <tr>
       <?php echo $this->load->view('document_detail_kualifikasi') ?>
      </tr>
     </table>
    </div>
   </div>
  </div>
 </div>
</div>