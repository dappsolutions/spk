<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="row">
  <div class="col-md-12">

    <ul class="timeline">

      <!-- timeline time label -->
      <li class="time-label">
        <span class="bg-yellow">
          Form Submission
        </span>
      </li>
      <!-- /.timeline-label -->

      <!-- timeline item -->
      <li>
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">Form Supplier</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_input'); ?>
              </div>
            </div>
          </div>
        </div>
      </li>

      <li>
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">Sertifikat Badan Usaha</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_input_sertifikat'); ?>
              </div>
            </div>
          </div>
        </div>
      </li>

      <li class="hide">
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">Agen</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_input_agen'); ?>
              </div>
            </div>
          </div>
        </div>
      </li>
     
      <li>
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">Akte</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_input_akta'); ?>
              </div>
            </div>
          </div>
        </div>
      </li>
      
      <li>
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">SPK Pengalaman Kerja</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_input_spk_pengalaman'); ?>
              </div>
            </div>
          </div>
        </div>
      </li>

      <li>
        <!-- timeline icon -->
        <i class="fa fa-sticky-note bg-blue"></i>
        <div class="timeline-item">
          <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
          <h3 class="timeline-header"><a href="#">Kualifikasi Lain</a> </h3>
          <div class="timeline-body">
            <div class="row">
              <div class="col-md-12">
                <?php echo $this->load->view('form_input_kualifikasi'); ?>
              </div>
            </div>
          </div>
          <div class="timeline-footer">
            <hr>
            <div class="row">
              <div class="col-md-12 text-right">
                <button class="btn btn-warning" onclick="Supplier.back(this)">Kembali</button>
                <button class="btn btn-default" onclick="Supplier.simpan(this)">Submit</button>
              </div>
            </div>
          </div>
        </div>
      </li>
      <!-- END timeline item -->
    </ul>
  </div>
</div>