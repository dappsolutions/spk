<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-data-agen">
    <thead>
     <tr>
      <th>Agen Resmi Dari Merk</th>
      <th>Upload File</th>
      <th></th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_agen)) { ?>
      <?php if (!empty($list_agen)) { ?>
       <?php foreach ($list_agen as $key => $value) { ?>
        <tr class="input" data_id="<?php echo $value['id'] ?>">
         <td>
          <input type="text" id="agen_resmi_merk" class="form-control" error="" value="<?php echo $value['agen_resmi_merk'] ?>">
         </td>
         <td id="file-agen">
          <?php if ($value['file_agen'] != '') { ?>
           <div class="form-group " id="detail_file_agen">
            <div class="input-group">
             <input disabled="" type="text" id="file_str_agen" class="form-control" value="<?php echo isset($value['file_agen']) ? $value['file_agen'] : '' ?>">
             <span class="input-group-addon">
              <i class="fa fa-image hover-content" file="<?php echo isset($value['file_agen']) ? $value['file_agen'] : '' ?>" onclick="Supplier.showFileAgen(this, event)"></i>
             </span>
             <span class="input-group-addon">
              <i class="fa fa-close hover-content" onclick="Supplier.gantiFileAgen(this, event)"></i>
             </span>
            </div>
           </div>
          <?php } else { ?>
           <input type="file" id="file_agen" class="form-control" onchange="Supplier.checkFile(this)">
          <?php } ?>
         </td>
         <td class="text-center" id="action">
          <a href="" onclick="Supplier.removeAgen(this, event)"><i class="mdi mdi-minus mdi-18px"></i></a>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?>
     <tr class="input" data_id="">
      <td>
       <input type="text" id="agen_resmi_merk" class="form-control" error="" value="">
      </td>
      <td id="file-agen">
       <input type="file" id="file_agen" class="form-control" onchange="Supplier.checkFile(this)">
      </td>
      <td class="text-center" id="action">
       <a href="" onclick="Supplier.addAgen(this, event)"><i class="mdi mdi-plus mdi-18px"></i></a>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>