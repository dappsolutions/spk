<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-data-spk-pengalaman">
    <thead>
     <tr>
      <th>SPK Pengalaman</th>
      <th>Upload File</th>
      <th>Expired Date</th>
      <th></th>
     </tr>
    </thead>
    <tbody>
     <?php $counter_index = 0 ?>
     <?php if (isset($list_spk)) { ?>
      <?php if (!empty($list_spk)) { ?>
       <?php foreach ($list_spk as $key => $value) { ?>
        <tr class="input" data_id="<?php echo $value['id'] ?>">
         <td>
          <input type="text" id="spk_pengalaman" class="form-control" error="" value="<?php echo $value['spk_pengalaman'] ?>">
         </td>
         <td id="file-spk-pengalaman">
          <?php if ($value['file_spk_pengalaman'] != '') { ?>
           <div class="form-group " id="detail_file_spk">
            <div class="input-group">
             <input disabled="" type="text" id="file_str_spk" class="form-control" value="<?php echo isset($value['file_spk_pengalaman']) ? $value['file_spk_pengalaman'] : '' ?>">
             <span class="input-group-addon">
              <i class="fa fa-image hover-content" file="<?php echo isset($value['file_spk_pengalaman']) ? $value['file_spk_pengalaman'] : '' ?>" onclick="Supplier.showFileSpk(this, event)"></i>
             </span>
             <span class="input-group-addon">
              <i class="fa fa-close hover-content" onclick="Supplier.gantiFileSpk(this, event)"></i>
             </span>
            </div>
           </div>
          <?php } else { ?>
           <input type="file" id="file_spk_pengalaman" class="form-control" onchange="Supplier.checkFile(this)">
          <?php } ?>
         </td>
         <td id="expired_spk">
          <input type="text" readonly index="<?php echo $counter_index  ?>" id="expired_date_spk<?php echo $counter_index  ?>" class="form-control" value="<?php echo $value['expired_date_spk'] ?>">
         </td>
         <td class="text-center" id="action">
          <a href="" onclick="Supplier.removeSpkPengalaman(this, event)"><i class="mdi mdi-minus mdi-18px"></i></a>
         </td>
        </tr>
        <?php $counter_index += 1 ?>
       <?php } ?>
      <?php } ?>
     <?php } ?>

     <tr class="input" data_id="">
      <td>
       <input type="text" id="spk_pengalaman" class="form-control" error="" value="">
      </td>
      <td id="file-spk-pengalaman">
       <input type="file" id="file_spk_pengalaman" class="form-control" onchange="Supplier.checkFile(this)">
      </td>
      <td id="expired_spk">
       <input type="text" readonly index="<?php echo $counter_index  ?>" id="expired_date_spk<?php echo $counter_index  ?>" class="form-control">
      </td>
      <td class="text-center" id="action">
       <a href="" onclick="Supplier.addSpkPengalaman(this, event)"><i class="mdi mdi-plus mdi-18px"></i></a>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>