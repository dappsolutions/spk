<?php if (isset($list_sertifikat)) { ?>
 <?php if (!empty($list_sertifikat)) { ?>
  <?php foreach ($list_sertifikat as $key => $value) { ?>
   <div data_id="<?php echo $value['id'] ?>" class="content-input-sertifikat-badan-usaha">
    <input type="hidden" id="sertifikat_id" value="<?php echo $value['id'] ?>">
    <div class="row">
     <div class="col-md-12">
      <div class="row">
       <div class="col-md-6 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? 'hide' : '' : ''  ?>">
        <div class="form-group" id="file_input_sertifikat">
         <label for="">Upload Sertifikat</label>
         <input type="file" id="file_sertifikat" class="form-control" onchange="Supplier.checkFile(this)">
        </div>
       </div>


       <div class="col-md-6 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? '' : 'hide' : 'hide'  ?>">
        <div class="form-group " id="detail_file_sertifikat">
         <label for="">File Sertifikat</label>
         <div class="input-group">
          <input disabled="" type="text" id="file_str_sertifikat" class="form-control" value="<?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] : '' ?>">
          <span class="input-group-addon">
           <i class="fa fa-image hover-content" file="<?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] : '' ?>" onclick="Supplier.showFile(this, event)"></i>
          </span>
          <span class="input-group-addon">
           <i class="fa fa-close hover-content" onclick="Supplier.gantiFileSertifikat(this, event)"></i>
          </span>
         </div>
        </div>
       </div>


       <div class="col-md-6">
        <div class="form-group" id="content_expired_sertifikat">
         <label for="">Expired Sertifikat</label>
         <input type="text" index="<?php echo intval($key) ?>" id="expired_sertifikat<?php echo intval($key) ?>" readonly class="form-control" error="" value="<?php echo isset($value['expired_sertifikat']) ? $value['expired_sertifikat'] : '' ?>">
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-6">
        <div class="form-group">
         <label for="">Kriteria</label>
         <select name="" id="kriteria" class="form-control">
          <option value="">--PILIH--</option>
          <?php foreach ($list_agen_data as $key => $val_agen) { ?>
           <?php $selected = '' ?>
           <?php if (isset($value['kriteria'])) { ?>
            <?php $selected = $value['kriteria'] == $val_agen['id'] ? 'selected' : '' ?>
           <?php } ?>
           <option <?php echo $selected ?> value="<?php echo $val_agen['id'] ?>"><?php echo $val_agen['nama_agen'] ?></option>
          <?php } ?>
         </select>
        </div>
       </div>

       <div class="col-md-6">
        <div class="form-group">
         <label for="">Bidang Usaha</label>
         <input type="text" id="bidang_usaha" class="form-control" error="" value="<?php echo isset($value['bidang_usaha']) ? $value['bidang_usaha'] : '' ?>">
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-6">
        <div class="form-group">
         <label for="">Sub Bidang Usaha</label>
         <input type="text" id="sub_bidang_usaha" class="form-control" error="" value="<?php echo isset($value['sub_bidang_usaha']) ? $value['sub_bidang_usaha'] : '' ?>">
        </div>
       </div>

       <div class="col-md-6">
        <div class="form-group">
         <label for="">Sub Kualifikasi</label>
         <input type="text" id="sub_kualifikasi" class="form-control" error="" value="<?php echo isset($value['sub_kualifikasi']) ? $value['sub_kualifikasi'] : '' ?>">
        </div>
       </div>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-12 text-right" id="action-badan-usaha">
      <button class="btn btn-success" onclick="Supplier.addSertifikatBadanUsaha(this)"><i class="fa fa-plus"></i></button>
     </div>
    </div>
   </div>
  <?php } ?>
 <?php } else { ?>
  <div data_id="" class="content-input-sertifikat-badan-usaha">
   <input type="hidden" id="sertifikat_id" value="<?php echo isset($sertifikat_id) ? $sertifikat_id : '' ?>">
   <div class="row">
    <div class="col-md-12">
     <div class="row">
      <div class="col-md-6 <?php echo isset($file_sertifikat) ? $file_sertifikat != '' ? 'hide' : '' : ''  ?>">
       <div class="form-group" id="file_input_sertifikat">
        <label for="">Upload Sertifikat</label>
        <input type="file" id="file_sertifikat" class="form-control" onchange="Supplier.checkFile(this)">
       </div>
      </div>


      <div class="col-md-6 <?php echo isset($file_sertifikat) ? $file_sertifikat != '' ? '' : 'hide' : 'hide'  ?>">
       <div class="form-group " id="detail_file_sertifikat">
        <label for="">File Sertifikat</label>
        <div class="input-group">
         <input disabled="" type="text" id="file_str_sertifikat" class="form-control" value="<?php echo isset($file_sertifikat) ? $file_sertifikat : '' ?>">
         <span class="input-group-addon">
          <i class="fa fa-image hover-content" file="<?php echo isset($file_sertifikat) ? $file_sertifikat : '' ?>" onclick="Supplier.showFile(this, event)"></i>
         </span>
         <span class="input-group-addon">
          <i class="fa fa-close hover-content" onclick="Supplier.gantiFileSertifikat(this, event)"></i>
         </span>
        </div>
       </div>
      </div>


      <div class="col-md-6">
       <div class="form-group" id="content_expired_sertifikat">
        <label for="">Expired Sertifikat</label>
        <input type="text" index="<?php echo count($list_sertifikat) ?>" id="expired_sertifikat<?php echo count($list_sertifikat) ?>" readonly class="form-control" error="" value="<?php echo isset($expired_sertifikat) ? $expired_sertifikat : '' ?>">
       </div>
      </div>
     </div>

     <div class="row">
      <div class="col-md-6">
       <div class="form-group">
        <label for="">Kriteria</label>
        <select name="" id="kriteria" class="form-control">
         <option value="">--PILIH--</option>
         <?php foreach ($list_agen_data as $key => $val_agen) { ?>
          <?php $selected = '' ?>
          <?php if (isset($kriteria)) { ?>
           <?php $selected = $kriteria == $val_agen['id'] ? 'selected' : '' ?>
          <?php } ?>
          <option <?php echo $selected ?> value="<?php echo $val_agen['id'] ?>"><?php echo $val_agen['nama_agen'] ?></option>
         <?php } ?>
        </select>
        <!-- <input type="text" id="kriteria" class="form-control" error="" value="<?php echo isset($kriteria) ? $kriteria : '' ?>"> -->
       </div>
      </div>

      <div class="col-md-6">
       <div class="form-group">
        <label for="">Bidang Usaha</label>
        <input type="text" id="bidang_usaha" class="form-control" error="" value="<?php echo isset($bidang_usaha) ? $bidang_usaha : '' ?>">
       </div>
      </div>
     </div>

     <div class="row">
      <div class="col-md-6">
       <div class="form-group">
        <label for="">Sub Bidang Usaha</label>
        <input type="text" id="sub_bidang_usaha" class="form-control" error="" value="<?php echo isset($sub_bidang_usaha) ? $sub_bidang_usaha : '' ?>">
       </div>
      </div>

      <div class="col-md-6">
       <div class="form-group">
        <label for="">Sub Kualifikasi</label>
        <input type="text" id="sub_kualifikasi" class="form-control" error="" value="<?php echo isset($sub_kualifikasi) ? $sub_bidang_usaha : '' ?>">
       </div>
      </div>
     </div>
    </div>
   </div>

   <div class="row">
    <div class="col-md-12 text-right" id="action-badan-usaha">
     <button class="btn btn-success" onclick="Supplier.addSertifikatBadanUsaha(this)"><i class="fa fa-plus"></i></button>
    </div>
   </div>
  </div>
 <?php } ?>
<?php } else { ?>
 <div data_id="" class="content-input-sertifikat-badan-usaha">
  <input type="hidden" id="sertifikat_id" value="<?php echo isset($sertifikat_id) ? $sertifikat_id : '' ?>">
  <div class="row">
   <div class="col-md-12">
    <div class="row">
     <div class="col-md-6 <?php echo isset($file_sertifikat) ? $file_sertifikat != '' ? 'hide' : '' : ''  ?>">
      <div class="form-group" id="file_input_sertifikat">
       <label for="">Upload Sertifikat</label>
       <input type="file" id="file_sertifikat" class="form-control" onchange="Supplier.checkFile(this)">
      </div>
     </div>


     <div class="col-md-6 <?php echo isset($file_sertifikat) ? $file_sertifikat != '' ? '' : 'hide' : 'hide'  ?>">
      <div class="form-group " id="detail_file_sertifikat">
       <label for="">File Sertifikat</label>
       <div class="input-group">
        <input disabled="" type="text" id="file_str_sertifikat" class="form-control" value="<?php echo isset($file_sertifikat) ? $file_sertifikat : '' ?>">
        <span class="input-group-addon">
         <i class="fa fa-image hover-content" file="<?php echo isset($file_sertifikat) ? $file_sertifikat : '' ?>" onclick="Supplier.showFile(this, event)"></i>
        </span>
        <span class="input-group-addon">
         <i class="fa fa-close hover-content" onclick="Supplier.gantiFileSertifikat(this, event)"></i>
        </span>
       </div>
      </div>
     </div>


     <div class="col-md-6">
      <div class="form-group" id="content_expired_sertifikat">
       <label for="">Expired Sertifikat</label>
       <input type="text" index="0" id="expired_sertifikat0" readonly class="form-control" error="" value="<?php echo isset($expired_sertifikat) ? $expired_sertifikat : '' ?>">
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-6">
      <div class="form-group">
       <label for="">Kriteria</label>
       <select name="" id="kriteria" class="form-control">
        <option value="">--PILIH--</option>
        <?php foreach ($list_agen_data as $key => $val_agen) { ?>
         <?php $selected = '' ?>
         <?php if (isset($kriteria)) { ?>
          <?php $selected = $kriteria == $val_agen['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $val_agen['id'] ?>"><?php echo $val_agen['nama_agen'] ?></option>
        <?php } ?>
       </select>
       <!-- <input type="text" id="kriteria" class="form-control" error="" value="<?php echo isset($kriteria) ? $kriteria : '' ?>"> -->
      </div>
     </div>

     <div class="col-md-6">
      <div class="form-group">
       <label for="">Bidang Usaha</label>
       <input type="text" id="bidang_usaha" class="form-control" error="" value="<?php echo isset($bidang_usaha) ? $bidang_usaha : '' ?>">
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-6">
      <div class="form-group">
       <label for="">Sub Bidang Usaha</label>
       <input type="text" id="sub_bidang_usaha" class="form-control" error="" value="<?php echo isset($sub_bidang_usaha) ? $sub_bidang_usaha : '' ?>">
      </div>
     </div>

     <div class="col-md-6">
      <div class="form-group">
       <label for="">Sub Kualifikasi</label>
       <input type="text" id="sub_kualifikasi" class="form-control" error="" value="<?php echo isset($sub_kualifikasi) ? $sub_bidang_usaha : '' ?>">
      </div>
     </div>
    </div>
   </div>
  </div>

  <div class="row">
   <div class="col-md-12 text-right" id="action-badan-usaha">
    <button class="btn btn-success" onclick="Supplier.addSertifikatBadanUsaha(this)"><i class="fa fa-plus"></i></button>
   </div>
  </div>
 </div>
<?php } ?>