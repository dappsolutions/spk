<?php if (isset($list_sertifikat_lain)) { ?>
 <?php if (!empty($list_sertifikat_lain)) { ?>
  <?php foreach ($list_sertifikat_lain as $key => $value) { ?>
   <div data_id="<?php echo $value['id'] ?>" class="content-input-sertifikat-lain">
    <input type="hidden" id="sertifikat_lain_id" value="<?php echo $value['id'] ?>">
    <div class="row">
     <div class="col-md-12">
      <div class="row">
       <div class="col-md-6 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? 'hide' : '' : ''  ?>">
        <div class="form-group" id="file_input_sertifikat_lain">
         <label for="">Upload Sertifikat</label>
         <input type="file" id="file_sertifikat_lain" class="form-control" onchange="Supplier.checkFile(this)">
        </div>
       </div>


       <div class="col-md-6 <?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] != '' ? '' : 'hide' : 'hide'  ?>">
        <div class="form-group " id="detail_file_sertifikat_lain">
         <label for="">File Sertifikat</label>
         <div class="input-group">
          <input disabled="" type="text" id="file_str_sertifikat_lain" class="form-control" value="<?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] : '' ?>">
          <span class="input-group-addon">
           <i class="fa fa-image hover-content" file="<?php echo isset($value['file_sertifikat']) ? $value['file_sertifikat'] : '' ?>" onclick="Supplier.showFile(this, event)"></i>
          </span>
         </div>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-6">
        <div class="form-group" id="content_expired_sertifikat_lain">
         <label for="">Expired Sertifikat</label>
        </div>
       </div>
       <div class="col-md-6">
        <div class="form-group" id="content_expired_sertifikat_lain">
         <?php echo $value['expired_sertifikat'] ?>
        </div>
       </div>
      </div>

      <div class="row">
       <div class="col-md-6">
        <div class="form-group">
         <label for="">Nama Sertifikat</label>
        </div>
       </div>
       <div class="col-md-6">
        <div class="form-group">
         <?php echo $value['nama_sertifikat'] ?>
        </div>
       </div>
      </div>
     </div>
    </div>
   </div>
   <hr>
  <?php } ?>
 <?php } ?>
<?php } ?>