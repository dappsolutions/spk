<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Vendor</label>
   <input type="text" id="nama_vendor" class="form-control required" error="Nama Vendor" value="<?php echo isset($nama_vendor) ? $nama_vendor : ''?>">
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Pimpinan</label>
   <input type="text" id="nama_pimpinan" class="form-control required" error="Nama Pimpinan" value="<?php echo isset($nama_pimpinan) ? $nama_pimpinan : ''?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jabatan Pimpinan</label>
   <input type="text" id="jabatan_pimpinan" class="form-control required" error="Jabatan Pimpinan" value="<?php echo isset($jabatan_pimpinan) ? $jabatan_pimpinan : ''?>">
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">Email</label>
   <input type="text" id="email" class="form-control required" error="Email" value="<?php echo isset($email) ? $email : ''?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Bank</label>
   <input type="text" id="nama_bank" class="form-control required" error="Nama Bank" value="<?php echo isset($nama_bank) ? $nama_bank : ''?>">
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">No Rekening</label>
   <input type="text" id="no_rekening" class="form-control required" error="No Rekening" value="<?php echo isset($no_rekening) ? $no_rekening : ''?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Alamat</label>
   <textarea name="" id="alamat" class="required form-control" error="Alamat"><?php echo isset($alamat) ? $alamat : ''?></textarea>
  </div>
 </div>
</div>