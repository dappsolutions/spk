<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Vendor</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nama_vendor ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Pimpinan</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nama_pimpinan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jabatan Pimpinan</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $jabatan_pimpinan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Email</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $email ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Bank</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nama_bank ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">No. Rekening</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $no_rekening ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Alamat</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $alamat ?>
  </div>
 </div>
</div>
