<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-data-agen">
    <thead>
     <tr>
      <th>Agen Resmi Dari Merk</th>
      <th>Upload File</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($list_agen)) { ?>
      <?php if (!empty($list_agen)) { ?>
       <?php foreach ($list_agen as $key => $value) { ?>
        <tr class="input" data_id="<?php echo $value['id'] ?>">
         <td>
          <?php echo $value['agen_resmi_merk'] ?>
         </td>
         <td id="file-agen">
          <?php if ($value['file_agen'] != '') { ?>
           <div class="form-group " id="detail_file_agen">
            <div class="input-group">
             <input disabled="" type="text" id="file_str_agen" class="form-control" value="<?php echo isset($value['file_agen']) ? $value['file_agen'] : '' ?>">
             <span class="input-group-addon">
              <i class="fa fa-image hover-content" file="<?php echo isset($value['file_agen']) ? $value['file_agen'] : '' ?>" onclick="Supplier.showFileAgen(this, event)"></i>
             </span>
            </div>
           </div>
          <?php } else { ?>
           -
          <?php } ?>
         </td>
        </tr>
       <?php } ?>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>