<?php

class Supplier extends MX_Controller
{

  public $segment;
  public $limit;
  public $page;
  public $last_no;
  public $menu_akses;

  public function __construct()
  {
    parent::__construct();
    $this->limit = 25;
    $this->menu_akses = json_decode($this->session->userdata('list_akses'));
  }

  public function getModuleName()
  {
    return 'supplier';
  }

  public function getHeaderJSandCSS()
  {
    //versioning
    $version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
    $version = substr($version, 0, 11);
    //versioning

    $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/supplier.js?v=' . $version . '"></script>'
    );

    return $data;
  }

  public function getTableName()
  {
    return 'vendor';
  }

  public function index()
  {
    $this->segment = 3;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;

    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Supplier";
    $data['title_content'] = 'Data Supplier';
    $content = $this->getDataSupplier();
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['menu_akses'] = $this->menu_akses;
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function getTotalDataSupplier($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('v.nama_vendor', $keyword),
        array('v.email', $keyword),
        array('v.nama_pimpinan', $keyword),
        array('v.nama_bank', $keyword),
        array('v.no_rekening', $keyword),
      );
    }
    $total = Modules::run('database/count_all', array(
      'table' => $this->getTableName() . ' v',
      'field' => array('v.*'),
      'like' => $like,
      'is_or_like' => true,
      'where' => "v.deleted = 0"
    ));

    return $total;
  }

  public function getDataSupplier($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('v.nama_vendor', $keyword),
        array('v.email', $keyword),
        array('v.nama_pimpinan', $keyword),
        array('v.nama_bank', $keyword),
        array('v.no_rekening', $keyword),
      );
    }
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' v',
      'field' => array('v.*'),
      'like' => $like,
      'is_or_like' => true,
      'limit' => $this->limit,
      'offset' => $this->last_no,
      'where' => "v.deleted = 0"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataSupplier($keyword)
    );
  }

  public function getDetailDataSupplier($id)
  {
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' kr',
      'field' => array(
        'kr.*',
        'sl.id as sertifikat_lain_id',
        'sl.nib',
        'sl.file_nib',
        'sl.expired_sertifikat_nib',
        'sl.akte',
        'sl.file_akte',
        'sl.expired_sertifikat_akte',
        'sl.npwp',
        'sl.file_npwp',
        'sl.expired_sertifikat_npwp',
        'sl.pkp',
        'sl.file_pkp',
        'sl.expired_sertifikat_pkp',
        'sl.susanan_pengurus',
        'sl.file_susunan',
        'sl.expired_sertifikat_susunan',
        'sl.surat_keterangan_domisili',
        'sl.file_domisili',
        'sl.expired_sertifikat_domisili',
        'sl.surat_keterangan_rekening',
        'sl.file_rekening',
        'sl.expired_sertifikat_rekening',
        'sl.pakta_integritas',
        'sl.file_integritas',
        'sl.expired_sertifikat_pakta',
        'sl.ak3',
        'sl.file_ak3',
        'sl.expired_sertifikat_ak3',
        'sl.spk_pengalaman_kerja',
        'sl.file_spk_pengalaman',
        'sl.expired_sertifikat_spk',
      ),
      'join' => array(
        array('sertifikat_lain sl', 'sl.vendor = kr.id', 'left'),
      ),
      'where' => "kr.id = '" . $id . "'"
    ));

    // echo '<pre>';
    // print_r($data->row_array());die;
    return $data->row_array();
  }

  public function getListAgenVendor($id)
  {
    $data = Modules::run('database/get', array(
      'table' => 'vendor_has_agen vhr',
      'field' => array(
        'vhr.*',
      ),
      'where' => "vhr.vendor = '" . $id . "' and vhr.deleted = 0"
    ));

    // echo '<pre>';
    // print_r($data->row_array());die;
    $result = [];
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }
    return $result;
  }

  public function getListAktaVendor($id)
  {
    $data = Modules::run('database/get', array(
      'table' => 'vendor_has_akta vhr',
      'field' => array(
        'vhr.*',
      ),
      'where' => "vhr.vendor = '" . $id . "' and vhr.deleted = 0"
    ));

    // echo '<pre>';
    // print_r($data->row_array());die;
    $result = [];
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }
    return $result;
  }

  public function getListSpkVendor($id)
  {
    $data = Modules::run('database/get', array(
      'table' => 'vendor_has_spk vhr',
      'field' => array(
        'vhr.*',
      ),
      'where' => "vhr.vendor = '" . $id . "' and vhr.deleted = 0"
    ));

    // echo '<pre>';
    // print_r($data->row_array());die;
    $result = [];
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }
    return $result;
  }

  public function getListSertifikatVendor($id)
  {
    $data = Modules::run('database/get', array(
      'table' => 'sertifikat_badan_usaha sbu',
      'field' => array(
        'sbu.*',
        'ag.nama_agen'
      ),
      'join' => array(
        array('agen ag', 'ag.id = sbu.kriteria')
      ),
      'where' => "sbu.vendor = '" . $id . "' and sbu.deleted = 0"
    ));

    // echo '<pre>';
    // print_r($data->row_array());die;
    $result = [];
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }
    return $result;
  }
  
  public function getListSertifikatVendorLain($id)
  {
    $data = Modules::run('database/get', array(
      'table' => 'sertifikat_lain sbu',
      'field' => array(
        'sbu.*',
      ),
      'where' => "sbu.vendor = '" . $id . "' and sbu.deleted = 0"
    ));

    // echo '<pre>';
    // print_r($data->row_array());die;
    $result = [];
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }
    return $result;
  }

  public function getListAgen()
  {
    $data = Modules::run('database/get', array(
      'table' => 'agen',
      'where' => "deleted = 0"
    ));

    // echo '<pre>';
    // print_r($data->row_array());die;
    $result = [];
    if (!empty($data)) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }
    return $result;
  }

  public function add()
  {
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Tambah Supplier";
    $data['title_content'] = 'Tambah Supplier';
    $data['list_agen_data'] = $this->getListAgen();
    echo Modules::run('template', $data);
  }

  public function ubah($id)
  {
    $data = $this->getDetailDataSupplier($id);
    // echo '<pre>';
    // print_r($data);die;
    $data['list_agen'] = $this->getListAgenVendor($id);
    $data['list_akta'] = $this->getListAktaVendor($id);
    $data['list_spk'] = $this->getListSpkVendor($id);
    $data['list_sertifikat'] = $this->getListSertifikatVendor($id);
    $data['list_sertifikat_lain'] = $this->getListSertifikatVendorLain($id);
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Ubah Supplier";
    $data['title_content'] = 'Ubah Supplier';
    $data['list_agen_data'] = $this->getListAgen();
    echo Modules::run('template', $data);
  }

  public function detail($id)
  {
    $data = $this->getDetailDataSupplier($id);
    $data['list_agen'] = $this->getListAgenVendor($id);
    $data['list_akta'] = $this->getListAktaVendor($id);
    $data['list_spk'] = $this->getListSpkVendor($id);
    $data['list_sertifikat'] = $this->getListSertifikatVendor($id);
    $data['list_sertifikat_lain'] = $this->getListSertifikatVendorLain($id);
    // echo '<pre>';
    // print_r($data);die;
    $data['view_file'] = 'detail_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Detail Supplier";
    $data['title_content'] = 'Detail Supplier';
    echo Modules::run('template', $data);
  }

  public function getPostDataHeader($value)
  {
    $data['nama_vendor'] = $value['nama_vendor'];
    $data['nama_pimpinan'] = $value['nama_pimpinan'];
    $data['jabatan_pimpinan'] = $value['jabatan_pimpinan'];
    $data['email'] = $value['email'];
    $data['nama_bank'] = $value['nama_bank'];
    $data['no_rekening'] = $value['no_rekening'];
    $data['alamat'] = $value['alamat'];
    return $data;
  }

  public function simpan()
  {
    $data = (array) json_decode($_POST['data']);
    $file = $_FILES;

    $id = $data['id'];
    $result['is_valid'] = false;
    $result['id'] = $id;

    // echo '<pre>';
    // print_r($file);die;

    $is_uploaded = true;
    $this->db->trans_begin();
    try {
      // //uploade sertifikat usaha
      // $file_sertifikat_usaha = '';
      // if (!empty($file)) {
      //   if (isset($file['file_sertifikat'])) {
      //     $response_upload = $this->uploadData('file_sertifikat', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_sertifikat_usaha = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      // //uploade sertifikat usaha

      //uploade sertifikat nib
      // $file_nib = '';
      // if (!empty($file)) {
      //   if (isset($file['file_nib'])) {
      //     $response_upload = $this->uploadData('file_nib', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_nib = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat nib

      //uploade sertifikat npwp
      // $file_npwp = "";
      // if (!empty($file)) {
      //   if (isset($file['file_npwp'])) {
      //     $response_upload = $this->uploadData('file_npwp', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_npwp = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat npwp

      //uploade sertifikat pkp
      // $file_pkp = "";
      // if (!empty($file)) {
      //   if (isset($file['file_pkp'])) {
      //     $response_upload = $this->uploadData('file_pkp', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_pkp = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat pkp

      //uploade sertifikat susunan
      // $file_susunan = "";
      // if (!empty($file)) {
      //   if (isset($file['file_susunan'])) {
      //     $response_upload = $this->uploadData('file_susunan', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_susunan = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat susunan

      //uploade sertifikat domisili
      // $file_domisili = "";
      // if (!empty($file)) {
      //   if (isset($file['file_domisili'])) {
      //     $response_upload = $this->uploadData('file_domisili', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_domisili = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat domisili

      //uploade sertifikat rekening
      // $file_rekening = "";
      // if (!empty($file)) {
      //   if (isset($file['file_rekening'])) {
      //     $response_upload = $this->uploadData('file_rekening', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_rekening = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat rekening

      //uploade sertifikat integritas
      // $file_integritas = "";
      // if (!empty($file)) {
      //   if (isset($file['file_integritas'])) {
      //     $response_upload = $this->uploadData('file_integritas', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_integritas = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat integritas

      //uploade sertifikat ak3
      // $file_ak3 = "";
      // if (!empty($file)) {
      //   if (isset($file['file_ak3'])) {
      //     $response_upload = $this->uploadData('file_ak3', 'sertifikat');
      //     if (!$response_upload['is_valid']) {
      //       $is_uploaded = false;
      //       $result['message'] .= $response_upload['response'];
      //     } else {
      //       $file_ak3 = $response_upload['response']['file_name'];
      //     }
      //   }
      // }
      //uploade sertifikat ak3

      $push = [];
      $push['user'] = $this->session->userdata('user_id');
      $push['createddate'] = date('Y-m-d H:i:s');
      $this->db->insert('actor', $push);

      $actorId = $this->db->insert_id();

      $post = $this->getPostDataHeader($data);
      if ($id == '') {
        $post['createddate'] = date('Y-m-d H:i:s');
        $post['createdby'] = $actorId;
        $this->db->insert($this->getTableName(), $post);
        $id = $this->db->insert_id();
        $result['id'] = $id;

        //insert sertifikat usaha
        foreach ($data['data_sertifikat'] as $key => $value) {
          $file_sertifikat_usaha = "";
          $name_file = 'file_sertifikat' . $value->counter_serifikat;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'sertifikat');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_sertifikat_usaha = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['expired_sertifikat'] = $value->expired_sertifikat;
          $push['kriteria'] = $value->kriteria;
          $push['bidang_usaha'] = $value->bidang_usaha;
          $push['sub_bidang_usaha'] = $value->sub_bidang_usaha;
          $push['sub_kualifikasi'] = $value->sub_kualifikasi;
          if ($file_sertifikat_usaha != '') {
            $push['file_sertifikat'] = $file_sertifikat_usaha;
          }
          $this->db->insert('sertifikat_badan_usaha', $push);
        }
        //insert sertifikat usaha

        //insert agen
        foreach ($data['data_agen'] as $key => $value) {
          $file_agen = "";
          $name_file = 'file_agen' . $value->counter_agen;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'agen');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_agen = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['agen_resmi_merk'] = $value->agen_resmi_merk;
          if ($file_agen != '') {
            $push['file_agen'] = $file_agen;
          }
          $this->db->insert('vendor_has_agen', $push);
        }
        //insert agen

        //insert akta
        foreach ($data['data_akta'] as $key => $value) {
          $file_akta = "";
          $name_file = 'file_akta' . $value->counter_akta;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'akta');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_akta = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['akta'] = $value->akta;
          $push['expired_date_akta'] = $value->expired_date_akta;
          if ($file_akta != '') {
            $push['file_akta'] = $file_akta;
          }
          $this->db->insert('vendor_has_akta', $push);
        }
        //insert akta

        //insert spk
        foreach ($data['data_spk'] as $key => $value) {
          $file_spk_pengalaman = "";
          $name_file = 'file_spk_pengalaman' . $value->counter_spk;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'spk');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_spk_pengalaman = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['spk_pengalaman'] = $value->spk_pengalaman;
          $push['expired_date_spk'] = $value->expired_date_spk;
          if ($file_spk_pengalaman != '') {
            $push['file_spk_pengalaman'] = $file_spk_pengalaman;
          }
          $this->db->insert('vendor_has_spk', $push);
        }
        //insert spk

        //insert sertifikat lain
        // $push = [];
        // $push['nib'] = $data['nib'];
        // $push['vendor'] = $id;
        // $push['expired_sertifikat_nib'] = $data['expired_sertifikat_nib'];
        // if ($file_nib != '') {
        //   $push['file_nib'] = $file_nib;
        // }
        // $push['npwp'] = $data['npwp'];
        // $push['expired_sertifikat_npwp'] = $data['expired_sertifikat_npwp'];
        // if ($file_npwp != '') {
        //   $push['file_npwp'] = $file_npwp;
        // }
        // $push['pkp'] = $data['pkp'];
        // $push['expired_sertifikat_pkp'] = $data['expired_sertifikat_pkp'];
        // if ($file_pkp != '') {
        //   $push['file_pkp'] = $file_pkp;
        // }
        // $push['susanan_pengurus'] = $data['susanan_pengurus'];
        // $push['expired_sertifikat_susunan'] = $data['expired_sertifikat_susunan'];
        // if ($file_susunan != '') {
        //   $push['file_susunan'] = $file_susunan;
        // }
        // $push['surat_keterangan_domisili'] = $data['surat_keterangan_domisili'];
        // $push['expired_sertifikat_domisili'] = $data['expired_sertifikat_domisili'];
        // if ($file_domisili != '') {
        //   $push['file_domisili'] = $file_domisili;
        // }
        // $push['surat_keterangan_rekening'] = $data['surat_keterangan_rekening'];
        // $push['expired_sertifikat_rekening'] = $data['expired_sertifikat_rekening'];
        // if ($file_rekening != '') {
        //   $push['file_rekening'] = $file_rekening;
        // }
        // $push['pakta_integritas'] = $data['pakta_integritas'];
        // $push['expired_sertifikat_pakta'] = $data['expired_sertifikat_pakta'];
        // if ($file_integritas != '') {
        //   $push['file_integritas'] = $file_integritas;
        // }
        // $push['ak3'] = $data['ak3'];
        // $push['expired_sertifikat_ak3'] = $data['expired_sertifikat_ak3'];
        // if ($file_ak3 != '') {
        //   $push['file_ak3'] = $file_ak3;
        // }
        // $this->db->insert('sertifikat_lain', $push);
        //insert sertifikat lain
        foreach ($data['data_sertifikat_lain'] as $key => $value) {
          $file_sertifikat_lain = "";
          $name_file = 'file_sertifikat_lain' . $value->counter_serifikat;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'sertifikat');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_sertifikat_lain = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['expired_sertifikat'] = $value->expired_sertifikat;
          $push['nama_sertifikat'] = $value->nama_sertifikat;
          if ($file_sertifikat_lain != '') {
            $push['file_sertifikat'] = $file_sertifikat_lain;
          }
          $this->db->insert('sertifikat_lain', $push);
        }
        //insert sertifikat lain
      } else {
        //update
        Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));

        //update sertifikat usaha
        foreach ($data['data_sertifikat'] as $key => $value) {
          $file_sertifikat_usaha = "";
          $name_file = 'file_sertifikat' . $value->counter_serifikat;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'sertifikat');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_sertifikat_usaha = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['expired_sertifikat'] = $value->expired_sertifikat;
          $push['kriteria'] = $value->kriteria;
          $push['bidang_usaha'] = $value->bidang_usaha;
          $push['sub_bidang_usaha'] = $value->sub_bidang_usaha;
          $push['sub_kualifikasi'] = $value->sub_kualifikasi;
          if ($file_sertifikat_usaha != '') {
            $push['file_sertifikat'] = $file_sertifikat_usaha;
          }
          if ($value->id == '') {
            $this->db->insert('sertifikat_badan_usaha', $push);
          } else {
            $this->db->update('sertifikat_badan_usaha', $push, array('id' => $value->id));
          }
        }
        //insert sertifikat usaha       

        //insert agen
        foreach ($data['data_agen'] as $key => $value) {
          $file_agen = "";
          $name_file = 'file_agen' . $value->counter_agen;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'agen');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_agen = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['agen_resmi_merk'] = $value->agen_resmi_merk;
          if ($file_agen != '') {
            $push['file_agen'] = $file_agen;
          }
          if ($value->remove == 1) {
            $push['deleted'] = 1;
          }
          if ($value->id == '') {
            $this->db->insert('vendor_has_agen', $push);
          } else {
            $this->db->update('vendor_has_agen', $push, array('id' => $value->id));
          }
        }
        //insert agen

        //insert akta
        foreach ($data['data_akta'] as $key => $value) {
          $file_akta = "";
          $name_file = 'file_akta' . $value->counter_akta;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'akta');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_akta = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['akta'] = $value->akta;
          $push['expired_date_akta'] = $value->expired_date_akta;
          if ($file_akta != '') {
            $push['file_akta'] = $file_akta;
          }
          if ($value->remove == 1) {
            $push['deleted'] = 1;
          }
          if ($value->id == '') {
            $this->db->insert('vendor_has_akta', $push);
          } else {
            $this->db->update('vendor_has_akta', $push, array('id' => $value->id));
          }
        }
        //insert akta

        //insert spk
        foreach ($data['data_spk'] as $key => $value) {
          $file_spk_pengalaman = "";
          $name_file = 'file_spk_pengalaman' . $value->counter_spk;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'spk');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_spk_pengalaman = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['spk_pengalaman'] = $value->spk_pengalaman;
          $push['expired_date_spk'] = $value->expired_date_spk;
          if ($file_spk_pengalaman != '') {
            $push['file_spk_pengalaman'] = $file_spk_pengalaman;
          }
          if ($value->remove == 1) {
            $push['deleted'] = 1;
          }
          if ($value->id == '') {
            $this->db->insert('vendor_has_spk', $push);
          } else {
            $this->db->update('vendor_has_spk', $push, array('id' => $value->id));
          }
        }
        //insert spk

        //insert sertifikat lain
        // $push = [];
        // $push['nib'] = $data['nib'];
        // $push['vendor'] = $id;
        // $push['expired_sertifikat_nib'] = $data['expired_sertifikat_nib'];
        // if ($file_nib != '') {
        //   $push['file_nib'] = $file_nib;
        // }
        // $push['npwp'] = $data['npwp'];
        // $push['expired_sertifikat_npwp'] = $data['expired_sertifikat_npwp'];
        // if ($file_npwp != '') {
        //   $push['file_npwp'] = $file_npwp;
        // }
        // $push['pkp'] = $data['pkp'];
        // $push['expired_sertifikat_pkp'] = $data['expired_sertifikat_pkp'];
        // if ($file_pkp != '') {
        //   $push['file_pkp'] = $file_pkp;
        // }
        // $push['susanan_pengurus'] = $data['susanan_pengurus'];
        // $push['expired_sertifikat_susunan'] = $data['expired_sertifikat_susunan'];
        // if ($file_susunan != '') {
        //   $push['file_susunan'] = $file_susunan;
        // }
        // $push['surat_keterangan_domisili'] = $data['surat_keterangan_domisili'];
        // $push['expired_sertifikat_domisili'] = $data['expired_sertifikat_domisili'];
        // if ($file_domisili != '') {
        //   $push['file_domisili'] = $file_domisili;
        // }
        // $push['surat_keterangan_rekening'] = $data['surat_keterangan_rekening'];
        // $push['expired_sertifikat_rekening'] = $data['expired_sertifikat_rekening'];
        // if ($file_rekening != '') {
        //   $push['file_rekening'] = $file_rekening;
        // }
        // $push['pakta_integritas'] = $data['pakta_integritas'];
        // $push['expired_sertifikat_pakta'] = $data['expired_sertifikat_pakta'];
        // if ($file_integritas != '') {
        //   $push['file_integritas'] = $file_integritas;
        // }
        // $push['ak3'] = $data['ak3'];
        // $push['expired_sertifikat_ak3'] = $data['expired_sertifikat_ak3'];
        // if ($file_ak3 != '') {
        //   $push['file_ak3'] = $file_ak3;
        // }
        // if ($data['sertifikat_lain_id'] == '') {
        //   $this->db->insert('sertifikat_lain', $push);
        // } else {
        //   $this->db->update('sertifikat_lain', $push, array('id' => $data['sertifikat_lain_id']));
        // }
         //insert sertifikat lain
         foreach ($data['data_sertifikat_lain'] as $key => $value) {
          $file_sertifikat_lain = "";
          $name_file = 'file_sertifikat_lain' . $value->counter_serifikat;
          if (!empty($file)) {
            if (isset($file[$name_file])) {
              $response_upload = $this->uploadData($name_file, 'sertifikat');
              if (!$response_upload['is_valid']) {
                $is_uploaded = false;
                $result['message'] .= $response_upload['response'];
              } else {
                $file_sertifikat_lain = $response_upload['response']['file_name'];
              }
            }
          }

          $push = [];
          $push['createddate'] = date('Y-m-d H:i:s');
          $push['createdby'] = $actorId;
          $push['vendor'] = $id;
          $push['expired_sertifikat'] = $value->expired_sertifikat;
          $push['nama_sertifikat'] = $value->nama_sertifikat;
          if ($file_sertifikat_lain != '') {
            $push['file_sertifikat'] = $file_sertifikat_lain;
          }
          if($value->id != ''){
            $this->db->update('sertifikat_lain', $push, array('id'=> $value->id));
          }else{
            $this->db->insert('sertifikat_lain', $push);
          }
        }
        //insert sertifikat lain
      }

      if (!$is_uploaded) {
        $this->db->trans_rollback();
      } else {
        $this->db->trans_commit();
        $result['is_valid'] = true;
      }
    } catch (Exception $ex) {
      $result['message'] = $ex->getMessage();
      $this->db->trans_rollback();
    }

    echo json_encode($result);
  }

  public function search($keyword)
  {
    $this->segment = 4;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;
    $keyword = urldecode($keyword);

    $data['keyword'] = $keyword;
    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Supplier";
    $data['title_content'] = 'Data Supplier';
    $content = $this->getDataSupplier($keyword);
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function delete($id)
  {
    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
      $this->db->trans_commit();
      $is_valid = true;
    } catch (Exception $ex) {
      $this->db->trans_rollback();
    }

    echo json_encode(array('is_valid' => $is_valid));
  }

  public function uploadData($name_of_field, $dir)
  {
    $config['upload_path'] = 'files/berkas/' . $dir . '/';
    $config['allowed_types'] = 'png|jpg|jpeg|pdf';
    $config['max_size'] = '1000';
    $config['max_width'] = '6000';
    $config['max_height'] = '6000';
    $config['encrypt_name'] = TRUE;


    $this->load->library('upload', $config);

    $is_valid = false;
    if (!$this->upload->do_upload($name_of_field)) {
      $response = $this->upload->display_errors();
    } else {
      $response = $this->upload->data();
      $is_valid = true;
    }

    // echo '<pre>';
    // print_r($response);die;

    return array(
      'is_valid' => $is_valid,
      'response' => $response
    );
  }

  public function showFile()
  {
    $data = $_POST;
    echo $this->load->view('file_sertifikat_usaha', $data, true);
  }    

  public function showFileAgen()
  {
    $data = $_POST;
    echo $this->load->view('file_agen', $data, true);
  }

  public function showFileAkta()
  {
    $data = $_POST;
    echo $this->load->view('file_akta', $data, true);
  }

  public function showFileSpk()
  {
    $data = $_POST;
    echo $this->load->view('file_spk', $data, true);
  }

  public function exportView()
  {
    $params = $_POST;
    $id = $params['id'];
    $data = $this->getDetailDataSupplier($id);
    $data['list_agen'] = $this->getListAgenVendor($id);
    $data['list_akta'] = $this->getListAktaVendor($id);
    $data['list_spk'] = $this->getListSpkVendor($id);
    $data['list_sertifikat'] = $this->getListSertifikatVendor($id);
    $data['list_sertifikat_lain'] = $this->getListSertifikatVendorLain($id);
    echo $this->load->view('export/detail_export', $data, true);
  }
}
