<?php

class M_dashboard extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getSpkOutStanding($params)
	{
		$sql = "
		select 
		phs.*
		from produk_has_satuan phs
		where phs.produk = " . $params['material_id'] . "
		and phs.satuan = " . $params['satuan_id'] . "
		and phs.deleted = 0";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

}
