<input type="hidden" value="<?php echo date('Y') ?>" id="year" class="form-control" />
<input type="hidden" value="<?php echo $grafik_spk['data'] ?>" id="data-spk" class="form-control" />
<input type="hidden" value="<?php echo $grafik_spk['total'] ?>" id="total-data-spk" class="form-control" />


<div class="row">
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h3><?php echo $count_spk_oustanding ?></h3>

				<p>Total Spk Outstanding</p>
			</div>
			<div class="icon">
				<i class="ion ion-person"></i>
			</div>
			<a href="<?php echo base_url() . 'document' ?>" class="small-box-footer"> Transaksi <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				<h3><?php echo $count_spk_oustanding ?></h3>

				<p>Total Spk Rilis</p>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="<?php echo base_url() . 'document' ?>" class="small-box-footer"> Transaksi <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
				<h3><?php echo $count_pengguna ?></h3>

				<p>Total Pengguna</p>
			</div>
			<div class="icon">
				<i class="ion ion-android-menu"></i>
			</div>
			<a href="<?php echo base_url()  ?>user" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<h3><?php echo $count_spk_oustanding ?></h3>

				<p>Total SPK</p>
			</div>
			<div class="icon">
				<i class="ion ion-pie-graph"></i>
			</div>
			<a href="<?php echo base_url() . 'document' ?>" class="small-box-footer">Transaksi <i class="fa fa-arrow-circle-right"></i></a>		</div>
	</div>
	<!-- ./col -->
</div>
