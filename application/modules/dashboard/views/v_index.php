<?php echo $this->load->view('top_dashboard', array(), true); ?>


<div class="content">
  <div class="animated fadeIn">
    <div class="row">
      <div class="col-md-12">
        <div class="box padding-16">
          <div class="box-header with-border">
            <div class="row">
              <div class="col-md-10">
                <div class="box-title">
                  <i class="mdi mdi-chart-bar mdi-18px"></i><strong class="card-title"><?php echo 'Grafik SPK' ?></strong>
                </div>
              </div>
              <div class="col-sm-2 text-right"></div>
            </div>
          </div>

          <div class="box-body chart-responsive">
            <div class="chart" id="line-chart" style="height: 300px;"></div>
          </div>
        </div>
      </div>

    </div>
    <div class="box padding-16">
      <div class="box-header with-border">
        <div class="row">
          <div class="col-md-10">
            <div class="box-title">
              <i class="mdi mdi-chart-bar mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
            </div>
          </div>
          <div class="col-sm-2 text-right"></div>
        </div>
      </div>
      <div class="box-body">

        <div class='row'>
          <div class='col-md-12'>
            <h4><u>Top 5 SPK</u></h4>
            <br />
            <div class='table-responsive'>
              <table class="table table-bordered">
                <thead>
                  <tr class="bg-primary">
                    <th>No</th>
                    <th>No Dokumen</th>
                    <th>No Rab</th>
                    <th>Pekerjaan</th>
                    <th>Tipe</th>
                    <th>Vendor</th>
                    <th>Tgl Pelaksanaan</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1 ?>
                  <?php foreach ($data_spk_oustanding as $key => $value) {?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $value['external_id'] ?></td>
                      <td><?php echo $value['type'] ?></td>
                      <td><?php echo $value['no_rab'] ?></td>
                      <td><?php echo $value['judul_pekerjaan'] ?></td>
                      <td><?php echo $value['nama_vendor'] ?></td>
                      <td><?php echo $value['tgl_pelaksanaan'] ?> s/d <?php echo $value['tgl_selesai_pekerjaan'] ?></td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="box padding-16">
      <div class="box-body">

        <div class='row'>
          <div class='col-md-12'>
            <h4><u>Data Supplier</u></h4>
            <br />
            <div class='table-responsive'>
              <table class="table table-bordered" id="tb_data_supplier">
                <thead>
                  <tr class="bg-primary">
                    <th>No</th>
                    <th>Nama Vendor</th>
                    <th>Nama Pimpinan</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1 ?>
                  <?php foreach ($list_supplier as $key => $value) {?>
                    <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $value['nama_vendor'] ?></td>
                      <td><?php echo $value['nama_pimpinan'] ?></td>
                      <td><?php echo $value['email'] ?></td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>