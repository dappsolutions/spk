<?php

class Dashboard extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function getModuleName()
	{
		return 'dashboard';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<link rel="stylesheet" href="'.base_url().'assets/css/datatable/dataTables.bootstrap.min.css">',
			'<script src="' . base_url() . 'assets/js/data-table/dataTables.bootstrap.min.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'v_index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Dashboard";
		$data['title_content'] = 'Dashboard';
		$data['count_spk_oustanding'] = $this->getTotalData();
		$data['count_pengguna'] = $this->getTotalPengguna();
		$data['data_spk_oustanding'] = $this->getData();
		$data['grafik_spk'] = $this->getDataGrafikSpk();
		$data['list_supplier'] = $this->getDataSupplier();
		// echo '<pre>';
		// print_r($data);die;
		$data['date_now'] = Modules::run('helper/getIndoDate', date('Y-m-d'), false);
		echo Modules::run('template', $data);
	}

	public function getDataSupplier(){
			$sql = "select * from vendor where deleted = 0";
			$data= $this->db->query($sql);
			$result = [];
			if(!empty($data->result_array())){
				$result = $data->result_array();
			}
			return $result;
	}

	public function getDataGrafikSpk()
	{

		$filterUpt = " and p.upt = '" . $_SESSION['upt'] . "'";
		if ($_SESSION['hak_akses'] == 'superadmin') {
			$filterUpt = "";
		}

		$date = date('Y') . '-';

		$result = array();
		$total = 0;
		$total_max = 0;
		for ($i = 1; $i <= 12; $i++) {
			$i = $i < 10 ? '0' . $i : $i;
			$date_str = $date . $i;
			$query = "SELECT d.* FROM document d
			join actor a 
				on a.id = d.createdby 
			join `user` u 
				on u.id = a.`user` 
			join pegawai p 
				on p.id = u.pegawai 
			WHERE d.createddate like '%".$date_str."%' 
			and d.deleted = 0 ".$filterUpt;

			// echo '<pre>';
			// echo $query;die;



			$data = Modules::run("database/get_custom", $query);


			$result[] = empty($data) ? 0 : count($data->result_array());
			$total += empty($data) ? 0 : count($data->result_array());

			if (!empty($data)) {				
					$total_max = count($data->result_array());
			}
		}
		$str = implode(',', $result);
		return array(
			'data' => $str,
			'total' => $total_max
		);
	}

	public function getTotalData()
	{
		$filterUpt = " and pg.upt = '" . $_SESSION['upt'] . "'";
		if ($_SESSION['hak_akses'] == 'superadmin') {
			$filterUpt = "";
		}

		$like = array();
		$total = Modules::run('database/count_all', array(
			'table' =>  'document v',
			'field' => array('v.*', 'sf.judul_pekerjaan', 'sf.no_rab', 'vd.nama_vendor', 'sf.tgl_pelaksanaan', 'sf.tgl_selesai_pekerjaan'),
			'join' => array(
				array('submmission_form sf', 'sf.document = v.id'),
				array('vendor vd', 'vd.id = sf.vendor'),
				array('(select max(id) id, document from document_transaction group by document) dts_max', 'dts_max.document = v.id', 'left'),
				array('document_transaction dts', 'dts.id = dts_max.id'),
				array('actor act', 'act.id = v.createdby'),
				array('user us', 'us.id = act.user'),
				array('pegawai pg', 'pg.id = us.pegawai'),
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "v.deleted = 0 " . $filterUpt
		));

		return $total;
	}

	public function getData()
	{
		$filterUpt = " and pg.upt = '" . $_SESSION['upt'] . "'";
		if ($_SESSION['hak_akses'] == 'superadmin') {
			$filterUpt = "";
		}

		$like = array();
		$data = Modules::run('database/get', array(
			'table' =>  'document v',
			'field' => array('v.*', 'sf.judul_pekerjaan', 'sf.no_rab', 'vd.nama_vendor', 'sf.tgl_pelaksanaan', 'sf.tgl_selesai_pekerjaan'),
			'join' => array(
				array('submmission_form sf', 'sf.document = v.id'),
				array('vendor vd', 'vd.id = sf.vendor'),
				array('(select max(id) id, document from document_transaction group by document) dts_max', 'dts_max.document = v.id', 'left'),
				array('document_transaction dts', 'dts.id = dts_max.id'),
				array('actor act', 'act.id = v.createdby'),
				array('user us', 'us.id = act.user'),
				array('pegawai pg', 'pg.id = us.pegawai'),
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "v.deleted = 0 " . $filterUpt,
			'orderby' => 'v.id desc',
			'limit' => 5
		));

		$result = [];
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				$result[] = $value;
			}
		}

		return $result;
	}

	public function getTotalPengguna()
	{
		$like = array();
		$total = Modules::run('database/count_all', array(
			'table' =>  'user v',
			'field' => array('v.*'),
			'like' => $like,
			'is_or_like' => true,
			'where' => "v.deleted = 0 "
		));

		return $total;
	}
}
