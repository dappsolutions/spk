	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Data</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<?php if (isset($menu_akses->jenis_anggaran)) { ?>
					<li><a href="<?php echo base_url() . 'jenis_anggaran' ?>"><i class="fa fa-file-text-o"></i> Jenis Anggaran </a></li>
				<?php } ?>
				<?php if (isset($menu_akses->wilayah)) { ?>
					<li><a href="<?php echo base_url() . 'wilayah' ?>"><i class="fa fa-file-text-o"></i> Wilayah </a></li>
				<?php } ?>
				<?php if (isset($menu_akses->pegawai)) { ?>
					<li><a href="<?php echo base_url() . 'pegawai' ?>"><i class="fa fa-file-text-o"></i> Organisasi Project </a></li>
				<?php } ?>
				<?php if (isset($menu_akses->supplier)) { ?>
					<li><a href="<?php echo base_url() . 'supplier' ?>"><i class="fa fa-file-text-o"></i> Supplier </a></li>
				<?php } ?>
				<?php if (isset($menu_akses->hari_libur)) { ?>
					<li><a href="<?php echo base_url() . 'hari_libur' ?>"><i class="fa fa-file-text-o"></i> Hari Libur </a></li>
				<?php } ?>
				<?php if (isset($menu_akses->skk_no)) { ?>
					<li><a href="<?php echo base_url() . 'skk_no' ?>"><i class="fa fa-file-text-o"></i> SKK </a></li>
				<?php } ?>
				<?php if (isset($menu_akses->jadwal)) { ?>
					<li><a href="<?php echo base_url() . 'jadwal' ?>"><i class="fa fa-file-text-o"></i> Jadwal </a></li>
				<?php } ?>
				<?php if (isset($menu_akses->agen)) { ?>
					<li><a href="<?php echo base_url() . 'agen' ?>"><i class="fa fa-file-text-o"></i> Agen </a></li>
				<?php } ?>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>HPS</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<?php if (isset($menu_akses->satuan)) { ?>
					<li><a href="<?php echo base_url() . 'satuan' ?>"><i class="fa fa-file-text-o"></i> Satuan</a></li>
				<?php } ?>
				<?php if (isset($menu_akses->produk)) { ?>
					<li><a href="<?php echo base_url() . 'produk' ?>"><i class="fa fa-file-text-o"></i> Produk </a></li>
				<?php } ?>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Transaksi</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<?php if (isset($menu_akses->document)) { ?>
					<li><a href="<?php echo base_url() . 'document' ?>"><i class="fa fa-file-text-o"></i> Dokumen </a></li>
				<?php } ?>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'laporan_spk' ?>"><i class="fa fa-file-text-o"></i> Laporan Spk</a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Pengaturan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<?php if (isset($menu_akses->hak_akses)) { ?>
					<li><a href="<?php echo base_url() . 'hak_akses' ?>"><i class="fa fa-file-text-o"></i> Hak Akses</a></li>
				<?php } ?>
				<?php if (isset($menu_akses->user)) { ?>
					<li><a href="<?php echo base_url() . 'user' ?>"><i class="fa fa-file-text-o"></i> Pengguna</a></li>
				<?php } ?>
				<?php if (isset($menu_akses->ppn)) { ?>
					<li><a href="<?php echo base_url() . 'ppn' ?>"><i class="fa fa-file-text-o"></i> PPN</a></li>
				<?php } ?>				
			</ul>
		</li>



		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>