<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/fastclick/lib/fastclick.js"></script>
<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/select2/dist/js/select2.full.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url() ?>assets/plugins/components/timepicker/bootstrap-timepicker.min.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/admin_lte/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/number-divider.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/url.js"></script>
<script src="<?php echo base_url() ?>assets/js/message.js"></script>
<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/bootbox.js"></script>

<?php
	if (isset($header_data)) {
		foreach ($header_data as $v_head) {
			echo $v_head;
		}
	}
	?>
<!-- AdminLTE for demo purposes -->
<!--<script src="<?php echo base_url() ?>assets/admin_lte/dist/js/demo.js"></script>-->

<script type="text/javascript">
	var url_link = window.location.href;
	// for sidebar menu entirely but not cover treeview
	$('ul.sidebar-menu a').filter(function() {
		return this.href == url_link;
	}).parent().addClass('active');
	// for treeview
	$('ul.treeview-menu a').filter(function() {
		return this.href == url_link;
	}).closest('.treeview').addClass('active');
</script>
