<!DOCTYPE html>
<html>
<?php echo $this->load->view('head_content'); ?>

<body class="hold-transition skin-blue fixed sidebar-mini">
	<div class="loader">

	</div>
	<div class="wrapper">

		<?php echo $this->load->view('navbar_content') ?>
		<!-- Left side column. contains the logo and sidebar -->
		<?php echo $this->load->view('sidebar_content') ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<section class="content-header">
				<h1>
					<?php echo $title ?>
					<small><?php echo 'SPK' ?></small>
				</h1>
				<ol class="breadcrumb">
					<li><a href="#"><i class="fa fa-file-text"></i> <?php echo 'Data' ?></a></li>
					<li><a href="#"><?php echo $title_content ?></a></li>
				</ol>
			</section>
			<!-- Main content -->
			<section class="content">
				<?php echo $this->load->view($module . '/' . $view_file); ?>
			</section>
			<!-- /.content -->
		</div>

		<!-- /.content-wrapper -->
		<?php echo $this->load->view('footer_content') ?>
	<?php echo $this->load->view('script_content') ?>
</body>

</html>
