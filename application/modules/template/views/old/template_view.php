<html>
 <?php echo Modules::run('header'); ?>
 <?php
 if (isset($header_data)) {
  foreach ($header_data as $v_head) {
   echo $v_head;
  }
 }
 ?>
 <body class="bg-content open">
  <div class="box-content">
   <?php echo $this->load->view('left_content'); ?>
   <div class="loader"></div>
   <div id="right-panel" class="right-panel">
    <?php echo $this->load->view('header_content'); ?>
    <!--Message-->
    <div class="body-content">
     <div class="message"></div>
     <?php echo $this->load->view($module . '/' . $view_file); ?>
    </div>
   </div>
  </div>
 </body>
</html>
