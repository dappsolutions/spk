<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SPK | <?php echo ucfirst($title) ?></title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>assets/images/logo/logo_apps.png" />
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/lib/mdi/css/materialdesignicons.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/select2/dist/css/select2.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
      folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/skins/_all-skins.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/css-loader.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
	<link href="<?php echo base_url() ?>assets/plugins/components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url() ?>assets/plugins/components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
	<style>
		.td_jsa {
			border: 1px solid black;
			font-family: tahoma;
			font-size: 12px;
			text-align: center;
			padding: 8px;
		}
	</style>
</head>