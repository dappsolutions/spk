<?php

class Template extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('user_id') == '') {
			redirect(base_url());
		}
	}

	public function index($data)
	{
		$data['menu_akses'] = json_decode($this->session->userdata('list_akses'));
		echo $this->load->view('template_view', $data, true);
	}
}
