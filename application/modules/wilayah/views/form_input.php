<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Unit</label>
   <select name="" id="upt" class="required form-control" error="Unit">
    <option value="">-- PILIH UNIT -- </option>
    <?php foreach ($list_upt as $key => $value) {?>
     <option value="<?php echo $value['id'] ?>" <?php echo isset($upt) ? $upt == $value['id'] ? 'selected' : '' : '' ?>><?php echo $value['nama_upt'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Kode Wilayah</label>
   <input type="text" id="kode_wilayah" class="form-control required" error="Kode Wilayah" value="<?php echo isset($kode_wilayah) ? $kode_wilayah : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Wilayah</label>
   <input type="text" id="nama_wilayah" class="form-control required" error="Nama Wilayah" value="<?php echo isset($nama_wilayah) ? $nama_wilayah : '' ?>">
  </div>
 </div>
</div>
