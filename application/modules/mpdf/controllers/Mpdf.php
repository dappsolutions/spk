<?php

class Mpdf extends MX_Controller {

 public function getInitPdf() {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  $mpdf = new \Mpdf\Mpdf();
  return $mpdf;
 }

 public function getInitPdfPaperA5() {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  //190mm wide x 236mm height
  $mpdf = new \Mpdf\Mpdf(array(
      'format' => [215, 140]
  ));

//  echo '<pre>';
//  var_dump(get_class_methods($mpdf));die;
  return $mpdf;
 }
 
 public function getInitPdfPaperCustom() {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  //190mm wide x 236mm height
  $mpdf = new \Mpdf\Mpdf(array(
      'format' => [160, 210]
  ));

//  echo '<pre>';
//  var_dump(get_class_methods($mpdf));die;
  return $mpdf;
 }

 public function initTCPDF() {
  error_reporting(0);
  require_once APPPATH . '/libraries/tcpdf/tcpdf.php'; //php 7

  $pdf = new TCPDF('L', 'mm', array(215, 140));

  $pdf->writeHTML($html, $ln);
  return $pdf;
 }

}
