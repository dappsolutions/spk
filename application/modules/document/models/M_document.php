<?php

class M_document extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getDetailProdukSatuan($params)
	{
		$sql = "
		select 
		phs.*
		from produk_has_satuan phs
		where phs.produk = " . $params['material_id'] . "
		and phs.satuan = " . $params['satuan_id'] . "
		and phs.deleted = 0";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDetailSatuanMaster($params)
	{
		$sql = "
		select s.*
		from satuan s 
		where s.nama_satuan like '%" . $params['satuan_name'] . "%'
		and s.deleted = 0
		";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDetailDataProduk($params)
	{
		$sql = "
		select * from produk
		where nama_produk = '" . $params['material_name'] . "'
		and deleted = 0
		";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getListDireksiLapangan($params)
	{
		$sql = "select 
		sfhdl.*
		, p.nama_pegawai 
		, p.nip 
		, p.jabatan
		from submission_form_has_direksi_lapangan sfhdl
		join pegawai p 
			on p.id = sfhdl.employee 
		where sfhdl.submmission_form = " . $params['sf_id'] . "
		and sfhdl.deleted = 0
		";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function getListPengawasLapangan($params)
	{
		$sql = "select 
		sfhdl.*
		, p.nama_pegawai 
		, p.nip 
		, p.jabatan
		from submission_form_has_pengawas sfhdl
		join pegawai p 
			on p.id = sfhdl.employee 
		where sfhdl.submmission_form = " . $params['sf_id'] . "
		and sfhdl.deleted = 0
		";

		$data = $this->db->query($sql);
		$result = [];
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}

	public function getListDetailDataJadwal($params)
	{

		$sql = "
		select 
		sts.*
		from schedule_transaction_submission sts
		where sts.document = " . $params['id'];

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$result[] = $value;
			}
		}

		return $result;
	}

	public function getListDetailDataRab($params)
	{

		$sql = "
		select 
		rab.*
		, p.nama_produk 
		, s.nama_satuan 
		, p.id as produk_id
		, s.id as satuan_id
		, qs.satuan_penawaran
		, qs.jumlah_penawaran
		, qs.harga_sepakat
		, qs.jumlah_sepakat
		from rab_submission rab
		join produk_has_satuan phs
			on phs.id = rab.produk_has_satuan 
			and phs.deleted = 0
		join produk p
			on p.id = phs.produk 
			and p.deleted = 0
		join satuan s 
			on s.id = phs.satuan 
			and s.deleted = 0
		left join quote_submission qs
			on qs.rab_submission = rab.id
		where rab.document = " . $params['id'] . " and rab.deleted = 0";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				$result[] = $value;
			}
		}

		return $result;
	}

	public function getDataQuoteSubmission($rab)
	{

		$sql = "select * from quote_submission qs where rab_submission = " . $rab;

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getDetailDataPajak()
	{

		$sql = "select * from ppn where deleted = 0";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function getListDetailDataRekanan($params =  [])
	{

		$sql = "select ri.* 
		, v.nama_vendor 
		, v.nama_pimpinan 
		, v.jabatan_pimpinan 
		, v.no_rekening 
		, v.nama_bank 
		, v.alamat 
		from rekanan_invitation ri 
		join vendor v 
			on v.id = ri.vendor
		where ri.deleted = 0
		and ri.document = ".$params['id'];

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}
}
