<?php

class Document extends MX_Controller
{

  public $segment;
  public $limit;
  public $page;
  public $last_no;
  public $menu_akses;

  public function __construct()
  {
    parent::__construct();
    $this->limit = 10;
    $this->load->model('jadwal/m_jadwal', 'jadwal');
    $this->load->model('m_document', 'document');
    $this->menu_akses = json_decode($this->session->userdata('list_akses'));
  }

  public function getModuleName()
  {
    return 'document';
  }

  public function getHeaderJSandCSS()
  {
    //versioning
    $version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
    $version = substr($version, 0, 11);
    //versioning

    $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/document.js?v=' . $version . '"></script>'
    );

    return $data;
  }

  public function getTableName()
  {
    return 'document';
  }

  public function index()
  {
    $this->segment = 3;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;

    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Dokumen";
    $data['title_content'] = 'Data Dokumen';
    $content = $this->getData();
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['menu_akses'] = $this->menu_akses;
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function getTotalData($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('v.external_id', $keyword),
        array('v.type', $keyword),
        array('sf.judul_pekerjaan', $keyword),
        array('sf.no_rab', $keyword),
        array('vd.nama_vendor', $keyword),
        array('sf.tgl_pelaksanaan', $keyword),
        array('sf.tgl_selesai_pekerjaan', $keyword),
      );
    }
    $total = Modules::run('database/count_all', array(
      'table' => $this->getTableName() . ' v',
      'field' => array('v.*', 'sf.judul_pekerjaan', 'sf.no_rab', 'vd.nama_vendor', 'sf.tgl_pelaksanaan', 'sf.tgl_selesai_pekerjaan'),
      'join' => array(
        array('submmission_form sf', 'sf.document = v.id'),
        array('type_transaction tt', 'sf.type_transaction = tt.id'),
        array('vendor vd', 'vd.id = sf.vendor'),
        array('(select max(id) id, document from document_transaction group by document) dts_max', 'dts_max.document = v.id', 'left'),
        array('document_transaction dts', 'dts.id = dts_max.id'),
      ),
      'like' => $like,
      'is_or_like' => true,
      'where' => "v.deleted = 0"
    ));

    return $total;
  }

  public function getData($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('v.external_id', $keyword),
        array('v.type', $keyword),
        array('sf.judul_pekerjaan', $keyword),
        array('sf.no_rab', $keyword),
        array('vd.nama_vendor', $keyword),
        array('sf.tgl_pelaksanaan', $keyword),
        array('sf.tgl_selesai_pekerjaan', $keyword),
      );
    }
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' v',
      'field' => array('v.*', 'sf.judul_pekerjaan', 'sf.no_rab', 'vd.nama_vendor', 'sf.tgl_pelaksanaan', 'sf.tgl_selesai_pekerjaan', 'dts.state', 'tt.term_id as transaksi_id'),
      'join' => array(
        array('submmission_form sf', 'sf.document = v.id'),
        array('type_transaction tt', 'sf.type_transaction = tt.id'),
        array('vendor vd', 'vd.id = sf.vendor'),
        array('(select max(id) id, document from document_transaction group by document) dts_max', 'dts_max.document = v.id', 'left'),
        array('document_transaction dts', 'dts.id = dts_max.id'),
      ),
      'like' => $like,
      'is_or_like' => true,
      'limit' => $this->limit,
      'offset' => $this->last_no,
      'where' => "v.deleted = 0"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        $value['bg_proses'] = "";
        if($value['state'] == 'DRAFT'){
          $value['bg_proses'] = "#fff";
        }
        if($value['state'] == 'JADWAL_SUBMISSION'){
          $value['bg_proses'] = "#fcf8e3";
        }
        if($value['state'] == 'RAB_SUBMISSION'){
          $value['bg_proses'] = "#d9edf7";
        }
        if($value['state'] == 'QUOTE_SUBMISSION'){
          $value['bg_proses'] = "#dff0d8";
        }
        array_push($result, $value);
      }
    }

    return array(
      'data' => $result,
      'total_rows' => $this->getTotalData($keyword)
    );
  }

  public function getDetailData($id)
  {
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' v',
      'field' => array(
        'v.*', 'sf.judul_pekerjaan', 'sf.no_rab', 'vd.nama_vendor', 'vd.nama_pimpinan', 'sf.tgl_pelaksanaan', 'sf.tgl_selesai_pekerjaan', 'tt.tipe as format_pengadaan', 'tw.tipe_pekerjaan as sifat_pekerjaan', 'tb.tipe_anggaran', 'sn.no_skk', 'sf.tanggal_rab', 'sf.id as sf_id', 'pt.tipe_pengadaan', 'sf.type_transaction', 'sf.type_of_work', 'sf.vendor', 'sf.type_of_budget', 'sf.skk_no', 'sf.procurement_type', 'sf.id as sf_id', 'sf.lama_pekerjaan', 'ut.nama_upt', 'vd.alamat', 'ut.alamat as alamat_upt', 'vd.no_rekening', 'vd.nama_bank', 'tt.term_id as transaksi_id',
        'sf.sub_bidang_usaha', 'sbu.sub_bidang', 'vd.alamat as alamat_vendor', 'vd.jabatan_pimpinan', 'sf.file_rab',
        'ds.pegawai as pegawai_pengesahan', 'ds.file_kak', 'ds.file_rpb'
      ),
      'join' => array(
        array('submmission_form sf', 'sf.document = v.id'),
        array('vendor vd', 'vd.id = sf.vendor'),
        array('type_transaction tt', 'tt.id = sf.type_transaction'),
        array('type_of_work tw', 'tw.id = sf.type_of_work'),
        array('type_of_budget tb', 'tb.id = sf.type_of_budget'),
        array('skk_no sn', 'sn.id = sf.skk_no'),
        array('procurement_type pt', 'pt.id = sf.procurement_type'),
        array('actor a', 'a.id = v.createdby'),
        array('user u', 'u.id = a.user'),
        array('pegawai pg', 'pg.id = u.pegawai'),
        array('upt ut', 'ut.id = pg.upt'),
        array('sub_bidang_usaha sbu', 'sbu.id = sf.sub_bidang_usaha', 'left'),
        array('dpl_submission ds', 'ds.document = v.id', 'left'),
      ),
      'where' => "v.id = '" . $id . "'"
    ));

    return $data->row_array();
  }

  public function getListSupplier()
  {
    $data = Modules::run('database/get', array(
      'table' => 'vendor v',
      'where' => "v.deleted  = 0"
    ));

    return $data->result_array();
  }

  public function getListPegawai()
  {
    $data = Modules::run('database/get', array(
      'table' => 'pegawai v',
      'where' => "v.deleted  = 0"
    ));

    return $data->result_array();
  }

  public function getListTransaksi($type = "")
  {
    $where = "v.term_id = '".$type."'";
    $data = Modules::run('database/get', array(
      'table' => 'type_transaction v',
      'where'=> $where
    ));

    return $data->result_array();
  }

  public function getListPekerjaan()
  {
    $data = Modules::run('database/get', array(
      'table' => 'type_of_work v',
    ));

    return $data->result_array();
  }

  public function getListPengadaan()
  {
    $data = Modules::run('database/get', array(
      'table' => 'procurement_type v',
    ));

    return $data->result_array();
  }

  public function getListAnggaran()
  {
    $data = Modules::run('database/get', array(
      'table' => 'type_of_budget v',
      'where' => 'v.deleted = 0'
    ));

    return $data->result_array();
  }

  public function getListNoSkk()
  {
    $data = Modules::run('database/get', array(
      'table' => 'skk_no v',
      'where' => 'v.deleted = 0'
    ));

    return $data->result_array();
  }
  
  public function getListSubBadanUSaha()
  {
    $data = Modules::run('database/get', array(
      'table' => 'sub_bidang_usaha v',
    ));

    return $data->result_array();
  }

  public function getListDayOff()
  {
    $data = Modules::run('database/get', array(
      'table' => 'day_off v',
      'where' => 'v.deleted = 0'
    ));

    return $data->result_array();
  }

  public function getListMaterial()
  {
    $data = Modules::run('database/get', array(
      'table' => 'produk_has_satuan phs',
      'field' => array('phs.*', 'p.nama_produk'),
      'join' => array(
        array('produk p', 'p.id = phs.produk'),
        array('satuan s', 's.id = phs.satuan'),
      ),
      'where' => 'phs.deleted = 0'
    ));

    // echo '<pre>';
    // print_r($this->db->last_query());die;

    $result = [];
    if (!empty($data->result_array())) {
      $temp = [];
      foreach ($data->result_array() as $key => $value) {
        if (!in_array($value['produk'], $temp)) {
          $result[] = $value;
          $temp[] = $value['produk'];
        }
      }
    }

    return $result;
  }

  public function getListSatuan($produk = "")
  {
    $data = Modules::run('database/get', array(
      'table' => 'produk_has_satuan phs',
      'field' => array('s.*', 'phs.harga_jual'),
      'join' => array(
        array('produk p', 'p.id = phs.produk'),
        array('satuan s', 's.id = phs.satuan'),
      ),
      'where' => "phs.deleted = 0 and p.id = " . $produk
    ));

    $result = [];
    if (!empty($data->result_array())) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }

    return $result;
  }

  public function getListSatuanAll()
  {
    $sql = "select distinct `s`.*, `phs`.`harga_jual`, `p`.`id` as `produk_id`
    FROM `produk_has_satuan` `phs`
    JOIN `produk` `p` ON `p`.`id` = `phs`.`produk`
    JOIN `satuan` `s` ON `s`.`id` = `phs`.`satuan`
    WHERE `phs`.`deleted` =0
    -- and p.id = 5
     LIMIT 1000";

    $data = $this->db->query($sql);
    // echo '<pre>';
    // print_r($this->db->last_query());die;

    $result = [];
    if (!empty($data->result_array())) {
      foreach ($data->result_array() as $key => $value) {
        $result[] = $value;
      }
    }

    return $result;
  }

  public function add()
  {
    $data['view_file'] = 'form_add_edit_view';
    $data['jenis_transaksi'] = 'SUB_SPBJL';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Tambah Dokumen";
    $data['title_content'] = 'Tambah Dokumen';
    $data['data_jadwal'] = $this->jadwal->getListDataJadwal('SUB_SPBJL');
    $data['list_direksi'] = array();
    $data['list_pengawas'] = array();
    $data['data_rab'] = array();
    $data['list_vendor'] = $this->getListSupplier();
    $data['list_transaksi'] = $this->getListTransaksi('SUB_SPBJL');
    $data['list_pekerjaan'] = $this->getListPekerjaan();
    $data['list_pengadaan'] = $this->getListPengadaan();
    $data['list_anggaran'] = $this->getListAnggaran();
    $data['list_skk'] = $this->getListNoSkk();
    $data['list_bidang_usaha'] = $this->getListSubBadanUSaha();
    $data['list_dayoff'] = json_encode($this->getListDayOff());
    echo Modules::run('template', $data);
  }
  
  public function addSpk()
  {
    $data['view_file'] = 'form_add_edit_view';
    $data['jenis_transaksi'] = 'SUB_SPK';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Tambah Dokumen";
    $data['title_content'] = 'Tambah Dokumen';
    $data['data_jadwal'] = $this->jadwal->getListDataJadwal('SUB_SPK');
    $data['list_direksi'] = array();
    $data['list_pengawas'] = array();
    $data['data_rab'] = array();
    $data['list_vendor'] = $this->getListSupplier();
    $data['list_transaksi'] = $this->getListTransaksi('SUB_SPK');
    $data['list_pekerjaan'] = $this->getListPekerjaan();
    $data['list_pengadaan'] = $this->getListPengadaan();
    $data['list_anggaran'] = $this->getListAnggaran();
    $data['list_skk'] = $this->getListNoSkk();
    $data['list_bidang_usaha'] = $this->getListSubBadanUSaha();
    $data['list_dayoff'] = json_encode($this->getListDayOff());
    $data['list_pegawai'] = $this->getListPegawai();
    echo Modules::run('template', $data);
  }

  public function ubah($id)
  {
    $data = $this->getDetailData($id);
    // echo '<pre>';
    // print_r($data);die;
    $data['view_file'] = 'form_add_edit_view';
    $data['jenis_transaksi'] = $data['transaksi_id'];
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Ubah Dokumen";
    $data['title_content'] = 'Ubah Dokumen';
    $data['list_vendor'] = $this->getListSupplier();
    $data['list_transaksi'] = $this->getListTransaksi($data['transaksi_id']);
    $data['list_pekerjaan'] = $this->getListPekerjaan();
    $data['list_pengadaan'] = $this->getListPengadaan();
    $data['list_anggaran'] = $this->getListAnggaran();
    $data['list_skk'] = $this->getListNoSkk();
    $data['list_dayoff'] = json_encode($this->getListDayOff());
    $data['list_direksi'] = $this->document->getListDireksiLapangan($data);
    $data['list_pengawas'] = $this->document->getListPengawasLapangan($data);
    $data['data_jadwal'] = $this->document->getListDetailDataJadwal($data);
    $data['data_rab'] = $this->document->getListDetailDataRab($data);
    $data['data_rekanan'] = $this->document->getListDetailDataRekanan($data);
    // echo '<pre>';
    // print_r($data['data_rekanan']);die;
    $data['material'] = $this->getListMaterial();
    $data['satuan'] = $this->getListSatuanAll();
    $data['list_bidang_usaha'] = $this->getListSubBadanUSaha();
    $data['list_pegawai'] = $this->getListPegawai();
    // echo '<pre>';
    // print_r($data['data_rab']);die;
    echo Modules::run('template', $data);
  }

  public function detail($id)
  {
    $data = $this->getDetailData($id);
    $data['jenis_transaksi'] = $data['transaksi_id'];
    $data['list_direksi'] = $this->document->getListDireksiLapangan($data);
    $data['list_pengawas'] = $this->document->getListPengawasLapangan($data);
    $data['data_jadwal'] = $this->document->getListDetailDataJadwal($data);
    $data['data_rab'] = $this->document->getListDetailDataRab($data);
    $data['data_rekanan'] = $this->document->getListDetailDataRekanan($data);
    // echo '<pre>';
    // print_r($data);die;
    $data['view_file'] = 'detail_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Detail Dokumen";
    $data['title_content'] = 'Detail Dokumen';
    echo Modules::run('template', $data);
  }

  public function getPostDataHeader($value)
  {
    $data['nama_vendor'] = $value->nama;
    $data['no_hp'] = $value->no_hp;
    $data['alamat'] = $value->alamat;
    $data['keterangan'] = $value->keterangan;
    $data['email'] = $value->email;
    $data['status'] = $value->status;
    $data['vendor_category'] = 2;
    return $data;
  }

  public function getProdukSatuan($params)
  {
    $produkSatuanId = "";
    if ($params['status'] == 'auto') {
      //cek sudah ada di db
      $dataProdukSatuan = $this->document->getDetailProdukSatuan($params);
      if (!empty($dataProdukSatuan)) {
        $produkSatuanId = $dataProdukSatuan['id'];
      }
    } else {
      //insert produk
      $produk = "";
      $dataProdukMaster = $this->document->getDetailDataProduk($params);
      if (!empty($dataProdukMaster)) {
        $produk = $dataProdukMaster['id'];
      } else {
        $push = [];
        $push['nama_produk'] = $params['material_name'];
        $push['kode_produk'] = '-';
        $push['createddate'] = date('Y-m-d H:i:s');
        $this->db->insert('produk', $push);
        $produk = $this->db->insert_id();
      }

      //cek satuan
      $dataSatuan = $this->document->getDetailSatuanMaster($params);
      $satuanId = "";
      if (!empty($dataSatuan)) {
        $satuanId = $dataSatuan['id'];
      } else {
        //insert satuan
        $push = [];
        $push['nama_satuan'] = $params['satuan_name'];
        $push['createddate'] = date('Y-m-d H:i:s');
        $this->db->insert('satuan', $push);
        $satuanId = $this->db->insert_id();
      }

      $push = [];
      $push['produk'] = $produk;
      $push['satuan'] = $satuanId;
      $push['harga_beli'] = $params['satuan_hps'];
      $push['harga_jual'] = $params['satuan_hps'];
      $push['createddate'] = date('Y-m-d H:i:s');
      $this->db->insert('produk_has_satuan', $push);
      $produkSatuanId = $this->db->insert_id();
    }
    return $produkSatuanId;
  }

  public function uploadData($name_of_field, $dir)
  {
    $config['upload_path'] = 'files/berkas/' . $dir . '/';
    $config['allowed_types'] = 'png|jpg|jpeg|pdf';
    $config['max_size'] = '1000';
    $config['max_width'] = '6000';
    $config['max_height'] = '6000';
    $config['encrypt_name'] = TRUE;


    $this->load->library('upload', $config);

    $is_valid = false;
    if (!$this->upload->do_upload($name_of_field)) {
      $response = $this->upload->display_errors();
    } else {
      $response = $this->upload->data();
      $is_valid = true;
    }

    // echo '<pre>';
    // print_r($response);die;

    return array(
      'is_valid' => $is_valid,
      'response' => $response
    );
  }

  public function saveAll()
  {
    $data =(array) json_decode($this->input->post('data'));
    $data['form_dokumen'] = (array)$data['form_dokumen'];
    $data['form_jadwal'] = (array)$data['form_jadwal'];
    $data['form_dpl'] = (array)$data['form_dpl'];
    $data['form_rab'] = (array)$data['form_rab'];
    $data['form_penawaran'] = (array)$data['form_penawaran'];
    $data['form_rekanan'] = (array)$data['form_rekanan'];
    $file = $_FILES;
    // echo '<pre>';
    // print_r($_FILES);
    // die;
    $id = $data['id'];
   
    $result['is_valid'] = false;
    $is_uploaded = true;
    //uploade rab
    $file_rab = '';
    if (!empty($file)) {
      if (isset($file['file_rab'])) {
        $response_upload = $this->uploadData('file_rab', 'rab');
        if (!$response_upload['is_valid']) {
          $is_uploaded = false;
          $result['message'] .= $response_upload['response'];
        } else {
          $file_rab = $response_upload['response']['file_name'];
        }
      }
    }
    //uploade rab
    
    //uploade kak
    $file_kak = '';
    if (!empty($file)) {
      if (isset($file['file_kak'])) {
        $response_upload = $this->uploadData('file_kak', 'kak');
        if (!$response_upload['is_valid']) {
          $is_uploaded = false;
          $result['message'] .= $response_upload['response'];
        } else {
          $file_kak = $response_upload['response']['file_name'];
        }
      }
    }
    //uploade kak
    
    //uploade rpb
    $file_rpb = '';
    if (!empty($file)) {
      if (isset($file['file_rpb'])) {
        $response_upload = $this->uploadData('file_rpb', 'rpb');
        if (!$response_upload['is_valid']) {
          $is_uploaded = false;
          $result['message'] .= $response_upload['response'];
        } else {
          $file_rpb = $response_upload['response']['file_name'];
        }
      }
    }
    //uploade rpb

    // echo $file_rab;die;
    //uploade rab
    $this->db->trans_begin();
    try {
      $date = date('Y-m-d H:i:s');
      if ($id == '') {
        $noDocument = Modules::run('no_generator/generateNoDocument', 'DOC');
        $push = [];
        $push['user'] = $this->session->userdata('user_id');
        $push['createddate'] = $date;
        $this->db->insert('actor', $push);
        $actorId = $this->db->insert_id();

        $doc = [];
        $doc['external_id'] = $noDocument;
        if($data['jenis_transaksi'] == 'SUB_SPBJL'){
          $doc['type'] = 'DOCT_SPBJL';
        }
        if($data['jenis_transaksi'] == 'SUB_SPK'){
          $doc['type'] = 'DOCT_SPK';
        }
        $doc['createddate'] = $date;
        $doc['createdby'] = $actorId;
        $doc['transaksi'] = 'document';
        $this->db->insert('document', $doc);
        $doc_id = $this->db->insert_id();
        $id = $doc_id;

         //insert document transaction
         $push = [];
         $push['document'] = $doc_id;
         $push['createddate'] = $date;
         $push['createdby'] = $actorId;
         $push['state'] = 'DRAFT';
         $this->db->insert('document_transaction', $push);

        $postSub = array();
        $postSub['document'] = $doc_id;
        $postSub['type_transaction'] = $data['form_dokumen']['format_pengadaan'];
        $postSub['type_of_work'] = $data['form_dokumen']['sifat_pekerjaan'];
        $postSub['judul_pekerjaan'] = $data['form_dokumen']['judul_pekerjaan'];
        $postSub['lama_pekerjaan'] = $data['form_dokumen']['lama_pekerjaan'];
        $postSub['vendor'] = $data['form_dokumen']['vendor'];
        $postSub['no_rab'] = $data['form_dokumen']['no_rab'];
        $postSub['tanggal_rab'] = $data['form_dokumen']['tgl_rab'];
        $postSub['tgl_pelaksanaan'] = $data['form_dokumen']['tanggal_awal'];
        $postSub['tgl_selesai_pekerjaan'] = $data['form_dokumen']['tanggal_selesai'];
        $postSub['type_of_budget'] = $data['form_dokumen']['jenis_anggaran'];
        $postSub['procurement_type'] = $data['form_dokumen']['jenis_pengadaan_id'];
        $postSub['skk_no'] = $data['form_dokumen']['no_skk'];
        $postSub['createddate'] = $date;
        $postSub['createdby'] = $actorId;
        if($data['form_dokumen']['sub_bidang_usaha'] != ''){
          $postSub['sub_bidang_usaha'] = $data['form_dokumen']['sub_bidang_usaha'];
        }
        if($file_rab != ''){
          $postSub['file_rab'] = $file_rab;
        }
        $this->db->insert('submmission_form', $postSub);
        $submission_id = $this->db->insert_id();

        $push = [];
        $push['document'] = $doc_id;
        $push['createddate'] = $date;
        $push['createdby'] = $actorId;
        $push['state'] = 'SUBMISSION_FORM';
        $this->db->insert('document_transaction', $push);

        //submission_form_has_direksi_lapangan
        if (isset($data['form_dokumen']['data_direksi_lap'])) {
          foreach ($data['form_dokumen']['data_direksi_lap'] as $key => $value) {
            $value = (array) $value;
            $push = array();
            $push['employee'] = $value['pegawai_id'];
            $push['submmission_form'] = $submission_id;
            $this->db->insert('submission_form_has_direksi_lapangan', $push);
          }
        }

        //submission_form_has_pengawas
        if (isset($data['form_dokumen']['data_pengawas_lap'])) {
          foreach ($data['form_dokumen']['data_pengawas_lap'] as $key => $value) {
            $value = (array) $value;
            $push = array();
            $push['employee'] = $value['pegawai_id'];
            $push['submmission_form'] = $submission_id;
            $this->db->insert('submission_form_has_pengawas', $push);
          }
        }

        //schedule_transaction_submission
        if (isset($data['form_jadwal']['data'])) {
          foreach ($data['form_jadwal']['data'] as $key => $value) {
            $value = (array) $value;
            $push = [];
            $push['no'] = $value['no'];
            $push['uraian_pekerjaan'] = $value['uraian_kegiatan'];
            $push['tanggal'] = $value['tanggal'];
            $push['nomor_pekerjaan'] = $value['no_dokumen'];
            $push['standard'] = $value['standard'];
            if (isset($value['manual'])) {
              $push['manual'] = $value['manual'];
            }
            $push['cepat'] = $value['cepat'];
            $push['document'] = $doc_id;
            $push['createddate'] = $date;
            $push['createdby'] = $actorId;
            $this->db->insert('schedule_transaction_submission', $push);
          }

          $push = [];
          $push['document'] = $doc_id;
          $push['createddate'] = $date;
          $push['createdby'] = $actorId;
          $push['state'] = 'JADWAL_SUBMISSION';
          $this->db->insert('document_transaction', $push);
        }

        //rab_submission
        if (isset($data['form_rab']['data'])) {
          foreach ($data['form_rab']['data'] as $key => $value) {
            $value = (array) $value;
            $push = [];
            $push['jml_vol'] = $value['jml_vol'];
            $push['satuan_anggaran'] = $value['satuan_anggaran'];
            $push['jumlah_anggaran'] = $value['jumlah_anggaran'];
            $push['satuan_hps'] = $value['satuan_hps'];
            $push['jumlah_hps'] = $value['jumlah_hps'];
            $push['document'] = $doc_id;
            $push['produk_has_satuan'] = $this->getProdukSatuan($value);
            $this->db->insert('rab_submission', $push);
          }

          $push = [];
          $push['document'] = $doc_id;
          $push['createddate'] = $date;
          $push['createdby'] = $actorId;
          $push['state'] = 'RAB_SUBMISSION';
          $this->db->insert('document_transaction', $push);

          //insert form dpl
          if(isset($data['form_dpl'])){
            if($data['form_dpl']){              
              if($data['form_dpl']['approval_pengesahan'] != ''){
                $push = [];
                $push['document'] = $doc_id;
                $push['pegawai'] = $data['form_dpl']['approval_pengesahan'];
                if($file_kak != ''){
                  $push['file_kak'] = $file_kak;
                }
                if($file_rpb != ''){
                  $push['file_rpb'] = $file_rpb;
                }
                $this->db->insert('dpl_submission', $push);
              }
            }
          }
        }       
      } else {
        //update
        $postSub = array();
        $postSub['document'] = $id;
        $postSub['type_transaction'] = $data['form_dokumen']['format_pengadaan'];
        $postSub['type_of_work'] = $data['form_dokumen']['sifat_pekerjaan'];
        $postSub['judul_pekerjaan'] = $data['form_dokumen']['judul_pekerjaan'];
        $postSub['lama_pekerjaan'] = $data['form_dokumen']['lama_pekerjaan'];
        $postSub['vendor'] = $data['form_dokumen']['vendor'];
        $postSub['no_rab'] = $data['form_dokumen']['no_rab'];
        $postSub['tanggal_rab'] = $data['form_dokumen']['tgl_rab'];
        $postSub['tgl_pelaksanaan'] = $data['form_dokumen']['tanggal_awal'];
        $postSub['tgl_selesai_pekerjaan'] = $data['form_dokumen']['tanggal_selesai'];
        $postSub['type_of_budget'] = $data['form_dokumen']['jenis_anggaran'];
        $postSub['procurement_type'] = $data['form_dokumen']['jenis_pengadaan_id'];
        $postSub['skk_no'] = $data['form_dokumen']['no_skk'];
        if($data['form_dokumen']['sub_bidang_usaha'] != ''){
          $postSub['sub_bidang_usaha'] = $data['form_dokumen']['sub_bidang_usaha'];
        }
        if($file_rab != ''){
          $postSub['file_rab'] = $file_rab;
        }
        $this->db->update('submmission_form', $postSub, array('document' => $id));
        $submission_id = $data['form_dokumen']['submission_form_id'];

        //submission_form_has_direksi_lapangan
        if (isset($data['form_dokumen']['data_direksi_lap'])) {
          foreach ($data['form_dokumen']['data_direksi_lap'] as $key => $value) {
            $value = (array) $value;
            $push = array();
            $push['employee'] = $value['pegawai_id'];
            $push['submmission_form'] = $submission_id;
            if ($value['id'] == '') {
              $this->db->insert('submission_form_has_direksi_lapangan', $push);
            } else {
              $push['deleted'] = $value['remove'];
              $this->db->update('submission_form_has_direksi_lapangan', $push, array('id' => $value['id']));
            }
          }
        }

        //submission_form_has_pengawas
        if (isset($data['form_dokumen']['data_pengawas_lap'])) {
          foreach ($data['form_dokumen']['data_pengawas_lap'] as $key => $value) {
            $value = (array) $value;
            $push = array();
            $push['employee'] = $value['pegawai_id'];
            $push['submmission_form'] = $submission_id;
            if ($value['id'] == '') {
              $this->db->insert('submission_form_has_pengawas', $push);
            } else {
              $push['deleted'] = $value['remove'];
              $this->db->update('submission_form_has_pengawas', $push, array('id' => $value['id']));
            }
          }
        }

        //schedule_transaction_submission
        if (isset($data['form_jadwal']['data'])) {
          foreach ($data['form_jadwal']['data'] as $key => $value) {
            $value = (array) $value;
            $push = [];
            $push['no'] = $value['no'];
            $push['uraian_pekerjaan'] = $value['uraian_kegiatan'];
            $push['tanggal'] = $value['tanggal'];
            $push['nomor_pekerjaan'] = $value['no_dokumen'];
            $push['standard'] = $value['standard'];
            if (isset($value['manual'])) {
              $push['manual'] = $value['manual'];
            }
            $push['cepat'] = $value['cepat'];
            $push['document'] = $id;
            $push['createddate'] = $date;
            $this->db->update('schedule_transaction_submission', $push, array('id' => $value['data_transaksi_id']));
          }
        }

        //rab_submission
        if (isset($data['form_rab']['data'])) {
          foreach ($data['form_rab']['data'] as $key => $value) {
            $value = (array) $value;
            $push = [];
            $push['jml_vol'] = $value['jml_vol'];
            $push['satuan_anggaran'] = $value['satuan_anggaran'];
            $push['jumlah_anggaran'] = $value['jumlah_anggaran'];
            $push['satuan_hps'] = $value['satuan_hps'];
            $push['jumlah_hps'] = $value['jumlah_hps'];
            $push['document'] = $id;
            $push['produk_has_satuan'] = $this->getProdukSatuan($value);
            if($value['id'] == ''){
              $this->db->insert('rab_submission', $push);
            }else{
              $push['deleted'] = $value['remove'];
              $this->db->update('rab_submission', $push, array('id'=> $value['id']));
            }
          }
        }

        //quote submission
        if(isset($data['form_penawaran']['data'])){
          foreach ($data['form_penawaran']['data'] as $key => $value) {
            $value = (array) $value;
            $push = [];
            $push['rab_submission'] = $value['id'];
            $push['satuan_penawaran'] = $value['satuan_penawaran'];
            $push['jumlah_penawaran'] = $value['jumlah_penawaran'];
            $push['harga_sepakat'] = $value['satuan_sepakat'];
            $push['jumlah_sepakat'] = $value['jumlah_sepakat'];
            $push['createddate'] = $date;
            $dataQuoteSubmission = $this->document->getDataQuoteSubmission($value['id']);
            if(empty($dataQuoteSubmission)){
              $this->db->insert('quote_submission', $push);
            }else{
              $push['deleted'] = $value['remove'];
              $this->db->update('quote_submission', $push, array('rab_submission'=> $value['id']));
            }
          }

          //insert form dpl
          if(isset($data['form_dpl'])){
            if($data['form_dpl']){              
              if($data['form_dpl']['approval_pengesahan'] != ''){
                $push = [];
                $push['document'] = $id;
                $push['pegawai'] = $data['form_dpl']['approval_pengesahan'];
                if($file_kak != ''){
                  $push['file_kak'] = $file_kak;
                }
                if($file_rpb != ''){
                  $push['file_rpb'] = $file_rpb;
                }
                $this->db->insert('dpl_submission', $push);
              }
            }
          }

          $push = [];
          $push['document'] = $id;
          $push['createddate'] = $date;
          $push['state'] = 'QUOTE_SUBMISSION';
          $this->db->insert('document_transaction', $push);           
        }
      }


      //insert rekanan
      if(!empty($data['form_rekanan']['data'])){
        foreach ($data['form_rekanan']['data'] as $key => $value) {
          $value = (array) $value;
          $push = [];
          $push['document'] = $id;
          $push['vendor'] = $value['vendor'];
          $push['createddate'] = $date;
          if($value['remove'] == 1){
            $push['deleted'] = 1;
          }
          if($value['id'] == ''){
            $this->db->insert('rekanan_invitation', $push);
          }else{
            $this->db->update('rekanan_invitation', $push, array('id'=> $value['id']));
          }
        }
      }
      //insert rekanan

      if (!$is_uploaded) {
        $this->db->trans_rollback();
      } else {
        $this->db->trans_commit();
        $result['is_valid'] = true;
      }
    } catch (Exception $ex) {
      $result['message'] = $ex->getMessage();
      $this->db->trans_rollback();
    }
    $result['id'] = $id;

    echo json_encode($result);
  }

  public function search($keyword)
  {
    $this->segment = 4;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;
    $keyword = urldecode($keyword);

    $data['keyword'] = $keyword;
    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Supplier";
    $data['title_content'] = 'Data Supplier';
    $content = $this->getData($keyword);
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function delete($id)
  {
    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
      $this->db->trans_commit();
      $is_valid = true;
    } catch (Exception $ex) {
      $this->db->trans_rollback();
    }

    echo json_encode(array('is_valid' => $is_valid));
  }

  public function showListPegawai()
  {
    $data = $_POST;
    $data['list_pegawai'] = $this->getListPegawai();
    echo $this->load->view('list_pegawai', $data, true);
  }

  public function addRab()
  {
    $data = $_POST;
    $data['material'] = $this->getListMaterial();
    echo $this->load->view('content_rab', $data, true);
  }

  public function getDetailSertifikatLainVendor($params){
    $sql = "";
  }
  
  public function getVendorEvaluasi()
  {
    $data = $_POST;
    $data['data'] = $this->getDetailSertifikatLainVendor($data);
    echo $this->load->view('content_rab', $data, true);
  }

  public function getSatuanProduk()
  {
    $data = $_POST;
    $data['satuan'] = $this->getListSatuan($data['produk']);
    echo $this->load->view('list_satuan', $data, true);
  }


  public function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	public function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
	}

  public function cetak($id)
	{
		$mpdf = Modules::run('mpdf/getInitPdf');
		// $mpdf = Modules::run('mpdf/getInitPdfPaperCustom');
    // echo '<pre>';
    // print_r(get_class_methods($mpdf));die;
    $data = [];
    $data = $this->getDetailData($id);
    $data['list_direksi'] = $this->document->getListDireksiLapangan($data);
    // echo '<pre>';
    // print_r($data);die;
    $data['list_pengawas'] = $this->document->getListPengawasLapangan($data);
    $data['data_jadwal'] = $this->document->getListDetailDataJadwal($data);
    $data['data_rab'] = $this->document->getListDetailDataRab($data);
    $data['data_pajak'] = $this->document->getDetailDataPajak();
    $data['ppn'] = 0.1;
    if(!empty($data['data_pajak'])){
      $data['ppn'] = $data['data_pajak']['pajak'] / 100;
    }
		$cover = $this->load->view('cetak/cover', $data, true);
    $jadwalPengadaan = $this->load->view('cetak/jadwal_pengadaan', $data, true);
    $dokumenHps = $this->load->view('cetak/form_hps', $data, true);
    $penawaran_harga = $this->load->view('cetak/penawaran_harga', $data, true);
    $ba_nego = $this->load->view('cetak/ba_nego', $data, true);
    $dokumen_spbjl = $this->load->view('cetak/dokumen_spbjl', $data, true);
    
    // echo '<pre>';
    // print_r($data['data_pajak']);die;
    $mpdf->packTableData = true;
    $mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($cover);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($jadwalPengadaan);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($dokumenHps);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($penawaran_harga);	
		$mpdf->AddPage('L');	
		$mpdf->WriteHTML($ba_nego);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($dokumen_spbjl);	

		$mpdf->Output('DOKUMEN_SPBJL.pdf', 'I');
	}
  
  public function cetakSpk($id)
	{
		$mpdf = Modules::run('mpdf/getInitPdf');
    $mpdf->packTableData = true;
		// $mpdf = Modules::run('mpdf/getInitPdfPaperCustom');
    // echo '<pre>';
    // print_r(get_class_methods($mpdf));die;
    $data = [];
    $data = $this->getDetailData($id);
    $data['list_direksi'] = $this->document->getListDireksiLapangan($data);
    // echo '<pre>';
    // print_r($data);die;
    $data['list_pengawas'] = $this->document->getListPengawasLapangan($data);
    $data['data_jadwal'] = $this->document->getListDetailDataJadwal($data);
    $data['data_rab'] = $this->document->getListDetailDataRab($data);
    $data['data_rekanan'] = $this->document->getListDetailDataRekanan($data);   
    // echo '<pre>';
    // print_r($data['data_rab']);die;
    $data['data_pajak'] = $this->document->getDetailDataPajak();
    $data['ppn'] = 0.1;
    if(!empty($data['data_pajak'])){
      $data['ppn'] = $data['data_pajak']['pajak'] / 100;
    }

    $data['total_harga_pln'] = 0;
    $data['total_anggaran'] = 0;
    $data['total_penawaran'] = 0;    
    $data['total_harga_sepakat'] = 0;    
    foreach ($data['data_rab'] as $key => $value) {
      $data['total_harga_pln'] += $value['jumlah_hps'];
      $data['total_anggaran'] += $value['jumlah_anggaran'];
      $data['total_penawaran'] += $value['jumlah_penawaran'];
      $data['total_harga_sepakat'] += $value['jumlah_sepakat'];
    }
    $data['total_anggaran'] = ($data['total_anggaran'] * $data['ppn']) + $data['total_anggaran'];
    $data['total_penawaran'] = ($data['total_penawaran'] * $data['ppn']) + $data['total_penawaran'];
    $data['total_harga_sepakat'] = ($data['total_harga_sepakat'] * $data['ppn']) + $data['total_harga_sepakat'];
    // echo '<pre>';
    // print_r($data);die;
		$cover = $this->load->view('cetakspk/cover', $data, true);
    $jadwalPengadaan = $this->load->view('cetakspk/jadwal_pengadaan', $data, true);
    $dokumen_pengadaan_langsung = $this->load->view('cetakspk/document_pengadaan_langsung', $data, true);
    $dokumen_pengadaan_langsung_second = $this->load->view('cetakspk/dokument_pengadaan_langsung_second', $data, true);
    $dokumen_pengadaan_langsung_third = $this->load->view('cetakspk/document_pengadaan_langsung_third', $data, true);
    $document_pengadaan_langsung_fourth = $this->load->view('cetakspk/document_pengadaan_langsung_fourth', $data, true);
    $document_pengadaan_langsung_fifth = $this->load->view('cetakspk/document_pengadaan_langsung_fifth', $data, true);
    $document_pengadaan_langsung_sixth = $this->load->view('cetakspk/document_pengadaan_langsung_sixth', $data, true);
    $dokumenHps = $this->load->view('cetakspk/form_hps', $data, true);
    $document_daftar_rekanan = $this->load->view('cetakspk/document_daftar_rekanan', $data, true);
    $document_bap = $this->load->view('cetakspk/document_bap', $data, true);
    $penawaran_harga = $this->load->view('cetakspk/penawaran_harga', $data, true);
    $document_pembukaan = $this->load->view('cetakspk/document_pembukaan', $data, true);
    $document_eva = $this->load->view('cetakspk/document_eva', $data, true);
    $document_nego = $this->load->view('cetakspk/document_nego', $data, true);
    $ba_nego = $this->load->view('cetakspk/ba_nego', $data, true);
    $document_spk_barang = $this->load->view('cetakspk/document_spk_barang', $data, true);
    $document_spk_barang_second = $this->load->view('cetakspk/document_spk_barang_second', $data, true);
    // $dokumen_spbjl = $this->load->view('cetakspk/dokumen_spk', $data, true);
    
    // echo '<pre>';
    // print_r($data['data_pajak']);die;
    $mpdf->packTableData = true;
    $mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($cover);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($jadwalPengadaan);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($dokumen_pengadaan_langsung);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($dokumen_pengadaan_langsung_second);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($dokumen_pengadaan_langsung_third);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_pengadaan_langsung_fourth);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_pengadaan_langsung_fifth);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_pengadaan_langsung_sixth);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($dokumenHps);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($penawaran_harga);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_daftar_rekanan);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_bap);	
		$mpdf->AddPage('L');	
		$mpdf->WriteHTML($document_pembukaan);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_eva);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_nego);	
		$mpdf->AddPage('L');	
		$mpdf->WriteHTML($ba_nego);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_spk_barang);	
		$mpdf->AddPage();	
		$mpdf->WriteHTML($document_spk_barang_second);	

		$mpdf->Output('DOKUMEN_SPK.pdf', 'I');
	}

  public function showFile()
  {
    $data = $_POST;
    echo $this->load->view('file/file_rab', $data, true);
  }
  
  public function showFileKak()
  {
    $data = $_POST;
    echo $this->load->view('file/file_kak', $data, true);
  }
  
  public function showFileRpb()
  {
    $data = $_POST;
    // echo '<pre>';
    // print_r($data);die;
    echo $this->load->view('file/file_rpb', $data, true);
  }

  public function tesExcel(){
    echo $this->load->view('tesexcel');
  }

  public function tesIsi(){
    echo $this->load->view('tesisi');
  }

  public function loadDoc(){
    require_once APPPATH . '/third_party/office/autoload.php'; //php 7
    $base = APPPATH.'document/';
    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($base.'spk_doc.docx');
    $templateProcessor->setValues([
      'nim'=> '1318033',
      'nama'=> 'Dodik Rismawan Affrudin',
      'alamat' => 'Blitar'
    ]);

    $no_trans = "DOC_SPK";
    header("Content-Disposition:attatchment;filename=".$no_trans.".docx");
    $templateProcessor->saveAs("php://output");
  }
}
