<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='day-off' class='form-control' value='<?php echo isset($list_dayoff) ? $list_dayoff : '' ?>' />

<div class="row">
  <div class="col-md-12">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#form-pengadaan" data-toggle="tab"><label class="label label-warning">1</label>&nbsp;Proses Pengadaan</a></li>
        <li><a href="#form-penawaran" data-toggle="tab"><label class="label label-warning">2</label> Penawaran</a></li>
        <li><a href="#form-negosiasi" data-toggle="tab"><label class="label label-warning">3</label> Negosiasi</a></li>
        <li><a href="#form-dokumen" data-toggle="tab"><label class="label label-warning">4</label> Dokumen</a></li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="form-pengadaan">
          <ul class="timeline">
            <li class="time-label">
              <span class="bg-yellow">
                Form Submission
              </span>
            </li>
            <li>
              <i class="fa fa-sticky-note bg-blue"></i>
              <div class="timeline-item">
                <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                <h3 class="timeline-header"><a href="#">Form Dokumen</a> </h3>
                <div class="timeline-body">
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->load->view('detail_form_dokumen_awal'); ?>                      
                    </div>
                    <div class="col-md-12 text-right">
                      <!-- <button class="btn btn-success" idexport="excel-form-document" onclick="Document.exportExcel(this, event)">Export</button> -->
                      <a class="btn btn-success" idexport="excel-form-document" onclick="Document.tableToExcel(this, 'table-excel-form-document', 'W3C Example Table')">Export</a>
                    </div>
                  </div>
                </div>

                <div class="timeline-footer">
                  <hr>
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <!-- <button class="btn btn-default" onclick="Document.simpan(this)">Submit</button> -->
                    </div>
                  </div>
                </div>
              </div>
            </li>

            <li>
              <!-- timeline icon -->
              <i class="fa fa-sticky-note bg-blue"></i>
              <!-- <div class="timeline-item" style="background-color: #f4f4f4;"> -->
              <div class="timeline-item">
                <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                <h3 class="timeline-header"><a href="#">Form Jadwal</a> </h3>
                <div class="timeline-body">
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->load->view('detail_form_dokumen_jadwal'); ?>
                    </div>
                  </div>
                </div>

                <div class="timeline-footer">
                  <hr>
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <!-- timeline icon -->
              <i class="fa fa-sticky-note bg-blue"></i>
              <!-- <div class="timeline-item" style="background-color: #f4f4f4;" > -->
              <div class="timeline-item">
                <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                <h3 class="timeline-header"><a href="#">Form RAB</a> </h3>
                <div class="timeline-body">
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->load->view('detail_form_dokumen_rab'); ?>
                    </div>
                  </div>
                </div>

                <div class="timeline-footer">
                  <hr>
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <!-- timeline icon -->
              <i class="fa fa-sticky-note bg-blue"></i>
              <!-- <div class="timeline-item" style="background-color: #f4f4f4;" > -->
              <div class="timeline-item">
                <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                <h3 class="timeline-header"><a href="#">Form DPL</a> </h3>
                <div class="timeline-body">
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->load->view('detail_form_dpl') ?>
                    </div>
                  </div>
                </div>

                <div class="timeline-footer">
                  <hr>
                  <div class="row">
                    <div class="col-md-12 text-right">
                      <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                    </div>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </div>

        <div class="tab-pane" id="form-penawaran">
          <ul class="timeline">
            <li class="time-label">
              <span class="bg-yellow">
                Form Submission
              </span>
            </li>
            <?php if ($jenis_transaksi == 'SUB_SPK') { ?>
              <li>
                <!-- timeline icon -->
                <i class="fa fa-sticky-note bg-blue"></i>
                <div class="timeline-item">
                  <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                  <h3 class="timeline-header"><a href="#">Form Rekanan Diundang</a> </h3>
                  <div class="timeline-body">
                    <div class="row">
                      <div class="col-md-12">
                        <?php echo $this->load->view('detail_rekanan_diundang');
                        ?>
                      </div>
                    </div>
                  </div>

                  <div class="timeline-footer">
                    <hr>
                    <div class="row">
                      <div class="col-md-12 text-right">
                        <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            <?php } ?>

            <li>
              <!-- timeline icon -->
              <i class="fa fa-sticky-note bg-blue"></i>
              <div class="timeline-item">
                <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                <h3 class="timeline-header"><a href="#">Form Penawaran Harga</a> </h3>
                <div class="timeline-body">
                  <div class="row">
                    <div class="col-md-12">
                      <?php echo $this->load->view('detail_dokumen_penawaran');
                      ?>
                    </div>
                  </div>
                </div>

                <?php if ($jenis_transaksi == 'SUB_SPBJL') { ?>
                  <div class="timeline-footer">
                    <hr>
                    <div class="row">
                      <div class="col-md-12 text-right">
                        <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </li>
            <!-- END timeline item -->
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<?php echo $this->load->view('table_export_spbjl') ?>

<div class="row">
  <div class="col-md-12 text-right">
    <!-- <a href="#" class="btn btn-success" download="<?php echo 'Dokumen_SPBJL_' . $id ?>.xls" onclick="return ExcellentExport.excel(this, 'content-spbjl-data', 'Dokumen_SPBJL_<?php echo $id ?>');">Export</a> -->
    <a href="#" class="btn btn-success" onclick="Document.exportExcel(this, event)">Export</a>
    <button class="btn btn-default" onclick="Document.back()">Kembali</button>
  </div>
</div>