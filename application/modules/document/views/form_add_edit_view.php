<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='submission-form-id' class='form-control' value='<?php echo isset($sf_id) ? $sf_id : '' ?>' />
<input type='hidden' name='' id='no-rab-hide' class='form-control' value='<?php echo isset($no_rab) ? $no_rab : '' ?>' />
<input type='hidden' name='' id='day-off' class='form-control' value='<?php echo isset($list_dayoff) ? $list_dayoff : '' ?>' />
<input type='hidden' name='' id='jenis_transaksi' class='form-control' value='<?php echo isset($jenis_transaksi) ? $jenis_transaksi : '' ?>' />

<div class="row">
  <div class="col-md-12">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#form-pengadaan" data-toggle="tab"><label class="label label-warning">1</label>&nbsp;Proses Pengadaan</a></li>
        <li><a href="#form-penawaran" data-toggle="tab"><label class="label label-warning">2</label> Penawaran</a></li>
        <?php if ($jenis_transaksi == 'SUB_SPK') { ?>
          <li><a href="#form-negosiasi" data-toggle="tab"><label class="label label-warning">3</label> Negosiasi</a></li>
          <li><a href="#form-dokumen" data-toggle="tab"><label class="label label-warning">4</label> Dokumen</a></li>
        <?php } ?>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="form-pengadaan">
          <div class="row">
            <div class="col-md-12">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-yellow">
                    Form Submission
                  </span>
                </li>
                <!-- /.timeline-label -->

                <!-- timeline item -->
                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Form Dokumen</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_dokumen_awal'); ?>
                        </div>
                      </div>
                    </div>

                    <div class="timeline-footer">
                      <hr>
                      <div class="row">
                        <div class="col-md-12 text-right">
                          <!-- <button class="btn btn-default" onclick="Document.simpan(this)">Submit</button> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </li>


                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <!-- <div class="timeline-item" style="background-color: #f4f4f4;"> -->
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Form Jadwal</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_dokumen_jadwal'); ?>
                        </div>
                      </div>
                    </div>

                    <div class="timeline-footer">
                      <hr>
                      <div class="row">
                        <div class="col-md-12 text-right">
                          <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </li>

                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <!-- <div class="timeline-item" style="background-color: #f4f4f4;" > -->
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Form HPS</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_dokumen_rab'); ?>
                        </div>
                      </div>
                    </div>

                    <div class="timeline-footer">
                      <hr>
                      <div class="row">
                        <div class="col-md-12 text-right">
                          <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </li>

                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <!-- <div class="timeline-item" style="background-color: #f4f4f4;" > -->
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Form DPL</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_dpl') ?>
                        </div>
                      </div>
                    </div>

                    <div class="timeline-footer">
                      <hr>
                      <div class="row">
                        <div class="col-md-12 text-right">
                          <!-- <button class="btn btn-default" onclick="Document.simpanJadwal(this)">Submit</button> -->
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="form-penawaran">
          <div class="row">
            <div class="col-md-12">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-yellow">
                    Form Submission
                  </span>
                </li>

                <?php if ($jenis_transaksi == 'SUB_SPK') { ?>
                  <li>
                    <!-- timeline icon -->
                    <i class="fa fa-sticky-note bg-blue"></i>
                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                      <h3 class="timeline-header"><a href="#">Form Rekanan Diundang</a> </h3>
                      <div class="timeline-body">
                        <div class="row">
                          <div class="col-md-12">
                            <?php echo $this->load->view('form_dokumen_rekanan'); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li>
                    <!-- timeline icon -->
                    <i class="fa fa-sticky-note bg-blue"></i>
                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                      <h3 class="timeline-header"><a href="#">Undangan Vendor</a> </h3>
                      <div class="timeline-body">
                        <div class="row">
                          <div class="col-md-12">
                            -
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>

                  <li>
                    <!-- timeline icon -->
                    <i class="fa fa-sticky-note bg-blue"></i>
                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                      <h3 class="timeline-header"><a href="#">BAP dan Bukti Pengambilan</a> </h3>
                      <div class="timeline-body">
                        <div class="row">
                          <div class="col-md-12">
                            -
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                <?php } ?>

                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Form Penawaran Harga</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_dokumen_penawaran'); ?>
                        </div>
                      </div>
                    </div>

                    <?php if ($jenis_transaksi == 'SUB_SPBJL') { ?>
                      <div class="timeline-footer">
                        <hr>
                        <div class="row">
                          <div class="col-md-12 text-right">
                            <button class="btn btn-default" onclick="Document.back()">Kembali</button>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="form-negosiasi">
          <div class="row">
            <div class="col-md-12">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-yellow">
                    Form Submission
                  </span>
                </li>

                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Evaluasi</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_evaluasi') ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Negosiasi</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_negosiasi') ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Lampiran Negosiasi</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <?php echo $this->load->view('form_lampiran_negosiasi') ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="form-dokumen">
          <div class="row">
            <div class="col-md-12">
              <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                  <span class="bg-yellow">
                    Form Submission
                  </span>
                </li>

                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">SPKPJ</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <button class="btn btn-primary">Export Doc</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <!-- timeline icon -->
                  <i class="fa fa-sticky-note bg-blue"></i>
                  <div class="timeline-item">
                    <span class="time"><i class="fa fa-sticky-note-o"></i> </span>
                    <h3 class="timeline-header"><a href="#">Lampiran SPKPJ</a> </h3>
                    <div class="timeline-body">
                      <div class="row">
                        <div class="col-md-12">
                          <button class="btn btn-primary">Export Doc</button>
                        </div>
                      </div>
                    </div>
                    <div class="timeline-footer">
                      <hr>
                      <div class="row">
                        <div class="col-md-12 text-right">
                          <button class="btn btn-default" onclick="Document.back()">Kembali</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="row">
  <div class="col-md-12">
    <a href="#" class="float" onclick="Document.saveAll()">
      <i class="fa fa-check my-float fa-lg"></i>
    </a>
  </div>
</div>



<style>
  .td_jsa {
    border: 1px solid black;
    font-family: tahoma;
    font-size: 12px;
    text-align: center;
    padding: 8px;
  }

  .th_jsa {
    border: 1px solid black;
    font-family: tahoma;
    font-size: 12px;
    text-align: center;
    padding: 8px;
    /* background-color: #fafafa; */
  }
</style>