<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table style="width: 100%;">
    <thead>
     <tr>
      <th rowspan="2" class="th_jsa">Nama Perusahaan</th>
      <th rowspan="2" class="th_jsa">Syarat penulisan pada sampul penawaran</th>
      <th rowspan="2" class="th_jsa">Tanda tangan penanggung jawab/cap perusahaan diatas materai</th>
      <th rowspan="2" class="th_jsa">Tanggal Surat Penawaran</th>
      <th rowspan="2" class="th_jsa">Penulisan Penawaran Dengan Angka dan Huruf</th>
      <th rowspan="2" class="th_jsa">Batas Waktu Surat Penawaran Harga</th>
      <th rowspan="2" class="th_jsa">Nilai Penawaran Harga Rp</th>
      <th rowspan="2" class="th_jsa">Batas Waktu Penyerahan Barang</th>
      <th colspan="8" class="th_jsa">Foto Copy</th>
      <th rowspan="2" class="th_jsa">Nomor Rekening Perusahaan</th>
      <th rowspan="2" class="th_jsa">Susunan Pengurus Perusahaan</th>
      <th rowspan="2" class="th_jsa">Pakta Integritas</th>
      <th rowspan="2" class="th_jsa">Keterangan</th>
     </tr>
     <tr>
      <th class="th_jsa">Akte Pendirian & Perubahan</th>
      <th class="th_jsa">SIUP Sesuai Pekerjaan Terkait</th>
      <th class="th_jsa">NPWP & PKP</th>
      <th class="th_jsa">Surat Keterangan Domisili</th>
      <th class="th_jsa">TDP/NIB</th>
      <th class="th_jsa">AK3 Umum</th>
      <th class="th_jsa">SBUJK</th>
      <th class="th_jsa"></th>
     </tr>
    </thead>
    <tbody>
     <tr>
      <td class="td_jsa" id="supplier-evaluasi"></td>
      <td class="td_jsa">
       <select name="" id="surat_penulisan_penawaran">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="ttd_penanggung_jawab">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="tgl_surat_ph">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="penulisan_ph_huruf">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="batas_waktu_ph">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="nilai_ph">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="batas_waktu_pb">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="akta_pendirian">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="siup_pekerjaan">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="npwp_pkp">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="surat_ket_domisili">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="tdp_nib">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="ak3_umum">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="sbjujk">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa"></td>
      <td class="td_jsa">
       <select name="" id="no_rek_perusahaan">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="susunan_pengurus_perusahaan">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <select name="" id="pakta_integritas">
        <option value="A">A</option>
        <option value="TA">TA</option>
       </select>
      </td>
      <td class="td_jsa">
       <p>A = Ada</p>
       <p>TA = Tidak Ada</p>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>

