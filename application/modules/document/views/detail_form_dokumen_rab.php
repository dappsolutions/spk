<div class="row">
	<div class="col-md-12">
		<div class="row content-input-rab-file">
			<div class="col-md-6 <?php echo isset($file_rab) ? $file_rab != '' ? 'hide' : '' : ''  ?>">
				<div class="form-group" id="file_input_rab">
					<label for="">Upload Rab</label>
					<input type="file" id="file_rab" class="form-control" onchange="Document.checkFile(this)">
				</div>
			</div>


			<div class="col-md-6 <?php echo isset($file_rab) ? $file_rab != '' ? '' : 'hide' : 'hide'  ?>">
				<div class="form-group " id="detail_file_rab">
					<label for="">File Rab</label>
					<div class="input-group">
						<input disabled="" type="text" id="file_str_rab" class="form-control"
							value="<?php echo isset($file_rab) ? $file_rab : '' ?>">
						<span class="input-group-addon">
							<i class="fa fa-image hover-content" file="<?php echo isset($file_rab) ? $file_rab : '' ?>"
								onclick="Document.showFileRab(this, event)"></i>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-rab">
    <thead>
     <tr class="bg-info">
      <th rowspan="2" class="text-center">Uraian</th>
      <th colspan="2" class="text-center">Volume</th>
      <th colspan="2" class="text-center">Harga Pada Anggaran Biaya Operasi</th>
      <th colspan="2" class="text-center">Harga HPS Pln</th>
     </tr>
     <tr class="bg-info">
      <th class="text-center">Jml</th>
      <th class="text-center">Sat</th>
      <th class="text-center">Satuan Rp</th>
      <th class="text-center">Jumlah Rp</th>
      <th class="text-center">Satuan Rp</th>
      <th class="text-center">Jumlah Rp</th>
     </tr>
    </thead>
    <tbody>
     <?php foreach ($data_rab as $key => $value) { ?>
      <tr>
       <td><?php echo $value['nama_produk'] ?></td>
       <td><?php echo $value['jml_vol'] ?></td>
       <td><?php echo $value['nama_satuan'] ?></td>
       <td><?php echo $value['satuan_anggaran'] ?></td>
       <td><?php echo $value['jumlah_anggaran'] ?></td>
       <td><?php echo $value['satuan_hps'] ?></td>
       <td><?php echo $value['jumlah_hps'] ?></td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>