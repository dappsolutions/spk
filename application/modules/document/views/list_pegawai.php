<div class="row">
 <div class="col-md-12">
  <div class="panel panel-default">
   <div class="panel-heading">
    <h5>Daftar Pegawai</h5>
   </div>
   <div class="panel-body">
    <div class="row">
     <div class="col-md-12">
      <input type="text" placeholder="Cari Data Pegawai" class="form-control" onkeyup="Document.searchPegawai(this, event)">
     </div>
    </div>

    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table table-bordered">
        <thead>
         <tr class="bg-warning">
          <th>NIP</th>
          <th>Nama Pegawai</th>
          <th></th>
         </tr>
        </thead>
        <tbody>
         <?php foreach ($list_pegawai as $key => $value) {?>
          <tr data_id="<?php echo $value['id'] ?>">
           <td id="nip"><?php echo $value['nip'] ?></td>
           <td id="nama_pegawai"><?php echo $value['nama_pegawai'] ?></td>
           <td class="text-center">
            <a href="" onclick="Document.pilihPegawai(this, event)" index="<?php echo $index ?>" state="<?php echo $state ?>"><label for="" class="label label-success">Pilih</label></a>
           </td>
          </tr>
         <?php }?>
        </tbody>
       </table>
      </div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>