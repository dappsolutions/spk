<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-data-rekanan">
    <thead>
     <tr class="bg-info">
      <th>No</th>
      <th>Vendor</th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($data_rekanan)) { ?>
      <?php $no = 1 ?>
      <?php foreach ($data_rekanan as $key => $value) { ?>
       <tr class="input" data_id="<?php echo $value['id'] ?>">
        <td><?php echo $no++ ?></td>
        <td><?php echo $value['nama_vendor'] ?></td>
       </tr>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>