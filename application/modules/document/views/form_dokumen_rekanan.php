<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table style="width:100%;" id="table-data-rekanan">
    <thead>
     <tr class="bg-info">
      <th class="th_jsa">No</th>
      <th class="th_jsa">Vendor</th>
      <th class="th_jsa"></th>
     </tr>
    </thead>
    <tbody>
     <?php if (isset($data_rekanan)) { ?>
      <?php $no = 1 ?>
      <?php foreach ($data_rekanan as $key => $value) { ?>
       <tr class="input" data_id="<?php echo $value['id'] ?>">
        <td class="td_jsa"><?php echo $no++ ?></td>
        <td class="td_jsa">
         <select name="" id="list-vendor" class="form-control required" error="Vendor">
          <option value="">PILIH VENDOR</option>
          <?php foreach ($list_vendor as $v_vendor) { ?>
           <?php $selected = $v_vendor['id'] == $value['vendor'] ? 'selected' : '' ?>
           <option <?php echo $selected ?> value="<?php echo $v_vendor['id'] ?>"><?php echo strtoupper($v_vendor['nama_vendor']) ?></option>
          <?php } ?>
         </select>
        </td>
        <td class="td_jsa text-center" id="action">
         <i class="fa fa-trash" onclick="Document.removeRekanan(this)"></i>
        </td>
       </tr>
      <?php } ?>
     <?php } ?>
     <tr class="input" data_id="">
      <td class="td_jsa">-</td>
      <td class="td_jsa">
       <select name="" id="list-vendor" class="form-control <?php echo isset($data_rekanan) ? '' : 'required' ?>" error="Vendor">
        <option value="">PILIH VENDOR</option>
        <?php foreach ($list_vendor as $key => $value) { ?>
         <option value="<?php echo $value['id'] ?>"><?php echo strtoupper($value['nama_vendor']) ?></option>
        <?php } ?>
       </select>
      </td>
      <td class="text-center td_jsa" id="action">
       <i class="fa fa-plus" onclick="Document.addRekanan(this)"></i>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>