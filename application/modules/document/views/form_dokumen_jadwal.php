<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="" style="width: 100%;" id="table-jadwal">
    <thead>
     <tr class="bg-info">
      <th class="th_jsa">No</th>
      <th class="th_jsa">Uraian Kegiatan</th>
      <th class="th_jsa" colspan="2">Nomor</th>
      <th class="th_jsa">Tanggal</th>
      <?php if (isset($tipe_pengadaan)) { ?>
       <?php if (strtolower($tipe_pengadaan) == 'standard') { ?>
        <th id='standard' class="th-nilai th_jsa">Standard</th>
        <th id='cepat' class="th-nilai th_jsa hide">Cepat</th>
        <th id='manual' class="th-nilai th_jsa hide">Manual</th>
       <?php } ?>
       <?php if (strtolower($tipe_pengadaan) == 'cepat') { ?>
        <th id='standard' class="th-nilai th_jsa hide">Standard</th>
        <th id='cepat' class="th-nilai th_jsa ">Cepat</th>
        <th id='manual' class="th-nilai th_jsa hide">Manual</th>
       <?php } ?>
       <?php if (strtolower($tipe_pengadaan) == 'manual') { ?>
        <th id='standard' class="th-nilai th_jsa hide">Standard</th>
        <th id='cepat' class="th-nilai th_jsa hide">Cepat</th>
        <th id='manual' class="th-nilai th_jsa">Manual</th>
       <?php } ?>
      <?php } else { ?>
       <th id='standard' class="th-nilai th_jsa">Standard</th>
       <th id='cepat' class="th-nilai th_jsa">Cepat</th>
       <th id='manual' class="th-nilai th_jsa">Manual</th>
      <?php } ?>
      <!-- <th>Unit</th> -->
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data_jadwal)) { ?>
      <?php $no = 1 ?>
      <?php foreach ($data_jadwal as $key => $value) { ?>
       <?php $bg_tanggal_pekerjaan = '' ?>
       <?php $as_tanggal_pekerjaan = '' ?>
       <?php if (isset($value['as_tanggal_pekerjaan'])) { ?>
        <?php $bg_tanggal_pekerjaan = $value['as_tanggal_pekerjaan'] == '0' ? '' : 'bg-success'; ?>
        <?php $as_tanggal_pekerjaan = $value['as_tanggal_pekerjaan'] == '0' ? '' : 'as_tanggal_pekerjaan'; ?>
       <?php } else { ?>
        <?php if ($no == count($data_jadwal)) { ?>
         <?php $bg_tanggal_pekerjaan = 'bg-success'; ?>
         <?php $as_tanggal_pekerjaan = 'as_tanggal_pekerjaan'; ?>
        <?php } ?>
       <?php } ?>
       <tr class="input <?php echo $bg_tanggal_pekerjaan ?> <?php echo $as_tanggal_pekerjaan ?>" data_id="<?php echo $value['id'] ?>" data_transaksi_id="<?php echo isset($value['nomor_pekerjaan']) ? $value['id'] : '' ?>">
        <td class="td_jsa"><?php echo $no++ ?></td>
        <td class="td_jsa" id="uraian_pekerjaan">
         <?php echo $value['uraian_pekerjaan'] ?>
        </td>
        <td class="td_jsa" id="no-rab">
         <?php if (isset($no_rab)) { ?>
          <?php echo $no_rab ?>
         <?php } else { ?>
          -
         <?php } ?>
        </td>
        <td class="td_jsa" id="no_dokumen">
         <?php if (isset($value['nomor_pekerjaan'])) { ?>
          <label for="" id="no-rab-label"></label><label for="" id="nomor-pekerjaan"><?php echo $value['nomor_pekerjaan'] ?></label>
         <?php } else { ?>
          <label for="" id="no-rab-label"></label><?php echo $value['no_dokumen'] ?>
         <?php } ?>
        </td>
        <td class="td_jsa" id="tanggal">
         <?php if (isset($value['tanggal'])) { ?>
          <?php echo $value['tanggal'] ?>
         <?php } else { ?>
          -
         <?php } ?>
        </td>
        <?php if (isset($tipe_pengadaan)) { ?>
         <?php if (strtolower($tipe_pengadaan) == 'standard') { ?>
          <td id="standard" class="td-nilai td_jsa bg-warning">
           <?php echo $value['standard'] == '0' ? '' : $value['standard'] ?>
          </td>
          <td id="cepat" class="td-nilai td_jsa hide">
           <?php echo $value['cepat'] == '0' ? '' : $value['cepat'] ?>
          </td>
          <td id="manual" class="td-nilai td_jsa hide">
           <?php echo $value['manual'] == '0' ? '' : $value['manual'] ?>
          </td>
         <?php } ?>
         <?php if (strtolower($tipe_pengadaan) == 'cepat') { ?>
          <td id="standard" class="td-nilai td_jsa hide">
           <?php echo $value['standard'] == '0' ? '' : $value['standard'] ?>
          </td>
          <td id="cepat" class="td-nilai td_jsa bg-warning">
           <?php echo $value['cepat'] == '0' ? '' : $value['cepat'] ?>
          </td>
          <td id="manual" class="td-nilai td_jsa hide">
           <?php echo $value['manual'] == '0' ? '' : $value['manual'] ?>
          </td>
         <?php } ?>
         <?php if (strtolower($tipe_pengadaan) == 'manual') { ?>
          <td id="standard" class="td-nilai td_jsa hide">
           <?php echo $value['standard'] == '0' ? '' : $value['standard'] ?>
          </td>
          <td id="cepat" class="td-nilai td_jsa hide">
           <?php echo $value['cepat'] == '0' ? '' : $value['cepat'] ?>
          </td>
          <td id="manual" class="td-nilai td_jsa bg-warning">
           <input id="manual" value="<?php echo $value['manual'] == '0' ? '' : $value['manual'] ?>" onkeyup="Document.calculateDate(this, event)">
          </td>
         <?php } ?>
        <?php } else { ?>
         <td id="standard" class="td-nilai td_jsa">
          <?php echo $value['standard'] == '0' ? '' : $value['standard'] ?>
         </td>
         <td id="cepat" class="td-nilai td_jsa">
          <?php echo $value['cepat'] == '0' ? '' : $value['cepat'] ?>
         </td>
         <td id="manual" class="td-nilai td_jsa">
          <?php echo $value['manual'] == '0' ? '' : $value['manual'] ?>
         </td>
        <?php } ?>
        <!-- <td class="td_jsa">
         <?php echo $value['nama_upt'] ?>
        </td> -->
       </tr>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>