<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label for="">Approval Pengesahan</label>
			<select name="" id="approval-pengesahan" class="form-control required" error="Approval Pengesahan">
				<option value="">-- PILIH APPROVAL --</option>
				<?php foreach ($list_pegawai as $key => $value) { ?>
				<?php $selected = '' ?>
				<?php if (isset($pegawai_pengesahan)) { ?>
				<?php $selected = $value['id'] == $pegawai_pengesahan ? 'selected' : '' ?>
				<?php } ?>
				<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nip'] ?> - <?php echo $value['nama_pegawai'] ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="row content-input-kak-file">
			<div class="col-md-6 <?php echo isset($file_kak) ? $file_kak != '' ? 'hide' : '' : ''  ?>">
				<div class="form-group" id="file_input_kak">
					<label for="">Upload KAK</label>
					<input type="file" id="file_kak" class="form-control" onchange="Document.checkFile(this)">
				</div>
			</div>


			<div class="col-md-6 <?php echo isset($file_kak) ? $file_kak != '' ? '' : 'hide' : 'hide'  ?>">
				<div class="form-group " id="detail_file_kak">
					<label for="">File KAK</label>
					<div class="input-group">
						<input disabled="" type="text" id="file_str_kak" class="form-control"
							value="<?php echo isset($file_kak) ? $file_kak : '' ?>">
						<span class="input-group-addon">
							<i class="fa fa-image hover-content" file="<?php echo isset($file_kak) ? $file_kak : '' ?>"
								onclick="Document.showFileKak(this, event)"></i>
						</span>
						<span class="input-group-addon">
							<i class="fa fa-close hover-content" onclick="Document.gantiFileKak(this, event)"></i>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="row content-input-rpb-file">
			<div class="col-md-6 <?php echo isset($file_rpb) ? $file_rpb != '' ? 'hide' : '' : ''  ?>">
				<div class="form-group" id="file_input_rpb">
					<label for="">Upload RPB</label>
					<input type="file" id="file_rpb" class="form-control" onchange="Document.checkFile(this)">
				</div>
			</div>


			<div class="col-md-6 <?php echo isset($file_rpb) ? $file_rpb != '' ? '' : 'hide' : 'hide'  ?>">
				<div class="form-group " id="detail_file_rpb">
					<label for="">File RPB</label>
					<div class="input-group">
						<input disabled="" type="text" id="file_str_rpb" class="form-control"
							value="<?php echo isset($file_rpb) ? $file_rpb : '' ?>">
						<span class="input-group-addon">
							<i class="fa fa-image hover-content" file="<?php echo isset($file_rpb) ? $file_rpb : '' ?>"
								onclick="Document.showFileRpb(this, event)"></i>
						</span>
						<span class="input-group-addon">
							<i class="fa fa-close hover-content" onclick="Document.gantiFileRpb(this, event)"></i>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
