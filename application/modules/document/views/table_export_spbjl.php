<div class="row hide" id="content-spbjl-data">
  <div class="col-md-12">
    <table>
      <tbody>
        <tr>
          <td colspan="3"><b>Form Dokumen</b></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Format Pengadaan</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $format_pengadaan ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Tanggal Awal Pekerjaan</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $tgl_pelaksanaan ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Sifat Pekerjaan</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $sifat_pekerjaan ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Selesai Pekerjaan</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $tgl_selesai_pekerjaan ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Judul Pekerjaan</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $judul_pekerjaan ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Lama Pekerjaan (Hari)</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $lama_pekerjaan ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Vendor</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $nama_vendor ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Jenis Anggaran</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $tipe_anggaran ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">No. RAB</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $no_rab ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">No SKK</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $no_skk ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Tanggal RAB</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $tanggal_rab ?></td>
        </tr>
        <tr>
          <td style="border:1px solid #000;">Jenis Pengadaan</td>
          <td style="border:1px solid #000;">:</td>
          <td style="border:1px solid #000;"><?php echo $tipe_pengadaan ?></td>
        </tr>
      </tbody>
    </table>

    <br>

    <table id="table-jadwal">
      <thead>
        <tr>
          <td colspan="5"><b>Form Jadwal</b></td>
        </tr>
        <tr class="bg-info">
          <th style="border:1px solid #000;">No</th>
          <th style="border:1px solid #000;">Uraian Kegiatan</th>
          <th style="border:1px solid #000;" colspan="2">Nomor</th>
          <th style="border:1px solid #000;">Tanggal</th>
          <?php if (strtolower($tipe_pengadaan) == 'standard') { ?>
            <th style="border:1px solid #000;" id='standard' class="th-nilai">Standard</th>
          <?php } ?>
          <?php if (strtolower($tipe_pengadaan) == 'cepat') { ?>
            <th style="border:1px solid #000;" id='cepat' class="th-nilai">Cepat</th>
          <?php } ?>
          <?php if (strtolower($tipe_pengadaan) == 'manual') { ?>
            <th style="border:1px solid #000;" id='manual' class="th-nilai">Manual</th>
          <?php } ?>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($data_jadwal)) { ?>
          <?php $no = 1 ?>
          <?php foreach ($data_jadwal as $key => $value) { ?>
            <tr class="input" data_id="<?php echo $value['id'] ?>">
              <td style="border:1px solid #000;"><?php echo $no++ ?></td>
              <td style="border:1px solid #000;" id="uraian_pekerjaan">
                <?php echo $value['uraian_pekerjaan'] ?>
              </td>
              <td style="border:1px solid #000;" id="no-rab">
                <?php echo $no_rab ?>
              </td>
              <td style="border:1px solid #000;" id="no_dokumen">
                <?php echo $value['nomor_pekerjaan'] ?>
              </td>
              <td style="border:1px solid #000;" id="tanggal">
                <?php echo $value['tanggal'] ?>
              </td>
              <?php if (strtolower($tipe_pengadaan) == 'standard') { ?>
                <td style="border:1px solid #000;" id="standard" class="td-nilai text-center">
                  <?php echo $value['standard'] == '0' ? '' : $value['standard'] ?>
                </td>
              <?php } ?>
              <?php if (strtolower($tipe_pengadaan) == 'cepat') { ?>
                <td style="border:1px solid #000;" id="cepat" class="td-nilai text-center">
                  <?php echo $value['cepat'] == '0' ? '' : $value['cepat'] ?>
                </td>
              <?php } ?>
              <?php if (strtolower($tipe_pengadaan) == 'manual') { ?>
                <td style="border:1px solid #000;" id="manual" class="td-nilai text-center">
                  <?php echo $value['manual'] == '0' ? '' : $value['manual'] ?>
                </td>
              <?php } ?>
            </tr>
          <?php } ?>
        <?php } ?>
      </tbody>
    </table>
    <br>

    <table>
      <thead>
        <tr>
          <td colspan="7"><b>Form RAB</b></td>
        </tr>
        <tr class="bg-info">
          <th style="border:1px solid #000;" rowspan="2" class="text-center">Uraian</th>
          <th style="border:1px solid #000;" colspan="2" class="text-center">Volume</th>
          <th style="border:1px solid #000;" colspan="2" class="text-center">Harga Pada Anggaran Biaya Operasi</th>
          <th style="border:1px solid #000;" colspan="2" class="text-center">Harga HPS Pln</th>
        </tr>
        <tr class="bg-info">
          <th style="border:1px solid #000;" class="text-center">Jml</th>
          <th style="border:1px solid #000;" class="text-center">Sat</th>
          <th style="border:1px solid #000;" class="text-center">Satuan Rp</th>
          <th style="border:1px solid #000;" class="text-center">Jumlah Rp</th>
          <th style="border:1px solid #000;" class="text-center">Satuan Rp</th>
          <th style="border:1px solid #000;" class="text-center">Jumlah Rp</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data_rab as $key => $value) { ?>
          <tr>
            <td style="border:1px solid #000;"><?php echo $value['nama_produk'] ?></td>
            <td style="border:1px solid #000;"><?php echo $value['jml_vol'] ?></td>
            <td style="border:1px solid #000;"><?php echo $value['nama_satuan'] ?></td>
            <td style="border:1px solid #000;"><?php echo $value['satuan_anggaran'] ?></td>
            <td style="border:1px solid #000;"><?php echo $value['jumlah_anggaran'] ?></td>
            <td style="border:1px solid #000;"><?php echo $value['satuan_hps'] ?></td>
            <td style="border:1px solid #000;"><?php echo $value['jumlah_hps'] ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
    <br>


    <table>
      <thead>
      <tr>
          <td colspan="9"><b>Form Penawaran Harga</b></td>
        </tr>
        <tr class="bg-info">
          <th style="border:1px solid #000;" rowspan="2" class="text-center">Uraian</th>
          <th style="border:1px solid #000;" colspan="2" class="text-center">Volume</th>
          <th style="border:1px solid #000;" colspan="2" class="text-center">Harga Penawaran</th>
          <th style="border:1px solid #000;" colspan="2" class="text-center">Harga HPS Pln</th>
          <th style="border:1px solid #000;" colspan="2" class="text-center">Harga Disepakati</th>
        </tr>
        <tr class="bg-info">
          <th style="border:1px solid #000;" class="text-center">Jml</th>
          <th style="border:1px solid #000;" class="text-center">Sat</th>
          <th style="border:1px solid #000;" class="text-center">Satuan Rp</th>
          <th style="border:1px solid #000;" class="text-center">Jumlah Rp</th>
          <th style="border:1px solid #000;" class="text-center">Satuan Rp</th>
          <th style="border:1px solid #000;" class="text-center">Jumlah Rp</th>
          <th style="border:1px solid #000;" class="text-center">Satuan Rp</th>
          <th style="border:1px solid #000;" class="text-center">Jumlah Rp</th>
        </tr>
      </thead>
      <tbody>
        <?php if (!empty($data_rab)) { ?>
          <?php foreach ($data_rab as $key => $value) { ?>
            <tr class="input" data_id="<?php echo $value['id'] ?>">
              <td style="border:1px solid #000;" id="uraian">
                <?php echo $value['nama_produk'] ?>
              </td>
              <td style="border:1px solid #000;" id="jumlah-vol">
                <?php echo $value['jml_vol'] ?>
              </td>
              <td style="border:1px solid #000;" id="satuan">
                <?php echo $value['nama_satuan'] ?>
              </td>
              <td style="border:1px solid #000;" id="satuan-rp">
                <?php echo $value['satuan_penawaran'] ?>
              </td>
              <td style="border:1px solid #000;">
                <?php echo $value['jumlah_penawaran'] ?>
              </td>
              <td style="border:1px solid #000;" id="satuan-rp-hps">
                <?php echo $value['satuan_hps'] ?>
              </td>
              <td style="border:1px solid #000;" id="jumlah-rp-hps">
                <?php echo $value['jumlah_hps'] ?>
              </td>
              <td style="border:1px solid #000;" id="satuan-sepakat">
                <?php echo $value['harga_sepakat'] ?>
              </td>
              <td style="border:1px solid #000;" id="jumlah-sepakat">
                <?php echo $value['jumlah_sepakat'] ?>
              </td>
            </tr>
          <?php } ?>
        <?php } else { ?>
          <tr>
            <td style="border:1px solid #000;" colspan="9">
              Tidak ada data ditemukan
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>