<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Format Pengadaan</label>
  </div>
 </div>
 <div class="col-md-6">
  <?php echo $format_pengadaan ?>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Tanggal Awal Pekerjaan</label>
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <?php echo $tgl_pelaksanaan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Sifat Pekerjaan</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $sifat_pekerjaan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Selesai Pekerjaan</label>
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <?php echo $tgl_selesai_pekerjaan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Judul Pekerjaan</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $judul_pekerjaan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Lama Pekerjaan (Hari)</label>
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <?php echo $lama_pekerjaan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Vendor</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nama_vendor ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jenis Anggaran</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $tipe_anggaran ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">No. RAB</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $no_rab ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">No SKK</label>
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <?php echo $no_skk ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Tanggal RAB</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $tanggal_rab ?>
  </div>
 </div>
</div>


<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jenis Pengadaan</label>
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <?php echo $tipe_pengadaan ?>
  </div>
 </div>
</div>

<?php if ($jenis_transaksi == 'SUB_SPK') { ?>
 <div class="row">
  <div class="col-md-6">
   <div class="form-group">
    <label for="">Sub Bidang Usaha</label>
   </div>
  </div>
  <div class="col-md-6">
   <div class="form-group">
    <?php echo $sub_bidang ?>
   </div>
  </div>
 </div>
<?php } ?>

<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-direksi-lapangan">
    <thead>
     <tr class="bg-warning">
      <th>Direksi Lapangan</th>
     </tr>
    </thead>
    <tbody>
     <?php foreach ($list_direksi as $key => $value) { ?>
      <tr>
       <td>(<?php echo $value['nip'] ?>) <?php echo $value['nama_pegawai'] ?></td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-pengawas-lapangan">
    <thead>
     <tr class="bg-warning">
      <th>Pengawas Lapangan</th>
     </tr>
    </thead>
    <tbody>
     <?php foreach ($list_pengawas as $key => $value) { ?>
      <tr>
       <td>(<?php echo $value['nip'] ?>) <?php echo $value['nama_pegawai'] ?></td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>

<!--FORM EXPORT EXCEL-->
<div id="excel-form-document">
 <table style="width: 100%;" id="table-excel-form-document">
  <tr>
   <td class="td_jsa" style="background-color: blue;color: white;">Format Pengadaan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $format_pengadaan ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Tanggal Awal Pekerjaan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $tgl_pelaksanaan ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Sifat Pekerjaan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $sifat_pekerjaan ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Selesai Pekerjaan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $tgl_selesai_pekerjaan ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Judul Pekerjaan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $judul_pekerjaan ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Lama Pekerjaan (Hari)</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $lama_pekerjaan ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Vendor</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $nama_vendor ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Jenis Anggaran</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $tipe_anggaran ?></td>
  </tr>
  <tr>
   <td class="td_jsa">No. RAB</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $no_rab ?></td>
  </tr>
  <tr>
   <td class="td_jsa">No. SKK</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $no_skk ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Tanggal RAB</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $tanggal_rab ?></td>
  </tr>
  <tr>
   <td class="td_jsa">Jenis Pengadaan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa"><?php echo $tipe_pengadaan ?></td>
  </tr>
  <?php if ($jenis_transaksi == 'SUB_SPK') { ?>
   <tr>
    <td class="td_jsa">Sub Bidang Usaha</td>
    <td class="td_jsa">:</td>
    <td class="td_jsa"><?php echo $sub_bidang ?></td>
   </tr>
  <?php } ?>
  <tr>
   <td class="td_jsa">Direksi Lapangan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa">
    <table>
     <?php foreach ($list_direksi as $key => $value) { ?>
      <tr>
       <td class="td_jsa">(<?php echo $value['nip'] ?>) <?php echo $value['nama_pegawai'] ?></td>
      </tr>
     <?php } ?>
    </table>
   </td>
  </tr>
  <tr>
   <td class="td_jsa">Pengawas Lapangan</td>
   <td class="td_jsa">:</td>
   <td class="td_jsa">
    <table>
    <?php foreach ($list_pengawas as $key => $value) { ?>
      <tr>
       <td class="td_jsa">(<?php echo $value['nip'] ?>) <?php echo $value['nama_pegawai'] ?></td>
      </tr>
     <?php } ?>
    </table>
   </td>
  </tr>
 </table>
</div>