<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table style="width:100%;" id="table-penawaran-harga">
    <thead>
     <tr class="bg-info">
      <th rowspan="2" class="text-center th_jsa">Uraian</th>
      <th colspan="2" class="text-center th_jsa">Volume</th>
      <th colspan="2" class="text-center th_jsa">Harga Penawaran</th>
      <th colspan="2" class="text-center th_jsa">Harga HPS Pln</th>
      <th colspan="2" class="text-center th_jsa">Harga Disepakati</th>
      <th rowspan="2" class="th_jsa"></th>
     </tr>
     <tr class="bg-info">
      <th class="text-center th_jsa">Jml</th>
      <th class="text-center th_jsa">Sat</th>
      <th class="text-center th_jsa">Satuan Rp</th>
      <th class="text-center th_jsa">Jumlah Rp</th>
      <th class="text-center th_jsa">Satuan Rp</th>
      <th class="text-center th_jsa">Jumlah Rp</th>
      <th class="text-center th_jsa">Satuan Rp</th>
      <th class="text-center th_jsa">Jumlah Rp</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data_rab)) { ?>
      <?php foreach ($data_rab as $key => $value) { ?>
       <tr class="input" data_id="<?php echo $value['id'] ?>">
        <td class="td_jsa" id="uraian">
         <?php echo $value['nama_produk'] ?>
        </td>
        <td class="td_jsa" id="jumlah-vol">
         <?php echo $value['jml_vol'] ?>
        </td>
        <td class="td_jsa" id="satuan">
         <?php echo $value['nama_satuan'] ?>
        </td>
        <td class="td_jsa" id="satuan-rp">
         <input type="text" id="satuan-rp-penawaran" class="form-control " error="Satuan Rp" onkeyup="Document.setHargaSepakat(this, event)" value="<?php echo $value['satuan_penawaran'] ?>">
        </td>
        <td class="td_jsa">
         <input type="text" readonly id="jumlah-rp-penawaran" class="form-control " error="Jumlah Rp" value="<?php echo $value['jumlah_penawaran'] ?>">
        </td>
        <td class="td_jsa" id="satuan-rp-hps">
         <?php echo $value['satuan_hps'] ?>
        </td>
        <td class="td_jsa" id="jumlah-rp-hps">
         <?php echo $value['jumlah_hps'] ?>
        </td>
        <td class="td_jsa" id="satuan-sepakat">
         <?php echo $value['harga_sepakat'] ?>
        </td>
        <td class="td_jsa" id="jumlah-sepakat">
         <?php echo $value['jumlah_sepakat'] ?>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td class="td_jsa" colspan="9">
        Tidak ada data ditemukan
       </td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>