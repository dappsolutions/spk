<td class="td_jsa">
 <select name="" id="uraian" class="form-control required" error="Uraian Material" onchange="Document.getSatuanProduk(this)">
  <option value="">--PILIH--</option>
  <?php foreach ($material as $key => $value) {?>
   <option value="<?php echo $value['produk'] ?>"><?php echo $value['nama_produk'] ?></option>
  <?php }?>
 </select>
</td>
<td class="td_jsa">
 <input type="text" id="jml_vol" onkeyup="Document.setHpsHarga(this)" class="form-control required" error="Jml Vol" />
</td>
<td class="td_jsa">
 <select name="" id="satuan_vol" class="form-control required" error="Satuan Vol">
  <option value="">--PILIH--</option>
 </select>
</td>
<td class="td_jsa">
 <input type="text" id="satuan_anggaran" class="form-control required" error="Satuan Anggaran" onkeyup="Document.hitungJumlahAnggaran(this, event)"/>
</td>
<td class="td_jsa">
 <input type="text" id="jumlah_anggaran" class="form-control required" error="Jumlah Anggaran" disabled/>
</td>
<td class="td_jsa">
 <input type="text" id="satuan_hps" class="form-control required" readonly error="Satuan HPS" />
</td>
<td class="td_jsa">
 <input type="text" id="jumlah_hps" class="form-control required" readonly error="Jumlah HPS" />
</td>
<td class="td_jsa text-center">
 <i class="fa fa-trash" onclick="Document.removeRab(this)"></i>
</td>