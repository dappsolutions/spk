<div class="row">
 <div class="col-md-6 hide">
  <div class="form-group">
   <label for="">Format Pengadaan</label>
   <select name="" id="format-pengadaan" class="form-control required" error="Format Pengadaan">
    <option value="">-- PILIH --</option>
    <?php foreach ($list_transaksi as $key => $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($type_transaction)) { ?>
      <?php $selected = $value['id'] == $type_transaction ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tipe'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">Sifat Pekerjaan</label>
   <select name="" id="sifat-pekerjaan" class="form-control required" error="Sifat Pekerjaan">
    <option value="">-- PILIH --</option>
    <?php foreach ($list_pekerjaan as $key => $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($type_of_work)) { ?>
      <?php $selected = $value['id'] == $type_of_work ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['tipe_pekerjaan'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">Tanggal Awal Pekerjaan</label>
   <input type="text" id="tgl-awal" class="form-control required" error="Tanggal Awal Pekerjaan" disabled value="<?php echo isset($tgl_pelaksanaan) ? $tgl_pelaksanaan : '' ?>">
  </div>
 </div>
</div>

<div class="row">

 <div class="col-md-6">
  <div class="form-group">
   <label for="">Judul Pekerjaan</label>
   <input type="text" onkeyup="Document.setBarangdanJasaPekerjaan(this, event)" id="judul-pekerjaan" class="form-control required" error="Judul Pekerjaan" value="<?php echo isset($judul_pekerjaan) ? $judul_pekerjaan : '' ?>">
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">Selesai Pekerjaan</label>
   <input type="text" id="tgl-selesai" onchange="Document.validasiTanggalSelesai(this, event)" class="form-control" error="Selesai Pekerjaan" readonly value="<?php echo isset($tgl_selesai_pekerjaan) ? $tgl_selesai_pekerjaan : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Vendor</label>
   <br>
   <select name="" id="vendor" class="form-control required" onchange="Document.setVendorEvalusai(this)" error="Vendor">
    <option value="">--PILIH--</option>
    <?php foreach ($list_vendor as $key => $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($vendor)) { ?>
      <?php $selected = $value['id'] == $vendor ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo strtoupper($value['nama_vendor']) ?></option>
    <?php } ?>
   </select>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">Lama Pekerjaan</label>
   <input type="text" id="lama-pekerjaan" class="form-control " error="Lama Pekerjaan" readonly value="<?php echo isset($lama_pekerjaan) ? $lama_pekerjaan : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">No. RAB</label>
   <input type="text" id="no-rab" class="form-control required" error="No Rab" onkeyup="Document.generateNoInJadwal(this, event)" value="<?php echo isset($no_rab) ? $no_rab : '' ?>">
  </div>
 </div>
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jenis Anggaran</label>
   <select name="" id="jenis-anggaran" class="form-control required" error="Jenis Anggaran">
    <option value="">-- PILIH --</option>
    <?php foreach ($list_anggaran as $key => $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($type_of_budget)) { ?>
      <?php $selected = $value['id'] == $type_of_budget ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo strtolower($value['id']) ?>"><?php echo $value['tipe_anggaran'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Tanggal RAB</label>
   <input type="text" id="tgl-rab" class="form-control required" error="Tanggal Rab" readonly onchange="Document.setTanggalPenjadwalan(this, event)" value="<?php echo isset($tanggal_rab) ? $tanggal_rab : '' ?>">
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <label for="">No SKK</label>
   <br>
   <select name="" id="no_skk" class="form-control required" error="No SKK">
    <option value="">--PILIH--</option>
    <?php foreach ($list_skk as $key => $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($skk_no)) { ?>
      <?php $selected = $value['id'] == $skk_no ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo strtoupper($value['no_skk']) ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jenis Pengadaan</label>
   <select name="" id="jenis-pengadaan" class="form-control required" error="Jenis Pengadaan" onchange="Document.setTanggalPenjadwalan(this, event)">
    <option value="">PILIH</option>
    <?php foreach ($list_pengadaan as $key => $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($procurement_type)) { ?>
      <?php $selected = $value['id'] == $procurement_type ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo strtolower($value['tipe_pengadaan']) ?>" data_id="<?php echo $value['id'] ?>"><?php echo $value['tipe_pengadaan'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
 <div class="col-md-6">
  <div class="table-responsive">
   <table style="width:100%;" id="table-direksi-lapangan">
    <thead>
     <tr class="bg-warning">
      <th class="th_jsa">Direksi Lapangan</th>
      <th class="text-center th_jsa">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php foreach ($list_direksi as $key => $value) { ?>
      <tr class="input" data_id="<?php echo $value['id'] ?>">
       <td class="td_jsa">
        <label for="" id="pegawai" data_id="<?php echo $value['employee'] ?>"><?php echo $value['nip'] ?> - <?php echo $value['nama_pegawai'] ?></label>
       </td>
       <td class="text-center td_jsa">
        <i class="fa fa-trash" onclick="Document.removeItemPegawai(this)"></i>
       </td>
      </tr>
     <?php } ?>
     <tr data_id="">
      <td class="td_jsa">
       <a href="" onclick="Document.showListPegawai(this, event)" state="direksi">Pilih Direksi Lapangan</a>
      </td>
      <td class="text-center td_jsa">
       <!-- <i class="fa fa-plus"></i> -->
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>

</div>

<div class="row">

 <div class="col-md-6">
  <div class="table-responsive">
   <table style="width:100%;" id="table-pengawas-lapangan">
    <thead>
     <tr class="bg-warning">
      <th class="th_jsa">Pengawas Lapangan</th>
      <th class="text-center th_jsa">Action</th>
     </tr>
    </thead>
    <tbody>
     <?php foreach ($list_pengawas as $key => $value) { ?>
      <tr class="input" data_id="<?php echo $value['id'] ?>">
       <td class="td_jsa">
        <label for="" id="pegawai" data_id="<?php echo $value['employee'] ?>"><?php echo $value['nip'] ?> - <?php echo $value['nama_pegawai'] ?></label>
       </td>
       <td class="text-center td_jsa">
        <i class="fa fa-trash" onclick="Document.removeItemPegawai(this)"></i>
       </td>
      </tr>
     <?php } ?>
     <tr data_id="">
      <td class="td_jsa">
       <a href="" onclick="Document.showListPegawai(this, event)" state="pengawas">Pilih Pengawas Lapangan</a>
      </td>
      <td class="text-center td_jsa">
       <!-- <i class="fa fa-plus"></i> -->
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>

<?php $hideFormSpk = 'hide' ?>
<?php if ($jenis_transaksi == 'SUB_SPK') { ?>
 <?php $hideFormSpk = 'hide' ?>
<?php } ?>

<div class="row <?php echo $hideFormSpk ?>">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Sub Badan Usaha</label>
   <select name="" id="sub-badan-usaha" class="form-control">
    <option value="">PILIH</option>
    <?php foreach ($list_bidang_usaha as $key => $value) { ?>
     <?php $selected = '' ?>
     <?php if (isset($sub_bidang_usaha)) { ?>
      <?php $selected = $value['id'] == $sub_bidang_usaha ? 'selected' : '' ?>
     <?php } ?>
     <option <?php echo $selected ?> value="<?php echo strtolower($value['id']) ?>" data_id="<?php echo $value['id'] ?>"><?php echo $value['sub_bidang'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>