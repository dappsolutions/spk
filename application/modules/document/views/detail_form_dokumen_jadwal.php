<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-jadwal">
    <thead>
     <tr class="bg-info">
      <th>No</th>
      <th>Uraian Kegiatan</th>
      <th colspan="2">Nomor</th>
      <th>Tanggal</th>
      <?php if (strtolower($tipe_pengadaan) == 'standard') { ?>
       <th id='standard' class="th-nilai">Standard</th>
      <?php } ?>
      <?php if (strtolower($tipe_pengadaan) == 'cepat') { ?>
       <th id='cepat' class="th-nilai">Cepat</th>
      <?php } ?>
      <?php if (strtolower($tipe_pengadaan) == 'manual') { ?>
       <th id='manual' class="th-nilai">Manual</th>
      <?php } ?>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data_jadwal)) { ?>
      <?php $no = 1 ?>
      <?php foreach ($data_jadwal as $key => $value) { ?>
       <tr class="input" data_id="<?php echo $value['id'] ?>">
        <td><?php echo $no++ ?></td>
        <td id="uraian_pekerjaan">
         <?php echo $value['uraian_pekerjaan'] ?>
        </td>
        <td id="no-rab">
         <?php echo $no_rab ?>
        </td>
        <td id="no_dokumen">
         <?php echo $value['nomor_pekerjaan'] ?>
        </td>
        <td id="tanggal">
         <?php echo $value['tanggal'] ?>
        </td>
        <?php if (strtolower($tipe_pengadaan) == 'standard') { ?>
         <td id="standard" class="td-nilai text-center">
          <?php echo $value['standard'] == '0' ? '' : $value['standard'] ?>
         </td>
        <?php } ?>
        <?php if (strtolower($tipe_pengadaan) == 'cepat') { ?>
         <td id="cepat" class="td-nilai text-center">
          <?php echo $value['cepat'] == '0' ? '' : $value['cepat'] ?>
         </td>
        <?php } ?>
        <?php if (strtolower($tipe_pengadaan) == 'manual') { ?>
         <td id="manual" class="td-nilai text-center">
          <?php echo $value['manual'] == '0' ? '' : $value['manual'] ?>
         </td>
        <?php } ?>
       </tr>
      <?php } ?>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>