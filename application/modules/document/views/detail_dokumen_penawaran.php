<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-penawaran-harga">
    <thead>
     <tr class="bg-info">
      <th rowspan="2" class="text-center">Uraian</th>
      <th colspan="2" class="text-center">Volume</th>
      <th colspan="2" class="text-center">Harga Penawaran</th>
      <th colspan="2" class="text-center">Harga HPS Pln</th>
      <th colspan="2" class="text-center">Harga Disepakati</th>
      <th rowspan="2"></th>
     </tr>
     <tr class="bg-info">
      <th class="text-center">Jml</th>
      <th class="text-center">Sat</th>
      <th class="text-center">Satuan Rp</th>
      <th class="text-center">Jumlah Rp</th>
      <th class="text-center">Satuan Rp</th>
      <th class="text-center">Jumlah Rp</th>
      <th class="text-center">Satuan Rp</th>
      <th class="text-center">Jumlah Rp</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($data_rab)) { ?>
      <?php foreach ($data_rab as $key => $value) { ?>
       <tr class="input" data_id="<?php echo $value['id'] ?>">
        <td id="uraian">
         <?php echo $value['nama_produk'] ?>
        </td>
        <td id="jumlah-vol">
         <?php echo $value['jml_vol'] ?>
        </td>
        <td id="satuan">
         <?php echo $value['nama_satuan'] ?>
        </td>
        <td id="satuan-rp">
         <?php echo $value['satuan_penawaran'] ?>
        </td>
        <td>
         <?php echo $value['jumlah_penawaran'] ?>
        </td>
        <td id="satuan-rp-hps">
         <?php echo $value['satuan_hps'] ?>
        </td>
        <td id="jumlah-rp-hps">
         <?php echo $value['jumlah_hps'] ?>
        </td>
        <td id="satuan-sepakat">
         <?php echo $value['harga_sepakat'] ?>
        </td>
        <td id="jumlah-sepakat">
         <?php echo $value['jumlah_sepakat'] ?>
        </td>
       </tr>
      <?php } ?>
     <?php } else { ?>
      <tr>
       <td colspan="9">
        Tidak ada data ditemukan
       </td>
      </tr>
     <?php } ?>
    </tbody>
   </table>
  </div>
 </div>
</div>