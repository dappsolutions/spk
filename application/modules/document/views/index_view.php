<?php if (isset($menu_akses->document)) { ?>
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-default">
    <div class="panel-heading">
     <div class="row">
      <div class="col-md-4">
       <h5>Daftar Dokumen Spk</h5>
      </div>
      <div class="col-md-8" style="padding: 8px;">
       <div class="row">
        <div class="col-md-1">
         <div style="width: 16px; height: 14px; background: #fff; margin-right: 8px; padding: 8px;border-radius: 100px;"></div>
        </div>
        <div class="col-md-1" style="margin-left: -34px;margin-top: -2px;">
         Draft
        </div>
        <div class="col-md-1">
         <div style="width: 16px; height: 14px; background: #f0ad4e; margin-right: 8px; padding: 8px;border-radius: 100px;"></div>
        </div>
        <div class="col-md-1" style="margin-left: -34px;margin-top: -2px;">
         Jadwal
        </div>
        <div class="col-md-1">
         <div style="width: 16px; height: 14px; background: #039be5; margin-right: 8px; padding: 8px;border-radius: 100px;"></div>
        </div>
        <div class="col-md-1" style="margin-left: -34px;margin-top: -2px;">
         Rab
        </div>
        <div class="col-md-1">
         <div style="width: 16px; height: 14px; background: #2e9d64; margin-right: 8px; padding: 8px;border-radius: 100px;"></div>
        </div>
        <div class="col-md-1" style="margin-left: -34px;margin-top: -2px;">
         Penawaran
        </div>
       </div>
      </div>
     </div>
    </div>
    <div class="panel-body">
     <div class="row">

     </div>
     <div class="row">
      <div class="col-md-12">
      <!-- <a href="#" class="btn btn-success" download="<?php echo 'Dokumen_SPBJL' ?>.xls" onclick="return ExcellentExport.excel(this, 'table-laporan-draft', 'Dokumen_SPBJL');">Export</a> -->
       <div class="input-group">
        <input type="text" class="form-control" onkeyup="Document.search(this, event)" id="keyword" placeholder="Pencarian">
        <span class="input-group-addon"><i class="fa fa-search"></i></span>
       </div>
      </div>
     </div>

     <br />
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>

     <br />
     <div class="row">
      <div class="col-md-12">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-list-draft" id="table-laporan-draft">
         <thead>
          <tr class="bg-info">
           <th>No</th>
           <th>No Dokumen</th>
           <th>Jenis Dokumen</th>
           <th>Pekerjaan</th>
           <th>Vendor</th>
           <th>Tanggal Pekerjaan</th>
           <th>Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = $pagination['last_no'] + 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr data_id="<?php echo $value['id'] ?>" state="<?php echo $value['state'] ?>" style="background-color:<?php echo $value['bg_proses'] ?>">
             <td><?php echo $no++ ?></td>
             <td><?php echo $value['external_id'] ?></td>
             <td><?php echo $value['type'] ?></td>
             <td><?php echo $value['judul_pekerjaan'] ?></td>
             <td><?php echo $value['nama_vendor'] ?></td>
             <td><?php echo $value['tgl_pelaksanaan'] . ' s/d ' . $value['tgl_selesai_pekerjaan'] ?></td>
             <td class="text-center">
              <i class="fa fa-trash grey-text hover" onclick="Document.delete('<?php echo $value['id'] ?>')"></i>
              &nbsp;
              <i class="fa fa-pencil grey-text  hover" onclick="Document.ubah('<?php echo $value['id'] ?>')"></i>
              &nbsp;
              <i class="fa fa-file-text grey-text  hover" onclick="Document.detail('<?php echo $value['id'] ?>')"></i>
              &nbsp;
              <?php if($value['state'] == 'QUOTE_SUBMISSION'){ ?>
               <?php if($value['transaksi_id'] == 'SUB_SPBJL'){ ?>
                <i class="fa fa-print grey-text  hover" onclick="Document.cetak(this)"></i>
               <?php } ?>
               <?php if($value['transaksi_id'] == 'SUB_SPK'){ ?>
                <i class="fa fa-print grey-text  hover" onclick="Document.cetakSpk(this)"></i>
               <?php } ?>
              <?php } ?>
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
           </tr>
          <?php } ?>

         </tbody>
        </table>
       </div>
      </div>
     </div>
    </div>

    <div class="panel-footer">
     <ul class="pagination pagination-sm no-margin pull-right">
      <?php echo $pagination['links'] ?>
     </ul>
    </div>
   </div>
  </div>
 </div>

 <?php if ($menu_akses->document->create) { ?>
  <div class="row">
   <div class="col-md-12">
    <a href="#" class="float" onclick="Document.add(this, event)">
     <i class="fa fa-plus my-float fa-lg"></i>
    </a>
   </div>
  </div>
 <?php } ?>
<?php } else { ?>
 <div class="row">
  <div class="col-md-12">
   <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
    Menu Tidak Tersedia
   </div>
  </div>
 </div>
<?php } ?>