<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Pengadaan Langsung</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;font-family: arial;font-size:12px;">
    <center><label for=""><b><u>DOKUMEN PENGADAAN LANGSUNG</u></b></label></center>
    <center><label for=""><b><?php echo 'No. ' . $data_jadwal[1]['nomor_pekerjaan']  ?></b></label></center>
    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">1.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">PAKET PEKERJAAN</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">1.1</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pemberi pekerjaan ialah :</td>
        </tr>
       </table>
      </td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;&nbsp; PT PLN (Persero) <?php echo $nama_upt ?> UIT JBM</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;&nbsp; <?php echo $alamat_upt ?>
      </td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">1.2</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pekerjaan yang akan dilaksanakan ialah :</td>
        </tr>
       </table>
      </td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;&nbsp; <b>" <?php echo $judul_pekerjaan ?> "</b> <br></td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">1.3</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Perencana untuk pekerjaan ini ialah :</td>
        </tr>
       </table>
      </td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;&nbsp; PT PLN (Persero) <?php echo $nama_upt ?> UIT JBM</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;&nbsp; <?php echo $alamat_upt ?></td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: justify;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">1.4</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pejabat Pelaksana Pengadaan adalah wakil dari pemberi tugas/PT PLN (Persero) UIT JBM - UPT Probolinggo yang melaksanakan proses pengadaan sesuai dengan rencana </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">2.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">KETENTUAN PENGAJUAN PENAWARAN</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">2.1</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pemberi pekerjaan ialah :</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          Surat Penawaran berikut kelengkapannya dibuat dalam rangkap 1 (satu) asli, terdiri dari persyaratan Administrasi, Teknis dan Perhitungan Harga serta persyaratan lain yang
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          Kepada Yth :
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <?php echo $nama_upt ?>
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <?php echo $alamat_upt ?>
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">2.2</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pada sampul muka penawaran sebelah kiri atas harus ditulis</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">" <b><?php echo $judul_pekerjaan ?></b> "</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">2.3</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Surat penawaran dikirim langsung atau lewat POS ke di Kantor PT. PLN (Persero) UIT JBM, <?php echo $nama_upt ?>, pada :</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Hari</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">:</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;"><?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[2]['tanggal'])))  ?></td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Waktu</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">:</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Pukul 09.00 WIB</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Tempat</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">:</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">PT. PLN (Persero) UIT JBM - <?php echo $nama_upt ?></td>
           </tr>
          </table>
         </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">3.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">SYARAT – SYARAT PENAWARAN</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.1</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Surat Penawaran harus ditandatangani diatas meterei Rp. 10.000 dan dicap oleh Penanggung Jawab Perusahaan.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.2</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila Perusahaan melampirkan penjelasan, dan keterangan – keterangan lainnya untuk memperjelas Surat Penawaran, harus dicap perusahaan.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.3</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy akte perusahaan beserta perubahan-perubahannya.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.4</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Surat ijin Usaha Perdagangan (SIUP) sesuai pekerjaan terkait.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.5</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Ketetapan Nomor Pokok Wajib Pajak (NPWP).</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.6</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Surat Keterangan Pengusaha Kena Pajak (PKP).</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.7</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Asli daftar Susunan Pengurus Perusahaan.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.8</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Surat Keterangan Domisili perusahaan yang masih berlaku.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.9</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Tanda Daftar Perusahaan (TDP) atau Nomor Induk Berusaha (NIB).</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.10</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Surat Keterangan Nomor Rekening Perusahaan/Referensi Bank.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.11</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Pakta Integritas. (Format Terlampir)</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.12</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Sertifikat Ahli K3 Umum.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.13</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Surat pernyataan ketersediaan pengawas K3 bersertifikat saat pelaksanaan pekerjaan di masing-masing lokasi.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.14</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila pengawas K3 bukan pegawai organik PT bersangkutan, surat dukungan dari lembaga yang berkompeten harus dilampirkan.</td>
        </tr>
        <!-- <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.15</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Surat Pernyataan Kinerja Baik dan Tidak Termasuk Daftar Hitam</td>
        </tr> -->
        <!-- <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.16</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan daftar pengalaman pekerjaan sesuai pekerjaan yang akan diambil.</td>
        </tr> -->
        <!--ADA Kondisi lagi dari database-->
        <!-- <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.17</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Sertifikat Kompetensi dan Kualifikasi Perusahaan bidang pemasok barang</td>
        </tr> -->
       </table>
      </td>
     </tr>
    </table>



    <!-- <table>
     <tr>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       ZENDIDIYA Y.
      </td>
     </tr>
    </table> -->
   </div>
   <br>
  </div>
 </div>
</body>

</html>