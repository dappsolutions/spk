<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Pengadaan Langsung</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;font-family: arial;font-size:12px;">
    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">PIHAK PERTAMA akan melakukan audit terkait tagihan pembayaran dan dokumen pembukuan serta pencatatan PIHAK KEDUA ketika terdapat indikasi ketidakwajaran berupa </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">6.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Pelaksanaan audit sebagaimana dimaksud dalam ayat (5) pasal ini, dilakukan sebagai berikut :</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">
             <table>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">A)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;"> Atas biaya PIHAK PERTAMA;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">B)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Dilakukan dengan pemberitahuan selambat-lambatnya 7 (tujuh) hari kalender sebelum pelaksanaan audit yang disampaikan PIHAK PERTAMA kepada PIHAK KEDUA</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">C)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Pelaksanaan audit dilaksanakan paling lambat dalam waktu 7 (tujuh) hari kerja, dilanjutkan dengan pembuatan pelaporan audit oleh PIHAK PERTAMA paling lambat dalam 3 (tiga) hari kerja:</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">D)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;"> Penyelesaiaan pembayaran (settlement/adjusment) dilakukan paling lambat dalam 30 (tiga puluh) hari kalender setelah PIHAK PERTAMA memaparkan laporan hasil audit kepada PIHAK KEDUA;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">E)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">PIHAK PERTAMA dapat menunjuk pihak lain untuk melakukan audit;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">F)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">PIHAK KEDUA wajib mengizinkan perwakilan PIHAK PERTAMA dan menyediakan dokumen dan data yang diperlukan;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">G)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Permintaan dan penyampaian dokumen dan data audit dilaksanakan pada jam kerja PIHAK KEDUA;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">H)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila terdapat kerjasama antara PIHAK KEDUA dengan Pihak Ketiga yang berkaitan dengan Pelaksanaan Perjanjian ini, maka PIHAK KEDUA wajib memastikan dan menuangkan ketentuan mengenai hak akses PIHAK PERTAMA terhadap dokumen dan data milik Pihak Ketiga dalam Perjanjian antara PIHAK KEDUA dengan Pihak Ketiga tersebut;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">I)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">PARA PIHAK termasuk perwakilannya, akan menjaga kerahasiaan informasi non-publik yang diperoleh dari pelaksanaan audit tersebut .</td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">7.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Dalam pelaksanaan audit terhadap tagihan pembayaran sebagaimana ayat (5) di atas, PIHAK PERTAMA berhak melakukan penundaan pembayaran atas tagihan pembayaran tersebut atau dapat melakukan pembayaran melalui suatu rekening khusus (Escrow Account) yang dibuat oleh PIHAK PERTAMA dan PIHAK KEDUA yang beban biayanya menjadi tanggung jawab PIHAK KEDUA sampai dilakukan penyesuaian pembayaran</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">8.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Dalam hal dilakukan audit sebagaimana dimaksud ayat (5) dan/atau pelaksanaan penundaan pembayaran sebagaimana ayat (7) pasal ini, PIHAK KEDUA wajib untuk tetap melaksanakan kewajibannya sesuai Perjanjian ini.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">9.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Dalam rangka penerapan tata kelola yang baik (Good Corporate Governance) di PIHAK PERTAMA, jika dalam proses pelaksanaan Perjanjian ini PIHAK KEDUA mengetahui adanya tindakan kecurangan, pelanggaran peraturan atau hukum, konflik kepentingan, penyuapan/gratifikasi maupun kelakukan tidak etis yang dilakukan oleh pegawai PIHAK PERTAMA, agar melaporkan melalui Whistle Blower System dengan media antara lain telepon, SMS, Whatsapp ke 08119861901, atau email ke wbpln@pln.co.id.</td>
           </tr>
          </table>
         </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">7.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">KESELAMATAN KERJA :</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">Wajib Melaksanakan ketentuan SMK3 (Sistem Manajemen Keselamatan dan Kesehatan Kerja) di Lingkungan PT PLN (Persero) sebagaimana surat PT PLN (Persero) kantor pusat.</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">Nomor 0170/KLH.00.01/DIVHSSE/2019 tanggal 06 Mei 2019 :</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">7.1</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pencegahan Kondisi Berbahaya (Unsafe Condition)</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja Wajib melakukan pengendalian teknis terhadap adanya kondisi berbahaya (unsafe condition) pada tempat-tempat kerja, antara lain :</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">a)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib mematuhi peraturan keselamatan dan kesehatan kerja yang berlaku di Lingkungan PT. PLN (PERSERO).</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">b)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib memiliki dan menerapkan Standing Operation Procedure (SOP) untuk setiap pekerjaan.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">c)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib menyediakan peratan dan APD sesuai standart bagi tenaga kerjanya pada pelaksanaan pekerjaan yang berpotensi bahaya.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">d)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib melakukan identifikasi bahaya, penilaian resiko dan pengendalian resiko (IBPPR) pada tempat kerja yang berpotensi Bahaya</td>
           </tr>
           <!-- <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">e)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib membuat Job Safety Analysis (JSA) dan Ijin kerja (working Permit) pada setiap melaksanakan pekerjaan yang berpotensi bahaya.</td>
           </tr> -->
           <!-- <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">f)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib melakukan pemeriksaan kesehatan kerja bagi tenaga kerjanya yang bekerja pada pekerjaan yang berpotensi.</td>
           </tr> -->
          </table>
         </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <!-- <table>
     <tr>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       ZENDIDIYA Y.
      </td>
     </tr>
    </table> -->
   </div>
   <br>
  </div>
 </div>
</body>

</html>