<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Acara Pembukaan</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 3px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="">
    <table style="width: 100%;">
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       <b><u>BERITA ACARA PEMBUKAAN SURAT-SURAT PENAWARAN HARGA PENGADAAN LANGSUNG</u></b>
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       Nomor : <?php echo $data_jadwal[9]['nomor_pekerjaan'] ?>
      </td>
     </tr>
    </table>

    <?php
    $datePembukaan = date('D', strtotime($data_jadwal[9]['tanggal']));
    $dayPembukaan = '';
    switch (strtolower($datePembukaan)) {
     case 'sun':
      $dayPembukaan = 'Minggu';
      break;
     case 'mon':
      $dayPembukaan = 'Senin';
      break;
     case 'tue':
      $dayPembukaan = 'Selasa';
      break;
     case 'wed':
      $dayPembukaan = 'Rabu';
      break;
     case 'thr':
      $dayPembukaan = 'Kamis';
      break;
     case 'fri':
      $dayPembukaan = 'Jumat';
      break;
     case 'sat':
      $dayPembukaan = 'Sabtu';
      break;

     default:
      # code...
      break;
    }

    ?>
    <table>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: justify;" valign="top">1.</td>
      <td style="font-family: arial;font-size:12px;text-align: justify;">Pada hari ini <?php echo $dayPembukaan ?>, tanggal <?php echo str_replace('rupiah', '', terbilang(intval(date('d', strtotime($data_jadwal[9]['tanggal']))))) ?>, bulan <?php echo translateOnlyMonthToIndo(date('d F Y', strtotime($data_jadwal[9]['tanggal']))) ?>, tahun <?php echo str_replace('rupiah', '', terbilang(intval(date('Y', strtotime($data_jadwal[9]['tanggal']))))) ?> (<?php echo date('d-m-Y', strtotime($data_jadwal[9]['tanggal'])) ?>) telah dilakukan pembukaan surat-surat penawaran harga, atas dasar :</td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: justify;" valign="top"></td>
      <td style="font-family: arial;font-size:12px;text-align: justify;"> Surat Permintaan Penawaran Harga Nomor: <?php echo $data_jadwal[4]['nomor_pekerjaan'] ?> Tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[4]['tanggal']))) ?> </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: justify;" valign="top">2.</td>
      <td style="font-family: arial;font-size:12px;text-align: justify;">Permintaan Penawaran Harga telah disampaikan kepada : 1 (satu) rekanan yaitu : </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: justify;" valign="top"></td>
      <td style="font-family: arial;font-size:12px;text-align: justify;">NAMA PERUSAHAAN : <?php echo strtoupper($nama_vendor) ?></td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: justify;" valign="top">3.</td>
      <td style="font-family: arial;font-size:12px;text-align: justify;">Yang telah memasukkan penawaran harga :</td>
     </tr>
    </table>

    <table style="width: 100%;border:1px solid #ccc;">
     <tr style="background-color: #d9edf7">
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>NAMA PERUSAHAAN</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Syarat penulisan pada sampul penawaran</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Tanda tangan penanggung jawab/cap perusahaan diatas materai</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Tanggal pada Surat Penawaran</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Penulisan Harga Penawaran dengan angka dan huruf</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Batas waktu surat penawaran harga</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Nilai Harga Penawaran</b>
       <br>
       <b>Rp</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Batas Waktu Penyerahan Barang</b>
      </td>
      <td colspan="7" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Foto Copy</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Nomor Rekening Perusahaan</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Susunan Pengurus Perusahaan</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Pakta Integritas</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Keterangan</b>
      </td>
     </tr>
     <tr style="background-color: #d9edf7">
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Akta Pendirian & Perubahan</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>SIUP sesuai pekerjaan terkait</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>NPWP & PKP</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Surat keterangan domisili</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>TDP/NIB</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>AK3 Umum</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Sertifikat Kadin</b>
      </td>
     </tr>
     <tr>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;"><?php echo strtoupper($nama_vendor) ?></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">
       Rp, <?php echo number_format($total_penawaran, 0, ',', '.') ?>,-
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">A</td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align:center;">
       A = ada
       <br>
       TA = Tidak ada
      </td>
     </tr>
    </table>

    <table>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: justify;" valign="top">4.</td>
      <td style="font-family: arial;font-size:12px;text-align: justify;">Yang melakukan pembukaan dan pemeriksaan terhadap surat-surat penawaran harga :</td>
     </tr>
    </table>

    <table>
     <tr>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       <br>
       <?php echo strtoupper($nama_vendor) ?>
       
       <br>
       <br>
       <br>
       <b><?php echo strtoupper($nama_pimpinan) ?></b>
      </td>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       <?php echo ucfirst(trim(str_replace('UPT', '', $nama_upt))) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[9]['tanggal']))) ?>
       <br>
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       <b>ZENDIDIYA Y.</b>
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>