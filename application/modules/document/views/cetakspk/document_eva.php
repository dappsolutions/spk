<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Dokumen Evaluasi</title>

  <style>
    #_wrapper {
      /* width: 100%; */
      /* margin: 0 auto; */
    }

    #_content {
      border: 1px solid #999;
      /* max-width: 100%; */
      text-align: center;
    }

    #_top-content {
      /* margin: 0 auto; */
      font-family: arial;
    }

    #_int {
      /* margin: 2% auto; */
      font-family: arial;
    }


    #_bottom-content {
      font-family: arial;
    }

    #_info-content {
      border: 1px solid black;
      margin-left: 16px;
      margin-right: 16px;
      text-align: left;
      border-radius: 30px;
      padding: 1px;
    }

    #_info-content-isi {
      border: 1px solid black;
      text-align: left;
      padding: 16px;
      border-radius: 30px;
    }

    #_cover {
      /* margin-left: 3%; */
    }

    h2 {
      /* margin: 0.5%; */
    }
  </style>
</head>

<body>
  <div style="text-align: right;"></div>
  <div id="_wrapper">
    <div id="_content">

      <div id="_top-content">
        <table>
          <tr>
            <td rowspan="3">
              <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
            </td>
            <td style="font-family: arial;font-size: 8px;">
              PT. PLN (PERSERO)
            </td>
          </tr>
          <tr>
            <td style="font-family: arial;font-size: 8px;">
              UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
            </td>
          </tr>
          <tr>
            <td style="font-family: arial;font-size: 8px;">
              UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
            </td>
          </tr>
        </table>
      </div>

      <div class="content" style="padding: 8px;">
        <center><b style="font-size:12px; font-family:arial;"><u>HASIL EVALUASI PENAWARAN HARGA</u></b></center>
        <center style="font-size:12px; font-family:arial;"><label for="">Nomor : <?php echo $data_jadwal[10]['nomor_pekerjaan'] ?></label></center>
        <br>

        <?php
        $datePenawaran = date('D', strtotime($data_jadwal[10]['tanggal']));
        $dayJadwal = '';
        switch (strtolower($datePenawaran)) {
          case 'sun':
            $dayJadwal = 'Minggu';
            break;
          case 'mon':
            $dayJadwal = 'Senin';
            break;
          case 'tue':
            $dayJadwal = 'Selasa';
            break;
          case 'wed':
            $dayJadwal = 'Rabu';
            break;
          case 'thr':
            $dayJadwal = 'Kamis';
            break;
          case 'fri':
            $dayJadwal = 'Jumat';
            break;
          case 'sat':
            $dayJadwal = 'Sabtu';
            break;

          default:
            # code...
            break;
        }

        ?>
        <table style="width: 100%;">
          <tr>
            <td style="font-size:12px; font-family:arial;text-align:justify;">
              Pada Hari ini <?php echo $dayJadwal ?>, tanggal <?php echo str_replace('rupiah', '', terbilang(date('d', strtotime($data_jadwal[10]['tanggal'])))) ?>, bulan <?php echo translateOnlyMonthToIndo(date('d F Y', strtotime($data_jadwal[10]['tanggal']))) ?>, tahun <?php echo str_replace('rupiah', '', terbilang(date('Y', strtotime($data_jadwal[10]['tanggal'])))) ?> (<?php echo date('d-m-Y', strtotime($data_jadwal[10]['tanggal'])) ?>), kami bertanda tangan di bawah ini selaku Pejabat Pelaksana Pengadaan telah mengadakan penilaian terhadap surat penawaran yang masuk dengan hasil sebagai berikut :
            </td>
          </tr>
        </table>
        <br>

        <table style="width: 100%;">
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"><b>1.</b></td>
            <td style="font-size:12px; font-family:arial;"><b>TATA CARA PENILAIAN PENGADAAN LANGSUNG</b></td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;"></td>
            <td style="font-size:12px; font-family:arial;">
              <table>
                <tr>
                  <td style="font-size:12px; font-family:arial;" valign="top"><b>1.1.</b></td>
                  <td style="font-size:12px; font-family:arial;"><b>Penilaian Administratif.</b></td>
                </tr>
                <tr>
                  <td style="font-size:12px; font-family:arial;" valign="top"></td>
                  <td style="font-size:12px; font-family:arial;">Surat penawaran yang masuk telah memenuhi persyaratan, dan sah sesuai dengan ketentuan dalam Dokumen Pengadaan Langsung dan dinyatakan <label for=""><b style="color:blue;"><i>LULUS</i></b></label>.</td>
                </tr>
                <tr>
                  <td style="font-size:12px; font-family:arial;" valign="top"><b>1.2.</b></td>
                  <td style="font-size:12px; font-family:arial;"><b>Penilaian Harga</b></td>
                </tr>
                <tr>
                  <td style="font-size:12px; font-family:arial;" valign="top"></td>
                  <td style="font-size:12px; font-family:arial;">
                    <table>
                      <tr>
                        <td style="font-size:12px; font-family:arial;" valign="top">a.</td>
                        <td style="font-size:12px; font-family:arial;">Penawaran harga secara teknis dapat dipertanggungjawabkan dan wajar.</td>
                      </tr>
                      <tr>
                        <td style="font-size:12px; font-family:arial;" valign="top">b.</td>
                        <td style="font-size:12px; font-family:arial;">Penawaran harga tidak lebih tinggi dari nilai total HPS, perlu diadakan nego</td>
                      </tr>
                      <tr>
                        <td style="font-size:12px; font-family:arial;" valign="top">c.</td>
                        <td style="font-size:12px; font-family:arial;">Dengan memperhatikan penggunaan hasil produksi dalam Negeri semaksimal mungkin.</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"><b>2.</b></td>
            <td style="font-size:12px; font-family:arial;"><b>PENILAIAN HARGA PENAWARAN </b></td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;">Setelah dilakukan penelitian terhadap syarat-syarat Administrasi dan Penawaran Harga dengan cara membandingkan harga per-paket maupun per-item dengan Harga Perhitungan Sendiri ( HPS ) untuk memperoleh harga yang wajar dan menguntungkan Negara, maka untuk Pekerjaan :</td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;"><b>* <?php echo $judul_pekerjaan ?></b></td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;">Pejabat Pelaksana Pengadaan dapat menetapkan, Rekanan :</td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;"></td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;"></td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;"><?php echo strtoupper($nama_vendor) ?>, <?php echo $alamat_vendor ?></td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;"></td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;" valign="top"></td>
            <td style="font-size:12px; font-family:arial;">Dengan Penawaran Harga sebesar Rp, <?php echo number_format($total_penawaran, 0, ',', '.') ?>,- (<?php echo terbilang($total_penawaran) ?>)</td>
          </tr>
          <tr>
            <td style="font-size:12px; font-family:arial;"></td>
            <td style="font-size:12px; font-family:arial;text-align: justify;">Demikian Evaluasi Penawaran Harga ini dibuat untuk dipergunakan sebagaimana mestinya.</td>
          </tr>
        </table>

        <table style="width: 100%;">
          <tr>
            <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
              Yang Mengusulkan,
              <br>
              <b>PEJABAT PELAKSANA PENGADAAN</b>
              <br>
              <br>
              <br>
              <br>
              <br>
              <b><u>ZENDIDIA Y.</u></b>
            </td>
            <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
              <?php echo ucfirst(trim(str_replace('UPT', '', $nama_upt))) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[10]['tanggal']))) ?>
              <br>
              <b>Menyetujui</b>
              <br>
              <b>MANAGER</b>
              <br>
              <br>
              <br>
              <br>
              <b><u>AHMAD AZHARI KEMMA</u></b>
            </td>
          </tr>
        </table>
      </div>
      <br>
    </div>
  </div>
  <div style="text-align: right;">

  </div>
</body>

</html>