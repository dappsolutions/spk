<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Pengadaan Langsung</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;font-family: arial;font-size:12px;">
    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">8.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">FORCE MAJEURE / SEBAB KAHAR</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">Apabila terjadi Force majeure maka segala akibat yang timbul akan segera diselesaikan bersama antara kedua belah pihak atas dasar musyawarah dan mufakat.</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">Force Majeure adalah peristiwa yang terjadi karena sesuatu hal diluar dugaan/kekuasaan kedua belah pihak yang berlangsung mengenai sasaran pekerjaan seperti :</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">a.</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Bencana alam (gempa bumi, banjir, badai/ topan, gunung meletus, Epidemi dan kegoncangan sosial dalam masyarakat, kerusuhan, pemogokan, demontrasi) yang dinyatakan/ ditetapkan oleh pemerintah/ pemerintah daerah setempat.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">b.</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Perang, blokade dan pemberontakan.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">c.</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Tindakan Pemerintah dalam bidang moneter/ keuangan.</td>
        </tr>
       </table>
      </td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">Hal-hal/ peristiwa lain yang tidak disebutkan diatas, tidak dapat dikategorikan sebagai Force majeure kecuali apabila ditetapkan dengan Peraturan Pemerintah/ Pemerintah Daerah setempat.</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">Sebagai akibat adanya Force Majeure, maka Rekanan dalam waktu 9 (Sembilan) hari kalender terhitung saat adanya Force Majeure tersebut untuk pertama kalinya harus memberitahukan kepada PT. PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur Dan Bali UPT Probolinggo secara tertulis. Jika sesuatu sebab Rekanan tidak melaporkan seperti ketentuan diatas, maka peristiwa Force majeure ini selanjutnya dianggap tidak pernah terjadi.</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">Apabila dalam waktu 14 (empat belas) hari kalender setelah diterimanya pemberitahuan tersebut PT. PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur Dan Bali UPT Probolinggo tidak memberikan jawaban, maka peristiwa Force majeure yang diusulkan oleh Rekanan tersebut dianggap diterima.</td>
     </tr>
    </table>

    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">9.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">LAIN-LAIN :</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;" valign="top"></td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">9.1</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Untuk memperoleh barang/jasa yang benar-benar memenuhi kebutuhan dengan harga yang wajar (serendah mungkin), dapat dipertanggungjawabkan, maka diadakan Negosiasi terhadap harga penawaran tersebut.</td>
        </tr>
        <tr>
         <!-- ada kondisi tertentu dari database -->
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">9.2</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Jangka waktu pelaksanaan sebagaimana dimaksud dalam butir 1.2 selambat-lambatnya adalah 39 (Tiga Puluh Sembilan) hari kalender, setelah ditandatanganinya Surat Perjanjian/Surat Perintah Kerja.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">9.3</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Tempat Penyerahan pekerjaan sebagaimana dimaksud dalam butir 1.2 adalah di Kantor PT PLN (Persero) UIT-JBM <?php echo $nama_upt ?>.</td>
        </tr>
        <tr>
         <!-- ada kondisi tertentu dari database -->
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">9.4</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Masa Pemeliharaan Pekerjaan ini selama 3 (Tiga) Bulan sejak serah terima pekerjaan dijamin dalam bentuk retensi sebesar 5% (lima persen) dari nilai keseluruhan pekerjaan/kontrak bebas dari kerusakan dan dapat berfungsi dengan baik serta memenuhi semua persyaratan/ketentuan yang berlaku.</td>
        </tr>
        <tr>
         <!-- ada kondisi tertentu dari database -->
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">9.5</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Apabila dalam jangka waktu tersebut di atas (poin 9.2), ternyata tidak memenuhi fungsi yang dipersyaratkan, atau terdapat adanya cacat/kerusakan karena kualitas material bermutu rendah atau kesalahan pelaksanaan pekerjaan dan bukan karena kesalahan penggunaan, maka penyedia barang/jasa diwajibkan memperbaiki/mengganti kerusakan tersebut tanpa tambahan biaya apapun</td>
        </tr>
        <tr>
         <!-- ada kondisi tertentu dari database -->
         <td style="font-size:12px;font-family: arial;text-align: left;" valign="top">9.6</td>
         <td style="font-size:12px;font-family: arial;text-align: left;">Perbaikan / penggantian harus dimulai selambat-lambatnya 14 (empat belas) hari kerja sejak tanggal pemberitahuan. Tentative waktu penyelesaian perbaikan tersebut harus sesuai kesepakatan yang disetujui terlebih dahulu antara PT PLN (Persero) UIT JBM <?php echo $nama_upt ?> dengan Penyedia Barang/Jasa</td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <br>
    <br>
    <center>Demikian Dokumen ini dibuat untuk diperhatikan dengan seksama.</center>
    <center><?php echo trim(str_replace('UPT', '', $nama_upt)) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[1]['tanggal']))) ?></center>
    <center>YANG MENGESAHKAN</center>
    <center>PEJABAT PELAKSANA PENGADAAN</center>
    <br>
    <br>
    <center>ZENDIDIA Y.</center>
    <!-- <table>
     <tr>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       ZENDIDIYA Y.
      </td>
     </tr>
    </table> -->
   </div>
   <br>
  </div>
 </div>
</body>

</html>