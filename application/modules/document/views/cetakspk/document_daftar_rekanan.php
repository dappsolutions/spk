<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Rekanan </title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;">
    <center><b style="font-size:12px; font-family:arial;">DAFTAR REKANAN YANG DIUNDANG (DRU)</b></center>
    <center style="font-size:12px; font-family:arial;"><label for="">Nomor :  <?php echo $data_jadwal[5]['nomor_pekerjaan'] ?></label></center>
    <center style="font-size:12px; font-family:arial;"><label for="">Tanggal :  <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[5]['tanggal']))) ?></label></center>

    <table>
     <tr>
      <td style="font-size:12px; font-family:arial;"><b><?php echo $judul_pekerjaan ?></b></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"><b>Klasifikasi Pekerjaan</b></td>
      <td style="font-size:12px; font-family:arial;">:</td>
      <td style="font-size:12px; font-family:arial;">Supply Erect Instalasi dan Sipil</td>
     </tr>
    </table>
    <br>

    <table style="width: 100%;border: 1px solid #ccc;border-collapse: collapse;">
     <tr>
      <td style="font-size:12px; font-family:arial;padding:6px;"><b>Bidang Usaha I </b></td>
      <td style="font-size:12px; font-family:arial;padding:6px;">:</td>
      <td style="font-size:12px; font-family:arial;padding:6px;">Bangunan Sipil</td>
      <td style="font-size:12px; font-family:arial;padding:6px;"><b>Bidang Usaha I </b></td>
      <td style="font-size:12px; font-family:arial;padding:6px;">:</td>
      <td style="font-size:12px; font-family:arial;padding:6px;">SBUJPTL</td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;padding:6px;"><b>Sub Bidang Usaha</b></td>
      <td style="font-size:12px; font-family:arial;padding:6px;">:</td>
      <td style="font-size:12px; font-family:arial;padding:6px;">Konstruksi Bangunan</td>
      <td style="font-size:12px; font-family:arial;padding:6px;"><b>Sub Bidang Usaha</b></td>
      <td style="font-size:12px; font-family:arial;padding:6px;">:</td>
      <td style="font-size:12px; font-family:arial;padding:6px;"><?php echo $sub_bidang ?></td>
     </tr>
    </table>

    <br>
    <table style="width: 600px;border: 1px solid #333;border-collapse: collapse;">
     <tr style="background-color: #d9edf7">
      <td width="100" rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>NO URUT</b>
      </td>      
      <td rowspan="1" width="400" colspan="3" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>DATA REKANAN</b>
      </td>
      <td width="100" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>KETERANGAN</b>
      </td>      
     </tr>
     <tr style="background-color: #d9edf7">
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Perusahaan </b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Pimpinan</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Alamat Perusahaan</b>
      </td>     
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       
      </td>     
     </tr>
     <?php $no =1 ?>
     <?php foreach ($data_rekanan as $key => $value) {?>
      <tr>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"><?php echo $no++ ?></td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"><?php echo $value['nama_vendor'] ?></td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"><?php echo $value['nama_pimpinan'] ?></td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"><?php echo $value['alamat'] ?></td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      </tr>
     <?php }?>
    </table>
    <br>

    <table style="width: 100%;">
     <tr>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
      Yang Mengusulkan,
       <br>
       <b>PEJABAT PELAKSANA PENGADAAN</b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>ZENDIDIA Y.</u></b>
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <?php echo ucfirst(trim(str_replace('UPT', '', $nama_upt))) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[5]['tanggal']))) ?>
       <br>
       <b>MANAGER</b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>AHMAD AZHARI KEMMA</u></b>
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>