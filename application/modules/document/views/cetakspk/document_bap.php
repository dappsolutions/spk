<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen BAP </title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;">
    <center><b style="font-size:12px; font-family:arial;"><u>BERITA ACARA PENJELASAN</u></b></center>
    <center style="font-size:12px; font-family:arial;"><label for="">Nomor : <?php echo $data_jadwal[6]['nomor_pekerjaan'] ?></label></center>
    <center style="font-size:12px; font-family:arial;"><label for="">Perihal</label></center>
    <center style="font-size:12px; font-family:arial;"><label for=""><b>PENGADAAN JASA LANGSUNG </b></label></center>
    <center style="font-size:12px; font-family:arial;"><label for=""><b><?php echo $judul_pekerjaan ?></b></label></center>
    <br>

    <?php
    $datePenawaran = date('D', strtotime($data_jadwal[6]['tanggal']));
    $dayJadwal = '';
    switch (strtolower($datePenawaran)) {
     case 'sun':
      $dayJadwal = 'Minggu';
      break;
     case 'mon':
      $dayJadwal = 'Senin';
      break;
     case 'tue':
      $dayJadwal = 'Selasa';
      break;
     case 'wed':
      $dayJadwal = 'Rabu';
      break;
     case 'thr':
      $dayJadwal = 'Kamis';
      break;
     case 'fri':
      $dayJadwal = 'Jumat';
      break;
     case 'sat':
      $dayJadwal = 'Sabtu';
      break;

     default:
      # code...
      break;
    }

    ?>
    <table style="width: 100%;">
     <tr>
      <td style="font-size:12px; font-family:arial;text-align:justify;">
       Pada Hari ini <?php echo $dayJadwal ?>, tanggal <?php echo str_replace('rupiah', '', terbilang(date('d', strtotime($data_jadwal[6]['tanggal'])))) ?>, bulan <?php echo translateOnlyMonthToIndo(date('d F Y', strtotime($data_jadwal[6]['tanggal']))) ?>, tahun <?php echo str_replace('rupiah', '', terbilang(date('Y', strtotime($data_jadwal[6]['tanggal'])))) ?> (<?php echo date('d-m-Y', strtotime($data_jadwal[6]['tanggal'])) ?>) pukul 09:00 WIB, bertempat di Kantor PT. PLN Unit Induk Transmisi Jawa Bagian Timur dan Bali - <?php echo $nama_upt ?>, telah diberikan penjelasan mengenai Rencana Kerja dan Syarat-syarat Pengadaan Langsung melalui Daftar Rekanan yang diundang pada tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[4]['tanggal']))) ?>.
      </td>
     </tr>
    </table>
    <br>
    
    <table style="width: 100%;">
     <tr>
      <td style="font-size:12px; font-family:arial;">1.</td>
      <td style="font-size:12px; font-family:arial;">Daftar Rekanan yang di undang 1 (satu) rekanan ialah :</td>
     </tr>
    </table>
    <br>

    <center style="font-size:12px; font-family:arial;"><i><b><?php echo strtoupper($nama_vendor) ?></b></i></center>
    <br>

    <table style="width: 100%;">
     <tr>
      <td style="font-size:12px; font-family:arial;">2.</td>
      <td style="font-size:12px; font-family:arial;">Rekanan yang hadir pada saat Pemberian Penjelasan yaitu :</td>
     </tr>
    </table>

    <br>
    <table style="width: 600px;border: 1px solid #333;border-collapse: collapse;">
     <tr style="background-color: #d9edf7">
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Nama Perusahaan</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Penanggung jawab</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Tanda Tangan</b>
      </td>
     </tr>
     <?php $no = 1 ?>
     <?php foreach ($data_rekanan as $key => $value) { ?>
      <tr>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"><?php echo $value['nama_vendor'] ?></td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"><?php echo $value['nama_pimpinan'] ?></td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      </tr>
     <?php } ?>
    </table>
    <br>

    <table style="width: 100%;">
     <tr>
      <td width="20" style="font-size:12px; font-family:arial;">3.</td>
      <td style="font-size:12px; font-family:arial;">Keterangan lain</td>
     </tr>
     <tr>
      <td width="20" style="font-size:12px; font-family:arial;"></td>
      <td style="font-size:12px; font-family:arial;">
      <table>
        <tr>
          <td style="font-size:12px; font-family:arial;" valign="top"><i>-</i></td>
          <td style="font-size:12px; font-family:arial;"><i><?php echo $judul_pekerjaan ?> dengan nilai total RAB sebesar Rp. <?php echo number_format($total_anggaran, 0, ',', '.') ?>, -(<?php echo terbilang($total_anggaran) ?>).</i></td>
        </tr>
        <tr>
          <td style="font-size:12px; font-family:arial;" valign="top"><i>-</i></td>
          <td style="font-size:12px; font-family:arial;"><i>Rekanan tersebut telah memahami isi Rencana Kerja dan syarat-syarat Pengadaan Langsung : <?php echo $data_jadwal[6]['nomor_pekerjaan'] ?>, Tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[6]['tanggal']))) ?></i></td>
        </tr>
      </table>
    </td>
     </tr>
    </table>

    <table style="width: 100%;">
     <tr>
      <!-- <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       Yang Mengusulkan,
       <br>
       <b>PEJABAT PELAKSANA PENGADAAN</b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>ZENDIDIA Y.</u></b>
      </td> -->
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <?php echo ucfirst(trim(str_replace('UPT', '', $nama_upt))) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[6]['tanggal']))) ?>
       <br>
       <b>Pejabat Pelaksana Pengadaan</b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>ZENDIDIA Y.</u></b>
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>