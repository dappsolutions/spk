<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Pengadaan Langsung</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;font-family: arial;font-size:12px;">
    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.15</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Surat Pernyataan Kinerja Baik dan Tidak Termasuk Daftar Hitam</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.16</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan daftar pengalaman pekerjaan sesuai pekerjaan yang akan diambil.</td>
        </tr>
        <!--ADA Kondisi lagi dari database-->
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.17</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Melampirkan Copy Sertifikat Kompetensi dan Kualifikasi Perusahaan bidang pemasok barang</td>
        </tr>
       </table>
      </td>
     </tr>
    </table>



    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">4.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">HARGA PENAWARAN</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">4.1</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Harga Barang/jasa Franko Lokasi Kantor PT PLN (Persero) UIT JBM UPT Probolinggo dan sudah termasuk Pajak.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">4.2</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Harga harus ditulis dengan angka dan huruf yang jelas, Jumlah yang tertera dalam angka harus sesuai dengan jumlah yang tertera dengan huruf.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">4.3</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Mencantumkan batas waktu berlakunya Surat Penawaran harga 30 (tiga puluh) hari kalender</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">4.4</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Penilaian harga berdasarkan harga paket.</td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">5.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">SYARAT – SYARAT ADMINISTRATIF</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.1</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Mencantumkan waktu penyerahan pekerjaan dengan jelas.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.2</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pembayaran akan dilaksanakan berdasarkan Berita Acara Pemeriksaan dan Penerimaan Penyerahan Pekerjaan</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.3</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Tidak diberikan uang muka.</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.4</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pembayaran akan dilakukan dengan transfer ke nomor rekening Bank yang ditunjuk oleh Kontraktor sesuai dengan Referensi Bank/Surat Keterangan Nomor rekening yang </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.5</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Dalam hal terjadi keterlambatan penyerahan pekerjaan yang melampaui batas waktu yang ditentukan, akan dikenakan sanksi berupa denda sebesar 1°/οο (satu perseribu) </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.6</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Sanksi tersebut tidak berlaku dalam hal terjadi sebab kahar (Force majeure). </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">5.7</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila terjadi keterlambatan pelaksanaan pekerjaan terjadi penyimpangan oleh Perusahaan atau tidak dapat melaksanakan pekerjaan tersebut pada butir 1.2 atau dalam </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">6.</td>
      <td style="font-size:12px;font-family: arial;text-align: left;font-weight:bold;">SYARAT – SYARAT TEKNIK :</td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.1</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Nama Pekerjaan</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;"><?php echo $judul_pekerjaan ?></td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.2</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Latar Belakang</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;"><i>Sesuai Lampiran Kerangka Acuan Kerja.</i></td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.3</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Maksud dan Tujuan</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;"><i>Sesuai Lampiran Kerangka Acuan Kerja.</i></td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.4</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Target /Sasaran</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;"><i>Sesuai Lampiran Kerangka Acuan Kerja.</i></td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.5</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Sumber Dana</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Menggunakan anggaran RKAU <?php echo $tipe_anggaran ?> nomer SKK : <?php echo $no_skk ?></td>
        </tr>        
       </table>
      </td>
     </tr>
    </table>


    <!-- <table>
     <tr>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       ZENDIDIYA Y.
      </td>
     </tr>
    </table> -->
   </div>
   <br>
  </div>
 </div>
</body>

</html>