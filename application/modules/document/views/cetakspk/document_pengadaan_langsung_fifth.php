<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Pengadaan Langsung</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;font-family: arial;font-size:12px;">
    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;"></td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">e)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib membuat Job Safety Analysis (JSA) dan Ijin kerja (working Permit) pada setiap melaksanakan pekerjaan yang berpotensi bahaya.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">f)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib melakukan pemeriksaan kesehatan kerja bagi tenaga kerjanya yang bekerja pada pekerjaan yang berpotensi.</td>
           </tr>
          </table>
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">7.2</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pencegahan Tindakan Berbahaya (Unsafe Action)</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib melakukan pengendalian personal terhadap perilaku berbahaya (unsafe act) dari pelaksana pekerjaan , antara lain :</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">a)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib menunjuk dan menetapkan pengawas pekerjaan / pengawas K3 yang memiliki kompetensi di bidang pekerjaannya.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">b)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib memasang LOTO (lock Out Tag Out) pada saat pelaksaan pekerjaan yang berpotensi bahaya.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">c)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Pengawas Pekerjaan dan Pelaksana pekerjaan dari Mitra kerja wajib menggunakan peralatan kerja dan APD sesuai standart (SNI, ANSI, CSA dll) pada pelaksanaan pekerjaan yang berpotensi bahaya</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">d)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib melakukan pembinaan dan pengawasan terhadap perilaku tenaga kerjanya yang membahayakan bagi diri sendiri maupun bagi orang lain, yang dapat menyebabkan terjadinya kecelakaan kerja.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">e)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib memberikan petunjuk dan arahan keselamatan (safety briefing) kepada pelaksana pekerjaan dan pengawas pekerjaan sebelum melaksanakan pekerjaan yang berpotensi bahaya</td>
           </tr>
          </table>
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">7.3</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Sertifikasi / Pendidikan & Pelatihan</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">a)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja wajib melakukan sertifikasi kompetensi bagi Pengawas Pekerjaan Pelaksana Pekerjaan dan Tenaga Teknik lainnya sesuai dengan bidang pekerjaannya;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">b)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib memiliki pengawas pekerjaan dan pelaksana pekerjaan yang telah memiliki kompetensi teknis sesuai dengan jenis pekerjaan;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">c)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib memiliki pengawas pekerjaan yang telah memiliki kompetensi K3;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">d)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra Kerja wajib memberikan Pendidikan dan Pelatihan bagi Pengawas Pekerjaan, Pelaksana Pekerjaan dan tenaga Teknik lainnya sesuai dengan bidang pekerjaanya;</td>
           </tr>
          </table>
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">7.4</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Sanksi K3</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">a)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila terjadi kecelakaan kerja akibat kelalaian Mitra Kerja dalam penerapan Sistem Manajemen Keselamatan dan Kesehatan Kerja, maka Mitra Kerja Bertanggung jawab secara penuh untuk menyelesaikan segala permasalahan yang ditimbulkan akibat kecelakaan tersebut.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">b)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila terjadi kecelakaan kerja akibat kelalaian Pelaksana Pekerjaan dari Mitra Kerja, maka Pelaksana Pekerjaan tersebut bertanggung jawab secara penuh atas akibat kecelakaan tersebut.</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">c)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila terjadi kecelakaan kerja yang mengakibatkan luka berat, luka berat yang menyebabkan cacat dan meninggal dunia pada pelaksana pejerjaan dari mitra kerja sebagai akibat dari kesalahan pekerjaan operasi dan pemeliharaan yang dilaksanakan oleh mitra kerja maka :</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">
            <table>
             <tr>
              <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">1.</td>              
              <td style="font-size:12px;font-family: arial;text-align: justify;">Pengawas pekerjaan dan pelaksana pekerjaan yang melaksanakan pekerjaan tersebut dilarang untuk bekerja atau di-suspend selama 2 (dua) bulan padapekerjaan teknis di lapangan;</td>              
             </tr>
             <tr>
              <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">2.</td>              
              <td style="font-size:12px;font-family: arial;text-align: justify;">Mitra kerja dikenakan denda maksimal 10% persen (sepuluh per serratus) dari nilai tagihan pada bulan kejadian.</td>              
             </tr>
            </table>
            </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">d)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila kecelakaan kerja terjadi pada masa transisi perjanjian kerja, maka untuk sanksi sesuai dengan nomor 7.4 poin C akan tetap diberlakukan</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">e)</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila terjadi kecelakaan kerja akibat kelalaian Mitra Kerja dalam penerapan Sistem Manajemen Keselamatan dan Kesehatan Kerja, maka PT PLN (Persero) berhak mengevaluasi, memutus perjanjian barang dan jasa yang sedang berlangsung secara sepihak serta memasukkan Mitra Kerja tersebut pada Daftar Hitam (black list) Perusahaan.</td>
           </tr>
          </table>
         </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <!-- <table>
     <tr>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       ZENDIDIYA Y.
      </td>
     </tr>
    </table> -->
   </div>
   <br>
  </div>
 </div>
</body>

</html>