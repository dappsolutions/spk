

<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Lampiran Dokumen BA</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 3px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="">
    <table style="width: 100%;">
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       <b><u>LAMPIRAN BERITA ACARA NEGOSIASI</u></b>
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       <b><?php echo $judul_pekerjaan ?></b>
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       Nomor : <?php echo $data_jadwal[12]['nomor_pekerjaan'] ?>
      </td>
     </tr>
    </table>

    <table style="width: 100%;border: 1px solid #333;border-collapse: collapse;">
     <tr style="background-color: #d9edf7">
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>NO</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>URAIAN</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>VOLUME</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>HARGA PADA</b> <br> ANGGARAN BIAYA OPERASI
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>HARGA PADA HPS PLN</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>HARGA DISEPAKATI</b>
      </td>
     </tr>
     <tr style="background-color: #d9edf7">
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       Jml
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       Sat
      </td>
      <td width="100" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       SATUAN
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       JUMLAH
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       SATUAN
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       JUMLAH
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       SATUAN
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       JUMLAH
       <br>
       Rp.
      </td>
     </tr>

     <tr>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
       <b>I</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
       <b>Material</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;"></td>
     </tr>
     <?php $total_harga_anggaran = 0 ?>
     <?php $total_harga_pln = 0 ?>
     <?php $total_harga_sepakat = 0 ?>
     <?php $no = 1 ?>
     <?php foreach ($data_rab as $key => $value) { ?>
      <tr>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo $no++ ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo $value['nama_produk'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo $value['jml_vol'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo $value['nama_satuan'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo number_format($value['satuan_anggaran'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo number_format($value['jumlah_anggaran'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo number_format($value['satuan_hps'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo number_format($value['jumlah_hps'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo number_format($value['harga_sepakat'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
        <?php echo number_format($value['jumlah_sepakat'], 0, ',', '.') ?>
       </td>
      </tr>
      <?php $total_harga_anggaran += $value['jumlah_anggaran'] ?>
      <?php $total_harga_pln += $value['jumlah_hps'] ?>
      <?php $total_harga_sepakat += $value['jumlah_sepakat'] ?>
     <?php } ?>

     <tr>
      <td rowspan="3" style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;text-align: right;">
       Total Harga :
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <?php echo number_format($total_harga_anggaran,0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <?php echo number_format($total_harga_pln,0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <?php echo number_format($total_harga_sepakat,0, ',', '.') ?>
      </td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;text-align: right;">
       PPN :
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <?php $harga_ppn = $total_harga_anggaran * $ppn ?>
       <?php $total_harga_ppn = $total_harga_anggaran + $harga_ppn ?>
       <?php echo number_format($harga_ppn,0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <?php $harga_ppn_pln = $total_harga_pln * $ppn ?>
       <?php $total_harga_ppn_pln = $total_harga_pln + $harga_ppn_pln ?>
       <?php echo number_format($harga_ppn_pln,0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <?php $harga_ppn_sepakat = $total_harga_sepakat * $ppn ?>
       <?php $total_harga_ppn_sepakat = $total_harga_sepakat + $harga_ppn_sepakat ?>
       <?php echo number_format($harga_ppn_sepakat,0, ',', '.') ?>
      </td>
     </tr>
     <tr>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       Jumlah harga termasuk PPN :
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <b><?php echo number_format($total_harga_ppn,0, ',', '.') ?></b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <b><?php echo number_format($total_harga_ppn_pln,0, ',', '.') ?></b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: right;">
       <b><?php echo number_format($total_harga_ppn_sepakat,0, ',', '.') ?></b>
      </td>
     </tr>
    </table>
    <table style="width: 100%;border: 1px solid #333;border-collapse: collapse;">
     <tr>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;text-align: center;">
       <i><b>Terbilang : <?php echo terbilang($total_harga_ppn_sepakat) ?></b></i>
      </td>
     </tr>
    </table>
    <br>


    <table>
     <tr>
      <td width="540" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       <?php echo ucfirst(trim(str_replace('UPT', '', $nama_upt))) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[12]['tanggal']))) ?>
       <br>
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       ZENDIDIYA Y.
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>