<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen Pengadaan Langsung</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;font-family: arial;font-size:12px;">
    <table>
     <tr>
      <td style="font-size:12px;font-family: arial;text-align: left;">&nbsp;</td>
      <td style="font-size:12px;font-family: arial;text-align: left;">
       <table>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.6</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Tata Cara Pembayaran</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Metode pembayaran dilakukan dalam 2 tahap, setelah pekerjaan selesai 100% Tagihan. Tagihan pembayaran dengan melampirkan persyaratan sebagai berikut : </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">a.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Surat Permohonan Pembayaran</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">b.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Kuitansi Rangkap 3 (tiga) lembar untuk lembar pertama bermaterai cukup;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">c.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">E-Faktur Pajak;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">d.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Berita Acara Pemeriksaan Barang;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">e.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Berita Acara Serah Terima Barang;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">f.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Berita Acara Pembayaran;</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">g.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Copy Nomor Pokok Wajib Pajak (NPWP);</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">h.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Copy Pengukuhan Pengusaha Kena Pajak (PKP);</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">i.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Copy Pengukuhan Pengusaha Kena Pajak (PKP);</td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">j.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Berita Acara Denda (Bila ada).</td>
           </tr>
          </table>
         </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.7</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Spesifikasi Teknis</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;"><i>Sesuai Lampiran Kerangka Acuan Kerja.</i></td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;" valign="top">6.8</td>
         <td style="font-size:12px;font-family: arial;text-align: justify;font-weight:bold;">Integritas dan Good Corporate Governance</td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">Pengguna Barang/Jasa selanjutnya disebut PIHAK PERTAMA, dengankan Penyedia Barang/Jasa disebut PIHAK KEDUA. PIHAK PERTAMA dan PIHAK KEDUA secara bersama </td>
        </tr>
        <tr>
         <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
         <td style="font-size:12px;font-family: arial;text-align: justify;">
          <table>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">1.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Sehubungan dengan Perjanjian dan hal-hal, dokumen-dokumen, kegiatan-kegiatan, dan transaksi-transaksi yang dimaksud dalam atau terkait dengan Perjanjian ini PARA </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">2.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">PARA PIHAK menyepakati bahwa Perjanjian ini dilaksanakan dengan itikad baik, tidak saling mempengaruhi baik langsung maupun tidak langsung guna memenuhi </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">3.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">PIHAK KEDUA menyatakan, menjamin dan berkomitmen kepada PIHAK PERTAMA bahwa dalam melaksanakan Perjanjian akan mematuhi ketentuan hukum yang berlaku </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">
             <table>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">A)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Menerapkan 4 No’s:</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">
                <table>
                 <tr>
                  <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">i) </td>
                  <td style="font-size:12px;font-family: arial;text-align: justify;">No bribery, menghindari suap menyuap dari pemerasan,</td>
                 </tr>
                 <tr>
                  <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">ii) </td>
                  <td style="font-size:12px;font-family: arial;text-align: justify;">No gift, menghindari hadiah atau gratifikasi yang bertentangan dengan peraturan yang berlaku,</td>
                 </tr>
                 <tr>
                  <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">iii) </td>
                  <td style="font-size:12px;font-family: arial;text-align: justify;">No kickback, menghindari komisi, tanda terima kasih baik dalam bentuk uang atau bentuk lainnya,</td>
                 </tr>
                 <tr>
                  <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">vi) </td>
                  <td style="font-size:12px;font-family: arial;text-align: justify;">No luxurious hospitality, menghindari jamuan yang berlebihan;</td>
                 </tr>
                </table>
               </td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">B)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Mengikuti prosedur uji kelayakan berbasis integritas (integrity due diligence) yang diterapkan PIHAK PERTAMA;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">C)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Mengikuti program sosialisasi sistem manajemen anti-penyuapan yang dilaksanakan PIHAK PERTAMA yang dapat diwakili oleh pegawai PIHAK KEDUA yang ditunjuk untuk mewakili perusahaan PIHAK KEDUA;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">D)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Menandatangani dan melaksanakan pakta integritas PIHAK KEDUA;</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">E)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Melaporkan insiden fraud melalui Whistle Blowing System PLN.</td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">4.</td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">Apabila dalam pelaksanaan Perjanjian terjadi tindakan penyimpangan dan/atau kecurangan dan berpotensi mengakibatkan dalam transaksi bisnis, maka PIHAK PERTAMA </td>
           </tr>
           <tr>
            <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top"></td>
            <td style="font-size:12px;font-family: arial;text-align: justify;">
             <table>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">A)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Adanya indikasi manipulasi harga baik penggelembungan (mark up) maupun pengurangan (mark down).</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">B)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Adanya indikasi proyek fiktif.</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">C)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Adanya indikasi pemalsuan identitas mitra bisnis.</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">D)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Adanya indikasi barang/jasa dibawah spesifikasi/kualitas yang disepakati.</td>
              </tr>
              <tr>
               <td style="font-size:12px;font-family: arial;text-align: justify;" valign="top">E)</td>
               <td style="font-size:12px;font-family: arial;text-align: justify;">Adanya indikasi pemberian gratifikasi dalam proses pengadaan barang/jasa atau pelaksanaan transaksi bisnis.</td>
              </tr>
             </table>
            </td>
           </tr>
          </table>
         </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>


    <!-- <table>
     <tr>
      <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
       PEJABAT PELAKSANA PENGADAAN
       <br>
       <br>
       <br>
       ZENDIDIYA Y.
      </td>
     </tr>
    </table> -->
   </div>
   <br>
  </div>
 </div>
</body>

</html>