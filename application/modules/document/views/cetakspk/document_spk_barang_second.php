<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen SPK</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;">
    <?php
    $datePenawaran = date('D', strtotime($data_jadwal[13]['tanggal']));
    $dayJadwal = '';
    switch (strtolower($datePenawaran)) {
     case 'sun':
      $dayJadwal = 'Minggu';
      break;
     case 'mon':
      $dayJadwal = 'Senin';
      break;
     case 'tue':
      $dayJadwal = 'Selasa';
      break;
     case 'wed':
      $dayJadwal = 'Rabu';
      break;
     case 'thr':
      $dayJadwal = 'Kamis';
      break;
     case 'fri':
      $dayJadwal = 'Jumat';
      break;
     case 'sat':
      $dayJadwal = 'Sabtu';
      break;

     default:
      # code...
      break;
    }

    ?>

    <table>
     <tr>
      <td style="font-size:12px; font-family:arial;"><b>4.</b></td>
      <td style="font-size:12px; font-family:arial;"><b>PEMBAYARAN</b></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"></td>
      <td style="font-size:12px; font-family:arial;text-align: justify;">Metode Pembayaran dapat dilakukan setelah pekerjaan selesai 100 % sesuai dengan yang di instruksikan, dibuktikan dengan BA serah terima barang sebagaimana yang diatur lampiran SPKPB.</td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"><b>5.</b></td>
      <td style="font-size:12px; font-family:arial;"><b>Instruksi kepada PIHAK KEDUA sebagaimana yang diatur dalam lampiran </b></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"></td>
      <td style="font-size:12px; font-family:arial;text-align: justify;">SPKPB ini dibuat dalam 3 (tiga) rangkap yang masing-masing mempunyai kekuatan hukum yang sama, 2 (dua) diantaranya ditandatangani di atas materai yang sama, Asli yang bermaterai masing-masing untuk PIHAK PERTAMA dan PIHAK KEDUA, Asli tidak bermaterai untuk arsip.</td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"></td>
      <td style="font-size:12px; font-family:arial;text-align: justify;">Demikian, SPKPB ini ditandatangani di <?php echo str_replace('UPT', '', $nama_upt) ?> pada hari <?php echo $dayJadwal ?>, tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[13]['tanggal']))) ?>, untuk dilaksanakan dengan sebagaimana mestinya.</td>
     </tr>
    </table>
    <br>
    <br>
    <br>
    <br>
    <table style="width: 100%;">
     <tr>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b>PIHAK KEDUA</b>
       <br>
       <b><?php echo strtoupper($nama_vendor) ?></b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u><?php echo strtoupper($nama_pimpinan) ?></u></b>
       <br>
       <b><?php echo $jabatan_pimpinan ?></b>
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b>PIHAK PERTAMA</b>
       <br>
       <b>PT. PLN (Persero) UIT JBM <?php echo $nama_upt ?></b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>AHMAD AZHARI KEMMA</u></b>
       <br>
       <b>MANAGER</b>
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>