<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen SPK</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;">
    <center><b style="font-size:12px; font-family:arial;"><b><u>SURAT PERINTAH KERJA PENGADAAN BARANG</u></b></center>
    <center><b style="font-size:12px; font-family:arial;"><b>(SPKB)</b></center>
    <center style="font-size:12px; font-family:arial;"><b><label for=""><?php echo $data_jadwal[13]['nomor_pekerjaan'] ?></label></b></center>
    <br>

    <?php
    $datePenawaran = date('D', strtotime($data_jadwal[13]['tanggal']));
    $dayJadwal = '';
    switch (strtolower($datePenawaran)) {
     case 'sun':
      $dayJadwal = 'Minggu';
      break;
     case 'mon':
      $dayJadwal = 'Senin';
      break;
     case 'tue':
      $dayJadwal = 'Selasa';
      break;
     case 'wed':
      $dayJadwal = 'Rabu';
      break;
     case 'thr':
      $dayJadwal = 'Kamis';
      break;
     case 'fri':
      $dayJadwal = 'Jumat';
      break;
     case 'sat':
      $dayJadwal = 'Sabtu';
      break;

     default:
      # code...
      break;
    }

    ?>

    <table>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top"><b>Berdasarkan</b></td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top">
       <table>
        <tr>
         <td style="font-size:12px; font-family:arial;text-align:justify;" valign="top">1.</td>
         <td style="font-size:12px; font-family:arial;text-align:justify;">Dokumen Pengadaan Langsung Nomor : <?php echo $data_jadwal[1]['nomor_pekerjaan'] ?>, Tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[1]['tanggal']))) ?></td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;text-align:justify;" valign="top">2.</td>
         <td style="font-size:12px; font-family:arial;text-align:justify;">Surat Penawaran Harga Nomor : <?php echo $data_jadwal[4]['nomor_pekerjaan'] ?>, Tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[4]['tanggal']))) ?></td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;text-align:justify;" valign="top">3.</td>
         <td style="font-size:12px; font-family:arial;text-align:justify;">Berita Acara Evaluasi Penawaran Harga Nomor : <?php echo $data_jadwal[10]['nomor_pekerjaan'] ?>, Tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[10]['tanggal']))) ?></td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;text-align:justify;" valign="top">4.</td>
         <td style="font-size:12px; font-family:arial;text-align:justify;">Berita Acara Negosiasi Nomor : <?php echo $data_jadwal[11]['nomor_pekerjaan'] ?>, Tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[11]['tanggal']))) ?></td>
        </tr>
       </table>
      </td>
      <td style="font-size:12px; font-family:arial;">
      </td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top"><b>Dengan ini kami :</b></td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top">
       <table>
        <tr>
         <td style="font-size:12px; font-family:arial;">NAMA</td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;">AHMAD AZHARI KEMMA</td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;">JABATAN</td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;">MANAGER</td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;">Bertindak untuk atas nama </td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;">PT. PLN (Persero) UITJBM - <?php echo $nama_upt ?></td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;">Alamat</td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;"><?php echo $alamat_upt ?></td>
        </tr>
       </table>
      </td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top">Selanjutnya disebut <b>PIHAK PERTAMA</b></td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top"><b>Memerintahka kepada :</b></td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top">
       <table>
        <tr>
         <td style="font-size:12px; font-family:arial;">NAMA</td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;"><?php echo strtoupper($nama_pimpinan) ?></td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;">JABATAN</td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;"><?php echo strtoupper($jabatan_pimpinan) ?></td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;">Bertindak untuk atas nama </td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;"><?php echo strtoupper($nama_vendor) ?></td>
        </tr>
        <tr>
         <td style="font-size:12px; font-family:arial;">Alamat</td>
         <td style="font-size:12px; font-family:arial;">:</td>
         <td style="font-size:12px; font-family:arial;"><?php echo $alamat_upt ?></td>
        </tr>
       </table>
      </td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top">Selanjutnya disebut <b>PIHAK KEDUA</b></td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
    </table>

    <center><b style="font-size:12px; font-family:arial;">UNTUK MELAKSANAKAN :</b></center>
    <center><b style="font-size:12px; font-family:arial;"><?php echo $judul_pekerjaan ?></b></center>

    <table>
     <tr>
      <td style="font-size:12px; font-family:arial;">Dengan ketentuan sebagai berikut :</td>
      <td style="font-size:12px; font-family:arial;"></td>
     </tr>
    </table>
    <table>
     <tr>
      <td style="font-size:12px; font-family:arial;"><b>1.</b></td>
      <td style="font-size:12px; font-family:arial;"><b>SUMBER DANA</b></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"></td>
      <td style="font-size:12px; font-family:arial;text-align: justify;">Dana untuk pembayaran Pengadaan Barang sebagaiamana dimaksud pada SPKPB ini disediakan dari Anggaran <?php echo $tipe_anggaran ?> Tahun <?php echo date('Y', strtotime($tgl_pelaksanaan)) ?> - <?php echo $no_skk ?> PT. PLN (Persero) UIT JBTB <?php echo $nama_upt ?>.</td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"><b>2.</b></td>
      <td style="font-size:12px; font-family:arial;"><b>HARGA PENGADAAN</b></td>
     </tr>    
     <tr>
      <td style="font-size:12px; font-family:arial;"></td>
      <td style="font-size:12px; font-family:arial;text-align: justify;">Harga dan rincian sebagaimana tersebut dalam lampiran SPKPB ini adalah Rp. <?php echo number_format($total_harga_sepakat, 0, ',', '.') ?>,- (<?php echo terbilang($total_harga_sepakat) ?>) Harga sudah termasuk Pajak Pertambahan Nilai (PPN) sebesar <?php echo $ppn*100 ?>% (<?php echo str_replace('rupiah', '', terbilang($ppn*100)) ?> persen) Dan sudah termasuk segala pengeluaran serta pajak-pajak sesuai dengan peraturan yang berlaku.</td>
     </tr>
     <tr>
      <td></td>
      <td></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"><b>3.</b></td>
      <td style="font-size:12px; font-family:arial;"><b>WAKTU PELAKSANAAN</b></td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;"></td>
      <td style="font-size:12px; font-family:arial;text-align: justify;">Penyerahan pekerjaan diserahkan dalam jangka waktu <?php echo $lama_pekerjaan ?> (<?php echo str_replace('rupiah', '', terbilang($lama_pekerjaan)) ?>) hari kalender terhitung sejak tanggal penandatanganan SPKPB ini paling lambat tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($tgl_selesai_pekerjaan))) ?>, seluruh pekerjaan tersebut harus sudah selesai 100% (seratus persen).</td>
     </tr>    
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>