<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen BA Nego</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;">
    <center><b style="font-size:12px; font-family:arial;"><u>BERITA ACARA NEGOSIASI</u></b></center>
    <center style="font-size:12px; font-family:arial;"><b><label for="">Nomor : <?php echo $data_jadwal[11]['nomor_pekerjaan'] ?></label></b></center>
    <br>

    <?php
    $datePenawaran = date('D', strtotime($data_jadwal[11]['tanggal']));
    $dayJadwal = '';
    switch (strtolower($datePenawaran)) {
     case 'sun':
      $dayJadwal = 'Minggu';
      break;
     case 'mon':
      $dayJadwal = 'Senin';
      break;
     case 'tue':
      $dayJadwal = 'Selasa';
      break;
     case 'wed':
      $dayJadwal = 'Rabu';
      break;
     case 'thr':
      $dayJadwal = 'Kamis';
      break;
     case 'fri':
      $dayJadwal = 'Jumat';
      break;
     case 'sat':
      $dayJadwal = 'Sabtu';
      break;

     default:
      # code...
      break;
    }

    ?>
    <table style="width: 100%;">
     <tr>
      <td style="font-size:12px; font-family:arial;text-align:justify;">
       Pada Hari ini <?php echo $dayJadwal ?>, tanggal <?php echo str_replace('rupiah', '', terbilang(date('d', strtotime($data_jadwal[11]['tanggal'])))) ?>, bulan <?php echo translateOnlyMonthToIndo(date('d F Y', strtotime($data_jadwal[11]['tanggal']))) ?>, tahun <?php echo str_replace('rupiah', '', terbilang(date('Y', strtotime($data_jadwal[11]['tanggal'])))) ?> (<?php echo date('d-m-Y', strtotime($data_jadwal[11]['tanggal'])) ?>), Pejabat Pelaksana Pengadaan PT. PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur dan Bali <?php echo $nama_upt ?> telah melakukan Negosiasi Penurunan Harga kepada : <?php echo strtoupper($nama_vendor) ?>
      </td>
     </tr>
    </table>
    <br>

    <table style="width: 100%;">
     <tr>
      <td width="200" style="font-size:12px; font-family:arial;" valign="top">Nama Pekerjaan </td>
      <td width="20" style="font-size:12px; font-family:arial;">:</td>
      <td style="font-size:12px; font-family:arial;"><?php echo $judul_pekerjaan ?></td>
     </tr>
     <tr>
      <td width="200" style="font-size:12px; font-family:arial;" valign="top">Harga yang ditawarkan</td>
      <td width="20" style="font-size:12px; font-family:arial;" valign="top">:</td>
      <td style="font-size:12px; font-family:arial;text-align: justify;">Rp, <?php echo number_format($total_penawaran, 0, ',', '.') ?>,- (<?php echo terbilang($total_penawaran) ?>)</td>
     </tr>
    </table>
    <br>

    <table style="width: 100%;">
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top">PT. PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur dan Bali <?php echo $nama_upt ?>, meminta kepada <?php echo strtoupper($nama_vendor) ?> untuk dapat menurunkan harga penawaran item pekerjaan.</td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;" valign="top">Berdasarkan kesepakatan maka dengan ini kami <?php echo strtoupper($nama_vendor) ?> setuju untuk menurunkan harga item, dengan rincian sebagai berikut :</td>
     </tr>
    </table>

    <br>
    <br>

    <table style="width: 100%;border-collapse: collapse;">
     <tr style="background-color: #d9edf7">
      <td style="border:1px solid #ccc;font-size:12px; font-family:arial;padding:16px;" valign="top"><b>NAMA BARANG/PEKERJAAN JASA</b></td>
      <td style="border:1px solid #ccc;font-size:12px; font-family:arial;padding:16px;"><b>HARGA PENAWARAN</b></td>
      <td style="border:1px solid #ccc;font-size:12px; font-family:arial;padding:16px;"><b>HARGA YANG DISEPAKATI</b></td>
     </tr>
     <tr>
      <td style="border:1px solid #ccc;font-size:12px; font-family:arial;padding:16px;" valign="top"><b><?php echo $judul_pekerjaan ?></b></td>
      <td style="border:1px solid #ccc;font-size:12px; font-family:arial;padding:16px;text-align: justify;"><b>Rp, <?php echo number_format($total_penawaran, 0, ',', '.') ?>,-</b></td>
      <td style="border:1px solid #ccc;font-size:12px; font-family:arial;padding:16px;" valign="top"><b>Rp, <?php echo number_format($total_harga_sepakat, 0, ',', '.') ?>,-</b></td>
     </tr>
    </table>

    <br>
    <table>
     <tr>
      <td style="font-size:12px; font-family:arial;">Harga sudah termasuk Pajak Pertambahan Nilai <?php echo $ppn*100 ?>%</td>
     </tr>
     <tr>
      <td style="font-size:12px; font-family:arial;">Demikian Berita Acara dibuat dan disepakati untuk dilaksanakan.</td>
     </tr>
    </table>

    <table style="width: 100%;">
     <tr>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b><?php echo strtoupper($nama_vendor) ?></b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u><?php echo strtoupper($nama_pimpinan) ?></u></b>
      </td>
      <!-- <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b>Menyetujui</b>
       <br>
       <b>MANAGER</b>
       <br>
       <br>
       <br>
       <br>
       <b><u>AHMAD AZHARI KEMMA</u></b>
      </td> -->
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b>Pejabat Pelaksana Pengadaan</b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>ZENDIDIA Y</u></b>
      </td>
     </tr>
    </table>
    
    <table style="width: 100%;">
     <tr>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b></b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b></b>
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b>Mengetahui</b>
       <br>
       <b>MANAGER</b>
       <br>
       <br>
       <br>
       <br>
       <b><u>AHMAD AZHARI KEMMA</u></b>
      </td>
      <td nowrap="nowrap" style="font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <b></b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b></b>
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>