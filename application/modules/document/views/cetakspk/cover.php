<?php 
 function translateMonthToIndo($date = ''){
  list ($day, $month, $year) = explode(' ', $date);

  $str = "";
  if($date != ''){
   switch (strtolower($month)) {
    case 'january':
     $month = "Januari";
     break;
    case 'february':
     $month = "Februari";
     break;
    case 'march':
     $month = "Maret";
     break;
    case 'may':
     $month = "Mei";
     break;
    case 'june':
     $month = "Juni";
     break;
    case 'july':
     $month = "Juli";
     break;
    case 'august':
     $month = "Agustus";
     break;
    case 'october':
     $month = "Oktober";
     break;
    case 'december':
     $month = "Desember";
     break;
    
    default:
     # code...
     break;
   }

   $str = $day.' '.$month.' '.$year;

   return $str;
  }
 }
 
 function translateOnlyMonthToIndo($date = ''){
  list ($day, $month, $year) = explode(' ', $date);

  $str = "";
  if($date != ''){
   switch (strtolower($month)) {
    case 'january':
     $month = "Januari";
     break;
    case 'february':
     $month = "Februari";
     break;
    case 'march':
     $month = "Maret";
     break;
    case 'may':
     $month = "Mei";
     break;
    case 'june':
     $month = "Juni";
     break;
    case 'july':
     $month = "Juli";
     break;
    case 'august':
     $month = "Agustus";
     break;
    case 'october':
     $month = "Oktober";
     break;
    case 'december':
     $month = "Desember";
     break;
    
    default:
     # code...
     break;
   }

   $month;

   return $month;
  }
 }

?>

<html>

<head>
 <title>Cover</title>

 <style>
  #_wrapper {
   width: 100%;
   margin: 0 auto;
  }

  #_content {
   border: 1px solid #039be5;
   /* max-width: 100%; */
   padding: 24px;
   margin-left: 36px;
   margin-right: 36px;
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
   font-size: 22px;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 24px;
   margin-right: 24px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div>&nbsp;</div>
   <div id="_top-content">
    <p style="font-size: 18px;font-family: arial;font-weight: bold;">PT. PLN (PERSERO)</p>
    <p style="font-size: 16px;font-family: arial;font-weight: bold;">
     Unit Induk Transmisi Jawa Bagian Timur dan Bali
     <br>
     <?php echo $nama_upt ?>
    </p>
   </div>
   <div id="_cover">
    <!-- <img src="<?php echo base_url() ?>files/img/smk3.png" height="45%"> -->
    <br />
    <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="20%">
   </div>
   <div>&nbsp;</div>
   <div style="font-family: arial;">
    <p style="font-size: 22px;font-family: arial;font-weight: bold;">
     <?php if ($sifat_pekerjaan == 'Supply Only') { ?>
      Surat Perintah Kerja Pengadaan Barang (SPKPB)
     <?php } else { ?>
      Surat Perintah Kerja Pemakaian Langsung (SPKPL)
     <?php } ?>
    </p>
   </div>
   <br>
   <br>

   <div id="_info-content">
    <div id="_info-content-isi">
     <h2 style="text-align: center;"></h2>
     <table style="font-size: 14px;font-family: arial;">
      <tr>
       <td width="120">Nomor</td>
       <td>:</td>
       <!-- <td><?php echo 'No. ' . $no_rab . '.SPBL/DAN.01.01/370400/' . date('Y', strtotime($tgl_pelaksanaan)) ?></td> -->
       <td><?php echo 'No. '.$data_jadwal[13]['nomor_pekerjaan']  ?></td>
      </tr>
      <tr>
       <td>Tanggal</td>
       <td>:</td>
       <td><?php echo translateMonthToIndo(date('d F Y', strtotime($tgl_pelaksanaan))) ?></td>
      </tr>
      <tr>
       <td>Perihal</td>
       <td>:</td>
       <td><?php echo $judul_pekerjaan ?></td>
      </tr>
      <tr>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
      </tr>
      <tr>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
      </tr>
      <tr>
       <td>Perusahaan</td>
       <td>:</td>
       <td><b><?php echo strtoupper($nama_vendor) ?></b></td>
      </tr>
      <tr>
       <td>&nbsp;</td>
       <td>&nbsp;</td>
       <td><?php echo ucfirst($alamat) ?></td>
      </tr>
     </table>
     <div>
      &nbsp;
     </div>
    </div>
   </div>
   <div id="_bottom-content">
    <h3><?php echo date('Y', strtotime($tgl_pelaksanaan)) ?></h3>
   </div>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>