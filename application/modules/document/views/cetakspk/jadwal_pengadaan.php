<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Jadwal Pengadaan</title>

  <style>
    #_wrapper {
      /* width: 100%; */
      /* margin: 0 auto; */
    }

    #_content {
      border: 1px solid white;
      /* max-width: 100%; */
      text-align: center;
      padding: 0;
    }

    #_top-content {
      /* margin: 0 auto; */
      font-family: arial;
    }

    #_int {
      /* margin: 2% auto; */
      font-family: arial;
    }


    #_bottom-content {
      font-family: arial;
    }

    #_info-content {
      border: 1px solid black;
      margin-left: 16px;
      margin-right: 16px;
      text-align: left;
      border-radius: 30px;
      padding: 1px;
    }

    #_info-content-isi {
      border: 1px solid black;
      text-align: left;
      padding: 6px;
      border-radius: 30px;
    }

    #_cover {
      /* margin-left: 3%; */
    }

    h2 {
      /* margin: 0.5%; */
    }
  </style>
</head>

<body>
  <div style="text-align: right;"></div>
  <div id="_wrapper">
    <div id="_content">
      <div id="_top-content">
        <table>
          <tr>
            <td rowspan="3">
              <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
            </td>
            <td style="font-family: arial;font-size: 8px;">
              PT. PLN (PERSERO)
            </td>
          </tr>
          <tr>
            <td style="font-family: arial;font-size: 8px;">
              UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
            </td>
          </tr>
          <tr>
            <td style="font-family: arial;font-size: 8px;">
              UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
            </td>
          </tr>
        </table>

        <!-- <p style="font-size: 12px;font-family: arial;font-weight: bold;"> -->
          <b><label for="">JADWAL PENGADAAN LANGSUNG</label></b>
          <br>
        <!-- </p> -->
        <!-- <p style="font-size: 12px;font-family: arial;font-weight: bold;"> -->
          <b><label for=""><?php echo ucfirst($judul_pekerjaan) ?></label></b>
          <br>
        <!-- </p> -->
        <!-- <p style="font-size: 12px;font-family: arial;font-weight: bold;"> -->
          <b><label for="">Sesuai SPBJL <?php echo 'No. ' . $data_jadwal[7]['nomor_pekerjaan']  ?></label></b>
          <br>
        <!-- </p> -->
        <!-- <p style="font-size: 12px;font-family: arial;font-weight: bold;"> -->
          <b><label for="">Tanggal <?php echo translateMonthToIndo(date('d F Y', strtotime($tgl_pelaksanaan))) ?></label></b>
          <br>
        <!-- </p> -->
      </div>

      <div class="content" style="padding:6px;">
        <table style="border: 1px solid #333;border-collapse: collapse;width: 100%;">
          <thead>
            <tr style="background-color: #d9edf7">
              <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">No</td>
              <td width="200" style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">URAIAN KEGIATAN</td>
              <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">Checklist</td>
              <!-- <td nowrap="nowrap" style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">&nbsp;</td> -->
              <td colspan="2" style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">Nomor</td>
              <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">Tanggal</td>
              <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">Keterangan</td>
            </tr>
            </tdead>
          <tbody>
            <?php echo $no = 1 ?>
            <?php foreach ($data_jadwal as $key => $value) { ?>
              <tr>
                <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;"><?php echo $no++ ?></td>
                <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;"><?php echo $value['uraian_pekerjaan'] ?></td>
                <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;">&nbsp;</td>
                <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;"><?php echo $no_rab ?></td>
                <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;"><?php echo $value['nomor_pekerjaan'] ?></td>
                <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;"><?php echo translateMonthToIndo(date('d F Y', strtotime($value['tanggal']))) ?></td>
                <td style="border:1px solid #ccc;font-size:14px;font-family: arial;padding:2px;"></td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>


      <?php $total_harga_anggaran = 0 ?>
      <?php $total_harga_penawaran = 0 ?>
      <?php $total_harga_pln = 0 ?>
      <?php $total_harga_sepakat = 0 ?>
      <?php $no = 1 ?>
      <?php foreach ($data_rab as $key => $value) { ?>
        <?php $total_harga_anggaran += $value['jumlah_anggaran'] ?>
        <?php $total_harga_penawaran += $value['jumlah_penawaran'] ?>
        <?php $total_harga_pln += $value['jumlah_hps'] ?>
        <?php $total_harga_sepakat += $value['jumlah_sepakat'] ?>
      <?php } ?>

      <div>
        <table>
          <tr>
            <td style="font-size:12px;font-family: arial;padding:2px;">Anggaran</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">:</td>
            <td style="font-size:12px;font-family: arial;padding:2px;"><?php echo $tipe_anggaran ?></td>
          </tr>
          <tr>
            <td style="font-size:12px;font-family: arial;padding:2px;">Harga RAB (Termasuk PPN 10 %)</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">:</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">Rp</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">
              <?php $harga_ppn = $total_harga_anggaran * $ppn ?>
              <?php $total_harga_ppn = $total_harga_anggaran + $harga_ppn ?>
              <?php echo number_format($total_harga_ppn, 0, ',', '.') ?>
            </td>
          </tr>
          <tr>
            <td style="font-size:12px;font-family: arial;padding:2px;">Harga Penawaran</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">:</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">Rp</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">
              <?php echo number_format($total_harga_penawaran, 0, ',', '.') ?>
            </td>
          </tr>
          <tr>
            <td style="font-size:12px;font-family: arial;padding:2px;">Harga HPS</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">:</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">Rp</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">
              <?php $harga_ppn_pln = $total_harga_pln * $ppn ?>
              <?php $total_harga_ppn_pln = $total_harga_pln + $harga_ppn_pln ?>
              <?php echo number_format($total_harga_ppn_pln, 0, ',', '.') ?>
            </td>
          </tr>
          <tr>
            <td style="font-size:12px;font-family: arial;padding:2px;">Harga Kesepakatan</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">:</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">Rp</td>
            <td style="font-size:12px;font-family: arial;padding:2px;">
              <?php $harga_ppn_sepakat = $total_harga_sepakat * $ppn ?>
              <?php $total_harga_ppn_sepakat = $total_harga_sepakat + $harga_ppn_sepakat ?>
              <?php echo number_format($total_harga_ppn_sepakat, 0, ',', '.') ?>
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <div style="text-align: right;">

  </div>
</body>

</html>