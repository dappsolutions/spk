<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Perihal Penawaran Harga</title>

  <style>
    #_wrapper {
      /* width: 100%; */
      /* margin: 0 auto; */
    }

    #_content {
      border: 1px solid #999;
      /* max-width: 100%; */
      text-align: center;
    }

    #_top-content {
      /* margin: 0 auto; */
      font-family: arial;
    }

    #_int {
      /* margin: 2% auto; */
      font-family: arial;
    }


    #_bottom-content {
      font-family: arial;
    }

    #_info-content {
      border: 1px solid black;
      margin-left: 16px;
      margin-right: 16px;
      text-align: left;
      border-radius: 30px;
      padding: 1px;
    }

    #_info-content-isi {
      border: 1px solid black;
      text-align: left;
      padding: 16px;
      border-radius: 30px;
    }

    #_cover {
      /* margin-left: 3%; */
    }

    h2 {
      /* margin: 0.5%; */
    }
  </style>
</head>

<body>
  <div style="text-align: right;"></div>
  <div id="_wrapper">
    <div id="_content">

      <div id="_top-content">
        <table>
          <tr>
            <td rowspan="3">
              <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
            </td>
            <td style="font-family: arial;font-size: 8px;">
              PT. PLN (PERSERO)
            </td>
          </tr>
          <tr>
            <td style="font-family: arial;font-size: 8px;">
              UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
            </td>
          </tr>
          <tr>
            <td style="font-family: arial;font-size: 8px;">
              UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
            </td>
          </tr>
        </table>
      </div>

      <div class="content" style="padding: 8px;">
        <table>
          <tr>
            <td width="50" style="font-size:12px;font-family: arial;text-align: left;">
              Nomor
            </td>
            <td width="20" style="font-size:12px;font-family: arial;text-align: left;">
              :
            </td>
            <td width="280" style="font-size:12px;font-family: arial;text-align: left;">
              <?php echo $data_jadwal[4]['nomor_pekerjaan'] ?>
            </td>
            <td width="220" style="font-size:12px;font-family: arial;text-align: left;">
              <?php echo ucfirst(trim(str_replace('UPT', '', $nama_upt))) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[4]['tanggal']))) ?>
            </td>
          </tr>
          <tr>
            <td width="50" style="font-size:12px;font-family: arial;text-align: left;">
              Lampiran
            </td>
            <td width="20" style="font-size:12px;font-family: arial;text-align: left;">
              :
            </td>
            <td width="280" style="font-size:12px;font-family: arial;text-align: left;">
              -
            </td>
            <td width="220" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
          </tr>
          <tr>
            <td width="50" style="font-size:12px;font-family: arial;text-align: left;">
              Sifat
            </td>
            <td width="20" style="font-size:12px;font-family: arial;text-align: left;">
              :
            </td>
            <td width="280" style="font-size:12px;font-family: arial;text-align: left;">
              -
            </td>
            <td width="220" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
          </tr>
          <tr>
            <td width="50" style="font-size:12px;font-family: arial;text-align: left;">
              Perihal
            </td>
            <td width="20" style="font-size:12px;font-family: arial;text-align: left;">
              :
            </td>
            <td width="280" style="font-size:12px;font-family: arial;text-align: left;">
              Permintaan Penawaran Harga
            </td>
            <td width="220" style="font-size:12px;font-family: arial;text-align: left;">
              Kepada :
            </td>
          </tr>
          <tr>
            <td width="50" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td width="20" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td width="280" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td width="220" style="font-size:12px;font-family: arial;text-align: left;">
              <br>
              <b><?php echo strtoupper($nama_vendor) ?></b>
              <br>
              <?php echo ucfirst($alamat) ?>
              <br>
              Di
              <br>
              Tempat
            </td>
          </tr>
        </table>
        <br>

        <table>
          <tr>
            <td width="30" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td width="550" style="font-size:12px;font-family: arial;text-align: justify;">
              Dalam rangka memenuhi Pekerjaan operasional PT. PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur dan Bali <?php echo $nama_upt ?>, dengan ini kami minta penawaran harga sesuai dengan rencana pengadaan (Rinician terlampir) pekerjaan :
          </tr>
        </table>

        <br>

        <table>
          <tr>
            <td width="30" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              <b>1. </b>
            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              <b>PENGADAAN BARANG / PENGADAAN JASA</b>
            </td>
          </tr>
          <tr>
            <td width="30" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              <br>
              <b>- <?php echo $judul_pekerjaan ?></b>
            </td>
          </tr>
          <tr>
            <td width="30" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              <br>
              <b>2. </b>
            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              <br>
              <b>PENGIRIMAN PENAWARAN HARGA</b>
            </td>
          </tr>
          <tr>
            <td width="30" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              Pengiriman penawaran harga atas pekerjaan tersebut selambat lambatnya dikirim pada :
            </td>
          </tr>
          <tr>
            <td width="30" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              <br>
              <table>
                <tr>
                  <td width="100" style="font-size:12px;font-family: arial;text-align: left;">Hari</td>
                  <td style="font-size:12px;font-family: arial;text-align: left;">:</td>
                  <td style="font-size:12px;font-family: arial;text-align: left;">
                  <?php
                  $datePenawaran = date('D', strtotime($data_jadwal[4]['tanggal']));
                  switch (strtolower($datePenawaran)) {
                    case 'sun':
                      echo 'Minggu';
                      break;
                    case 'mon':
                      echo 'Senin';
                      break;
                    case 'tue':
                      echo 'Selasa';
                      break;
                    case 'wed':
                      echo 'Rabu';
                      break;
                    case 'thr':
                      echo 'Kamis';
                      break;
                    case 'fri':
                      echo 'Jumat';
                      break;
                    case 'sat':
                      echo 'Sabtu';
                      break;
                    
                    default:
                      # code...
                      break;
                  }
                  
                  ?>
                  </td>
                </tr>
                <tr>
                  <td width="100" style="font-size:12px;font-family: arial;text-align: left;">Tanggal</td>
                  <td style="font-size:12px;font-family: arial;text-align: left;">:</td>
                  <td style="font-size:12px;font-family: arial;text-align: left;"><?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[4]['tanggal']))) ?></td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td width="30" style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">

            </td>
            <td style="font-size:12px;font-family: arial;text-align: left;">
              <br>
              Demikian, agar menjadi maklum dan atas perhatian Saudara kami ucapkan terima kasih.
            </td>
          </tr>
        </table>
        <br>
        <table>
          <tr>
            <td width="300" nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
              &nbsp;
            </td>
            <td nowrap="nowrap" style="font-size:12px;font-family: arial;text-align: center;">
              PEJABAT PELAKSANA PENGADAAN
              <br>
              <br>
              <br>
              ZENDIDIYA Y.
            </td>
          </tr>
        </table>
      </div>
      <br>
    </div>
  </div>
  <div style="text-align: right;">

  </div>
</body>

</html>