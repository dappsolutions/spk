<?php
function penyebut($nilai)
{
 $nilai = abs($nilai);
 $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
 $temp = "";
 if ($nilai < 12) {
  $temp = " " . $huruf[$nilai];
 } else if ($nilai < 20) {
  $temp = penyebut($nilai - 10) . " belas";
 } else if ($nilai < 100) {
  $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
 } else if ($nilai < 200) {
  $temp = " seratus" . penyebut($nilai - 100);
 } else if ($nilai < 1000) {
  $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
 } else if ($nilai < 2000) {
  $temp = " seribu" . penyebut($nilai - 1000);
 } else if ($nilai < 1000000) {
  $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
 } else if ($nilai < 1000000000) {
  $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
 } else if ($nilai < 1000000000000) {
  $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
 } else if ($nilai < 1000000000000000) {
  $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
 }
 return $temp;
}

function terbilang($nilai)
{
 if ($nilai < 0) {
  $hasil = "minus " . trim(penyebut($nilai));
 } else {
  $hasil = trim(penyebut($nilai));
 }
 return $hasil . ' rupiah';
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen HPS</title>

 <style>

  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="padding: 8px;">
    <table style="width: 100%;">
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       <b><u>HARGA PERKIRAAN SENDIRI</u></b>
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       <b><?php echo $judul_pekerjaan ?></b>
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       <i>Nomor : <?php echo $data_jadwal[3]['nomor_pekerjaan'] ?></i>
      </td>
     </tr>
    </table>
    <br>

    <table style="width: 100%;border: 1px solid #333;border-collapse: collapse;">
     <tr style="background-color: #d9edf7">
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>NO</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>URAIAN</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>VOLUME</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>HARGA PADA</b> <br> ANGGARAN BIAYA OPERASI
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>HARGA PADA HPS PLN</b>
      </td>
     </tr>
     <tr style="background-color: #d9edf7">
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       Jml
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       Sat
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       SATUAN
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       JUMLAH
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       SATUAN
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       JUMLAH
       <br>
       Rp.
      </td>
     </tr>

     <tr>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;">-</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;padding:3px;text-align: center;"></td>
     </tr>
     <tr>
      <td nowrap="nowrap" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>I</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <b>Material</b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
     </tr>
     <?php $no = 1 ?>
     <tr>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
       <?php echo $judul_pekerjaan ?>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;"></td>
     </tr>

     <?php $total_harga_anggaran = 0 ?>
     <?php $total_harga_pln = 0 ?>
     <?php foreach ($data_rab as $key => $value) { ?>
      <tr>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo $no++ ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo $value['nama_produk'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo $value['jml_vol'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo $value['nama_satuan'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo number_format($value['satuan_anggaran'],0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo number_format($value['jumlah_anggaran'],0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo number_format($value['satuan_hps'],0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">
        <?php echo number_format($value['jumlah_hps'],0, ',', '.') ?>
       </td>
      </tr>
      <?php $total_harga_anggaran += $value['jumlah_anggaran'] ?>
      <?php $total_harga_pln += $value['jumlah_hps'] ?>
     <?php } ?>

     <tr>
      <td rowspan="3" style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: right;">
       Total Harga :
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">
       <?php echo number_format($total_harga_anggaran,0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">
       <?php echo number_format($total_harga_pln,0, ',', '.') ?>
      </td>
     </tr>
     <tr>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="font-size:12px;font-family: arial;padding:3px;text-align: right;">
       PPN :
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">
       <?php $harga_ppn = $total_harga_anggaran * $ppn ?>
       <?php $total_harga_ppn = $total_harga_anggaran + $harga_ppn ?>
       <?php echo number_format($harga_ppn,0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">
       <?php $harga_ppn_pln = $total_harga_pln * $ppn ?>
       <?php $total_harga_ppn_pln = $total_harga_pln + $harga_ppn_pln ?>
       <?php echo number_format($harga_ppn_pln,0, ',', '.') ?>
      </td>
     </tr>
     <tr>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">
       Jumlah harga termasuk PPN :
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">
       <b><?php echo number_format($total_harga_ppn,0, ',', '.') ?></b>
      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:12px;font-family: arial;padding:3px;text-align: right;">
       <b><?php echo number_format($total_harga_ppn_pln,0, ',', '.') ?></b>
      </td>
     </tr>
    </table>
    <table style="width: 100%;border: 1px solid #333;border-collapse: collapse;">
     <tr>
      <td style="border:1px solid #ccc;font-size: 12px;font-family: arial;padding:3px;text-align: center;">
       <i><b>Terbilang : <?php echo terbilang($total_harga_ppn) ?> </b></i>
      </td>
     </tr>
    </table>
    <table style="width: 100%;border: 1px solid #333;border-collapse: collapse;">
     <tr>
      <td nowrap="nowrap" style="border-bottom:1px solid #ccc; border-top:1px solid #ccc; border-left:1px solid #ccc;font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       Menyetujui :
       <br>
       <b>MANAGER</b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>HENDRIK MARYONO</u></b>
      </td>
      <td nowrap="nowrap" style="border-bottom:1px solid #ccc; border-top:1px solid #ccc; border-right:1px solid #ccc;font-size:12px;font-family: arial;padding: 16px;text-align: center;">
       <?php echo ucfirst(trim(str_replace('UPT', '', $nama_upt))) ?>, <?php echo translateMonthToIndo(date('d F Y', strtotime($data_jadwal[3]['tanggal']))) ?>
       <br>
       <b>PEJABAT PELAKSANA PENGADAAN</b>
       <br>
       <br>
       <br>
       <br>
       <br>
       <b><u>ZENDIDIYA Y</u></b>
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>