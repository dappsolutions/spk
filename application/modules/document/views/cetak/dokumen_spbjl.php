<!DOCTYPE html>
<html lang="en">

<head>
 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>Dokumen SPBJL</title>

 <style>
  #_wrapper {
   /* width: 100%; */
   /* margin: 0 auto; */
  }

  #_content {
   border: 1px solid #999;
   /* max-width: 100%; */
   text-align: center;
  }

  #_top-content {
   /* margin: 0 auto; */
   font-family: arial;
  }

  #_int {
   /* margin: 2% auto; */
   font-family: arial;
  }


  #_bottom-content {
   font-family: arial;
  }

  #_info-content {
   border: 1px solid black;
   margin-left: 16px;
   margin-right: 16px;
   text-align: left;
   border-radius: 30px;
   padding: 1px;
  }

  #_info-content-isi {
   border: 1px solid black;
   text-align: left;
   padding: 16px;
   border-radius: 30px;
  }

  #_cover {
   /* margin-left: 3%; */
  }

  h2 {
   /* margin: 0.5%; */
  }
 </style>
</head>

<body>
 <div style="text-align: right;"></div>
 <div id="_wrapper">
  <div id="_content">

   <div id="_top-content">
    <table>
     <tr>
      <td rowspan="3">
       <img src="<?php echo base_url() ?>files/img/_logo_new_old.png" height="50" width="35">
      </td>
      <td style="font-family: arial;font-size: 8px;">
       PT. PLN (PERSERO)
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT INDUK TRANSMISI JAWA BAGIAN TIMUR DAN BALI
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size: 8px;">
       UNIT <?php echo trim(str_replace('UPT', '', $nama_upt)) ?>
      </td>
     </tr>
    </table>
   </div>

   <div class="content" style="">
    <table style="width: 100%;">
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;padding: 8px;">
       <b><u>Surat Pesanan Barang untuk pemakaian langsung</u></b>
       <br>
       <i>Nomor : <?php echo 'No. ' . $no_rab . '.SPBL/DAN.01.01/370400/' . date('Y', strtotime($tgl_pelaksanaan)) ?>, Tanggal : <?php echo date('d F Y', strtotime($tgl_pelaksanaan)) ?></i>
      </td>
     </tr>
    </table>

    <table style="width: 100%;">
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       Kepada
      </td>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       :
      </td>
      <td style="font-family: arial;font-size:12px;text-align: left;">
       <i><b><?php echo strtoupper($nama_vendor) ?></b></i>
       <br>
      </td>
     </tr>
     <tr>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       &nbsp;
      </td>
      <td style="font-family: arial;font-size:12px;text-align: center;">
       &nbsp;
      </td>
      <td style="font-family: arial;font-size:12px;text-align: left;">
       <i><?php echo $alamat ?></i>
       <br>
      </td>
     </tr>
    </table>

    <p style="font-family: arial;font-size:12px;text-align: justify;padding:8px;">Bersama ini kami mengajukan : <?php echo $judul_pekerjaan ?>, ke Perusahaan Saudara, dan agar dikirim ke alamat kami PT PLN (PERSERO) UIT-JBTB <?php echo $nama_upt ?>, <?php echo $alamat_upt ?></p>

    <table style="width: 100%;border: 1px solid #333;border-collapse: collapse;">
     <tr style="background-color: #d9edf7">
      <td rowspan="2" style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       <b>No.</b>
      </td>
      <td rowspan="2" style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       <b>Nama Barang</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       <b>Volume /</b>
       <br>
       <b>Satuan</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       <b>Harga Satuan (Rp)</b>
      </td>
      <td colspan="2" style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       <b>Jumlah</b>
       <br>
       <b>(Rp)</b>
      </td>
     </tr>
     <tr style="background-color: #d9edf7">
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       Jml
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       Sat
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       SATUAN
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       JUMLAH
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       SATUAN
       <br>
       Rp.
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       JUMLAH
       <br>
       Rp.
      </td>
     </tr>

     <tr>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">1</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">2</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">3</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">4</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">5</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">6</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">7</td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">8</td>
     </tr>
     <tr>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       <b>I</b>
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
       <b>Material</b>
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;"></td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;"></td>
     </tr>

     <?php $total_harga_anggaran = 0 ?>
     <?php $total_harga_penawaran = 0 ?>
     <?php $total_harga_pln = 0 ?>
     <?php $total_harga_sepakat = 0 ?>
     <?php $no = 1 ?>
     <?php foreach ($data_rab as $key => $value) { ?>
      <tr>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo $no++ ?>
       </td>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo $value['nama_produk'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo $value['jml_vol'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo $value['nama_satuan'] ?>
       </td>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo number_format($value['satuan_anggaran'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo number_format($value['jumlah_anggaran'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo number_format($value['harga_sepakat'], 0, ',', '.') ?>
       </td>
       <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">
        <?php echo number_format($value['jumlah_sepakat'], 0, ',', '.') ?>
       </td>
      </tr>
      <?php $total_harga_anggaran += $value['jumlah_anggaran'] ?>
      <?php $total_harga_penawaran += $value['jumlah_penawaran'] ?>
      <?php $total_harga_pln += $value['jumlah_hps'] ?>
      <?php $total_harga_sepakat += $value['jumlah_sepakat'] ?>
     <?php } ?>

     <tr>
      <td rowspan="3" style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:10px;font-family: arial;text-align: right;">
       Total Harga :
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">
       <?php echo number_format($total_harga_anggaran, 0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">
       <?php echo number_format($total_harga_sepakat, 0, ',', '.') ?>
      </td>
     </tr>
     <tr>
      <td style="font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="font-size:10px;font-family: arial;text-align: right;">
       PPN :
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">
       <?php $harga_ppn = $total_harga_anggaran * $ppn ?>
       <?php $total_harga_ppn = $total_harga_anggaran + $harga_ppn ?>
       <?php echo number_format($harga_ppn, 0, ',', '.') ?>
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">
       <?php $harga_ppn_sepakat = $total_harga_sepakat * $ppn ?>
       <?php $total_harga_ppn_sepakat = $total_harga_sepakat + $harga_ppn_sepakat ?>
       <?php echo number_format($harga_ppn_sepakat, 0, ',', '.') ?>
      </td>
     </tr>
     <tr>
      <td style="border-bottom: 1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:10px;font-family: arial;text-align: center;">

      </td>
      <td style="border-bottom: 1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">
       Jumlah harga termasuk PPN :
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">
       <b><?php echo number_format($total_harga_ppn, 0, ',', '.') ?></b>
      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">

      </td>
      <td style="border:1px solid #ccc;font-size:10px;font-family: arial;text-align: right;">
       <b><?php echo number_format($total_harga_ppn_sepakat, 0, ',', '.') ?></b>
      </td>
     </tr>
    </table>
    <table style="width: 100%;border: 1px solid #333;border-collapse: collapse;">
     <tr>
      <td style="border:1px solid #ccc;font-size: 10px;font-family: arial;text-align: center;">
       <i><b>Terbilang : <?php echo terbilang($total_harga_ppn_sepakat) ?> </b></i>
      </td>
     </tr>
    </table>

    <table style="width: 100%;">
     <tr>
      <td style="font-size: 10px;font-family: arial;text-align: left;">
       <b><u>Catatan :</u></b>
      </td>
     </tr>
     <tr>
      <td style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       1.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;" valign="top">
       Masa Pelaksanaan
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;" valign="top">
       :
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;" valign="top">
       <?php echo $lama_pekerjaan ?> (<?php echo str_replace('rupiah', '', terbilang($lama_pekerjaan)) ?>) Hari Kalender atau maksimal pada tanggal <?php echo date('d F Y', strtotime($tgl_selesai_pekerjaan)) ?>
      </td>
     </tr>
     <tr>
      <td style="font-size: 9px;font-family: arial;text-align: right;">
       2.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       Direksi Pekerjaan
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       :
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       <b>Manager Bagian Kontruksi</b>
      </td>
     </tr>
     <tr>
      <td style="font-size: 9px;font-family: arial;text-align: right;">
       &nbsp;
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       Direksi Lapangan
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       :
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       <?php foreach ($list_direksi as $key => $value) { ?>
        <b><?php echo $value['jabatan'] ?></b>
       <?php } ?>
      </td>
     </tr>
     <tr>
      <td style="font-size: 9px;font-family: arial;text-align: right;">
       &nbsp;
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       Pengawas Lapangan
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       :
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       <?php foreach ($list_pengawas as $key => $value) { ?>
        <b><?php echo $value['jabatan'] ?></b>
       <?php } ?>
      </td>
     </tr>
     <tr>
      <td style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       3.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;" valign="top">
       Penyerahan Pekerjaan
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;" valign="top">
       :
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       di Kantor PT PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur dan Bali - <?php echo $nama_upt ?>
      </td>
     </tr>
     <!-- <tr>
      <td style="font-size: 9px;font-family: arial;text-align: right;">
       4.
      </td>
      <td width="" style="font-size: 9px;font-family: arial;text-align: left;">       
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
       
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: left;">
      Surat Pesanan ini dapat dirubah oleh Pengguna di Kantor PT PLN (Persero) Unit Induk Transmisi Jawa Bagian Timur dan Bali - UPT Probolinggo
      </td>
     </tr> -->
    </table>


    <table style="width: 100%;">
     <tr>
      <td width="50" style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       4.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: justify;">
       Surat Pesanan ini dapat dirubah oleh Pengguna (Pihak PLN) setelah dilakukan klarifikasi dengan Penyedia barang/jasa yang dituangkan dalam Amandemen dan merupakan bagian yang tidak terpisahkan dari Surat Pesanan.
      </td>
     </tr>
     <tr>
      <td width="50" style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       5.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: justify;">
       Perpanjangan masa berlaku dan perubahan lokasi Surat Pesanan barang/jasa langsung dapat dilakukan atas kesepakatan Pengguna (Pihak PLN) dengan Penyedia barang/jasa yang dituangkan dalam suatu Berita Acara yang digunakan sebagai dasar Serah Terima Pekerjaan dan merupakan bagian yang tidak terpisahkan dari Surat Pesanan.
      </td>
     </tr>
     <tr>
      <td width="50" style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       6.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: justify;">
       Denda : denda keterlambatan sebesar 1*/100 (satu perseribu) per-hari dengan maksimum 5% dari Total Harga Barang.
      </td>
     </tr>
     <tr>
      <td width="50" style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       7.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: justify;">
       Permohonan Pembayaran, dilampiri :
       <br>
       <table>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">a.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Surat Permohonan Pembayaran</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">b.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Kuitansi Rangkap 4 (empat) lembar untuk lembar pertama bermaterai secukupnya.</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">c.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">E-Faktur Pajak.</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">d.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Berita Acara Pemeriksaan.</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">e.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Berita Penerimaan Barang.</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">f.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Berita Acara Pembayaran.</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">g.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Copy Nomor Pokok Wajib Pajak (NPWP).</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">h.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Copy Pengukuhan Pengusaha Kena Pajak (PKP).</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">i.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Copy Surat Pesanan Barang untuk Pemakaian Langsung (SPBL).</td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">j.</td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">Faktur Barang Rangkap 3 (tiga).</td>
        </tr>
       </table>
      </td>
     </tr>
     <tr>
      <td width="50" style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       8.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: justify;">
       Klausul Safety
       <p style="font-size: 9px;font-family: arial;text-align: justify;margin-left:3px;">Penyedia Barang/Jasa wajib mematuhi peraturan Keselamatan dan Kesehatan Kerja yang berlaku di lingkungan sebagaimana surat PT. PLN (Persero) Kantor Pusat No. 0170/KLH.00.01/DIVHSSE/2019 tanggal 6 Mei 2019</p>
      </td>
     </tr>
     <tr>
      <td width="50" style="font-size: 9px;font-family: arial;text-align: right;" valign="top">
       9.
      </td>
      <td style="font-size: 9px;font-family: arial;text-align: justify;">
       Klausul Lingkungan
       <p style="font-size: 9px;font-family: arial;text-align: justify;">Penyedia Barang/Jasa wajib mematuhi peraturan perlindungan lingkungan yang berlaku sebagaimana surat PT. PLN (Persero) UIT TJBTB No. 191/DAN.01.03/B37000000/2021 tanggal 19 Januari 2021</p>
       <p style="font-size: 9px;font-family: arial;text-align: justify;">Pelaksanaan Pembayaran harga pekerjaan kepada PENYEDIA BARANG/JASA dilakukan dengan cara transfer ke rekening</p>
       <br>

       <table>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b>Nama Perusahaan</b>
         </td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b>:</b>
         </td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b><?php echo strtoupper($nama_vendor) ?></b>
         </td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b>No Rekening</b>
         </td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b>:</b>
         </td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b><?php echo $no_rekening ?></b>
         </td>
        </tr>
        <tr>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b>Nama Bank</b>
         </td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b>:</b>
         </td>
         <td style="font-size: 9px;font-family: arial;text-align: justify;">
          <b><?php echo $nama_bank ?></b>
         </td>
        </tr>
       </table>
      </td>
     </tr>
    </table>

    <table>
     <tr>
      <td width="480" nowrap="nowrap" style="font-size: 10px;font-family: arial;text-align: center;">
       &nbsp;
      </td>
      <td nowrap="nowrap" style="font-size: 10px;font-family: arial;text-align: center;">
       MANAGER
       <br>
       <br>
       <br>
       HENDRIK MARYONO
      </td>
     </tr>
    </table>
   </div>
   <br>
  </div>
 </div>
 <div style="text-align: right;">

 </div>
</body>

</html>