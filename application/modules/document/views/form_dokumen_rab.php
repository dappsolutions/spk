<div class="row">
	<div class="col-md-12">
		<div class="row content-input-rab-file">
			<div class="col-md-6 <?php echo isset($file_rab) ? $file_rab != '' ? 'hide' : '' : ''  ?>">
				<div class="form-group" id="file_input_rab">
					<label for="">Upload Rab</label>
					<input type="file" id="file_rab" class="form-control" onchange="Document.checkFile(this)">
				</div>
			</div>


			<div class="col-md-6 <?php echo isset($file_rab) ? $file_rab != '' ? '' : 'hide' : 'hide'  ?>">
				<div class="form-group " id="detail_file_rab">
					<label for="">File Rab</label>
					<div class="input-group">
						<input disabled="" type="text" id="file_str_rab" class="form-control" value="<?php echo isset($file_rab) ? $file_rab : '' ?>">
						<span class="input-group-addon">
							<i class="fa fa-image hover-content" file="<?php echo isset($file_rab) ? $file_rab : '' ?>" onclick="Document.showFileRab(this, event)"></i>
						</span>
						<span class="input-group-addon">
							<i class="fa fa-close hover-content" onclick="Document.gantiFileRab(this, event)"></i>
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table style="width:100%;" id="table-rab">
				<thead>
					<tr class="bg-info">
						<th rowspan="2" class="text-center th_jsa">Uraian</th>
						<th colspan="2" class="text-center th_jsa">Volume</th>
						<th colspan="2" class="text-center th_jsa">Harga Pada Anggaran Biaya Operasi</th>
						<th colspan="2" class="text-center th_jsa">Harga HPS Pln</th>
						<th rowspan="2" class="th_jsa"></th>
					</tr>
					<tr class="bg-info">
						<th class="text-center th_jsa">Jml</th>
						<th class="text-center th_jsa">Sat</th>
						<th class="text-center th_jsa">Satuan Rp</th>
						<th class="text-center th_jsa">Jumlah Rp</th>
						<th class="text-center th_jsa">Satuan Rp</th>
						<th class="text-center th_jsa">Jumlah Rp</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($data_rab as $key => $value) { ?>
						<tr data_id="<?php echo $value['id'] ?>" class="input auto">
							<td class="td_jsa">
								<select name="" id="uraian" class="form-control required" error="Uraian Material" onchange="Document.getSatuanProduk(this)">
									<option value="">--PILIH--</option>
									<?php foreach ($material as $v_mat) { ?>
										<?php $selected = $v_mat['produk'] == $value['produk_id'] ? 'selected' : '' ?>
										<option <?php echo $selected ?> value="<?php echo $v_mat['produk'] ?>">
											<?php echo $v_mat['nama_produk'] ?></option>
									<?php } ?>
								</select>
							</td>
							<td class="td_jsa">
								<input type="text" id="jml_vol" onkeyup="Document.setHpsHarga(this)" class="form-control required" error="Jml Vol" value="<?php echo $value['jml_vol'] ?>">
							</td>
							<td class="td_jsa">
								<select name="" id="satuan_vol" class="form-control required" error="Satuan Vol" onchange="Document.setHpsHarga(this)">
									<option value="">--PILIH--</option>
									<?php foreach ($satuan as $v_sat) { ?>
										<?php if ($v_sat['produk_id'] == $value['produk_id']) { ?>
											<?php $selected = $value['satuan_id'] == $v_sat['id'] ? 'selected' : '' ?>
											<option <?php echo $selected ?> harga="<?php echo $v_sat['harga_jual'] ?>" value="<?php echo $v_sat['id'] ?>"><?php echo $v_sat['nama_satuan'] ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</td>
							<td class="td_jsa">
								<input type="text" id="satuan_anggaran" class="form-control required" error="Satuan Anggaran" value="<?php echo $value['satuan_anggaran'] ?>" onkeyup="Document.hitungJumlahAnggaran(this, event)">
							</td>
							<td class="td_jsa">
								<input type="text" id="jumlah_anggaran" disabled class="form-control required" error="Jumlah Anggaran" value="<?php echo $value['jumlah_anggaran'] ?>">
							</td>
							<td class="td_jsa">
								<input type="text" id="satuan_hps" class="form-control required" readonly="" error="Satuan HPS" value="<?php echo $value['satuan_hps'] ?>">
							</td>
							<td class="td_jsa">
								<input type="text" id="jumlah_hps" class="form-control required" readonly="" error="Jumlah HPS" value="<?php echo $value['jumlah_hps'] ?>">
							</td>
							<td class="td_jsa">
								<i class="fa fa-trash" onclick="Document.removeRab(this)"></i>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<td class="td_jsa" colspan="7">
							<a href="" onclick="Document.addRab(this, event)" state="auto">Add Item | </a>
							<a href="" onclick="Document.addRab(this, event)" state="manual">Add Item Manual</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 text-right">
		<button class="btn btn-success">Export Xls</button>
	</div>
</div>