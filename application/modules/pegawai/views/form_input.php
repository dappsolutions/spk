<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Unit</label>
   <select name="" id="upt" class="required form-control" error="Unit">
    <option value="">-- PILIH UNIT -- </option>
    <?php foreach ($list_upt as $key => $value) {?>
     <option value="<?php echo $value['id'] ?>" <?php echo isset($upt) ? $upt == $value['id'] ? 'selected' : '' : '' ?>><?php echo $value['nama_upt'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Wilayah</label>
   <select name="" id="wilayah" class="required form-control" error="Wilayah">
    <option value="">-- PILIH WILAYAH -- </option>
    <?php foreach ($list_wilayah as $key => $value) {?>
     <option value="<?php echo $value['id'] ?>" <?php echo isset($wilayah) ? $wilayah == $value['id'] ? 'selected' : '' : '' ?>><?php echo $value['nama_wilayah'] ?></option>
    <?php } ?>
   </select>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">NIP</label>
   <input type="text" id="nip" class="form-control required" error="NIP" value="<?php echo isset($nip) ? $nip : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Pegawai</label>
   <input type="text" id="nama_pegawai" class="form-control required" error="Nama Pegawai" value="<?php echo isset($nama_pegawai) ? $nama_pegawai : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jabatan</label>
   <input type="text" id="jabatan" class="form-control required" error="Jabatan" value="<?php echo isset($jabatan) ? $jabatan : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Email</label>
   <input type="text" id="email" class="form-control required" error="Email" value="<?php echo isset($email) ? $email : '' ?>">
  </div>
 </div>
</div>