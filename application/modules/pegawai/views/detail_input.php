<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Nama Pegawai</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nama_pegawai ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">NIP</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nip ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Jabatan</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $jabatan ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Email</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $email ?>
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Unit</label>
  </div>
 </div>

 <div class="col-md-6">
  <div class="form-group">
   <?php echo $nama_upt ?>
  </div>
 </div>
</div>
