<?php if (isset($menu_akses->pegawai)) { ?>
 <div class="row">
  <div class="col-md-12">
   <div class="panel panel-default">
    <div class="panel-heading">
     <h5>Daftar Pegawai</h5>
    </div>
    <div class="panel-body">
     <div class="row">
      <div class="col-md-12">
       <div class="input-group">
        <input type="text" class="form-control" onkeyup="Pegawai.search(this, event)" id="keyword" placeholder="Pencarian">
        <span class="input-group-addon"><i class="fa fa-search"></i></span>
       </div>
      </div>
     </div>

     <br />
     <div class='row'>
      <div class='col-md-12'>
       <?php if (isset($keyword)) { ?>
        <?php if ($keyword != '') { ?>
         Cari Data : "<b><?php echo $keyword; ?></b>"
        <?php } ?>
       <?php } ?>
      </div>
     </div>

     <br />
     <div class="row">
      <div class="col-md-12">
       <div class="table-responsive">
        <table class="table table-striped table-bordered table-list-draft">
         <thead>
          <tr class="bg-info">
           <th>No</th>
           <th>Nip</th>
           <th>Nama Pegawai</th>
           <th>Unit</th>
           <th>Action</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php $no = $pagination['last_no'] + 1; ?>
           <?php foreach ($content as $value) { ?>
            <tr>
             <td><?php echo $no++ ?></td>
             <td><?php echo $value['nip'] ?></td>
             <td><?php echo $value['nama_pegawai'] ?></td>
             <td><?php echo $value['nama_upt'] ?></td>
             <td class="text-center">
              <?php if ($menu_akses->pegawai->delete) { ?>
               <i class="fa fa-trash grey-text hover" onclick="Pegawai.delete('<?php echo $value['id'] ?>')"></i>
              <?php } ?>
              <?php if ($menu_akses->pegawai->update) { ?>
               &nbsp;
               <i class="fa fa-pencil grey-text  hover" onclick="Pegawai.ubah('<?php echo $value['id'] ?>')"></i>
              <?php } ?>
              &nbsp;
              <i class="fa fa-file-text grey-text  hover" onclick="Pegawai.detail('<?php echo $value['id'] ?>')"></i>
             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
           </tr>
          <?php } ?>

         </tbody>
        </table>
       </div>
      </div>
     </div>
    </div>

    <div class="panel-footer">
     <ul class="pagination pagination-sm no-margin pull-right">
      <?php echo $pagination['links'] ?>
     </ul>
    </div>
   </div>
  </div>
 </div>

 <?php if ($menu_akses->pegawai->create) { ?>
  <div class="row">
   <div class="col-md-12">
    <a href="#" class="float" onclick="Pegawai.add()">
     <i class="fa fa-plus my-float fa-lg"></i>
    </a>
   </div>
  </div>
 <?php } ?>
<?php } else { ?>
 <div class="row">
  <div class="col-md-12">
   <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
    Menu Tidak Tersedia
   </div>
  </div>
 </div>
<?php } ?>