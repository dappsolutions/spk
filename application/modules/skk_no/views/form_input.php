<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label for="">Skk No</label>
   <input type="text" id="skk_no" class="form-control required" error="Skk No" value="<?php echo isset($no_skk) ? $no_skk : '' ?>">
  </div>
 </div>
</div>

<div class="row">
 <div class="col-md-6">
  <div class="form-group <?php echo isset($file_skk) ? $file_skk != '' ? 'hide' : '' : ''  ?>" id="file_input_skk">
   <label for="">File SKK</label>
   <input type="file" id="file_skk" class="form-control" onchange="SkkNo.checkFile(this)">
  </div>
 </div>
</div>

<div class="row <?php echo isset($file_skk) ? $file_skk != '' ? '' : 'hide' : 'hide'  ?>">
 <div class="col-md-6">
  <div class="form-group " id="detail_file_skk">
   <label for="">File SKK</label>
   <div class="input-group">
    <input disabled="" type="text" id="file_str" class="form-control" value="<?php echo isset($file_skk) ? $file_skk : '' ?>">
    <span class="input-group-addon">
     <i class="fa fa-image hover-content" file="<?php echo isset($file_skk) ? $file_skk : '' ?>" onclick="SkkNo.showFile(this, event)"></i>
    </span>
    <span class="input-group-addon">
     <i class="fa fa-close hover-content" onclick="SkkNo.gantiFile(this, event)"></i>
    </span>
   </div>
  </div>
 </div>
</div>