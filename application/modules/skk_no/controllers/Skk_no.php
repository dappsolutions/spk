<?php

class Skk_no extends MX_Controller
{

  public $segment;
  public $limit;
  public $page;
  public $last_no;
  public $menu_akses;

  public function __construct()
  {
    parent::__construct();
    $this->limit = 25;
    $this->menu_akses = json_decode($this->session->userdata('list_akses'));
  }

  public function getModuleName()
  {
    return 'skk_no';
  }

  public function getHeaderJSandCSS()
  {
    //versioning
    $version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
    $version = substr($version, 0, 11);
    //versioning

    $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/skk_no.js?v=' . $version . '"></script>'
    );

    return $data;
  }

  public function getTableName()
  {
    return 'skk_no';
  }

  public function index()
  {
    $this->segment = 3;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;

    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Skk No";
    $data['title_content'] = 'Data Skk No';
    $content = $this->getData();
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['menu_akses'] = $this->menu_akses;
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function getTotalData($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('ja.no_skk', $keyword),
      );
    }
    $total = Modules::run('database/count_all', array(
      'table' => $this->getTableName() . ' ja',
      'field' => array('ja.*'),
      'like' => $like,
      'is_or_like' => true,
      'where' => "ja.deleted = 0"
    ));

    return $total;
  }

  public function getData($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('ja.no_skk', $keyword),
      );
    }
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' ja',
      'field' => array('ja.*'),
      'like' => $like,
      'is_or_like' => true,
      'limit' => $this->limit,
      'offset' => $this->last_no,
      'where' => "ja.deleted = 0"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    return array(
      'data' => $result,
      'total_rows' => $this->getTotalData($keyword)
    );
  }

  public function getDetailData($id)
  {
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' kr',
      'field' => array('kr.*'),
      'where' => "kr.id = '" . $id . "'"
    ));

    return $data->row_array();
  }
  

  public function add()
  {
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Tambah Skk No";
    $data['title_content'] = 'Tambah Skk No';
    echo Modules::run('template', $data);
  }

  public function ubah($id)
  {
    $data = $this->getDetailData($id);
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Ubah Skk No";
    $data['title_content'] = 'Ubah Skk No';
    echo Modules::run('template', $data);
  }

  public function detail($id)
  {
    $data = $this->getDetailData($id);
    $data['view_file'] = 'detail_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Detail Skk No";
    $data['title_content'] = 'Detail Skk No';
    echo Modules::run('template', $data);
  }

  public function getPostDataHeader($value)
  {
    $data['no_skk'] = $value['skk_no'];
    return $data;
  }

  public function simpan()
  {
    $data = (array) json_decode($_POST['data']);
    $file =$_FILES;
    // echo '<pre>';
    // print_r($data);die;

    $id = $data['id'];
    $result['is_valid'] = false;
    $result['id'] = $id;

    $file_skk = "";
    $is_uploaded = true;
    if (!empty($file)) {
      if (isset($file['file_skk'])) {
        $response_upload = $this->uploadData('file_skk', 'skk');
        if ($response_upload['is_valid']) {
          // $post_wp['file_spk'] = $file['file_spk']['name'];
          $file_skk = $response_upload['response']['file_name'];
        } else {
          $is_uploaded = false;
          $result['message'] = $response_upload['response'];
        }
      }
    }

    if($is_uploaded){
      $this->db->trans_begin();
      try {     
        $push = [];
        $push['user'] = $this->session->userdata('user_id');
        $push['createddate'] = date('Y-m-d H:i:s');
        $this->db->insert('actor', $push);
  
        $actorId = $this->db->insert_id();
  
        $post = $this->getPostDataHeader($data);
        if($file_skk != ''){
          $post['file_skk'] = $file_skk;
        }

        if ($id == '') {
          $post['createddate'] = date('Y-m-d H:i:s');
          $post['createdby'] = $actorId;
          $this->db->insert($this->getTableName(), $post);
          $id = $this->db->insert_id();
          $result['id'] = $id;
        } else {
          //update
          Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
        }
        $this->db->trans_commit();
        $result['is_valid'] = true;
      } catch (Exception $ex) {
        $result['message'] = $ex->getMessage();
        $this->db->trans_rollback();
      }
    }

    echo json_encode($result);
  }

  public function showFile(){
    $data = $_POST;
    echo $this->load->view('file_skk', $data, true);
  }

  public function uploadData($name_of_field, $dir)
	{
		$config['upload_path'] = 'files/berkas/' . $dir . '/';
		$config['allowed_types'] = 'png|jpg|jpeg|pdf';
		$config['max_size'] = '1000';
		$config['max_width'] = '6000';
		$config['max_height'] = '6000';
		$config['encrypt_name'] = TRUE;


		$this->load->library('upload', $config);

		$is_valid = false;
		if (!$this->upload->do_upload($name_of_field)) {
			$response = $this->upload->display_errors();
		} else {
			$response = $this->upload->data();
			$is_valid = true;
		}

		// echo '<pre>';
		// print_r($response);die;

		return array(
			'is_valid' => $is_valid,
			'response' => $response
		);
	}

  public function search($keyword)
  {
    $this->segment = 4;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;
    $keyword = urldecode($keyword);

    $data['keyword'] = $keyword;
    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Supplier";
    $data['title_content'] = 'Data Supplier';
    $content = $this->getData($keyword);
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function delete($id)
  {
    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
      $this->db->trans_commit();
      $is_valid = true;
    } catch (Exception $ex) {
      $this->db->trans_rollback();
    }

    echo json_encode(array('is_valid' => $is_valid));
  }
}
