<?php

class Hari_libur extends MX_Controller
{

  public $segment;
  public $limit;
  public $page;
  public $last_no;
  public $menu_akses;

  public function __construct()
  {
    parent::__construct();
    $this->limit = 25;
    $this->menu_akses = json_decode($this->session->userdata('list_akses'));
  }

  public function getModuleName()
  {
    return 'hari_libur';
  }

  public function getHeaderJSandCSS()
  {
    //versioning
    $version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
    $version = substr($version, 0, 11);
    //versioning

    $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/moment.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/hari_libur.js?v=' . $version . '"></script>'
    );

    return $data;
  }

  public function getTableName()
  {
    return 'day_off';
  }

  public function index()
  {
    $this->segment = 3;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;

    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Hari Libur";
    $data['title_content'] = 'Data Hari Libur';
    $content = $this->getData();
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['menu_akses'] = $this->menu_akses;
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function getTotalData($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('ja.tanggal', $keyword),
      );
    }
    $total = Modules::run('database/count_all', array(
      'table' => $this->getTableName() . ' ja',
      'field' => array('ja.*'),
      'like' => $like,
      'is_or_like' => true,
      'where' => "ja.deleted = 0"
    ));

    return $total;
  }

  public function getData($keyword = '')
  {
    $like = array();
    if ($keyword != '') {
      $like = array(
        array('ja.tanggal', $keyword),
      );
    }
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' ja',
      'field' => array('ja.*'),
      'like' => $like,
      'is_or_like' => true,
      'limit' => $this->limit,
      'offset' => $this->last_no,
      'where' => "ja.deleted = 0"
    ));

    $result = array();
    if (!empty($data)) {
      foreach ($data->result_array() as $value) {
        array_push($result, $value);
      }
    }

    return array(
      'data' => $result,
      'total_rows' => $this->getTotalData($keyword)
    );
  }

  public function getDetailData($id)
  {
    $data = Modules::run('database/get', array(
      'table' => $this->getTableName() . ' kr',
      'field' => array('kr.*'),
      'where' => "kr.id = '" . $id . "'"
    ));

    return $data->row_array();
  }
  

  public function add()
  {
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Tambah Hari Libur";
    $data['title_content'] = 'Tambah Hari Libur';
    echo Modules::run('template', $data);
  }

  public function ubah($id)
  {
    $data = $this->getDetailData($id);
    $data['view_file'] = 'form_add_edit_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Ubah Hari Libur";
    $data['title_content'] = 'Ubah Hari Libur';
    echo Modules::run('template', $data);
  }

  public function detail($id)
  {
    $data = $this->getDetailData($id);
    $data['view_file'] = 'detail_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Detail Hari Libur";
    $data['title_content'] = 'Detail Hari Libur';
    echo Modules::run('template', $data);
  }

  public function getPostDataHeader($value)
  {
    $data['tanggal'] = date('Y-m-d', strtotime($value['tanggal']));
    return $data;
  }

  public function simpan()
  {
    $data = $_POST;

    $id = $this->input->post('id');
    $result['is_valid'] = false;
    $result['id'] = $id;

    $this->db->trans_begin();
    try {
      $push = [];
      $push['user'] = $this->session->userdata('user_id');
      $push['createddate'] = date('Y-m-d H:i:s');
      $this->db->insert('actor', $push);

      $actorId = $this->db->insert_id();

      $post = $this->getPostDataHeader($data);
      if ($id == '') {
        $post['createddate'] = date('Y-m-d H:i:s');
        $post['createdby'] = $actorId;
        $this->db->insert($this->getTableName(), $post);
        $id = $this->db->insert_id();
        $result['id'] = $id;
      } else {
        //update
        Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
      }
      $this->db->trans_commit();
      $result['is_valid'] = true;
    } catch (Exception $ex) {
      $result['message'] = $ex->getMessage();
      $this->db->trans_rollback();
    }

    echo json_encode($result);
  }

  public function search($keyword)
  {
    $this->segment = 4;
    $this->page = $this->uri->segment($this->segment) ?
      $this->uri->segment($this->segment) - 1 : 0;
    $this->last_no = $this->page * $this->limit;
    $keyword = urldecode($keyword);

    $data['keyword'] = $keyword;
    $data['view_file'] = 'index_view';
    $data['header_data'] = $this->getHeaderJSandCSS();
    $data['module'] = $this->getModuleName();
    $data['title'] = "Data Supplier";
    $data['title_content'] = 'Data Supplier';
    $content = $this->getData($keyword);
    $data['content'] = $content['data'];
    $total_rows = $content['total_rows'];
    $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
    echo Modules::run('template', $data);
  }

  public function delete($id)
  {
    $is_valid = false;
    $this->db->trans_begin();
    try {
      Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
      $this->db->trans_commit();
      $is_valid = true;
    } catch (Exception $ex) {
      $this->db->trans_rollback();
    }

    echo json_encode(array('is_valid' => $is_valid));
  }
}
