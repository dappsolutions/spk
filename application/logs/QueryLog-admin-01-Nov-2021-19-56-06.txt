
----------../modules/template/controllers/product_stock/index----------
admin - Tanggal/Waktu : 01-Nov-2021/19:56:06 PM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `ps`.`satuan`, `g`.`nama_gudang`, `r`.`nama_rak`, `s`.`nama_satuan`, `p`.`kode_product` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    AND `ps`.`satuan_terkecil` = '1' 
    LIMIT 10
 --> Execution Time: 0.021343946456909, Seconds : 21.343946456909
admin - Tanggal/Waktu : 01-Nov-2021/19:56:06 PM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    AND `ps`.`satuan_terkecil` = '1'
 --> Execution Time: 0.030661821365356, Seconds : 30.661821365356
admin - Tanggal/Waktu : 01-Nov-2021/19:56:06 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00042510032653809, Seconds : 0.42510032653809
admin - Tanggal/Waktu : 01-Nov-2021/19:56:06 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00086092948913574, Seconds : 0.86092948913574
admin - Tanggal/Waktu : 01-Nov-2021/19:56:06 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00090885162353516, Seconds : 0.90885162353516
admin - Tanggal/Waktu : 01-Nov-2021/19:56:06 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00061511993408203, Seconds : 0.61511993408203
