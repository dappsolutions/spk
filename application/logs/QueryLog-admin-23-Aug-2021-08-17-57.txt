
----------../modules/template/controllers/order/detail----------
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '6' 
    LIMIT 1000
 --> Execution Time: 0.0010988712310791, Seconds : 1.0988712310791
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `ps`.`qty` as `konversi`, `p`.`id` as `product_id` 
    FROM `order_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`order` = '6' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.00148606300354, Seconds : 1.48606300354
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '6' 
    LIMIT 1000
 --> Execution Time: 0.0023560523986816, Seconds : 2.3560523986816
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT `ps`.*, `pst`.`stock`, `s`.`nama_satuan` as `current_satuan`, `sp`.`nama_satuan` as `parent_satuan`, `psp`.`id` as `parent_product_satuan`, `psp`.`qty` as `qty_parent`, `pstp`.`stock` as `parent_stock`, `pst`.`id` as `cur_product_stock`, `pstp`.`id` as `parent_product_stock`, `pdc`.`product` as `nama_product` 
    FROM `product_satuan` `ps` 
    JOIN `product_stock` `pst` ON `pst`.`product_satuan` = `ps`.`id` 
    JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    LEFT     JOIN `product_satuan` `psp` ON `psp`.`satuan` = `sp`.`id` 
    AND `psp`.`product` = `ps`.`product` 
    LEFT     JOIN `product_stock` `pstp` ON `pstp`.`product_satuan` = `psp`.`id` 
    JOIN `product` `pdc` ON `pdc`.`id` = `ps`.`product` 
    WHERE `ps`.`deleted` 
    IS     NULL     OR `ps`.`deleted` =0 
    AND `ps`.`id` = '1675' 
    LIMIT 1000
 --> Execution Time: 0.0021359920501709, Seconds : 2.1359920501709
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT ps.* , s.nama_satuan , ps.qty as konversi , ps.ket_harga as keterangan 
    FROM product_satuan ps 
    JOIN satuan s on s.id = ps.satuan 
    WHERE ps.product = 1677 
    AND ps.deleted = 0 
    AND ps.satuan_terkecil = 1
 --> Execution Time: 0.00058197975158691, Seconds : 0.58197975158691
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0021021366119385, Seconds : 2.1021366119385
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.001046895980835, Seconds : 1.046895980835
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00111985206604, Seconds : 1.11985206604
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00090289115905762, Seconds : 0.90289115905762

----------../modules/template/controllers/order/add----------
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.001878023147583, Seconds : 1.878023147583
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.00035786628723145, Seconds : 0.35786628723145
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00035881996154785, Seconds : 0.35881996154785
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00028705596923828, Seconds : 0.28705596923828
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND pst.satuan_terkecil = 1
 --> Execution Time: 0.026001930236816, Seconds : 26.001930236816
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00047397613525391, Seconds : 0.47397613525391
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00078988075256348, Seconds : 0.78988075256348
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00083708763122559, Seconds : 0.83708763122559
admin - Tanggal/Waktu : 23-Aug-2021/08:17:57 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00069689750671387, Seconds : 0.69689750671387
