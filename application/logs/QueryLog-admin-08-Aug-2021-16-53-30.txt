
----------../modules/template/controllers/product_stock/index----------
admin - Tanggal/Waktu : 08-Aug-2021/16:53:30 PM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `ps`.`satuan`, `g`.`nama_gudang`, `r`.`nama_rak`, `s`.`nama_satuan`, `p`.`kode_product` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    AND `ps`.`satuan_terkecil` = '1' 
    LIMIT 830, 10
 --> Execution Time: 0.20728182792664, Seconds : 207.28182792664
admin - Tanggal/Waktu : 08-Aug-2021/16:53:30 PM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    AND `ps`.`satuan_terkecil` = '1'
 --> Execution Time: 0.1677930355072, Seconds : 167.7930355072
admin - Tanggal/Waktu : 08-Aug-2021/16:53:30 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00058197975158691, Seconds : 0.58197975158691
admin - Tanggal/Waktu : 08-Aug-2021/16:53:30 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00092601776123047, Seconds : 0.92601776123047
admin - Tanggal/Waktu : 08-Aug-2021/16:53:30 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00073885917663574, Seconds : 0.73885917663574
admin - Tanggal/Waktu : 08-Aug-2021/16:53:30 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00071287155151367, Seconds : 0.71287155151367
