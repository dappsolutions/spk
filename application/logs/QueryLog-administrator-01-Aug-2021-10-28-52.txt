
----------../modules/database/controllers/order/confirmBayar----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `product_log_stock` ( `product_satuan`, `status`, `qty`, `keterangan`, `reference_id`, `createddate`, `createdby`)  
    VALUES ( '9', 'ORDER', 1, 'Order Pelanggan', '26', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.00078701972961426, Seconds : 0.78701972961426
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    JOIN `satuan` `s` ON `s`.`id` = `pst`.`satuan` 
    WHERE `ps`.`deleted` =0 
    AND `p`.`id` = '3159' 
    AND `s`.`nama_satuan` = 'PCS' 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0020239353179932, Seconds : 2.0239353179932
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `product_stock` ( `stock`, `product`, `product_satuan`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( 93, '3159', '9', 1, 1, '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.00040578842163086, Seconds : 0.40578842163086
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '26', 'CONFIRM', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.00024890899658203, Seconds : 0.24890899658203

----------../modules/template/controllers/order/detail----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '26' 
    LIMIT 1000
 --> Execution Time: 0.0015881061553955, Seconds : 1.5881061553955
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `ps`.`qty` as `konversi`, `p`.`id` as `product_id` 
    FROM `order_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`order` = '26' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.00082993507385254, Seconds : 0.82993507385254
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '28' 
    LIMIT 1000
 --> Execution Time: 0.0016260147094727, Seconds : 1.6260147094727
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT `ps`.*, `pst`.`stock`, `s`.`nama_satuan` as `current_satuan`, `sp`.`nama_satuan` as `parent_satuan`, `psp`.`id` as `parent_product_satuan`, `psp`.`qty` as `qty_parent`, `pstp`.`stock` as `parent_stock`, `pst`.`id` as `cur_product_stock`, `pstp`.`id` as `parent_product_stock`, `pdc`.`product` as `nama_product` 
    FROM `product_satuan` `ps` 
    JOIN `product_stock` `pst` ON `pst`.`product_satuan` = `ps`.`id` 
    JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    LEFT     JOIN `product_satuan` `psp` ON `psp`.`satuan` = `sp`.`id` 
    AND `psp`.`product` = `ps`.`product` 
    LEFT     JOIN `product_stock` `pstp` ON `pstp`.`product_satuan` = `psp`.`id` 
    JOIN `product` `pdc` ON `pdc`.`id` = `ps`.`product` 
    WHERE `ps`.`deleted` 
    IS     NULL     OR `ps`.`deleted` =0 
    AND `ps`.`id` = '9' 
    LIMIT 1000
 --> Execution Time: 0.0011880397796631, Seconds : 1.1880397796631
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0003812313079834, Seconds : 0.3812313079834
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00086593627929688, Seconds : 0.86593627929688
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0024659633636475, Seconds : 2.4659633636475
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00076389312744141, Seconds : 0.76389312744141

----------../modules/no_generator/controllers/order/validateBayar----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '26' 
    LIMIT 1000
 --> Execution Time: 0.0015339851379395, Seconds : 1.5339851379395
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT op.* , roi.qty as retur_qty , roi.sub_total as retur_sub_total , ps.harga 
    FROM order_product op 
    JOIN `order` o on o.id = op.`order` 
    LEFT     JOIN retur_order_item roi on roi.order_product = op.id 
    JOIN product_satuan ps on ps.id = op.product_satuan 
    WHERE o.id = '26' 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.0010390281677246, Seconds : 1.0390281677246
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT * 
    FROM `invoice` 
    WHERE `no_faktur` 
    LIKE '%INV21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00054192543029785, Seconds : 0.54192543029785
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `invoice` ( `no_faktur`, `pembeli`, `tanggal_faktur`, `tanggal_bayar`, `ref`, `potongan`, `metode_bayar`, `pot_faktur`, `total`, `createddate`, `createdby`)  
    VALUES ( 'INV21AUG003', '380', '2021-08-01 10:28:45', '2021-08-01', '26', '3', '1', '0', '4000', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.00041079521179199, Seconds : 0.41079521179199
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `invoice_product` ( `qty`, `sub_total`, `product_satuan`, `invoice`, `createddate`, `createdby`)  
    VALUES ( 1, 4000, '9', 29, '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.0050361156463623, Seconds : 5.0361156463623
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '28' 
    LIMIT 1000
 --> Execution Time: 0.0033609867095947, Seconds : 3.3609867095947
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `invoice_status` ( `invoice`, `user`, `status`, `createddate`, `createdby`)  
    VALUES ( 29, '1', 'PAID', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.0010149478912354, Seconds : 1.0149478912354
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `invoice_sisa` ( `invoice`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 29, '0', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.0033009052276611, Seconds : 3.3009052276611
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '26', 'VALIDATE', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.0025441646575928, Seconds : 2.5441646575928
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    SELECT * 
    FROM `payment` 
    WHERE `no_faktur_bayar` 
    LIKE '%PAY21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00064396858215332, Seconds : 0.64396858215332
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `payment` ( `no_faktur_bayar`, `tanggal_faktur`, `tanggal_bayar`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 'PAY21AUG003', '2021-08-01 10:28:52', '2021-08-01', '5000', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.00036287307739258, Seconds : 0.36287307739258
administrator - Tanggal/Waktu : 01-Aug-2021/10:28:52 AM => 
 --> 
    INSERT     INTO `payment_item` ( `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`)  
    VALUES ( 73, 29, '5000', '2021-08-01 10:28:52', '1') 
 --> Execution Time: 0.00051307678222656, Seconds : 0.51307678222656
