
----------../modules/template/controllers/satuan/index----------
admin - Tanggal/Waktu : 08-Aug-2021/16:48:08 PM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `hg`.`harga` as `harga_jual_fix` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `k`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    LIMIT 830, 10
 --> Execution Time: 0.014448881149292, Seconds : 14.448881149292
admin - Tanggal/Waktu : 08-Aug-2021/16:48:08 PM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0
 --> Execution Time: 0.0064749717712402, Seconds : 6.4749717712402
admin - Tanggal/Waktu : 08-Aug-2021/16:48:08 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00054812431335449, Seconds : 0.54812431335449
admin - Tanggal/Waktu : 08-Aug-2021/16:48:08 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00086212158203125, Seconds : 0.86212158203125
admin - Tanggal/Waktu : 08-Aug-2021/16:48:08 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00079202651977539, Seconds : 0.79202651977539
admin - Tanggal/Waktu : 08-Aug-2021/16:48:08 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00077986717224121, Seconds : 0.77986717224121
