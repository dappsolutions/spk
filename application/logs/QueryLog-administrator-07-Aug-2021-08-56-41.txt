
----------../modules/template/controllers/satuan/index----------
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:41 AM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `hg`.`harga` as `harga_jual_fix` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `k`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    LIMIT 10
 --> Execution Time: 0.0020449161529541, Seconds : 2.0449161529541
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:41 AM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0
 --> Execution Time: 0.00061297416687012, Seconds : 0.61297416687012
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:41 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00040102005004883, Seconds : 0.40102005004883
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:41 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00098800659179688, Seconds : 0.98800659179688
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:41 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0012350082397461, Seconds : 1.2350082397461
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:41 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00064897537231445, Seconds : 0.64897537231445
