
----------../modules/template/controllers/faktur_pelanggan/index----------
admin - Tanggal/Waktu : 08-Aug-2021/14:19:20 PM => 
 --> 
    SELECT `i`.*, `isa`.`status`, `sisa`.`jumlah` as `sisa_hutang`, `isp`.`approve` as `approve_print`, `pb`.`nama` as `nama_pembeli` 
    FROM `invoice` `i` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `isa` ON `isa`.`id` = `iss`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_sisa 
    GROUP     BY invoice)  issa ON `issa`.`invoice` = `i`.`id` 
    LEFT     JOIN `invoice_sisa` `sisa` ON `sisa`.`id` = `issa`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_print 
    GROUP     BY invoice)  issp ON `issp`.`invoice` = `i`.`id` 
    LEFT     JOIN `invoice_print` `isp` ON `isp`.`id` = `issp`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM payment_item 
    GROUP     BY invoice)  pi_group ON `pi_group`.`invoice` = `i`.`id` 
    JOIN `payment_item` `pi` ON `pi`.`id` = `pi_group`.`id` 
    LEFT     JOIN `pembeli` `pb` ON `pb`.`id` = `i`.`pembeli` 
    WHERE `i`.`deleted` 
    IS     NULL     OR `i`.`deleted` =0 
    ORDER     BY `i`.`id` 
    DESC     LIMIT 10
 --> Execution Time: 0.0023431777954102, Seconds : 2.3431777954102
admin - Tanggal/Waktu : 08-Aug-2021/14:19:20 PM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `invoice` `i` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `isa` ON `isa`.`id` = `iss`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_sisa 
    GROUP     BY invoice)  issa ON `issa`.`invoice` = `i`.`id` 
    LEFT     JOIN `invoice_sisa` `sisa` ON `sisa`.`id` = `issa`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_print 
    GROUP     BY invoice)  issp ON `issp`.`invoice` = `i`.`id` 
    LEFT     JOIN `invoice_print` `isp` ON `isp`.`id` = `issp`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM payment_item 
    GROUP     BY invoice)  pi_group ON `pi_group`.`invoice` = `i`.`id` 
    JOIN `payment_item` `pi` ON `pi`.`id` = `pi_group`.`id` 
    LEFT     JOIN `pembeli` `pb` ON `pb`.`id` = `i`.`pembeli` 
    WHERE `i`.`deleted` 
    IS     NULL     OR `i`.`deleted` =0
 --> Execution Time: 0.0014688968658447, Seconds : 1.4688968658447
admin - Tanggal/Waktu : 08-Aug-2021/14:19:20 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0004270076751709, Seconds : 0.4270076751709
admin - Tanggal/Waktu : 08-Aug-2021/14:19:20 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00079202651977539, Seconds : 0.79202651977539
admin - Tanggal/Waktu : 08-Aug-2021/14:19:20 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00069284439086914, Seconds : 0.69284439086914
admin - Tanggal/Waktu : 08-Aug-2021/14:19:20 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0018088817596436, Seconds : 1.8088817596436
