
----------../modules/database/controllers/order/confirmBayar----------
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `product_log_stock` ( `product_satuan`, `status`, `qty`, `keterangan`, `reference_id`, `createddate`, `createdby`)  
    VALUES ( '1684', 'ORDER', 1, 'Order Pelanggan', '8', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.019937038421631, Seconds : 19.937038421631
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    JOIN `satuan` `s` ON `s`.`id` = `pst`.`satuan` 
    WHERE `ps`.`deleted` =0 
    AND `p`.`id` = '1686' 
    AND `pst`.`satuan_terkecil` = 1 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0013670921325684, Seconds : 1.3670921325684
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `product_stock` ( `stock`, `product`, `product_satuan`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( 9, '1686', '1684', 1, 1, '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00043892860412598, Seconds : 0.43892860412598
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '8', 'CONFIRM', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00029802322387695, Seconds : 0.29802322387695

----------../modules/no_generator/controllers/order/validateBayar----------
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '8' 
    LIMIT 1000
 --> Execution Time: 0.0027539730072021, Seconds : 2.7539730072021
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT op.* , roi.qty as retur_qty , roi.sub_total as retur_sub_total , ps.harga 
    FROM order_product op 
    JOIN `order` o on o.id = op.`order` 
    LEFT     JOIN retur_order_item roi on roi.order_product = op.id 
    JOIN product_satuan ps on ps.id = op.product_satuan 
    WHERE o.id = '8' 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.0024559497833252, Seconds : 2.4559497833252
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT * 
    FROM `invoice` 
    WHERE `no_faktur` 
    LIKE '%INV21SEP%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00047492980957031, Seconds : 0.47492980957031
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `invoice` ( `no_faktur`, `pembeli`, `tanggal_faktur`, `tanggal_bayar`, `ref`, `potongan`, `metode_bayar`, `pot_faktur`, `total`, `createddate`, `createdby`)  
    VALUES ( 'INV21SEP001', '1', '2021-09-30 18:10:07', '2021-09-30', '8', '3', '1', '0', '12000', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00081205368041992, Seconds : 0.81205368041992
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `invoice_product` ( `qty`, `sub_total`, `product_satuan`, `invoice`, `createddate`, `createdby`)  
    VALUES ( 1, 12000, '1684', 5, '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00036406517028809, Seconds : 0.36406517028809
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '8' 
    LIMIT 1000
 --> Execution Time: 0.0005500316619873, Seconds : 0.5500316619873
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `invoice_status` ( `invoice`, `user`, `status`, `createddate`, `createdby`)  
    VALUES ( 5, '3', 'PAID', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00035500526428223, Seconds : 0.35500526428223
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `invoice_sisa` ( `invoice`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 5, '0', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00024700164794922, Seconds : 0.24700164794922
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '8', 'VALIDATE', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00023698806762695, Seconds : 0.23698806762695
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT * 
    FROM `payment` 
    WHERE `no_faktur_bayar` 
    LIKE '%PAY21SEP%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00046110153198242, Seconds : 0.46110153198242
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `payment` ( `no_faktur_bayar`, `tanggal_faktur`, `tanggal_bayar`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 'PAY21SEP001', '2021-09-30 18:10:21', '2021-09-30', '12000', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.00031113624572754, Seconds : 0.31113624572754
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    INSERT     INTO `payment_item` ( `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`)  
    VALUES ( 5, 5, '12000', '2021-09-30 18:10:21', '3') 
 --> Execution Time: 0.0022428035736084, Seconds : 2.2428035736084

----------../modules/template/controllers/order/add----------
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.00069618225097656, Seconds : 0.69618225097656
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.001255989074707, Seconds : 1.255989074707
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.0043399333953857, Seconds : 4.3399333953857
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00064706802368164, Seconds : 0.64706802368164
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND pst.satuan_terkecil = 1
 --> Execution Time: 0.025712013244629, Seconds : 25.712013244629
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0026581287384033, Seconds : 2.6581287384033
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00080704689025879, Seconds : 0.80704689025879
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00083208084106445, Seconds : 0.83208084106445
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00066399574279785, Seconds : 0.66399574279785

----------../modules/template/controllers/faktur_pelanggan/detail----------
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `i`.*, `p`.`nama` as `nama_pembeli`, `ist`.`status`, `o`.`no_order`, `p`.`alamat`, `pt`.`potongan` as `jenis_potongan`, `jp`.`metode`, `pg`.`nama` as `sales` 
    FROM `invoice` `i` 
    JOIN `pembeli` `p` ON `i`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `ist` ON `ist`.`id` = `iss`.`id` 
    LEFT     JOIN `order` `o` ON `o`.`id` = `i`.`ref` 
    LEFT     JOIN `metode_bayar` `jp` ON `jp`.`id` = `i`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `i`.`potongan` 
    LEFT     JOIN `user` `usr` ON `usr`.`id` = `o`.`createdby` 
    LEFT     JOIN `pegawai` `pg` ON `pg`.`id` = `usr`.`pegawai` 
    WHERE `i`.`id` = '5' 
    LIMIT 1000
 --> Execution Time: 0.0044651031494141, Seconds : 4.4651031494141
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `p`.`kode_product`, `b`.`nama_bank`, `b`.`akun`, `b`.`no_rekening`, `s`.`nama_satuan` 
    FROM `invoice_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `bank` `b` ON `b`.`id` = `ip`.`bank` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`invoice` = '5' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.0058228969573975, Seconds : 5.8228969573975
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `invoice_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`invoice_product` = '5' 
    LIMIT 1000
 --> Execution Time: 0.0024600028991699, Seconds : 2.4600028991699
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT * 
    FROM `jenis_pembayaran` 
    LIMIT 1000
 --> Execution Time: 0.002032995223999, Seconds : 2.032995223999
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00041699409484863, Seconds : 0.41699409484863
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00075912475585938, Seconds : 0.75912475585938
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00089597702026367, Seconds : 0.89597702026367
admin - Tanggal/Waktu : 30-Sep-2021/18:10:21 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0028088092803955, Seconds : 2.8088092803955
