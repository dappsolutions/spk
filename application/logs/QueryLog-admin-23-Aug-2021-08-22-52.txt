
----------../modules/template/controllers/order/detail----------
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '7' 
    LIMIT 1000
 --> Execution Time: 0.003040075302124, Seconds : 3.040075302124
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `ps`.`qty` as `konversi`, `p`.`id` as `product_id` 
    FROM `order_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`order` = '7' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.00076079368591309, Seconds : 0.76079368591309
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '7' 
    LIMIT 1000
 --> Execution Time: 0.00052809715270996, Seconds : 0.52809715270996
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT `ps`.*, `pst`.`stock`, `s`.`nama_satuan` as `current_satuan`, `sp`.`nama_satuan` as `parent_satuan`, `psp`.`id` as `parent_product_satuan`, `psp`.`qty` as `qty_parent`, `pstp`.`stock` as `parent_stock`, `pst`.`id` as `cur_product_stock`, `pstp`.`id` as `parent_product_stock`, `pdc`.`product` as `nama_product` 
    FROM `product_satuan` `ps` 
    JOIN `product_stock` `pst` ON `pst`.`product_satuan` = `ps`.`id` 
    JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    LEFT     JOIN `product_satuan` `psp` ON `psp`.`satuan` = `sp`.`id` 
    AND `psp`.`product` = `ps`.`product` 
    LEFT     JOIN `product_stock` `pstp` ON `pstp`.`product_satuan` = `psp`.`id` 
    JOIN `product` `pdc` ON `pdc`.`id` = `ps`.`product` 
    WHERE `ps`.`deleted` 
    IS     NULL     OR `ps`.`deleted` =0 
    AND `ps`.`id` = '1675' 
    LIMIT 1000
 --> Execution Time: 0.0033860206604004, Seconds : 3.3860206604004
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT ps.* , s.nama_satuan , ps.qty as konversi , ps.ket_harga as keterangan 
    FROM product_satuan ps 
    JOIN satuan s on s.id = ps.satuan 
    WHERE ps.product = 1677 
    AND ps.deleted = 0 
    AND ps.satuan_terkecil = 1
 --> Execution Time: 0.00062799453735352, Seconds : 0.62799453735352
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00039005279541016, Seconds : 0.39005279541016
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00076198577880859, Seconds : 0.76198577880859
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00089693069458008, Seconds : 0.89693069458008
admin - Tanggal/Waktu : 23-Aug-2021/08:22:52 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00071501731872559, Seconds : 0.71501731872559
