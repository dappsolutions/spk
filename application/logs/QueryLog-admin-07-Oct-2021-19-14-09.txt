
----------../modules/database/controllers/order/confirmBayar----------
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `product_log_stock` ( `product_satuan`, `status`, `qty`, `keterangan`, `reference_id`, `createddate`, `createdby`)  
    VALUES ( '1675', 'ORDER', 1, 'Order Pelanggan', '9', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.002701997756958, Seconds : 2.701997756958
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    JOIN `satuan` `s` ON `s`.`id` = `pst`.`satuan` 
    WHERE `ps`.`deleted` =0 
    AND `p`.`id` = '1677' 
    AND `pst`.`satuan_terkecil` = 1 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00084519386291504, Seconds : 0.84519386291504
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `product_stock` ( `stock`, `product`, `product_satuan`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( 5, '1677', '1675', 1, 1, '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.00040507316589355, Seconds : 0.40507316589355
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '9', 'CONFIRM', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.00029110908508301, Seconds : 0.29110908508301

----------../modules/no_generator/controllers/order/validateBayar----------
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '9' 
    LIMIT 1000
 --> Execution Time: 0.001410961151123, Seconds : 1.410961151123
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT op.* , roi.qty as retur_qty , roi.sub_total as retur_sub_total , ps.harga 
    FROM order_product op 
    JOIN `order` o on o.id = op.`order` 
    LEFT     JOIN retur_order_item roi on roi.order_product = op.id 
    JOIN product_satuan ps on ps.id = op.product_satuan 
    WHERE o.id = '9' 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.0027530193328857, Seconds : 2.7530193328857
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT * 
    FROM `invoice` 
    WHERE `no_faktur` 
    LIKE '%INV21OCT%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00099492073059082, Seconds : 0.99492073059082
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `invoice` ( `no_faktur`, `pembeli`, `tanggal_faktur`, `tanggal_bayar`, `ref`, `potongan`, `metode_bayar`, `pot_faktur`, `total`, `createddate`, `createdby`)  
    VALUES ( 'INV21OCT001', '1', '2021-10-07 19:14:00', '2021-10-07', '9', '3', '1', '0', '26000', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.0018489360809326, Seconds : 1.8489360809326
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `invoice_product` ( `qty`, `sub_total`, `product_satuan`, `invoice`, `createddate`, `createdby`)  
    VALUES ( 1, 26000, '1675', 6, '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.0005791187286377, Seconds : 0.5791187286377
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '9' 
    LIMIT 1000
 --> Execution Time: 0.00063705444335938, Seconds : 0.63705444335938
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `invoice_status` ( `invoice`, `user`, `status`, `createddate`, `createdby`)  
    VALUES ( 6, '3', 'PAID', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.0004119873046875, Seconds : 0.4119873046875
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `invoice_sisa` ( `invoice`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 6, '0', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.00038504600524902, Seconds : 0.38504600524902
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '9', 'VALIDATE', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.00027108192443848, Seconds : 0.27108192443848
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT * 
    FROM `payment` 
    WHERE `no_faktur_bayar` 
    LIKE '%PAY21OCT%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00054383277893066, Seconds : 0.54383277893066
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `payment` ( `no_faktur_bayar`, `tanggal_faktur`, `tanggal_bayar`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 'PAY21OCT001', '2021-10-07 19:14:09', '2021-10-07', '26000', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.00031304359436035, Seconds : 0.31304359436035
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    INSERT     INTO `payment_item` ( `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`)  
    VALUES ( 6, 6, '26000', '2021-10-07 19:14:09', '3') 
 --> Execution Time: 0.0004270076751709, Seconds : 0.4270076751709

----------../modules/template/controllers/order/add----------
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.0005640983581543, Seconds : 0.5640983581543
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.00029397010803223, Seconds : 0.29397010803223
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00048613548278809, Seconds : 0.48613548278809
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00026917457580566, Seconds : 0.26917457580566
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND pst.satuan_terkecil = 1
 --> Execution Time: 0.024778127670288, Seconds : 24.778127670288
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00050997734069824, Seconds : 0.50997734069824
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00084710121154785, Seconds : 0.84710121154785
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00096917152404785, Seconds : 0.96917152404785
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00072407722473145, Seconds : 0.72407722473145

----------../modules/template/controllers/faktur_pelanggan/detail----------
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `i`.*, `p`.`nama` as `nama_pembeli`, `ist`.`status`, `o`.`no_order`, `p`.`alamat`, `pt`.`potongan` as `jenis_potongan`, `jp`.`metode`, `pg`.`nama` as `sales` 
    FROM `invoice` `i` 
    JOIN `pembeli` `p` ON `i`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `ist` ON `ist`.`id` = `iss`.`id` 
    LEFT     JOIN `order` `o` ON `o`.`id` = `i`.`ref` 
    LEFT     JOIN `metode_bayar` `jp` ON `jp`.`id` = `i`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `i`.`potongan` 
    LEFT     JOIN `user` `usr` ON `usr`.`id` = `o`.`createdby` 
    LEFT     JOIN `pegawai` `pg` ON `pg`.`id` = `usr`.`pegawai` 
    WHERE `i`.`id` = '6' 
    LIMIT 1000
 --> Execution Time: 0.0055358409881592, Seconds : 5.5358409881592
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `p`.`kode_product`, `b`.`nama_bank`, `b`.`akun`, `b`.`no_rekening`, `s`.`nama_satuan` 
    FROM `invoice_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `bank` `b` ON `b`.`id` = `ip`.`bank` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`invoice` = '6' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.0030691623687744, Seconds : 3.0691623687744
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `invoice_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`invoice_product` = '6' 
    LIMIT 1000
 --> Execution Time: 0.0029358863830566, Seconds : 2.9358863830566
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT * 
    FROM `jenis_pembayaran` 
    LIMIT 1000
 --> Execution Time: 0.0021622180938721, Seconds : 2.1622180938721
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00039005279541016, Seconds : 0.39005279541016
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00081086158752441, Seconds : 0.81086158752441
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.003000020980835, Seconds : 3.000020980835
admin - Tanggal/Waktu : 07-Oct-2021/19:14:09 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0007169246673584, Seconds : 0.7169246673584
