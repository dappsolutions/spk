
----------../modules/template/controllers/lappenjualan/index----------
admin - Tanggal/Waktu : 07-Oct-2021/19:35:28 PM => 
 --> 
    SELECT i.no_faktur , i.tanggal_faktur , i.tanggal_bayar , p.potongan , pd.product , pd.kode_product , pd.kodebarcode , ps.harga , s.nama_satuan as satuan , ip.qty , ip.sub_total , pb.nama as nama_pembeli 
    FROM invoice i 
    JOIN invoice_product ip on ip.invoice = i.id 
    JOIN product_satuan ps on ps.id = ip.product_satuan 
    JOIN product pd on pd.id = ps.product 
    JOIN satuan s on s.id = ps.satuan 
    JOIN potongan p on p.id = i.potongan 
    JOIN pembeli pb on pb.id = i.pembeli 
    WHERE i.tanggal_bayar = '2021-10-07' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    ASC     LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.0019381046295166, Seconds : 1.9381046295166
admin - Tanggal/Waktu : 07-Oct-2021/19:35:28 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00049400329589844, Seconds : 0.49400329589844
admin - Tanggal/Waktu : 07-Oct-2021/19:35:28 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00084185600280762, Seconds : 0.84185600280762
admin - Tanggal/Waktu : 07-Oct-2021/19:35:28 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010039806365967, Seconds : 1.0039806365967
admin - Tanggal/Waktu : 07-Oct-2021/19:35:28 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00094103813171387, Seconds : 0.94103813171387
