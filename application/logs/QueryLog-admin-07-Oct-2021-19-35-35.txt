
----------../modules/database/controllers/lappenjualan/tampilkan----------
admin - Tanggal/Waktu : 07-Oct-2021/19:35:35 PM => 
 --> 
    SELECT i.no_faktur , i.tanggal_faktur , i.tanggal_bayar , p.potongan , pd.product , pd.kode_product , pd.kodebarcode , ps.harga , s.nama_satuan as satuan , ip.qty , ip.sub_total , pb.nama as nama_pembeli 
    FROM invoice i 
    JOIN invoice_product ip on ip.invoice = i.id 
    JOIN product_satuan ps on ps.id = ip.product_satuan 
    JOIN product pd on pd.id = ps.product 
    JOIN satuan s on s.id = ps.satuan 
    JOIN potongan p on p.id = i.potongan 
    JOIN pembeli pb on pb.id = i.pembeli 
    WHERE i.createddate >= '2021-10-01' 
    AND i.createddate <= '2021-10-07' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    ASC     LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.0021309852600098, Seconds : 2.1309852600098
