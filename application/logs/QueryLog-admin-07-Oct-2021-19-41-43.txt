
----------../modules/template/controllers/lappenjualan/index----------
admin - Tanggal/Waktu : 07-Oct-2021/19:41:43 PM => 
 --> 
    SELECT i.no_faktur , i.tanggal_faktur , i.tanggal_bayar , p.potongan , pd.product , pd.kode_product , pd.kodebarcode , ps.harga , s.nama_satuan as satuan , ip.qty , ip.sub_total , pb.nama as nama_pembeli 
    FROM invoice i 
    JOIN invoice_product ip on ip.invoice = i.id 
    JOIN product_satuan ps on ps.id = ip.product_satuan 
    JOIN product pd on pd.id = ps.product 
    JOIN satuan s on s.id = ps.satuan 
    JOIN potongan p on p.id = i.potongan 
    JOIN pembeli pb on pb.id = i.pembeli 
    WHERE i.tanggal_bayar = '2021-10-07' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    ASC     LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.0013101100921631, Seconds : 1.3101100921631
admin - Tanggal/Waktu : 07-Oct-2021/19:41:43 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00045299530029297, Seconds : 0.45299530029297
admin - Tanggal/Waktu : 07-Oct-2021/19:41:43 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0008847713470459, Seconds : 0.8847713470459
admin - Tanggal/Waktu : 07-Oct-2021/19:41:43 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010979175567627, Seconds : 1.0979175567627
admin - Tanggal/Waktu : 07-Oct-2021/19:41:43 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00083589553833008, Seconds : 0.83589553833008
