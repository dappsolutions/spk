
----------../modules/database/controllers/satuan/simpan----------
administrator - Tanggal/Waktu : 07-Aug-2021/08:39:23 AM => 
 --> 
    INSERT     INTO `product_satuan` ( `product`, `satuan`, `qty`, `harga`, `harga_beli`, `ket_harga`, `createddate`, `createdby`)  
    VALUES ( '3160', '956', '1', '3500', '3000', '-', '2021-08-07 08:39:23', '1') 
 --> Execution Time: 0.00059795379638672, Seconds : 0.59795379638672
administrator - Tanggal/Waktu : 07-Aug-2021/08:39:23 AM => 
 --> 
    INSERT     INTO `product_has_harga_jual` ( `product_satuan`, `harga`, `createddate`, `createdby`)  
    VALUES ( 10, '3500', '2021-08-07 08:39:23', '1') 
 --> Execution Time: 0.00033402442932129, Seconds : 0.33402442932129

----------../modules/template/controllers/satuan/detail----------
administrator - Tanggal/Waktu : 07-Aug-2021/08:39:23 AM => 
 --> 
    SELECT `kr`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `sp`.`nama_satuan` as `satuan_parent`, `hg`.`harga` as `harga_jual_fix`, `hgs`.`harga` `harga_grosir_fix` 
    FROM `product_satuan` `kr` 
    JOIN `product` `p` ON `kr`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `kr`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_grosir 
    GROUP     BY product_satuan)  hgs_max ON `hgs_max`.`product_satuan` = `kr`.`id` 
    LEFT     JOIN `product_has_harga_grosir` `hgs` ON `hgs`.`id` = `hgs_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `kr`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    WHERE `kr`.`id` = '10' 
    LIMIT 1000
 --> Execution Time: 0.0014030933380127, Seconds : 1.4030933380127
administrator - Tanggal/Waktu : 07-Aug-2021/08:39:23 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00059700012207031, Seconds : 0.59700012207031
administrator - Tanggal/Waktu : 07-Aug-2021/08:39:23 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0012178421020508, Seconds : 1.2178421020508
administrator - Tanggal/Waktu : 07-Aug-2021/08:39:23 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0013000965118408, Seconds : 1.3000965118408
administrator - Tanggal/Waktu : 07-Aug-2021/08:39:23 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00062799453735352, Seconds : 0.62799453735352
