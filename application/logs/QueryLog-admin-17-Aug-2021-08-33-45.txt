
----------../modules/database/controllers/order/confirmBayar----------
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `product_log_stock` ( `product_satuan`, `status`, `qty`, `keterangan`, `reference_id`, `createddate`, `createdby`)  
    VALUES ( '1675', 'ORDER', 1, 'Order Pelanggan', '2', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.00059700012207031, Seconds : 0.59700012207031
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    JOIN `satuan` `s` ON `s`.`id` = `pst`.`satuan` 
    WHERE `ps`.`deleted` =0 
    AND `p`.`id` = '1677' 
    AND `pst`.`satuan_terkecil` = 1 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0010099411010742, Seconds : 1.0099411010742
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `product_stock` ( `stock`, `product`, `product_satuan`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( 8, '1677', '1675', 1, 1, '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.00054097175598145, Seconds : 0.54097175598145
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '2', 'CONFIRM', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.00038599967956543, Seconds : 0.38599967956543

----------../modules/no_generator/controllers/order/validateBayar----------
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '2' 
    LIMIT 1000
 --> Execution Time: 0.0019328594207764, Seconds : 1.9328594207764
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT op.* , roi.qty as retur_qty , roi.sub_total as retur_sub_total , ps.harga 
    FROM order_product op 
    JOIN `order` o on o.id = op.`order` 
    LEFT     JOIN retur_order_item roi on roi.order_product = op.id 
    JOIN product_satuan ps on ps.id = op.product_satuan 
    WHERE o.id = '2' 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.00079703330993652, Seconds : 0.79703330993652
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT * 
    FROM `invoice` 
    WHERE `no_faktur` 
    LIKE '%INV21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00041794776916504, Seconds : 0.41794776916504
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `invoice` ( `no_faktur`, `pembeli`, `tanggal_faktur`, `tanggal_bayar`, `ref`, `potongan`, `metode_bayar`, `pot_faktur`, `total`, `createddate`, `createdby`)  
    VALUES ( 'INV21AUG002', '1', '2021-08-17 08:33:36', '2021-08-17', '2', '3', '1', '0', '24000', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.00084710121154785, Seconds : 0.84710121154785
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `invoice_product` ( `qty`, `sub_total`, `product_satuan`, `invoice`, `createddate`, `createdby`)  
    VALUES ( 1, 24000, '1675', 2, '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.0045850276947021, Seconds : 4.5850276947021
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '2' 
    LIMIT 1000
 --> Execution Time: 0.00061392784118652, Seconds : 0.61392784118652
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `invoice_status` ( `invoice`, `user`, `status`, `createddate`, `createdby`)  
    VALUES ( 2, '3', 'PAID', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.00077199935913086, Seconds : 0.77199935913086
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `invoice_sisa` ( `invoice`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 2, '0', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.0039210319519043, Seconds : 3.9210319519043
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '2', 'VALIDATE', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.00038409233093262, Seconds : 0.38409233093262
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT * 
    FROM `payment` 
    WHERE `no_faktur_bayar` 
    LIKE '%PAY21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0010039806365967, Seconds : 1.0039806365967
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `payment` ( `no_faktur_bayar`, `tanggal_faktur`, `tanggal_bayar`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 'PAY21AUG002', '2021-08-17 08:33:45', '2021-08-17', '24000', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.00042605400085449, Seconds : 0.42605400085449
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    INSERT     INTO `payment_item` ( `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`)  
    VALUES ( 2, 2, '24000', '2021-08-17 08:33:45', '3') 
 --> Execution Time: 0.0025629997253418, Seconds : 2.5629997253418

----------../modules/template/controllers/order/add----------
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.00061893463134766, Seconds : 0.61893463134766
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.0041000843048096, Seconds : 4.1000843048096
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00051403045654297, Seconds : 0.51403045654297
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00038886070251465, Seconds : 0.38886070251465
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND pst.satuan_terkecil = 1
 --> Execution Time: 0.024785995483398, Seconds : 24.785995483398
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00054407119750977, Seconds : 0.54407119750977
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010709762573242, Seconds : 1.0709762573242
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00080204010009766, Seconds : 0.80204010009766
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00080490112304688, Seconds : 0.80490112304688

----------../modules/template/controllers/faktur_pelanggan/detail----------
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `i`.*, `p`.`nama` as `nama_pembeli`, `ist`.`status`, `o`.`no_order`, `p`.`alamat`, `pt`.`potongan` as `jenis_potongan`, `jp`.`metode`, `pg`.`nama` as `sales` 
    FROM `invoice` `i` 
    JOIN `pembeli` `p` ON `i`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `ist` ON `ist`.`id` = `iss`.`id` 
    LEFT     JOIN `order` `o` ON `o`.`id` = `i`.`ref` 
    LEFT     JOIN `metode_bayar` `jp` ON `jp`.`id` = `i`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `i`.`potongan` 
    LEFT     JOIN `user` `usr` ON `usr`.`id` = `o`.`createdby` 
    LEFT     JOIN `pegawai` `pg` ON `pg`.`id` = `usr`.`pegawai` 
    WHERE `i`.`id` = '2' 
    LIMIT 1000
 --> Execution Time: 0.0012741088867188, Seconds : 1.2741088867188
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `p`.`kode_product`, `b`.`nama_bank`, `b`.`akun`, `b`.`no_rekening`, `s`.`nama_satuan` 
    FROM `invoice_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `bank` `b` ON `b`.`id` = `ip`.`bank` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`invoice` = '2' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.0026168823242188, Seconds : 2.6168823242188
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `invoice_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`invoice_product` = '2' 
    LIMIT 1000
 --> Execution Time: 0.00069999694824219, Seconds : 0.69999694824219
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT * 
    FROM `jenis_pembayaran` 
    LIMIT 1000
 --> Execution Time: 0.00040292739868164, Seconds : 0.40292739868164
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00039911270141602, Seconds : 0.39911270141602
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00091695785522461, Seconds : 0.91695785522461
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00082778930664062, Seconds : 0.82778930664062
admin - Tanggal/Waktu : 17-Aug-2021/08:33:45 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00078701972961426, Seconds : 0.78701972961426
