
----------../modules/database/controllers/product_stock/simpan----------
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    WHERE `ps`.`deleted` =0 
    AND `ps`.`product_satuan` = '14' 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00075793266296387, Seconds : 0.75793266296387
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    INSERT     INTO `product_stock` ( `product`, `product_satuan`, `stock`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( '3161', '14', '30', '1', '1', '2021-08-07 09:01:37', '1') 
 --> Execution Time: 0.00034689903259277, Seconds : 0.34689903259277

----------../modules/template/controllers/product_stock/detail----------
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    SELECT `kr`.*, `p`.`product` as `nama_product`, `ps`.`satuan`, `ps`.`product`, `r`.`nama_rak`, `g`.`nama_gudang`, `s`.`nama_satuan`, `sp`.`nama_satuan` as `satuan_parent` 
    FROM `product_stock` `kr` 
    JOIN `product_satuan` `ps` ON `kr`.`product_satuan` = `ps`.`id` 
    JOIN `product` `p` ON `ps`.`product` = `p`.`id` 
    LEFT     JOIN `gudang` `g` ON `kr`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `kr`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    WHERE `kr`.`id` = '23' 
    LIMIT 1000
 --> Execution Time: 0.0013720989227295, Seconds : 1.3720989227295
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    SELECT `pls`.* 
    FROM `product_log_stock` `pls` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `pls`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    WHERE `pls`.`deleted` =0 
    AND `p`.`id` = '3161' 
    ORDER     BY `pls`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0039749145507812, Seconds : 3.9749145507812
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00041794776916504, Seconds : 0.41794776916504
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010759830474854, Seconds : 1.0759830474854
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0013809204101562, Seconds : 1.3809204101562
administrator - Tanggal/Waktu : 07-Aug-2021/09:01:37 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00078511238098145, Seconds : 0.78511238098145
