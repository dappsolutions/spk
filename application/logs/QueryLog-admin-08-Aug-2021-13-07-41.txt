
----------../modules/template/controllers/product_stock/index----------
admin - Tanggal/Waktu : 08-Aug-2021/13:07:41 PM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `ps`.`satuan`, `g`.`nama_gudang`, `r`.`nama_rak`, `s`.`nama_satuan`, `p`.`kode_product` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    AND `ps`.`satuan_terkecil` = '1' 
    LIMIT 60, 10
 --> Execution Time: 0.017802953720093, Seconds : 17.802953720093
admin - Tanggal/Waktu : 08-Aug-2021/13:07:41 PM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    AND `ps`.`satuan_terkecil` = '1'
 --> Execution Time: 0.019927978515625, Seconds : 19.927978515625
admin - Tanggal/Waktu : 08-Aug-2021/13:07:41 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00052595138549805, Seconds : 0.52595138549805
admin - Tanggal/Waktu : 08-Aug-2021/13:07:41 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00081610679626465, Seconds : 0.81610679626465
admin - Tanggal/Waktu : 08-Aug-2021/13:07:41 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00075697898864746, Seconds : 0.75697898864746
admin - Tanggal/Waktu : 08-Aug-2021/13:07:41 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00079798698425293, Seconds : 0.79798698425293
