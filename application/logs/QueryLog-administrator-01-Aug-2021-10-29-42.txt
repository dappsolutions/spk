
----------../modules/template/controllers/order/detail----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '27' 
    LIMIT 1000
 --> Execution Time: 0.0015780925750732, Seconds : 1.5780925750732
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `ps`.`qty` as `konversi`, `p`.`id` as `product_id` 
    FROM `order_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`order` = '27' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.00086402893066406, Seconds : 0.86402893066406
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '29' 
    LIMIT 1000
 --> Execution Time: 0.00063991546630859, Seconds : 0.63991546630859
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `ps`.*, `pst`.`stock`, `s`.`nama_satuan` as `current_satuan`, `sp`.`nama_satuan` as `parent_satuan`, `psp`.`id` as `parent_product_satuan`, `psp`.`qty` as `qty_parent`, `pstp`.`stock` as `parent_stock`, `pst`.`id` as `cur_product_stock`, `pstp`.`id` as `parent_product_stock`, `pdc`.`product` as `nama_product` 
    FROM `product_satuan` `ps` 
    JOIN `product_stock` `pst` ON `pst`.`product_satuan` = `ps`.`id` 
    JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    LEFT     JOIN `product_satuan` `psp` ON `psp`.`satuan` = `sp`.`id` 
    AND `psp`.`product` = `ps`.`product` 
    LEFT     JOIN `product_stock` `pstp` ON `pstp`.`product_satuan` = `psp`.`id` 
    JOIN `product` `pdc` ON `pdc`.`id` = `ps`.`product` 
    WHERE `ps`.`deleted` 
    IS     NULL     OR `ps`.`deleted` =0 
    AND `ps`.`id` = '6' 
    LIMIT 1000
 --> Execution Time: 0.0010471343994141, Seconds : 1.0471343994141
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00037693977355957, Seconds : 0.37693977355957
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010330677032471, Seconds : 1.0330677032471
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.001317024230957, Seconds : 1.317024230957
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00065779685974121, Seconds : 0.65779685974121

----------../modules/template/controllers/faktur_pelanggan/detail----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `i`.*, `p`.`nama` as `nama_pembeli`, `ist`.`status`, `o`.`no_order`, `p`.`alamat`, `pt`.`potongan` as `jenis_potongan`, `jp`.`metode`, `pg`.`nama` as `sales` 
    FROM `invoice` `i` 
    JOIN `pembeli` `p` ON `i`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `ist` ON `ist`.`id` = `iss`.`id` 
    LEFT     JOIN `order` `o` ON `o`.`id` = `i`.`ref` 
    LEFT     JOIN `metode_bayar` `jp` ON `jp`.`id` = `i`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `i`.`potongan` 
    LEFT     JOIN `user` `usr` ON `usr`.`id` = `o`.`createdby` 
    LEFT     JOIN `pegawai` `pg` ON `pg`.`id` = `usr`.`pegawai` 
    WHERE `i`.`id` = '30' 
    LIMIT 1000
 --> Execution Time: 0.0027329921722412, Seconds : 2.7329921722412
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `p`.`kode_product`, `b`.`nama_bank`, `b`.`akun`, `b`.`no_rekening`, `s`.`nama_satuan` 
    FROM `invoice_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `bank` `b` ON `b`.`id` = `ip`.`bank` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`invoice` = '30' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.0010018348693848, Seconds : 1.0018348693848
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `invoice_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`invoice_product` = '30' 
    LIMIT 1000
 --> Execution Time: 0.00061988830566406, Seconds : 0.61988830566406
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT * 
    FROM `jenis_pembayaran` 
    LIMIT 1000
 --> Execution Time: 0.00034093856811523, Seconds : 0.34093856811523
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0018720626831055, Seconds : 1.8720626831055
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0012490749359131, Seconds : 1.2490749359131
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0014328956604004, Seconds : 1.4328956604004
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00076699256896973, Seconds : 0.76699256896973

----------../modules/template/controllers/order/add----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `ps`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `pst`.`stock`, `p`.`kode_product` 
    FROM `product_satuan` `ps` 
    JOIN `product` `p` ON `ps`.`product` = `p`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    JOIN `product_stock` `pst` ON `p`.`id` = `pst`.`product` 
    WHERE `ps`.`deleted` =0 
    OR `ps`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.0034101009368896, Seconds : 3.4101009368896
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.00062108039855957, Seconds : 0.62108039855957
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.00032711029052734, Seconds : 0.32711029052734
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00043392181396484, Seconds : 0.43392181396484
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00028109550476074, Seconds : 0.28109550476074
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND s.nama_satuan = 'PCS'
 --> Execution Time: 0.00094294548034668, Seconds : 0.94294548034668
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00038003921508789, Seconds : 0.38003921508789
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0011060237884521, Seconds : 1.1060237884521
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0013799667358398, Seconds : 1.3799667358398
administrator - Tanggal/Waktu : 01-Aug-2021/10:29:42 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00076699256896973, Seconds : 0.76699256896973
