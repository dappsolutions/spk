<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  /**
   * this helper made of itself..
   * author : Dwi Mursito
   **/

  //--------------------------------------------------------------------------//

  if( !function_exists('bar128') )
  {
      /**
       * Random Numeric,String
       *
       * @return String.
       */
      global $char128asc,$char128charWidth;
	  $char128asc=' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
	  $char128wid = array(
			'212222','222122','222221','121223','121322','131222','122213','122312','132212','221213', // 0-9
			'221312','231212','112232','122132','122231','113222','123122','123221','223211','221132', // 10-19
			'221231','213212','223112','312131','311222','321122','321221','312212','322112','322211', // 20-29
			'212123','212321','232121','111323','131123','131321','112313','132113','132311','211313', // 30-39
			'231113','231311','112133','112331','132131','113123','113321','133121','313121','211331', // 40-49
			'231131','213113','213311','213131','311123','311321','331121','312113','312311','332111', // 50-59
			'314111','221411','431111','111224','111422','121124','121421','141122','141221','112214', // 60-69
			'112412','122114','122411','142112','142211','241211','221114','413111','241112','134111', // 70-79
			'111242','121142','121241','114212','124112','124211','411212','421112','421211','212141', // 80-89
			'214121','412121','111143','111341','131141','114113','114311','411113','411311','113141', // 90-99
			'114131','311141','411131','211412','211214','211232','23311120'   );
      function bar128($text)
      {
      		global $char128asc,$char128wid;
			$w = $char128wid[$sum = 104];							// START symbol
			$onChar=1;
			for($x=0;$x<strlen($text);$x++)								// GO THRU TEXT GET LETTERS
				if (!( ($pos = strpos($char128asc,$text[$x])) === false )){	// SKIP NOT FOUND CHARS
				 	$w.= $char128wid[$pos];
				  	$sum += $onChar++ * $pos;
				}
			$w.= $char128wid[ $sum % 103 ].$char128wid[106];  		//Check Code, then END
				 					 						//Part 2, Write rows
			//$html='<center><div style="border:3px double #ababab; padding:5px;margin:5px auto;">';
			$html="<table cellpadding=0 cellspacing=0><tr>";
			for($x=0;$x<strlen($w);$x+=2)   						// code 128 widths: black border, then white space
			$html .= "<td><div class=\"b128\" style=\"border-left-width:{$w[$x]};width:{$w[$x+1]}\"></div>";
			return "$html<tr><td  colspan=".strlen($w)." align=center><font family=arial size=2><b>$text</table>";
      }
  }

  if( !function_exists('checkNull') )
  {
      /**
       * Remove Underscore
       *
       * @return String.
       */
      function checkNull($value) {
      		empty($value) ? $value = 'aa' : $value = $value;
            return $value;
      }
  }

  if( !function_exists('convertNomerSample') )
  {
      /**
       * Remove Underscore
       *
       * @return String.
       */
      function convertNomerSample($nomersample){
        /* nomorsample = '2014J00005' */
        $StartCharAscii = 64;
        $tahun = substr($nomersample,0,4);
        $urut = substr($nomersample,-5);
        $bulan = ord(substr($nomersample,-6,1)) - $StartCharAscii;
        if($bulan < 10) $bulan = '0'.$bulan;
        return substr($tahun,2).$bulan.$urut;
      }
  }

  if( !function_exists('removeUnderscore') )
  {
      /**
       * Remove Underscore
       *
       * @return String.
       */
      function removeUnderscore($str) {
            $tmp = explode('_', $str);
            return implode(' ', $tmp);
      }
  }

  if( !function_exists('randomNomor') )
  {
      /**
       * Random Numeric,String
       *
       * @return String.
       */
      function randomNomor($jumlah_digit)
      {
            for($i=0; $i<$jumlah_digit; $i++)
            {
                $num_ar[] = rand(0,9);
            }
            return implode('',$num_ar);
      }
  }

  if( !function_exists('isRomanNumeral') )
  {
      /**
       * For checking whether string is Roman Numeral.
       * @return Bool
       */
      function isRomanNumeral($string)
      {
          return preg_match('/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/',$string) ? TRUE : FALSE;
      }
  }

  if( !function_exists('setTanggal') )
  {
      /**
       * Date Configuration to be format Indonesian Date
       *
       * @return String Date within display numeric style.
       */
      function setTanggal($dataTanggal,$getSeparator,$setSeparator)
      {
            $arrSplit = explode($getSeparator,$dataTanggal);
            $tanggal  = $arrSplit[2].$setSeparator.$arrSplit[1].$setSeparator.$arrSplit[0];
            return $tanggal;
      }
  }

  if( !function_exists('setTanggal_strBulan') )
  {
      /**
       * Date Configuration to be format Indonesian Date
       *
       * @return string Date within display monthly name style..
       */
      function setTanggal_strBulan($dataTanggal,$getSeparator='-',$setSeparator=' ',$jenis_format='inisial')
      {
                $strBulan_inisial = array("Jan","Peb","Mar","Apr","Mei","Jun","Jul","Agu","Sep","Okt","Nop","Des");
                $strBulan_full    = array("Januari","Pebruari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");

                $pecah_tanggal = explode($getSeparator,$dataTanggal);
                $index_bulan   = floor($pecah_tanggal[1])-1;
                switch($jenis_format){
                    case "inisial":
                        default:
                        $tanggal = $pecah_tanggal[2].$setSeparator.$strBulan_inisial[$index_bulan].$setSeparator.$pecah_tanggal[0];
                        break;
                    case "full":
                        $tanggal = $pecah_tanggal[2].$setSeparator.$strBulan_full[$index_bulan].$setSeparator.$pecah_tanggal[0];
                        break;
                }
                return $tanggal;
       }
   }

   if( !function_exists('convertDate_oracleToMysql') )
   {
       /**
        * Converter Oracle Date To MySQL Date.
        */
       function convertDate_oracleToMysql($oracleDate)
       {
          return date("Y-m-d",strtotime($oracleDate));
       }
   }

   if( !function_exists('convertDate_MySQLToOracle') )
   {
       /**
        * Converter MySQL Date To Oracle Date.
        */
       function convertDate_MySQLToOracle($MySQLDate)
       {
          return date("d-M-y",strtotime($MySQLDate));
       }
   }

   if( !function_exists('convertToNumIfRoman') )
   {
       function convertToNumIfRoman($romanNumber)
       {
           return isRomanNumeral($romanNumber)==TRUE ? romanToNumber($romanNumber) : $romanNumber;
       }
   }

   if( !function_exists('numberToRoman') )
   {
      /**
       * Converter Number To Roman.
       */
      function numberToRoman($num,$isUpper=true)
      {
            $n   = intval($num);
            $res = '';

            #--/roman_numerals array
            $roman_numerals = array(
                        'M'  => 1000,
                        'CM' => 900,
                        'D'  => 500,
                        'CD' => 400,
                        'C'  => 100,
                        'XC' => 90,
                        'L'  => 50,
                        'XL' => 40,
                        'X'  => 10,
                        'IX' => 9,
                        'V'  => 5,
                        'IV' => 4,
                        'I'  => 1
            );
            foreach($roman_numerals as $roman => $number)
            {
                #--/divide to get matches
                $matches = intval($n / $number);

                #--/assign the roman char * $matches
                $res .= str_repeat($roman, $matches);

                #--/substract from the number
                $n = $n % $number;
            }
            return $isUpper ? $res : strtolower($res); #--/return the res
       }
  }

  if ( !function_exists('catchSerializeArray') )
  {
      /**
       *  Untuk mengubah json data dari jQuery.serializeArray()
       *  ke bentuk array PHP.
       *
       *  @return Array
       */
      function catchSerializeArray($getData, $json=FALSE)
      {
          $data = $json==TRUE ? json_decode($getData, TRUE) : $getData;
          $result = array();
          foreach($data as $elm) {
             $result[$elm['name']] = $elm['value'];
          }
          return $result;
      }
  }

  if ( !function_exists('objectTo_array') )
  {
     /**
      * Object to Array
      *
      * Takes an object as input and converts the class variables to array key/vals
      * Uses the magic __FUNCTION__ callback method for multi arrays.
      *
      * $array = object_to_array($object);
      * print_r($array);
      *
      * @param object - The $object to convert to an array
      * @return array
      */
     function objectTo_array($object)
     {
          if (is_object($object))
          {
              // Gets the properties of the given object with get_object_vars function
              $object = get_object_vars($object);
          }
          return (is_array($object)) ? array_map(__FUNCTION__, $object) : $object;
     }
  }

  if ( ! function_exists('arrayTo_object'))
  {
     /**
      * Array to Object
      *
      * Takes an array as input and converts the class variables to an object
      * Uses the magic __FUNCTION__ callback method for multi objects.
      *
      * $object = array_to_object($array);
      * print_r($object);
      *
      * @param array - The $array to convert to an object
      * @return object
      */
     function arrayTo_object($array)
     {
        return (is_array($array)) ? (object) array_map(__FUNCTION__, $array) : $array;
     }
  }
  if ( !function_exists('objectTo_arrayRecursive') )
  {
      /**
       * Change stdClass object to array within recursively ways.
       * ---
       * @param stdClass object
       * @return Array
       */
      function objectTo_arrayRecursive($obj)
      {
            if(is_object($obj)) $obj = (array) $obj;
            if(is_array($obj))
            {
                $newArray = array();
                foreach ($obj as $key => $val) {
                    $newArray[$key] = objectTo_arrayRecursive($val);
                }
            }
            else $newArray = $obj;
            return $newArray;
      }
  }

  if ( !function_exists('arrayTo_objectRecursive') )
  {
      /**
       * Change  array to stdClass object within recursively ways.
       * ---
       * @param Array
       * @return stdClass Object
       */
      function arrayTo_objectRecursive($array)
      {
            $obj = new stdClass;
            foreach($array as $k => $v) {
               if(strlen($k)) {
                  if(is_array($v)) {
                     $obj->{$k} = arrayTo_objectRecursive($v); #--/RECURSION->>
                  } else {
                     $obj->{$k} = $v;
                  }
               }
            }
            return $obj;
      }

  }

  if ( !function_exists('super_unique') )
  {
      /**
       * Remove duplicate values an multidimensional array.
       * Example :
       * $array = array( array(12,21,13,31), array(12,21,13,31), array(42,24,23,32) );
       * $result= super_unique( $array );
       *
       * Equal :
       *     Array(
       *           [0] => Array( [0] => 12 [1] => 21 [2] => 13 [3] => 31 )
       *           [2] => Array( [0] => 42 [1] => 24 [2] => 23 [3] => 32 )
       *     )
       *
       * @return Array
       */
      function super_unique($array)
      {
          $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

          foreach ($result as $key => $value)
          {
            if ( is_array($value) )
            {
              $result[$key] = super_unique($value);
            }
          }
          return $result;
      }
  }

  if ( !function_exists('nominee_strToNumber') )
  {
      /**
       * Konversi nominal `String` ke nominal `Number`
       * @return Number/Double
       */
      function nominee_strToNumber($strNominee)
      {
            return floatval(str_replace(',', '.', str_replace('.', '', $strNominee)));
      }
  }

  if ( !function_exists('sortByValue') )
  {
      /**
       * Pengurutan array multidimensi(bercabang) order dari nilai/value array.
       * ---
       * @param key of value in array.
       * @return Array.
       */
      function sortByValue(&$array, $key)
      {
            $sorter=array();
            $ret=array();
            reset($array);

            foreach ($array as $ii => $val) {
                $sorter[$ii]=$val[$key];
            }
            asort($sorter);
            foreach ($sorter as $ii => $val){
                $ret[$ii]=$array[$ii];
            }
            return $ret;
      }
  }

  if ( !function_exists('romanToNumber') )
  {
      /**
       * Converter Roman To Number.
       */
      function romanToNumber($roman)
      {
            $conv = array(
                 array("letter" => 'I', "number" => 1)
                ,array("letter" => 'V', "number" => 5)
                ,array("letter" => 'X', "number" => 10)
                ,array("letter" => 'L', "number" => 50)
                ,array("letter" => 'C', "number" => 100)
                ,array("letter" => 'D', "number" => 500)
                ,array("letter" => 'M', "number" => 1000)
                ,array("letter" => 0,   "number" => 0)
            );
            $arabic = 0;
            $state  = 0;
            $sidx   = 0;
            $len    = strlen($roman);
            while($len >= 0)
            {
                $i = 0;
                $sidx = $len;
                while ($conv[$i]['number'] > 0)
                {
                    if (@strtoupper($roman[$sidx]) == $conv[$i]['letter'])
                    {
                        if ($state > $conv[$i]['number'])
                        {
                            $arabic -= $conv[$i]['number'];
                        }
                        else
                        {
                            $arabic += $conv[$i]['number'];
                            $state = $conv[$i]['number'];
                        }
                    }
                    $i++;
                }
                $len--;
            }
            return($arabic);
      }
  }

  if( !function_exists('get_yesterdayDate') )
  {
      /**
       * This method is if you want get Date to yesterday value..
       *
       * @return String.
       */
      function get_yesterdayDate()
      {
    	return date("F j, Y", time() - 60 * 60 * 24);
      }
  }

  if( !function_exists('setToGrouping') )
  {
      /**
       * If you want change array structures to grouping array,
       * within [your_key_to_group] from array element is your choice..
       *
       * @return Array.
       */
      function setToGrouping($array,$key_to_group)
      {
            $groups = array();
            foreach($array as $item)
            {
                $key = $item[$key_to_group];

                if(!isset($groups[$key])) {
                    $groups[$key] = array(
                        'items' => array($item),
                        'count' => 1,
                    );
                }else{
                    $groups[$key]['items'][] = $item;
                    $groups[$key]['count'] += 1;
                }
            }
            return $groups;
      }
  }

  if( !function_exists('simpleGrouping') )
  {
      /**
       * If you want change array structures to grouping array,
       * within [your_key_to_group] from array element is your choice..
       *
       * @return Array.
       */
      function simpleGrouping($array,$key_to_group)
      {
            $groups = array();
            foreach($array as $item)
            {
                $key = $item[$key_to_group];

                if(!isset($groups[$key])) {
                    $groups[$key] = array($item);

                }else{
                    $groups[$key][] = $item;
                }
            }
            return $groups;
      }
  }

  if( !function_exists('arr2dimensionTo1dimension') )
  {
      /**
       * Change array structure from array two dimensions
       * convert to one dimension.
       * @return Array
       */
      function arr2dimensionTo1dimension($arr2dimension)
      {
          $newAr = array();
          foreach($arr2dimension as $keyAr => $elmAr){
              foreach($elmAr as $subelmAr){
                  $newAr[] = $subelmAr;
              }
          }
          return $newAr;
      }
  }

  if( !function_exists('directory_map') )
  {
      /**
       * Untuk eksplorasi sebuah folder/direktori.
       */
      function directory_map($source_dir, $top_level_only = FALSE)
      {
            if ($fp = @opendir($source_dir))
            {
                $filedata = array();
                while (FALSE !== ($file = readdir($fp)))
                {
                    if (@is_dir($source_dir.$file) && substr($file, 0, 1) != '.' AND $top_level_only == FALSE)
                    {
                        $temp_array = array();

                        $temp_array = directory_map($source_dir.$file."/");

                        $filedata[$source_dir.$file] = $temp_array; //$k => directory path; $v => directory files && sub-directories
                        uksort($filedata[$source_dir.$file], 'mysort');//this will always be an array, so sort it here!
                    }
                    elseif (substr($file, 0, 1) != ".")
                    {
                        $filedata[$source_dir.$file] = $source_dir.$file; //$k => file path; $v => file path
                    }
                }
                return $filedata;
            }
      }
  }

  if( !function_exists('get_filename') )
  {
      /**
       * Untuk mengambil hanya nama filenya tanpa ekstension.
       */
      function get_filename($filename)
      {
            $info= pathinfo($filename);
            return basename($filename,'.'.$info['extension']);
      }
  }

  if( !function_exists('rm_extension') )
  {
      /**
       * Untuk menghapus ekstension dari sebuah file.
       */
      function rm_extension($filename)
      {
            return preg_replace('/\\.[^.\\s]{3,4}$/','',$filename);
      }
  }

  if( !function_exists('set_forValuesQuerySql') )
  {
      /**
       * Untuk mengubah format penulisan pada values dalam melakukan insert query sql,
       * data sumber dari array.
       * ---
       * contoh:
       *    $dataArray = array('A','B','C');
       *    $dataArray = Array [
       *                         0 => A
       *                         1 => B
       *                         2 => C
       *                       ]
       *    set_forValuesQuerySql($dataArray);
       * hasil String:
       *                'A','B','C'
       */
      function set_forValuesQuerySql($array)
      {
          array_walk($array, function(&$value, $key, $wrapper){ $value = $wrapper.$value.$wrapper; }, "'");
          return implode(",", $array);
      }
  }

	if ( ! function_exists('get_navigation')){
		function get_navigation($menu = array()){
			$CI =& get_instance();

			$nav = array(
				'home'=> array(
					'id' => 'nav_home',
					'class' => '',
					'title' => 'Home',
					'url' => '#'
				),
				'permintaanpembelian'=> array(
					'id' => 'nav_permintaanpembelian',
					'class' => '',
					'title' => 'Permintaan Pembelian',
					'url' => 'permintaan_pembelian/'
				),
				'orderpembelian' => array(
					'id' => 'nav_orderpembelian',
					'class' => '',
					'title' => 'Order Pembalian',
					'url' => 'order_pembelian/'
				),
				'Verifikasi' => array(
					'id' => 'nav_verifikasi',
					'class' => '',
					'title' => 'Verifikasi',
					'url' => 'verifikasi/'
				),
        'Perintah Bongkar'=> array(
	          'id' => 'nav_perintahbongkar',
	          'class' => '',
	          'title' => 'Perintah Bongkar',
	          'url' => 'perintah_bongkar/'
        )
		);

			$cur_control = $CI->router->class;
			$listMenu = array();
			array_push($listMenu,$nav['home']);
			if(!empty($menu)){
				foreach($menu as $item){
					array_push($listMenu,$nav[$item['id']]);
				}
			}
			foreach($listMenu as $key => $value){
				$compared_str = str_replace('nav_','',$value['id']);
				if($cur_control == $compared_str)
					$listMenu[$key]['class'] = 'active';
				else
					$listMenu[$key]['class'] = '';
			}

			return $listMenu;
		}
	}
if(!function_exists('isTanpaTolakan')){
  function isOPTanpaTolakan($nomerop, $idVendor = NULL)
  {
    $result = 0;
    $prefixOP = substr($nomerop,0,6);
    $prefixOP_1 = substr($nomerop,0,3); /* hanya untuk mengecek RIB saja, sekarang dicek awalnya gak perlu dicek */
    if($prefixOP_1 == 'RIB'){
       $result = 1;
       return $result;
    }
    if($prefixOP == 'OPIIMP'){
      $result = 1;
      return $result;
    }
    $CI = & get_instance();
    $CI->config->load('probe');

    $opHarusTerima = $CI->config->item('opHarusTerima');
    if(in_array($nomerop,$opHarusTerima)){
      $result = 1;
      return $result;
    }

    if(!empty($idVendor)){
    //  $vendorHarusTerima = array(12,183);
      $vendorHarusTerima = $CI->config->item('opVendorHarusTerima');
      if(in_array($idVendor,$vendorHarusTerima)){
        $result = 1;
        return $result;
      }
    }
    return $result;
  }
}

if(!function_exists('isOPLocal')){
  function isOPLocal($nomerop)
  {
    $result = 0;
    $polalkl = "/^\D+(LKL)+/i";
    $polalocal = "/^\d+/i";
    $result = preg_match($polalkl,$nomerop) || preg_match($polalocal,$nomerop);
    return $result;
  }
}

if(!function_exists('array_column')){
  /* ambil dari https://github.com/ramsey/array_column/blob/master/src/array_column.php */
 function array_column($input = null, $columnKey = null, $indexKey = null)
    {
        // Using func_get_args() in order to check for proper number of
        // parameters and trigger errors exactly as the built-in array_column()
        // does in PHP 5.5.
        $argc = func_num_args();
        $params = func_get_args();

        if ($argc < 2) {
            trigger_error("array_column() expects at least 2 parameters, {$argc} given", E_USER_WARNING);
            return null;
        }

        if (!is_array($params[0])) {
            trigger_error('array_column() expects parameter 1 to be array, ' . gettype($params[0]) . ' given', E_USER_WARNING);
            return null;
        }

        if (!is_int($params[1])
            && !is_float($params[1])
            && !is_string($params[1])
            && $params[1] !== null
            && !(is_object($params[1]) && method_exists($params[1], '__toString'))
        ) {
            trigger_error('array_column(): The column key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        if (isset($params[2])
            && !is_int($params[2])
            && !is_float($params[2])
            && !is_string($params[2])
            && !(is_object($params[2]) && method_exists($params[2], '__toString'))
        ) {
            trigger_error('array_column(): The index key should be either a string or an integer', E_USER_WARNING);
            return false;
        }

        $paramsInput = $params[0];
        $paramsColumnKey = ($params[1] !== null) ? (string) $params[1] : null;

        $paramsIndexKey = null;
        if (isset($params[2])) {
            if (is_float($params[2]) || is_int($params[2])) {
                $paramsIndexKey = (int) $params[2];
            } else {
                $paramsIndexKey = (string) $params[2];
            }
        }

        $resultArray = array();

        foreach ($paramsInput as $row) {

            $key = $value = null;
            $keySet = $valueSet = false;

            if ($paramsIndexKey !== null && array_key_exists($paramsIndexKey, $row)) {
                $keySet = true;
                $key = (string) $row[$paramsIndexKey];
            }

            if ($paramsColumnKey === null) {
                $valueSet = true;
                $value = $row;
            } elseif (is_array($row) && array_key_exists($paramsColumnKey, $row)) {
                $valueSet = true;
                $value = $row[$paramsColumnKey];
            }

            if ($valueSet) {
                if ($keySet) {
                    $resultArray[$key] = $value;
                } else {
                    $resultArray[] = $value;
                }
            }

        }

        return $resultArray;
    }
}

if(!function_exists('sd_square')){
// Function to calculate square of value - mean
function sd_square($x, $mean) { return pow($x - $mean,2); }
}


if(!function_exists('avg')){
// Function to calculate square of value - mean
function avg($x) { return array_sum($x) / count($x); }
}


if(!function_exists('stdev')){
// Function to calculate standard deviation (uses sd_square)
  function stdev($array) {
      // square root of sum of squares devided by N-1
      return sqrt(array_sum(array_map("sd_square", $array, array_fill(0,count($array), (array_sum($array) / count($array)) ) ) ) / (count($array)-1) );
  }
}
if(!function_exists('tglSebelum')){
    function tglSebelum($tgl,$hari){
        $date = new \DateTime($tgl);
        return $date->sub(new \DateInterval('P'.$hari.'D'))->format('Y-m-d');
    }
}
if(!function_exists('setTanggalDisplay')){
    function setTanggalDisplay($datetime){
        return empty($datetime) ? '' : date('d M Y', strtotime($datetime));
    }
}
if(!function_exists('setWaktuDisplay')){
    function setWaktuDisplay($datetime){
        return empty($datetime) ? '' : date('d M Y H:i', strtotime($datetime));
    }
}
if(!function_exists('setNumberDisplay')){
    function setNumberDisplay($number, $comma = 3, $comma_separator = '.', $thousand_separator = ','){
        return $number == '' ? '' : number_format($number, $comma, $comma_separator, $thousand_separator);
    }
}
if (!function_exists('array_group_by')) {
  function array_group_by(array $array, $key)
  {
    if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
      trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
      return null;
    }
    $func = (is_callable($key) ? $key : null);
    $_key = $key;
    $grouped = [];
    foreach ($array as $value) {
      if (is_callable($func)) {
        $key = call_user_func($func, $value);
      } elseif (is_object($value) && isset($value->{$_key})) {
        $key = $value->{$_key};
      } elseif (isset($value[$_key])) {
        $key = $value[$_key];
      } else {
        continue;
      }
      $grouped[$key][] = $value;
    }
    if (func_num_args() > 2) {
      $args = func_get_args();
      foreach ($grouped as $key => $value) {
        $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
        $grouped[$key] = call_user_func_array('array_group_by', $params);
      }
    }
    return $grouped;
  }
}
/* End of file common_helper.php */
/* Location: ./application/helpers/common_helper.php */
